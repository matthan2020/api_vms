﻿
using System;
using System.Collections.Generic;
using System.Text;
using WAR3_DAL.Entities;

namespace WAR3_DAL.ViewModel
{
    public class FaceShiftSearchViewModel 
    {
        public int SettingShiftId { get; set; }
        public int DepartmnetId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }

    }
}
