﻿
using System;
using System.Collections.Generic;
using System.Text;
using WAR3_DAL.Entities;

namespace WAR3_DAL.ViewModel
{
    public class ImageEventHistoryJsonViewModel
    {
        public string EventId { get; set; }
        public string Search { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int FaceId { get; set; }
        public string StaffCode { get; set; }

    }
}
