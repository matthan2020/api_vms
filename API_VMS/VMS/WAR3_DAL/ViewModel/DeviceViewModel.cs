﻿
using System;
using System.Collections.Generic;
using System.Text;
using WAR3_DAL.Entities;

namespace WAR3_DAL.ViewModel
{
    public class DeviceViewModel : Device
    {
        public int ROW_NUM { get; set; }
        public string DeviceTypeName { get; set; }
        public string SecurityName { get; set; }
        public string StatusName { get; set; }
        public int TKSettingId { set; get; }
        public bool IsCheckIn { get; set; }
        public bool IsCheckOut { get; set; }
        public bool IsTKOn { get; set; }

    }
}
