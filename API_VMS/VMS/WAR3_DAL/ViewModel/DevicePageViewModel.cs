﻿
using System;
using System.Collections.Generic;
using System.Text;
using WAR3_DAL.Entities;

namespace WAR3_DAL.ViewModel
{
    public class DevicePageViewModel
    {
        public string KeyWord { set; get; }
        public int Page { set; get; }
        public int PageSize { set; get; }


    }
}
