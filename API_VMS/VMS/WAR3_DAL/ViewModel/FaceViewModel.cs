﻿
using System;
using System.Collections.Generic;
using System.Text;
using WAR3_DAL.Entities;

namespace WAR3_DAL.ViewModel
{
    public class FaceViewModel
    {
        public int ImageId { get; set; }
        public string Base64ContentUrl { get; set; }
        public int FaceId { get; set; }
        public string FaceName { get; set; }
        public string AgeYear { get; set; }
        public string Job { get; set; }
        public string Sex { get; set; }
        public string ImagePath { get; set; }
        public string IsKnown { get; set; }
        public string ImportantLevel { get; set; }
        public int FaceIdCloud { get; set; }
        public string DepartmentId { get; set; }
        public string StaffCode { get; set; }
        public string DepartmentName { get; set; }

    }
}
