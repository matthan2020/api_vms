﻿
using System;
using System.Collections.Generic;
using System.Text;
using WAR3_DAL.Entities;

namespace WAR3_DAL.ViewModel
{
    public class ImageEventHistoryViewModel : ImageEventHistory
    {
        public string Job { set; get; }
        public string AgeYear { set; get; }
        public int ImportantLevel { set; get; }
        public string StaffCode { set; get; }

    }
}
