﻿
using System;
using System.Collections.Generic;
using System.Text;
using WAR3_DAL.Entities;

namespace WAR3_DAL.ViewModel
{
    public class StatisticalFaceShiftViewModel : StatisticalFaceShift
    {
        public int ROW_NUM { get; set; }
        public int TkYear { get; set; }
        public int TkMonth { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public float Cong01 { get; set; }
        public float Cong02 { get; set; }
        public float Cong03 { get; set; }
        public float Cong04 { get; set; }
        public float Cong05 { get; set; }
        public float Cong06 { get; set; }
        public float Cong07 { get; set; }
        public float Cong08 { get; set; }
        public float Cong09 { get; set; }
        public float Cong10 { get; set; }
        public float Cong11 { get; set; }
        public float Cong12 { get; set; }
        public float Cong13 { get; set; }
        public float Cong14 { get; set; }
        public float Cong15 { get; set; }
        public float Cong16 { get; set; }
        public float Cong17 { get; set; }
        public float Cong18 { get; set; }
        public float Cong19 { get; set; }
        public float Cong20 { get; set; }
        public float Cong21 { get; set; }
        public float Cong22 { get; set; }
        public float Cong23 { get; set; }
        public float Cong24 { get; set; }
        public float Cong25 { get; set; }
        public float Cong26 { get; set; }
        public float Cong27 { get; set; }
        public float Cong28 { get; set; }
        public float Cong29 { get; set; }
        public float Cong30 { get; set; }
        public float Cong31 { get; set; }
        public float CongKP01 { get; set; }
        public float CongKP02 { get; set; }
        public float CongKP03 { get; set; }
        public float CongKP04 { get; set; }
        public float CongKP05 { get; set; }
        public float CongKP06 { get; set; }
        public float CongKP07 { get; set; }
        public float CongKP08 { get; set; }
        public float CongKP09 { get; set; }
        public float CongKP10 { get; set; }
        public float CongKP11 { get; set; }
        public float CongKP12 { get; set; }
        public float CongKP13 { get; set; }
        public float CongKP14 { get; set; }
        public float CongKP15 { get; set; }
        public float CongKP16 { get; set; }
        public float CongKP17 { get; set; }
        public float CongKP18 { get; set; }
        public float CongKP19 { get; set; }
        public float CongKP20 { get; set; }
        public float CongKP21 { get; set; }
        public float CongKP22 { get; set; }
        public float CongKP23 { get; set; }
        public float CongKP24 { get; set; }
        public float CongKP25 { get; set; }
        public float CongKP26 { get; set; }
        public float CongKP27 { get; set; }
        public float CongKP28 { get; set; }
        public float CongKP29 { get; set; }
        public float CongKP30 { get; set; }
        public float CongKP31 { get; set; }
    }
}
