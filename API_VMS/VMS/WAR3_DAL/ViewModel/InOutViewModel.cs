﻿
using System;
using System.Collections.Generic;
using System.Text;
using WAR3_DAL.Entities;

namespace WAR3_DAL.ViewModel
{
    public class InOutViewModel
    {
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string StaffCode { get; set; }
        public string FaceName { get; set; }
        public string RecordTimeIn { get; set; }
        public string RecordTimeOut { get; set; }
        public string RecordTime { get; set; }
        public string DeviceNameIn { get; set; }
        public string DeviceNameOut { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public float Tong { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }

    }
}
