﻿
using System;
using System.Collections.Generic;
using System.Text;
using WAR3_DAL.Entities;

namespace WAR3_DAL.ViewModel
{
    public class AddEventViewModel 
    {
        public string StaffCode { get; set; }
        public int Device { get; set; }
        public string RecordTime { get; set; }
        public string CreatedUser { get; set; }
        public int Event { get; set; }

    }
}
