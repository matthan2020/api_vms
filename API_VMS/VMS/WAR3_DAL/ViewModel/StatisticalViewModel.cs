﻿
using System;
using System.Collections.Generic;
using System.Text;
using WAR3_DAL.Entities;

namespace WAR3_DAL.ViewModel
{
    public class StatisticalViewModel : Statistical
    {
        public int ROW_NUM { get; set; }
        public int TkYear { get; set; }
        public int TkMonth { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }

    }
}
