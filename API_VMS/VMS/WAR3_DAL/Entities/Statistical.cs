﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class Statistical
    {
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string StaffCode { get; set; }
        public string StaffName { get; set; }
        public int YearTK { get; set; }
        public int MonthTK { get; set; }
        public float Day1 { get; set; }
        public float Day2 { get; set; }
        public float Day3 { get; set; }
        public float Day4 { get; set; }
        public float Day5 { get; set; }
        public float Day6 { get; set; }
        public float Day7 { get; set; }
        public float Day8 { get; set; }
        public float Day9 { get; set; }
        public float Day10 { get; set; }
        public float Day11 { get; set; }
        public float Day12 { get; set; }
        public float Day13 { get; set; }
        public float Day14 { get; set; }
        public float Day15 { get; set; }
        public float Day16 { get; set; }
        public float Day17 { get; set; }
        public float Day18 { get; set; }
        public float Day19 { get; set; }
        public float Day20 { get; set; }
        public float Day21 { get; set; }
        public float Day22 { get; set; }
        public float Day23 { get; set; }
        public float Day24 { get; set; }
        public float Day25 { get; set; }
        public float Day26 { get; set; }
        public float Day27 { get; set; }
        public float Day28 { get; set; }
        public float Day29 { get; set; }
        public float Day30 { get; set; }
        public float Day31 { get; set; }
        public bool IsLock { get; set; }

    }
}
