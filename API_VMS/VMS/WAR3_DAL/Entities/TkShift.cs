﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class TkShift
    {
        public int ShiftId { get; set; }
        public string ShiftName { get; set; }
        public int SettingShiftId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int Gio { get; set; }
        public string FromDateMain { get; set; }
        public string ToDateMain { get; set; }
        public string FromDateExtra { get; set; }
        public string ToDateExtra { get; set; }
        public int SettingRiceId { set; get; }
    }
}
