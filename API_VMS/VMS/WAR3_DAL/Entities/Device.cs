﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class Device
    {
        public int DeviceId { set; get; }
        public int IdDeviceType { set; get; }
        public string DeviceName { set; get; }
        public string IpAddress { set; get; }
        public int PortNo { set; get; }
        public string UserName { set; get; }
        public string Pass { set; get; }
        public string idCompany { set; get; }
        public string idModel { set; get; }
        public string AmountCam { set; get; }
        public bool isDelete { set; get; }
        public int IdSecurity { set; get; }
        public string DeviceCode { set; get; }
        public int IdStatus { set; get; }
        public int GroupId { set; get; }
        public int Channel { set; get; }
        public int Thread { set; get; }
        public string LinkRTSP { set; get; }
    }
}
