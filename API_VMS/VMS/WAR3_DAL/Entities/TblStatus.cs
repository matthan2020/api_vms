﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class TblStatus
    {
        public int IdStatus { get; set; }
        public string Status { get; set; }
    }
}
