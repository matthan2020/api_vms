﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class TblFaceShift
    {
        public int FaceShiftId { get; set; }
        public int FaceId { get; set; }
        public int SettingShiftId { get; set; }
        public string ShiftName { get; set; }
        public string FaceDate { get; set; }
    }
}
