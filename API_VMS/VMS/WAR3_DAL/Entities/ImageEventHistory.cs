﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class ImageEventHistory
    {
        public int HisEventId { set; get; }
        public int FaceId { set; get; }
        public int EventId { set; get; }
        public int DeviceId { set; get; }
        public string RecordTime { set; get; }
        public string Base64ContentUrl { set; get; }
        public string FaceName { set; get; }
        public string EventType { set; get; }
        public string DeviceName { set; get; }
        public string Sex { set; get; }
        public int FaceIdCloud { set; get; }
        public string CreatedUser { set; get; }
        public string CreatedDate { set; get; }
    }
}
