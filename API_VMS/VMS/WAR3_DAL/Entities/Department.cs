﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class Department
    {
        public int DepartmentId { set; get; }
        public string Acronym { set; get; }
        public string DepartmentName { set; get; }
        
    }
}
