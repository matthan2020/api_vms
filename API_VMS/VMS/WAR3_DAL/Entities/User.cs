﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace WAR3_DAL.Entities
{
    public class User
    {
        public string UserName { set; get; }
        public string Pwd { set; get; }
        public int UserId { set; get; }
        public string NAME_COMPANY { set; get; }
        public string ADDRESS { set; get; }
        public string EMAIL { set; get; }
        public decimal PHONE { set; get; }
        public int STATUS { set; get; }
        public string CREATE_DATE { set; get; }
        public string CREATE_USER { set; get; }
    }
}
