﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class Company
    {
        public int IdCompany { get; set; }
        public string CompanyName { get; set; }
    }
}
