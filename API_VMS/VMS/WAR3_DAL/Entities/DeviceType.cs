﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class DeviceType
    {
        public int IdDeviceType { get; set; }
        public string DeviceTypeName { get; set; }
    }
}
