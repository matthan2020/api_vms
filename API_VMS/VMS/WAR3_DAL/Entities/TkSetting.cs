﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class TkSetting
    {
        public int TKSettingId { get; set; }
        public int DeviceId { get; set; }
        public bool IsCheckIn { get; set; }
        public bool IsCheckOut { get; set; }
        public bool IsTKOn { get; set; }
    }
}
