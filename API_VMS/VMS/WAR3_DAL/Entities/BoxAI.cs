﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class BoxAI
    {
        public int id { get; set; }
        public string name { get; set; }
        public string score { get; set; }
    }
}
