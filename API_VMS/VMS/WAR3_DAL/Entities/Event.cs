﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class Event
    {
        public int EventId { set; get; }
        public string EventName { set; get; }
    }
}
