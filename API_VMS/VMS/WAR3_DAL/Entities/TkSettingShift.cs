﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Entities
{
    public class TkSettingShift
    {
        public int SettingShiftId { get; set; }
        public string SettingShiftName { get; set; }
        public string SettingShiftCode { get; set; }
    }
}
