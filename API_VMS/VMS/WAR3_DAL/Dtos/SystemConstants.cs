﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WAR3_DAL.Dtos
{
    public class SystemConstants
    {
        public const string MainConnectionString = "DefaultConnection";

        public class AppSettings
        {
            public const string DefaultLanguageId = "DefaultLanguageId";
            public const string Token = "Token";
            public const string BaseAddress = "BaseAddress";
        }
    }
}
