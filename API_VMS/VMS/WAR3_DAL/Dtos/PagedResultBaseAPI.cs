﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WAR3_DAL.Dtos
{
    public abstract class PagedResultBaseAPI
    {
        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        // public int TotalRecords { get; set; }
        public int TotalRow { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public string FileSave { get; set; }

        public int Id { get; set; }

        public int PageCount
        {
            get
            {
                var pageCount = (double)TotalRow / PageSize;
                return (int)Math.Ceiling(pageCount);
            }
        }
    }
}
