﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WAR3_DAL.Dtos
{
    public class PagedResultAPI<T> : PagedResultBaseAPI 
    {
        public List<T> Items { set; get; }
    }
}
