﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace WAR3_DAL.Dtos
{
    public class BaseApiClient
    {
        private readonly string ConnectedString;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        protected BaseApiClient(IHttpClientFactory httpClientFactory,
                    IConfiguration configuration)
        {
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
            ConnectedString = _configuration.GetConnectionString("APIWeb");
        }

        protected async Task<TResponse> GetAsync<TResponse>(string url)
        {
            try {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(ConnectedString);//_configuration[SystemConstants.AppSettings.BaseAddress]);
                var response = await client.GetAsync(url);
                var body = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    TResponse myDeserializedObjList = (TResponse)JsonConvert.DeserializeObject(body,
                        typeof(TResponse));

                    return myDeserializedObjList;
                }
                return JsonConvert.DeserializeObject<TResponse>(body);
            }
            catch (Exception ex)
            {
                var body = "{" + "Success :" + false + ", Message : " + ex.Message + "}";
            
            return JsonConvert.DeserializeObject<TResponse>(body);
            }


        }

      

        public async Task<List<T>> GetListAsync<T>(string url)
        {
           
            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(ConnectedString);//_configuration[SystemConstants.AppSettings.BaseAddress]);

            var response = await client.GetAsync(url);
            var body = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                var data = (List<T>)JsonConvert.DeserializeObject(body, typeof(List<T>));
                return data;
            }
            else
            {
               var body1 = "[" + body + "]";
                var data = (List<T>)JsonConvert.DeserializeObject(body1, typeof(List<T>));
                return data;
            }
            throw new Exception(body);
        }
        //khong tra ve gia tri lít anh sach
        public async Task<List<T>> GetListAsyncOther<T>(string url)
        {

            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(ConnectedString);//_configuration[SystemConstants.AppSettings.BaseAddress]);

            var response = await client.GetAsync(url);
            var body1 = await response.Content.ReadAsStringAsync();
            var body = "[" + body1 + "]";
            if (response.IsSuccessStatusCode)
            {
                var data = (List<T>)JsonConvert.DeserializeObject(body, typeof(List<T>));
                return data;
            }
            else
            {

            }
            throw new Exception(body);
        }

        protected async Task<TResponse> PostAsync<TResponse>(string url, string json)
        {


            var client = _httpClientFactory.CreateClient();
            //client.BaseAddress = new Uri("https://localhost:44343/");
            client.BaseAddress = new Uri(ConnectedString);//_configuration[SystemConstants.AppSettings.BaseAddress]);
            // var response = await client.GetAsync(url);
            // var body = await response.Content.ReadAsStringAsync();

            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(url, httpContent);
            var body = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                TResponse myDeserializedObjList = (TResponse)JsonConvert.DeserializeObject(body,
                    typeof(TResponse));

                return myDeserializedObjList;
            }
            //var abc = JsonConvert.DeserializeObject<TResponse>(result);

            return JsonConvert.DeserializeObject<TResponse>(body);


        }

        protected async Task<List<T>> PostAsyncOther<T>(string url, string json)
        {


            var client = _httpClientFactory.CreateClient(); 
            //client.BaseAddress = new Uri("https://localhost:44343/");
            client.BaseAddress = new Uri(ConnectedString);//_configuration[SystemConstants.AppSettings.BaseAddress]);
            // var response = await client.GetAsync(url);
            // var body = await response.Content.ReadAsStringAsync();

            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(url, httpContent);
            var body = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                var boddy2 = body.Substring(0, 1);
                if (boddy2 == "[")
                {
                  
                }
                else
                {
                  body= "[" + body + "]";
                }
                var data = (List<T>)JsonConvert.DeserializeObject(body, typeof(List<T>));

                return data;
            }
            else
            {
                var body1 = "[" + body + "]";
                var data = (List<T>)JsonConvert.DeserializeObject(body1, typeof(List<T>));
                return data;
            }
            throw new Exception(body);


        }
        //khong tra ve gia tri list danh sach
        protected async Task<List<T>> PostAsyncNoList<T>(string url, string json)
        {


            var client = _httpClientFactory.CreateClient();
            //client.BaseAddress = new Uri("https://localhost:44343/");
            client.BaseAddress = new Uri(ConnectedString);//_configuration[SystemConstants.AppSettings.BaseAddress]);
            // var response = await client.GetAsync(url);
            // var body = await response.Content.ReadAsStringAsync();

            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(url, httpContent);
            var body1 = await response.Content.ReadAsStringAsync();
            var body = "[" + body1 + "]";
            if (response.IsSuccessStatusCode)
            {
                var data = (List<T>)JsonConvert.DeserializeObject(body, typeof(List<T>));

                return data;
            }
            throw new Exception(body);


        }

    }
}
