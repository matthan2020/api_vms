﻿
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WAR3_DAL.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.Repositories
{
    public class StatisticalRepositories : BaseApiClient, IStatisticalRepositories
    {
        private readonly string ConnectedString;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        public StatisticalRepositories(IHttpClientFactory httpClientFactory,
                    IConfiguration configuration)
            : base(httpClientFactory, configuration)
        {
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
            ConnectedString = _configuration.GetConnectionString("APIWeb");
        }
        public async Task<PagedResultAPI<StatisticalViewModel>> GetStatistical(string staffCode, string departmentId, int month, int year, int page, int pageSize)
        {
            PagedResultAPI<StatisticalViewModel> data1 = new PagedResultAPI<StatisticalViewModel>();
            try
            {
                StatisticalViewModel data = new StatisticalViewModel();

                data.DepartmentId = departmentId;
                data.StaffCode = staffCode;
                data.TkYear = year;
                data.TkMonth = month;
                data.Page = page;
                data.PageSize = pageSize;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<StatisticalViewModel>>(
                 $"api/tk/statistic/get-page", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
          
        }

        public async Task<PagedResultAPI<StatisticalViewModel>> ExportExcel(string staffCode, string departmentId, int month, int year)
        {
            StatisticalViewModel data = new StatisticalViewModel();

            data.DepartmentId = departmentId;
            data.StaffCode = staffCode;
            data.TkYear = year;
            data.TkMonth = month;
            var json = JsonConvert.SerializeObject(data);
            var data1 = await PostAsyncOther<StatisticalViewModel>(
                 $"api/tk/statistic/get", json);
            PagedResultAPI<StatisticalViewModel> data2 = new PagedResultAPI<StatisticalViewModel>();
            data2.Items = data1;
            if (data1.Count == 0)
            {
                data2.Message = "NoData";
            }
            else
            {
                data2.Message = "SUCCESS";
            }
            data2.Success = true;

            return data2;
        }

        public async Task<PagedResultAPIOther> UpdateData( int month, int year)
        {
           PagedResultAPIOther data3 = new PagedResultAPIOther();
            try
            {
                StatisticalViewModel data2 = new StatisticalViewModel();

                var json = JsonConvert.SerializeObject(data2);
                var url = $"api/tk/statistic/update/{year}/{month}";
                ////////////////////////////////
                var client = _httpClientFactory.CreateClient();
                //client.BaseAddress = new Uri("https://localhost:44343/");
                client.BaseAddress = new Uri("http://192.168.1.168/vms-api/");//_configuration[SystemConstants.AppSettings.BaseAddress]);
                                                                              // var response = await client.GetAsync(url);
                                                                              // var body = await response.Content.ReadAsStringAsync();

                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PostAsync(url, httpContent);
                var body = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    
                    //data3 = JsonConvert.DeserializeObject<PagedResultAPIOther>(body);
                    data3= (PagedResultAPIOther)JsonConvert.DeserializeObject(body,
                    typeof(PagedResultAPIOther));

                }
                else
                {
                    data3.Status = response.StatusCode.ToString();
                    data3.Message = response.RequestMessage.ToString();
                }
                ////////////////////////////////
                //var data = await PostAsync<PagedResultAPIOther>(
                //  $"api/tk/statistic/update/{year}/{month}", json);
                //return data;
                //data1.Success = true;
            }
            catch (Exception ex)
            {
                data3.Status = "Failed";
                data3.Message = ex.Message;
            }
            return data3;
           

        }

        public async Task<PagedResultAPI<InOutViewModel>> GetInOutPage(string staffCode, string departmentId, string fromDate, string toDate, int page, int pageSize)
        {
            PagedResultAPI<InOutViewModel> data1 = new PagedResultAPI<InOutViewModel>();
            try
            {
                InOutViewModel data = new InOutViewModel();

                data.DepartmentId = departmentId;
                data.StaffCode = staffCode;
                data.FromDate = fromDate;
                data.ToDate = toDate;
                data.Page = page;
                data.PageSize = pageSize;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<InOutViewModel>>(
                 $"api/tk/statistic/get-inout-page", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }

        public async Task<PagedResultAPI<InOutViewModel>> ExportExcelInOut(string staffCode, string departmentId, string fromDate, string toDate)
        {
           
          
            PagedResultAPI<InOutViewModel> data1 = new PagedResultAPI<InOutViewModel>();
           
            try
            {
                InOutViewModel data = new InOutViewModel();

                data.DepartmentId = departmentId;
                data.StaffCode = staffCode;
                data.FromDate = fromDate;
                data.ToDate = toDate;
                var json = JsonConvert.SerializeObject(data);

                data1 = await PostAsync<PagedResultAPI<InOutViewModel>>(
               $"api/tk/statistic/get-inout", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }

        public async Task<PagedResultAPI<StatisticalFaceShiftViewModel>> GetStatisticalFaceShift(string staffCode, string departmentId,string shiftName, int month, int year, int page, int pageSize)
        {
            PagedResultAPI<StatisticalFaceShiftViewModel> data1 = new PagedResultAPI<StatisticalFaceShiftViewModel>();
            try
            {
                StatisticalFaceShiftViewModel data = new StatisticalFaceShiftViewModel();

                data.DepartmentId = departmentId;
                data.StaffCode = staffCode;
                data.ShiftName = shiftName;
                data.TkYear = year;
                data.TkMonth = month;
                data.Page = page;
                data.PageSize = pageSize;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<StatisticalFaceShiftViewModel>>(
                 $"api/tk/statistic/get-face-shift-page", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }
        public async Task<PagedResultAPIOther> UpdateDataFaceShift(int month, int year)
        {
            PagedResultAPIOther data3 = new PagedResultAPIOther();
            try
            {
                StatisticalViewModel data2 = new StatisticalViewModel();

                var json = JsonConvert.SerializeObject(data2);
                var url = $"api/tk/statistic/update-face-shift/{year}/{month}";
                ////////////////////////////////
                var client = _httpClientFactory.CreateClient();
                //client.BaseAddress = new Uri("https://localhost:44343/");
                client.BaseAddress = new Uri(ConnectedString);//_configuration[SystemConstants.AppSettings.BaseAddress]);
                                                                              // var response = await client.GetAsync(url);
                                                                              // var body = await response.Content.ReadAsStringAsync();

                //var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.GetAsync(url);
                var body = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {

                    //data3 = JsonConvert.DeserializeObject<PagedResultAPIOther>(body);
                    data3 = (PagedResultAPIOther)JsonConvert.DeserializeObject(body,
                    typeof(PagedResultAPIOther));

                }
                else
                {
                    data3.Status = response.StatusCode.ToString();
                    data3.Message = response.RequestMessage.ToString();
                }
                ////////////////////////////////
                //var data = await PostAsync<PagedResultAPIOther>(
                //  $"api/tk/statistic/update/{year}/{month}", json);
                //return data;
                //data1.Success = true;
            }
            catch (Exception ex)
            {
                data3.Status = "Failed";
                data3.Message = ex.Message;
            }
            return data3;


        }
        public async Task<PagedResultAPI<StatisticalFaceShiftViewModel>> ExportExcelFaceShift(string staffCode, string departmentId, string shiftName, int month, int year)
        {
            StatisticalFaceShiftViewModel data = new StatisticalFaceShiftViewModel();

            data.DepartmentId = departmentId;
            data.StaffCode = staffCode;
            data.ShiftName = shiftName;
            data.TkYear = year;
            data.TkMonth = month;
            var json = JsonConvert.SerializeObject(data);
            var data1 = await PostAsyncOther<StatisticalFaceShiftViewModel>(
                 $"api/tk/statistic/get-statistic-face-shift", json);
            PagedResultAPI<StatisticalFaceShiftViewModel> data2 = new PagedResultAPI<StatisticalFaceShiftViewModel>();
            data2.Items = data1;
            if (data1.Count == 0)
            {
                data2.Message = "NoData";
            }
            else
            {
                data2.Message = "SUCCESS";
            }
            data2.Success = true;

            return data2;
        }
    }
}
