﻿
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.Repositories
{
    public class DeviceRepositories : BaseApiClient, IDeviceRepositories
    {
        private readonly string ConnectedString;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        public DeviceRepositories(IHttpClientFactory httpClientFactory,
                    IConfiguration configuration)
            : base(httpClientFactory, configuration)
        {
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
            ConnectedString = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<PagedResultAPI<DeviceViewModel>> GetAllDevice(string keyword, int page, int pageSize)
        {
            PagedResultAPI<DeviceViewModel> data1 = new PagedResultAPI<DeviceViewModel>();
            try
            {
                DevicePageViewModel data = new DevicePageViewModel();

                data.KeyWord = keyword;
                data.Page = page;
                data.PageSize = pageSize;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<DeviceViewModel>>(
                 $"api/device/get-page", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
           
        }

        public async Task<PagedResultAPI<DeviceType>> GetDeviceType()
        {
            var data = await GetListAsync<DeviceType>(
                $"api/device/get-device-type");
            PagedResultAPI<DeviceType> data1 = new PagedResultAPI<DeviceType>();
            data1.Items = data;
            if (data.Count == 0)
            {
                data1.Message = "NoData";
            }
            data1.Success = true;
            return data1;

        }
        public async Task<PagedResultAPI<PagedResultAPIOther>> SaveDevice(int deviceType, string deviceName, string ipAddress, int portNo, string userName, string pass,string link, string model, string campany, string amountCam)
        {

            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                Device data = new Device();

                data.IdDeviceType = deviceType;
                data.DeviceName = deviceName;
                data.IpAddress = ipAddress;
                data.PortNo = portNo;
                data.UserName = userName;
                data.Pass = pass;
                data.idModel = model;
                data.idCompany = campany;
                data.AmountCam = amountCam;
                data.LinkRTSP = link;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                var data2 = await PostAsyncNoList<PagedResultAPIOther>(
                 $"api/device/add", json);
                if (data2[0].Status == "OK")
                {
                    data1.Success = true;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data2[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }
        public async Task<PagedResultAPI<Device>> GetDevicetById(int id)
        {

            var data = await GetListAsync<Device>(
               $"api/device/get/{id}");
            PagedResultAPI<Device> data1 = new PagedResultAPI<Device>();
            data1.Items = data;
            if (data.Count == 0)
            {
                data1.Message = "NoData";
            }
            data1.Success = true;
            return data1;

        }
        public async Task<PagedResultAPI<PagedResultAPIOther>> UpdateDevice(int id,int deviceType, string deviceName, string ipAddress, int portNo, string userName, string pass,string link, string model, string campany, string amountCam)
        {

            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                Device data = new Device();

                data.IdDeviceType = deviceType;
                data.DeviceName = deviceName;
                data.IpAddress = ipAddress;
                data.PortNo = portNo;
                data.UserName = userName;
                data.Pass = pass;
                data.idModel = model;
                data.idCompany = campany;
                data.AmountCam = amountCam;
                data.LinkRTSP = link;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                var data2 = await PostAsyncNoList<PagedResultAPIOther>(
                 $"api/device/edit/{id}", json);
                if (data2[0].Status == "OK")
                {
                    data1.Success = true;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data2[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }
        public async Task<PagedResultAPI<PagedResultAPIOther>> DeleteDevice(int id)
        {

            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                var data = await GetListAsyncOther<PagedResultAPIOther>(
                               $"api/device/delete/{id}");
                if (data[0].Status == "OK")
                {
                    data1.Success = true;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }

        public async Task<PagedResultAPI<DeviceViewModel>> GetListDevice()
        {
            var data = await GetListAsync<DeviceViewModel>(
               $"api/device/get-list-device");
            PagedResultAPI<DeviceViewModel> data1 = new PagedResultAPI<DeviceViewModel>();
            data1.Items = data;
            if (data.Count == 0)
            {
                data1.Message = "NoData";
            }
            data1.Success = true;
            return data1;
        }
        public async Task<PagedResultAPI<PagedResultAPIOther>> UpdateDeviceStatus(int id,int idStatus)
        {

            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                var data = await GetListAsyncOther<PagedResultAPIOther>(
                               $"api/device/update-device-status/{id}/{idStatus}");
                if (data[0].Status == "OK")
                {
                    data1.Success = true;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }
    }
}
