﻿
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.Repositories
{
    public class SettingRepositories : BaseApiClient, ISettingRepositories
    {
        private readonly string ConnectedString;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        public SettingRepositories(IHttpClientFactory httpClientFactory,
                    IConfiguration configuration)
            : base(httpClientFactory, configuration)
        {
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
            ConnectedString = _configuration.GetConnectionString("DefaultConnection");
        }
       
        public async Task<PagedResultAPI<PagedResultAPIOther>>  AddSettingShift(string shiftname)
        {

            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                var data2 = await GetListAsyncOther<PagedResultAPIOther>(
                 $"api/tk/add-setting-shift/{shiftname}");
                if (data2[0].Status == "OK")
                {
                    data1.Success = true;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data2[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }
        public async Task<PagedResultAPI<TkSettingShift>> GetListSettingShift()
        {

            PagedResultAPI<TkSettingShift> data1 = new PagedResultAPI<TkSettingShift>();
            try
            {
                var data = await GetListAsync<TkSettingShift>(
                               $"api/tk/get-setting-shift");
                data1.Items = data;
                if (data.Count == 0)
                {
                    data1.Message = "NoData";
                }
                data1.Success = true;
            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }
        public async Task<PagedResultAPI<TkShift>> GetShiftBySettingShiftId(int id)
        {

            PagedResultAPI<TkShift> data1 = new PagedResultAPI<TkShift>();
            try
            {
                var data = await GetListAsync<TkShift>(
                 $"api/tk/get-shift/{id}");
                data1.Items = data;
                if (data.Count == 0)
                {
                    data1.Message = "NoData";
                }
                data1.Success = true;
            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }
        public async Task<PagedResultAPI<PagedResultAPIOther>> AddShift(int shiftId,int settingShift, string ShiftName, string FromDate, string ToDate, string deleteShiftId)
        {

            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                ShiftViewModel data = new ShiftViewModel();

                data.ShiftId = shiftId;
                data.SettingShiftId = settingShift;
                data.ShiftName = ShiftName;
                data.FromDate = FromDate;
                data.ToDate = ToDate;
                data.DeleteShiftId = deleteShiftId;

                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                var data2 = await PostAsyncNoList<PagedResultAPIOther>(
                 $"api/tk/add-shift", json);
                if (data2[0].Status == "OK")
                {
                    data1.Success = true;
                    data1.Id = data2[0].id;
                    data1.FileSave = data2[0].FileSave;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data2[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }
        public async Task<PagedResultAPI<PagedResultAPIOther>> DeleteSettingShiftCa(int settingShiftId)
        {

            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                var data = await GetListAsyncOther<PagedResultAPIOther>(
                               $"api/tk/delete-shift/{settingShiftId}");
                if (data[0].Status == "OK")
                {
                    data1.Success = true;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }
        public async Task<PagedResultAPI<FaceShiftViewModel>> GetListFaceShift(int settingShift, int department, string fromDate, string toDate)
        {


            FaceShiftSearchViewModel data = new FaceShiftSearchViewModel();

            data.SettingShiftId = settingShift;
            data.DepartmnetId = department;
            data.FromDate = fromDate;
            data.ToDate = toDate;
            var json = JsonConvert.SerializeObject(data);
            var data1 = await PostAsyncOther<FaceShiftViewModel>(
                 $"api/tk/get-face-shift", json);
            PagedResultAPI<FaceShiftViewModel> data2 = new PagedResultAPI<FaceShiftViewModel>();
           
            if (data1.Count == 0)
            {
                data2.Items = data1;
                data2.Message = "NoData";
                data2.Success = true;
                data2.TotalRow = data1[0].CheckSave;
            }
            else
            {
                if(data1[0].Status==null && data1[0].Message == null)
                {
                    data2.Items = data1;
                    data2.Message = "SUCCESS";
                    data2.Success = true;
                    data2.TotalRow = data1[0].CheckSave;
                }
                else
                {
                    data2.Message = data1[0].Message;
                    data2.Success = false;
                }

            }
           

            return data2;

        }

        public async Task<PagedResultAPI<TkShift>> GetListShiftGroup(int id)
        {

            PagedResultAPI<TkShift> data1 = new PagedResultAPI<TkShift>();
            try
            {
                var data = await GetListAsync<TkShift>(
                 $"api/tk/get-shift-group/{id}");
                data1.Items = data;
                if (data.Count == 0)
                {
                    data1.Message = "NoData";
                }
                data1.Success = true;
            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }

        public async Task<PagedResultAPI<PagedResultAPIOther>> AddFaceShift(int settingShift, string fromDate, string toDate, List<FaceShiftViewModel> items)
        {

            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            //PagedResultAPI<PagedResultAPIOther> data2 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                foreach (var item in items)
                {
                    FaceShiftViewModel data2 = new FaceShiftViewModel();

                    data2.FaceId = item.FaceId;
                    data2.FromDate = fromDate;
                    data2.ToDate = toDate;
                    data2.SettingShiftId = settingShift;
                    data2.Day1 = item.Day1;
                    data2.Day2 = item.Day2;
                    data2.Day3 = item.Day3;
                    data2.Day4 = item.Day4;
                    data2.Day5 = item.Day5;
                    data2.Day6 = item.Day6;
                    data2.Day7 = item.Day7;
                    data2.RiceShift1 = item.RiceShift1;
                    data2.RiceShift2 = item.RiceShift2;
                    data2.RiceShift3 = item.RiceShift3;
                    data2.RiceShift4 = item.RiceShift4;
                    data2.RiceShift5 = item.RiceShift5;
                    data2.RiceShift6 = item.RiceShift6;
                    data2.RiceShift7 = item.RiceShift7;
                    var json = JsonConvert.SerializeObject(data2);
                    var data = await PostAsyncNoList<PagedResultAPIOther>(
                                   $"api/tk/add-face-shift", json);
                    if (data[0].Status == "OK")
                    {
                        data1.Success = true;
                    }
                    else
                    {
                        data1.Success = false;
                        data1.Message = data[0].Message;
                    }
                }
                   

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }

        public async Task<PagedResultAPI<FaceShiftViewModel>> GetListFaceShiftExcel(int settingShift, int department, string fromDate, string toDate)
        {


            FaceShiftSearchViewModel data = new FaceShiftSearchViewModel();

            data.SettingShiftId = settingShift;
            data.DepartmnetId = department;
            data.FromDate = fromDate;
            data.ToDate = toDate;
            var json = JsonConvert.SerializeObject(data);
            var data1 = await PostAsyncOther<FaceShiftViewModel>(
                 $"api/tk/get-face-shift-excel", json);
            PagedResultAPI<FaceShiftViewModel> data2 = new PagedResultAPI<FaceShiftViewModel>();

            if (data1.Count == 0)
            {
                data2.Items = data1;
                data2.Message = "NoData";
                data2.Success = false;
                //data2.TotalRow = data1[0].CheckSave;
            }
            else
            {
                if (data1[0].Status == null && data1[0].Message == null)
                {
                    data2.Items = data1;
                    data2.Message = "SUCCESS";
                    data2.Success = true;
                    //data2.TotalRow = data1[0].CheckSave;
                }
                else
                {
                    data2.Message = data1[0].Message;
                    data2.Success = false;
                }

            }


            return data2;

        }
    }
}
