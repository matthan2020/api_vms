﻿
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WAR3_DAL.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.Repositories
{
    public class DepartmentRepositories : BaseApiClient, IDepartmentRepositories
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        public DepartmentRepositories(IHttpClientFactory httpClientFactory,
                    IConfiguration configuration)
            : base(httpClientFactory, configuration)
        {
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        public async Task<PagedResultAPI<DepartmentViewModel>> GetListDepartment()
        {
            var data = await GetListAsync<DepartmentViewModel>(
                 $"api/department/get" );
            PagedResultAPI<DepartmentViewModel> data1 = new PagedResultAPI<DepartmentViewModel>();
            data1.Items = data;
            if (data.Count == 0)
            {
                data1.Message = "NoData";
            }
            data1.Success = true;
            return data1;
        }

        public async Task<PagedResultAPI<DepartmentViewModel>> GetAllDepartmentPage(string keyword, int page, int pageSize)
        {
            PagedResultAPI<DepartmentViewModel> data1 = new PagedResultAPI<DepartmentViewModel>();
            try
            {
                DevicePageViewModel data = new DevicePageViewModel();

                data.KeyWord = keyword;
                data.Page = page;
                data.PageSize = pageSize;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<DepartmentViewModel>>(
                 $"api/department/get-page", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }

        public async Task<PagedResultAPI<PagedResultAPIOther>> SaveDepartment(string departmentCode, string departmentName)
        {

            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                DepartmentViewModel data2 = new DepartmentViewModel();

                data2.DepartmentName = departmentName;
                data2.Acronym = departmentCode;
                var json = JsonConvert.SerializeObject(data2);
                var data = await PostAsyncNoList<PagedResultAPIOther>(
                               $"api/department/add", json);
                if (data[0].Status == "OK")
                {
                    data1.Success = true;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }
        public async Task<PagedResultAPI<DepartmentViewModel>> GetDepartmentById(int id)
        {

            var data = await GetListAsync<DepartmentViewModel>(
               $"api/department/get/{id}");
            PagedResultAPI<DepartmentViewModel> data1 = new PagedResultAPI<DepartmentViewModel>();
            data1.Items = data;
            if (data.Count == 0)
            {
                data1.Message = "NoData";
            }
            data1.Success = true;
            return data1;

        }
        public async Task<PagedResultAPI<PagedResultAPIOther>> UpdateDepartment(int id, string departmentCode, string departmentName)
        {

            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                DepartmentViewModel data = new DepartmentViewModel();

                data.DepartmentName = departmentName;
                data.Acronym = departmentCode;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                var data2 = await PostAsyncNoList<PagedResultAPIOther>(
                 $"api/department/edit/{id}", json);
                if (data2[0].Status == "OK")
                {
                    data1.Success = true;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data2[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }
        public async Task<PagedResultAPI<PagedResultAPIOther>> DeleteDepartment(int id)
        {

            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                var data = await GetListAsyncOther<PagedResultAPIOther>(
                               $"api/department/delete/{id}");
                if (data[0].Status == "OK")
                {
                    data1.Success = true;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }
    }
}
