﻿
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Oracle.ManagedDataAccess.Client;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.Repositories
{
    public class FaceRepositories : BaseApiClient, IFaceRepositories
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        public FaceRepositories(IHttpClientFactory httpClientFactory,
                    IConfiguration configuration)
            : base(httpClientFactory, configuration)
        {
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
     
        public async Task<PagedResultAPI<FaceViewModel>> GetSearchFaceCB(string search, int page, int pageSize)
        {
            PagedResultAPI<FaceViewModel> data1 = new PagedResultAPI<FaceViewModel>();
            try
            {
                DevicePageViewModel data = new DevicePageViewModel();

                data.KeyWord = search;
                data.Page = page;
                data.PageSize = pageSize;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<FaceViewModel>>(
                 $"api/face/base-search-page-keyword", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }

        public async Task<PagedResultAPI<FaceViewModel>> GetSearchFaceNC(string faceName, string staffCode, string departmentId, string job, string sex, string known, int page, int pageSize)
        {
            PagedResultAPI<FaceViewModel> data1 = new PagedResultAPI<FaceViewModel>();
            try
            {
                Face data = new Face();

                data.DepartmentId = departmentId;
                data.StaffCode = staffCode;
                data.FaceName = faceName;
                data.Job = job;
                data.Sex = sex;
                data.IsKnown = known;
                data.Page = page;
                data.PageSize = pageSize;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<FaceViewModel>>(
                 $"api/face/advanced-search-page", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }

        public async Task<PagedResultAPI<FaceViewModel>> GetSearchFace(string search,string faceName, string staffCode, string departmentId, string job, string sex, string known, int page, int pageSize)
        {
            PagedResultAPI<FaceViewModel> data1 = new PagedResultAPI<FaceViewModel>();
            try
            {
                Face data = new Face();

                data.DepartmentId = departmentId;
                data.StaffCode = staffCode;
                data.FaceName = faceName;
                data.Job = job;
                data.Sex = sex;
                data.IsKnown = known;
                data.Search = search;
                data.Page = page;
                data.PageSize = pageSize;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<FaceViewModel>>(
                 $"api/face/face-search-page", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }
        public async Task<PagedResultAPI<PagedResultAPIOther>> SaveFace(int idFaceC, string staffCode, string faceName, string agent, string job, string department, string sex, string known, string filePath, string duoifile)
        {
            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                FaceViewModel data = new FaceViewModel();

                data.FaceIdCloud = idFaceC;
                data.StaffCode = staffCode;
                data.FaceName = faceName;
                data.AgeYear = agent;
                data.Job = job;
                data.DepartmentId = department;
                data.Sex = sex;
                data.ImportantLevel = known;
                data.Base64ContentUrl = filePath;
                data.ImagePath = duoifile;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                var data2 = await PostAsyncNoList<PagedResultAPIOther>(
                 $"api/face/info/add", json);
                if (data2[0].Status == "OK")
                {
                    data1.Success = true;
                    data1.Id = data2[0].id;
                    data1.FileSave = data2[0].FileSave;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data2[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }

        //public async Task<PagedResultAPI<PagedResultAPIOther>> SaveDepartment(string departmentCode, string departmentName)
        //{

        //    PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
        //    try
        //    {
        //        DepartmentViewModel data2 = new DepartmentViewModel();

        //        data2.DepartmentName = departmentName;
        //        data2.Acronym = departmentCode;
        //        var json = JsonConvert.SerializeObject(data2);
        //        var data = await PostAsyncNoList<PagedResultAPIOther>(
        //                       $"api/department/add",json);
        //        if (data[0].Status == "OK")
        //        {
        //            data1.Success = true;
        //        }
        //        else
        //        {
        //            data1.Success = false;
        //            data1.Message = data[0].Message;
        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //        data1.Success = false;
        //        data1.Message = ex.Message;
        //    }

        //    return data1;

        //}
        public async Task<PagedResultAPI<PagedResultAPIOther>> SaveFaceImg(int idFace, string filePath, string duoifile)
        {
            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                FaceViewModel data = new FaceViewModel();

                data.FaceId = idFace;
                data.Base64ContentUrl = filePath;
                data.ImagePath = duoifile;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                var data2 = await PostAsyncNoList<PagedResultAPIOther>(
                 $"api/face/info/add-face-img", json);
                if (data2[0].Status == "OK")
                {
                    data1.Success = true;
                    data1.FileSave = data2[0].FileSave;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data2[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }
        public async Task<PagedResultAPI<FaceDetailViewModel>> GetFaceDetailById(int id)
        {

            PagedResultAPI<FaceDetailViewModel> data1 = new PagedResultAPI<FaceDetailViewModel>();
            try
            {
                var data = await GetListAsync<FaceDetailViewModel>(
                               $"api/face/get-face-by-id-top10-img/{id}");
                data1.Items = data;
                if (data.Count == 0)
                {
                    data1.Message = "NoData";
                    data1.Success = false;
                }
                else
                {
                    data1.Success = true;
                }
                
            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }

        public async Task<PagedResultAPI<PagedResultAPIOther>> DeleteFace(string IdFace, string IdCloud)
        {
            //if(IdCloud==null || IdCloud == "")
            //{
                PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
                try
                {
                    var data = await GetListAsyncOther<PagedResultAPIOther>(
                                   $"api/face/delete-face-list-id/{IdFace}");
                    if (data[0].Status == "OK")
                    {
                        data1.Success = true;
                    }
                    else
                    {
                        data1.Success = false;
                        data1.Message = data[0].Message;
                    }

                }
                catch (Exception ex)
                {

                    data1.Success = false;
                    data1.Message = ex.Message;
                }

                return data1;
            //}
            //else
            //{
            //    var IdCloudcheck = IdCloud.Split(',');
            //    for(int i=0;i< IdCloudcheck.Length; i++) 
            //        {
            //        var client = new RestClient("https://api.luxand.cloud/subject/"+ IdCloudcheck[i]);
            //        client.Timeout = -1;
            //        var request = new RestRequest(Method.DELETE);
            //        request.AddHeader("token", "13ff2b7b42954550879e3d8eba394e3a");
            //        IRestResponse response = client.Execute(request);
                     
            //    }
            //    PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            //    try
            //    {
            //        var data = await GetListAsyncOther<PagedResultAPIOther>(
            //                       $"api/face/delete-face-list-id/{IdFace}");
            //        if (data[0].Status == "OK")
            //        {
            //            data1.Success = true;
            //        }
            //        else
            //        {
            //            data1.Success = false;
            //            data1.Message = data[0].Message;
            //        }

            //    }
            //    catch (Exception ex)
            //    {

            //        data1.Success = false;
            //        data1.Message = ex.Message;
            //    }

            //    return data1;
            //}
           

        }
        public async Task<PagedResultAPI<PagedResultAPIOther>> UpdateFace(int idFace, string staffCode, string faceName, string agent, string job, int department, string sex, int importantLevel)
        {
            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                FaceDetailViewModel data = new FaceDetailViewModel();

                data.StaffCode = staffCode;
                data.FaceName = faceName;
                data.AgeYear = agent;
                data.Job = job;
                data.DepartmentId = department;
                data.Sex = sex;
                data.ImportantLevel = importantLevel;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                var data2 = await PostAsyncNoList<PagedResultAPIOther>(
                 $"api/face/update-face/{idFace}", json);
                if (data2[0].Status == "OK")
                {
                    data1.Success = true;
                    data1.Id = data2[0].id;
                    data1.FileSave = data2[0].FileSave;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data2[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }

        public async Task<PagedResultAPI<PagedResultAPIOther>> CheckStaffCode( string staffCode)
        {
            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                var data2 = await GetListAsyncOther<PagedResultAPIOther>(
                 $"api/face/check-staffcode/{staffCode}");
                if (data2[0].Status == "OK")
                {
                    data1.Success = true;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data2[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }
        public async Task<PagedResultAPI<PagedResultAPIOther>> UpdateTKSetting(int id, int deviceId, bool isCheckIn, bool isCheckOut, bool isTKOn)
        {
            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                TkSetting data = new TkSetting();

                data.DeviceId = deviceId;
                data.IsCheckIn = isCheckIn;
                data.IsCheckOut = isCheckOut;
                data.IsTKOn = isTKOn;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                List<PagedResultAPIOther> data2 = new List<PagedResultAPIOther>();
                if (id > 0)
                {
                     data2 = await PostAsyncNoList<PagedResultAPIOther>(
                 $"api/tk/edit/{id}", json);
                }
                else
                {
                    data2 = await PostAsyncNoList<PagedResultAPIOther>(
                                     $"api/tk/add", json);
                }
                    
                if (data2[0].Status == "OK")
                {
                    data1.Success = true;
                    data1.Id = data2[0].id;
                    data1.FileSave = data2[0].FileSave;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data2[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }
        public async Task<PagedResultAPI<PagedResultAPIOther>> CannelTKSetting(int id)
        {
            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                

                
              
                  var  data = await GetListAsyncOther<PagedResultAPIOther>(
                $"api/tk/delete/{id}");


                if (data[0].Status == "OK")
                {
                    data1.Success = true;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
        }
    }
}
