﻿
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.Repositories
{
    public class EventRepositories : BaseApiClient, IEventRepositories
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        public EventRepositories(IHttpClientFactory httpClientFactory,
                    IConfiguration configuration)
            : base(httpClientFactory, configuration)
        {
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        public async Task<PagedResultAPI<Event>> GetListEvent()
        {
           
            PagedResultAPI<Event> data1 = new PagedResultAPI<Event>();
            try
            {
                var data = await GetListAsync<Event>(
                               $"api/event/get");
                data1.Items = data;
                if (data.Count == 0)
                {
                    data1.Message = "NoData";
                }
                data1.Success = true;
            }
            catch(Exception ex)
            {
               
                data1.Success = false;
                data1.Message = ex.Message;
            }
           
            return data1;

        }
        public async Task<PagedResultAPI<ImageEventHistory>> GetStatisticalEvent(string StaffCode,int idFace,string eventID, string fromDate, string toDate, string search, int page, int pageSize)
        {
            PagedResultAPI<ImageEventHistory> data1 = new PagedResultAPI<ImageEventHistory>();
            try
            {
                ImageEventHistoryJsonViewModel data = new ImageEventHistoryJsonViewModel();

                data.EventId = eventID;
                data.FromDate = fromDate;
                data.ToDate = toDate;
                data.Search = search;
                data.Page = page;
                data.PageSize = pageSize;
                data.FaceId = idFace;
                data.StaffCode = StaffCode;
                var json = JsonConvert.SerializeObject(data);

                //data1 = await PostAsync<PagedResultAPI<ProductViewModel>>(
                //             $"/api/Product/IUProduct", json);
                data1 = await PostAsync<PagedResultAPI<ImageEventHistory>>(
                 $"api/event/get-images-history-page", json);
            }
            catch (Exception ex)
            {
                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;
          
        }
        public async Task<PagedResultAPI<ImageEventHistoryViewModel>> GetEventDetailById(int id)
        {

            PagedResultAPI<ImageEventHistoryViewModel> data1 = new PagedResultAPI<ImageEventHistoryViewModel>();
            try
            {
                var data = await GetListAsync<ImageEventHistoryViewModel>(
                               $"api/event/get-event-by-id/{id}");
                data1.Items = data;
                if (data.Count == 0)
                {
                    data1.Message = "NoData";
                    data1.Success = false;
                }
                else
                {
                    data1.Success = true;
                }
                
            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }

        public async Task<PagedResultAPI<PagedResultAPIOther>> DeleteEventStatistical(int id)
        {

            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                var data = await GetListAsyncOther<PagedResultAPIOther>(
                               $"api/event/delete/{id}");
                if (data[0].Status == "OK")
                {
                    data1.Success = true;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data[0].Message ;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }
        public async Task<PagedResultAPI<PagedResultAPIOther>> SaveEvent(string UserName,string StaffCode, int Event, int Device, string RecordTime)
        {

            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                AddEventViewModel data2 = new AddEventViewModel();

                data2.StaffCode = StaffCode;
                data2.Device = Device;
                data2.Event = Event;
                data2.RecordTime = RecordTime;
                data2.CreatedUser = UserName;
                var json = JsonConvert.SerializeObject(data2);
                var data = await PostAsyncNoList<PagedResultAPIOther>(
                               $"api/event/add-event", json);
                if (data[0].Status == "OK")
                {
                    data1.Success = true;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }
        public async Task<PagedResultAPI<PagedResultAPIOther>> UpdateEvent(string UserName,int id, string StaffCode, int Device, string RecordTime)
        {

            PagedResultAPI<PagedResultAPIOther> data1 = new PagedResultAPI<PagedResultAPIOther>();
            try
            {
                AddEventViewModel data2 = new AddEventViewModel();

                data2.StaffCode = StaffCode;
                data2.Device = Device;
                data2.RecordTime = RecordTime;
                data2.CreatedUser = UserName;
                var json = JsonConvert.SerializeObject(data2);
                var data = await PostAsyncNoList<PagedResultAPIOther>(
                               $"api/event/update-event/{id}", json);
                if (data[0].Status == "OK")
                {
                    data1.Success = true;
                }
                else
                {
                    data1.Success = false;
                    data1.Message = data[0].Message;
                }

            }
            catch (Exception ex)
            {

                data1.Success = false;
                data1.Message = ex.Message;
            }

            return data1;

        }

    }
}
