﻿
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.Repositories
{
    public class CompanyRepositories : BaseApiClient, ICompanyRepositories
    {
        private readonly string ConnectedString;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        public CompanyRepositories(IHttpClientFactory httpClientFactory,
                    IConfiguration configuration)
            : base(httpClientFactory, configuration)
        {
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
            ConnectedString = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<PagedResultAPI<Company>> GetCompany()
        {
            var data = await GetListAsync<Company>(
                $"api/company/get");
            PagedResultAPI<Company> data1 = new PagedResultAPI<Company>();
            data1.Items = data;
            if (data.Count == 0)
            {
                data1.Message = "NoData";
            }
            data1.Success = true;
            return data1;

        }

        public async Task<PagedResultAPI<CompanyViewModel>> GetCompanyModelById(int id)
        {
            var data = await GetListAsync<CompanyViewModel>(
                $"api/company/get-company-model/{id}");
            PagedResultAPI<CompanyViewModel> data1 = new PagedResultAPI<CompanyViewModel>();
            data1.Items = data;
            if (data.Count == 0)
            {
                data1.Message = "NoData";
            }
            data1.Success = true;
            return data1;

        }


    }
}
