﻿
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.Repositories
{
    public class StatusRepositories : BaseApiClient, IStatusRepositories
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        public StatusRepositories(IHttpClientFactory httpClientFactory,
                    IConfiguration configuration)
            : base(httpClientFactory, configuration)
        {
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        public async Task<PagedResultAPI<TblStatus>> GetListStatus()
        {
            var data = await GetListAsync<TblStatus>(
                 $"api/tk/get-list-status");
            PagedResultAPI<TblStatus> data1 = new PagedResultAPI<TblStatus>();
            data1.Items = data;
            if (data.Count == 0)
            {
                data1.Message = "NoData";
            }
            data1.Success = true;
            return data1;
        }

    }
}
