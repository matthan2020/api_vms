﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.IRepositories
{
    public interface IStatusRepositories
    {
        Task<PagedResultAPI<TblStatus>> GetListStatus( );
       
    }
}
