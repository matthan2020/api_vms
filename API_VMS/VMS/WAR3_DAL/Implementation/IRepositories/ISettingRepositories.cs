﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.IRepositories
{
    public interface ISettingRepositories
    {
        Task<PagedResultAPI<PagedResultAPIOther>> AddSettingShift(string shiftname);
        Task<PagedResultAPI<TkSettingShift>> GetListSettingShift();
        Task<PagedResultAPI<TkShift>> GetShiftBySettingShiftId(int id);
        Task<PagedResultAPI<PagedResultAPIOther>> AddShift(int shiftId,int settingShift, string ShiftName, string FromDate, string ToDate,string deleteShiftId);
        Task<PagedResultAPI<PagedResultAPIOther>> DeleteSettingShiftCa(int settingShiftId);
        Task<PagedResultAPI<FaceShiftViewModel>> GetListFaceShift(int settingShift, int department, string fromDate, string toDate);
        Task<PagedResultAPI<TkShift>> GetListShiftGroup(int id);
        Task<PagedResultAPI<PagedResultAPIOther>> AddFaceShift(int settingShift, string fromDate, string toDate, List<FaceShiftViewModel> items);
        Task<PagedResultAPI<FaceShiftViewModel>> GetListFaceShiftExcel(int settingShift, int department, string fromDate, string toDate);
    }
}
