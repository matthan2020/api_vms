﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.IRepositories
{
    public interface IFaceRepositories
    {
        Task<PagedResultAPI<FaceViewModel>> GetSearchFaceCB( string search, int page, int pageSize);

        Task<PagedResultAPI<FaceViewModel>> GetSearchFaceNC(string faceName, string staffCode, string departmentId, string job, string sex, string known, int page, int pageSize);

        Task<PagedResultAPI<FaceViewModel>> GetSearchFace(string search,string faceName, string staffCode, string departmentId, string job, string sex, string known, int page, int pageSize);

        Task<PagedResultAPI<PagedResultAPIOther>> SaveFace(int idFaceC, string staffCode, string faceName,string agent,  string job, string department, string sex, string known, string filePath, string duoifile);

        //Task<PagedResultAPI<PagedResultAPIOther>> SaveDepartment(string departmentCode, string departmentName);
        Task<PagedResultAPI<PagedResultAPIOther>> SaveFaceImg(int idFace,  string filePath, string duoifile);
        Task<PagedResultAPI<FaceDetailViewModel>> GetFaceDetailById( int id);
        Task<PagedResultAPI<PagedResultAPIOther>> DeleteFace(string IdFace, string IdCloud);
        Task<PagedResultAPI<PagedResultAPIOther>> UpdateFace(int idFace, string staffCode, string faceName, string agent, string job, int department, string sex, int importantLevel);
        Task<PagedResultAPI<PagedResultAPIOther>> CheckStaffCode( string staffCode);
        Task<PagedResultAPI<PagedResultAPIOther>> UpdateTKSetting(int id, int deviceId, bool isCheckIn, bool isCheckOut, bool isTKOn);
        Task<PagedResultAPI<PagedResultAPIOther>> CannelTKSetting(int id);

    }
}
