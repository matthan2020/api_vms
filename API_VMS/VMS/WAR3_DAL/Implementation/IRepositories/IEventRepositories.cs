﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.IRepositories
{
    public interface IEventRepositories
    {
        Task<PagedResultAPI<Event>> GetListEvent();
        Task<PagedResultAPI<ImageEventHistory>> GetStatisticalEvent(string StaffCode,int idFace,string eventID, string fromDate, string toDate, string search, int page, int pageSize);

        Task<PagedResultAPI<ImageEventHistoryViewModel>> GetEventDetailById( int id);
        Task<PagedResultAPI<PagedResultAPIOther>> DeleteEventStatistical(int id);
        Task<PagedResultAPI<PagedResultAPIOther>> SaveEvent(string UserName,string StaffCode, int Event, int Device, string RecordTime);
        Task<PagedResultAPI<PagedResultAPIOther>> UpdateEvent(string UserName,int id, string StaffCode, int Device, string RecordTime);

    }
}
