﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.IRepositories
{
    public interface IDeviceRepositories
    {
        Task<PagedResultAPI<DeviceViewModel>> GetAllDevice( string keyword, int page, int pageSize);
        Task<PagedResultAPI<DeviceType>> GetDeviceType();
        Task<PagedResultAPI<PagedResultAPIOther>> SaveDevice(int deviceType, string deviceName, string ipAddress, int portNo, string userName, string pass,string link, string model, string campany, string amountCam);
        Task<PagedResultAPI<Device>> GetDevicetById(int id);
        Task<PagedResultAPI<PagedResultAPIOther>> UpdateDevice(int id,int deviceType, string deviceName, string ipAddress, int portNo, string userName, string pass,string link, string model, string campany, string amountCam);
        Task<PagedResultAPI<PagedResultAPIOther>> DeleteDevice(int id);
        Task<PagedResultAPI<DeviceViewModel>> GetListDevice();
        Task<PagedResultAPI<PagedResultAPIOther>> UpdateDeviceStatus(int id, int idStatus);
    }
}
