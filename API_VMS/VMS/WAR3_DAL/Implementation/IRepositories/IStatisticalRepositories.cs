﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.IRepositories
{
    public interface IStatisticalRepositories
    {
        Task<PagedResultAPI<StatisticalViewModel>> GetStatistical(string staffCode, string departmentId, int month, int year , int page, int pageSize);

        Task<PagedResultAPI<StatisticalViewModel>> ExportExcel(string staffCode, string departmentId, int month, int year);

        Task<PagedResultAPIOther> UpdateData(int month, int year);

        Task<PagedResultAPI<InOutViewModel>> GetInOutPage(string staffCode, string departmentId, string fromDate, string toDate, int page, int pageSize);
        Task<PagedResultAPI<InOutViewModel>> ExportExcelInOut(string staffCode, string departmentId, string fromDate, string toDate);
        Task<PagedResultAPI<StatisticalFaceShiftViewModel>> GetStatisticalFaceShift(string staffCode, string departmentId,string shiftName, int month, int year, int page, int pageSize);
        Task<PagedResultAPIOther> UpdateDataFaceShift(int month, int year);
        Task<PagedResultAPI<StatisticalFaceShiftViewModel>> ExportExcelFaceShift(string staffCode, string departmentId, string shiftName, int month, int year);
    }
}
