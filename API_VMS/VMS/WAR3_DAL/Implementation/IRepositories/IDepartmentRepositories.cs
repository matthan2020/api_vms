﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WAR3_DAL.Dtos;
using WAR3_DAL.ViewModel;

namespace WAR3_DAL.Implementation.IRepositories
{
    public interface IDepartmentRepositories
    {
        Task<PagedResultAPI<DepartmentViewModel>> GetListDepartment( );
        Task<PagedResultAPI<DepartmentViewModel>> GetAllDepartmentPage(string keyword, int page, int pageSize);
        Task<PagedResultAPI<PagedResultAPIOther>> SaveDepartment(string departmentCode, string departmentName);
        Task<PagedResultAPI<DepartmentViewModel>> GetDepartmentById(int ide);
        Task<PagedResultAPI<PagedResultAPIOther>> UpdateDepartment(int id, string departmentCode, string departmentName);
        Task<PagedResultAPI<PagedResultAPIOther>> DeleteDepartment(int id);
    }
}
