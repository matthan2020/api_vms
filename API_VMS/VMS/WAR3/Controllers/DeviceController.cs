﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using WAR3.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class DeviceController : Controller
    {
        private IDeviceRepositories _deviceRepositories;
        private readonly IHostingEnvironment _hostingEnvironment;

        public DeviceController(IDeviceRepositories deviceRepositories,  IHostingEnvironment hostingEnvironment)
        {
            _deviceRepositories = deviceRepositories;
            _hostingEnvironment = hostingEnvironment; ;
        }
        public IActionResult Index()
        {
            if (Request.Cookies["LOAD"] != null)
            {
                ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return new RedirectResult("../Event/Index");
                return View();
            }
            else
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return View();
                return new RedirectResult("~/Login/Login");
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAllDevice(string keyword, int page, int pageSize)
        {
            var model = await _deviceRepositories.GetAllDevice(keyword, page, pageSize);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetDeviceType()
        {
            var model = await _deviceRepositories.GetDeviceType();
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }
        [HttpPost]
        public async Task<IActionResult> SaveDevice(int deviceType,string deviceName,string ipAddress,int portNo,string userName,string pass,string link,string model,string campany,string amountCams)
        {
            var data = await _deviceRepositories.SaveDevice(deviceType, deviceName, ipAddress, portNo, userName,pass, link, model, campany, amountCams);
            return new OkObjectResult(new GenericResult(data.Success, data.Message)); //new OkObjectResult(model);
        }
        [HttpGet]
        public async Task<IActionResult> GetDevicetById(int id)
        {
            var model = await _deviceRepositories.GetDevicetById(id);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateDevice(int id,int deviceType, string deviceName, string ipAddress, int portNo, string userName, string pass, string link, string model, string campany, string amountCam)
        {
            var data = await _deviceRepositories.UpdateDevice(id,deviceType, deviceName, ipAddress, portNo, userName, pass, link, model, campany, amountCam);
            return new OkObjectResult(new GenericResult(data.Success, data.Message)); //new OkObjectResult(model);
        }
        [HttpPost]
        public async Task<IActionResult> DeleteDevice(int id)
        {
            var model = await _deviceRepositories.DeleteDevice(id);
            return new OkObjectResult(new GenericResult(model.Success, model.Message)); //new OkObjectResult(model);
        }
        [HttpGet]
        public async Task<IActionResult> GetListDevice()
        {
            var model = await _deviceRepositories.GetListDevice();
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateDeviceStatus(int id,int idStatus)
        {
            var model = await _deviceRepositories.UpdateDeviceStatus(id, idStatus);
            return new OkObjectResult(new GenericResult(model.Success, model.Message)); //new OkObjectResult(model);
        }
    }
}
