﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using RestSharp;
using WAR3.Dtos;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class SettingController : Controller
    {
        private ISettingRepositories _shiftRepositories;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IHttpClientFactory _httpClientFactory;
        public SettingController(ISettingRepositories shiftRepositories, IHostingEnvironment hostingEnvironment, IHttpClientFactory httpClientFactory)
        {
            _shiftRepositories = shiftRepositories;
            _hostingEnvironment = hostingEnvironment;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            if (Request.Cookies["LOAD"] != null)
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return new RedirectResult("../Event/Index");
                return View();
            }
            else
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return View();
                return new RedirectResult("~/Login/Login");
            }
        }
        public IActionResult Shift()
        {
            if (Request.Cookies["LOAD"] != null)
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return new RedirectResult("../Event/Index");
                return View();
            }
            else
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return View();
                return new RedirectResult("~/Login/Login");
            }
        }
        public IActionResult ShiftOther()
        {
            if (Request.Cookies["LOAD"] != null)
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return new RedirectResult("../Event/Index");
                return View();
            }
            else
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return View();
                return new RedirectResult("~/Login/Login");
            }
        }
        public IActionResult FaceShift()
        {
            if (Request.Cookies["LOAD"] != null)
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return new RedirectResult("../Event/Index");
                return View();
            }
            else
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return View();
                return new RedirectResult("~/Login/Login");
            }
        }
        [HttpPost]
        public async Task<IActionResult> AddSettingShift(string shiftname)
        {
            var data = await _shiftRepositories.AddSettingShift(shiftname);
            return new OkObjectResult(new GenericResult(data.Success, data.Message));

        }
        [HttpGet]
        public async Task<IActionResult> GetListSettingShift()
        {
            var data = await _shiftRepositories.GetListSettingShift();
            return new OkObjectResult(new GenericResult(data.Success, data.Message,data));

        }

        [HttpGet]
        public async Task<IActionResult> GetShiftBySettingShiftId(int id)
        {
            var data = await _shiftRepositories.GetShiftBySettingShiftId(id);
            return new OkObjectResult(new GenericResult(data.Success, data.Message, data));

        }

        [HttpPost]
        public async Task<IActionResult> AddShift(int settingShift,string deleteShiftId,string fromDateMain,string toDateMain,string fromDateExtra,string toDateExtra, List<TkShift> items)
        {
            foreach (var item in items)
            {
               var data= await _shiftRepositories.AddShift(item.ShiftId,settingShift, item.ShiftName, item.FromDate,item.ToDate, deleteShiftId);
                if(data.Success == false)
                {
                    return new OkObjectResult(new GenericResult(data.Success, data.Message));
                }
            }
            return new OkObjectResult(new GenericResult(true, null));

        }

        [HttpGet]
        public async Task<IActionResult> DeleteSettingShiftCa(int settingShiftId)
        {
          
                var data = await _shiftRepositories.DeleteSettingShiftCa(settingShiftId);

            return new OkObjectResult(new GenericResult(data.Success, data.Message, data));

        }
        [HttpGet]
        public async Task<IActionResult> GetListFaceShift(int settingShift,int department,string fromDate,string toDate)
        {
            var data = await _shiftRepositories.GetListFaceShift(settingShift, department, fromDate, toDate);
            return new OkObjectResult(new GenericResult(data.Success, data.Message, data));

        }
        [HttpGet]
        public async Task<IActionResult> GetListShiftGroup(int id)
        {
            var data = await _shiftRepositories.GetListShiftGroup(id);
            return new OkObjectResult(new GenericResult(data.Success, data.Message, data));

        }
        [HttpPost]
        public async Task<IActionResult> AddFaceShift(int settingShift, string fromDate, string toDate, List<FaceShiftViewModel> items)
        {
           
                var data = await _shiftRepositories.AddFaceShift( settingShift, fromDate, toDate, items);
               
                    return new OkObjectResult(new GenericResult(data.Success, data.Message));
               

        }
        [HttpGet]
        public async Task<IActionResult> ExportExcel(int settingShift,string settingShiftName, int department,string departmentName, string fromDate, string toDate)
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string directory = Path.Combine(sWebRootFolder, "export-files", "LICHLV");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            string sFileName = $"LichLV_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
            string fileUrl = $"{Request.Scheme}://{Request.Host}/vms/export-files/LICHLV/{sFileName}";
            string templateDocument = Path.Combine(sWebRootFolder, "Template", "TemplateExport", "ExportLichLV.xlsx");
            FileInfo fileName = new FileInfo(Path.Combine(directory, sFileName));
            if (fileName.Exists)
            {
                fileName.Delete();

                fileName = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

            }

            var model = await _shiftRepositories.GetListFaceShiftExcel(settingShift, department, fromDate, toDate);

            if (model.Success == true && model.Message == "SUCCESS")
            {
                using (FileStream templateDocumentStream = System.IO.File.OpenRead(templateDocument))
                {
                    using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                    {
                        // add a new worksheet to the empty workbook
                        // ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(fileNameTemp);

                        ExcelWorksheet workSheet = package.Workbook.Worksheets["LICH_LV"];
                        int i = 9;
                        var NgayBD = Convert.ToInt32(fromDate.Split("/")[0]);
                        var ThangBD = Convert.ToInt32(fromDate.Split("/")[1]);
                        var NamBD = Convert.ToInt32(fromDate.Split("/")[2]);
                        var NgayKT = Convert.ToInt32(toDate.Split("/")[0]);
                        var ThangKT = Convert.ToInt32(toDate.Split("/")[1]);
                        var NamKT = Convert.ToInt32(toDate.Split("/")[2]);
                        var gan = 0;
                        var gan1 = 0;
                        if (ThangBD==ThangKT)
                        {
                            
                            for(int b = 0; b < 7; b++)
                            {
                                workSheet.Cells[8, 4+gan].Value = NgayBD+b+"/"+ThangBD+"/"+ NamBD;
                                gan = gan + 2;
                            }
                            
                        }
                        else
                        {
                                for (int c = NgayKT; c >= 1; c--)
                                {
                                    var checkthang = c;
                                    if (checkthang > 0)
                                    {
                                    workSheet.Cells[8, 16- gan].Value = c + "/" + ThangKT + "/" + NamKT;
                                    gan = gan + 2;
                                }
                                   
                                   
                                }
                            for (int d = 0; d <= 7- NgayKT; d++)
                            {
                                workSheet.Cells[8, 4 + gan1].Value = (NgayBD + d) + "/" + ThangBD + "/" + NamBD;
                                gan1 = gan1 + 2;
                            }

                        }
                        workSheet.Cells[3, 8].Value = "Thiết lập ca: " + settingShiftName;

                        workSheet.Cells[4, 8].Value = "Phòng ban: " + departmentName;
                        workSheet.Cells[5, 8].Value = "Từ tuần: " + fromDate+" Đến :"+ toDate;
                       
                       
                        foreach (var item in model.Items)
                        {
                            workSheet.Cells[i, 1].Value = i - 8;
                            workSheet.Cells[i, 2].Value = item.StaffCode;
                            workSheet.Cells[i, 3].Value = item.FaceName;
                            workSheet.Cells[i, 4].Value = item.Day1;
                            workSheet.Cells[i, 5].Value = item.RiceShift1==1?"Ăn Chính": item.RiceShift1 == 2 ? "Ăn phụ":item.RiceShift1 == 3 ? "Ăn chính, Ăn phụ":"";
                            workSheet.Cells[i, 6].Value = item.Day2;
                            workSheet.Cells[i, 7].Value = item.RiceShift2 == 1 ? "Ăn Chính" : item.RiceShift2 == 2 ? "Ăn phụ" : item.RiceShift2 == 3 ? "Ăn chính, Ăn phụ" : "";
                            workSheet.Cells[i, 8].Value = item.Day3;
                            workSheet.Cells[i, 9].Value = item.RiceShift3 == 1 ? "Ăn Chính" : item.RiceShift3 == 2 ? "Ăn phụ" : item.RiceShift3 == 3 ? "Ăn chính, Ăn phụ" : "";
                            workSheet.Cells[i, 10].Value = item.Day4;
                            workSheet.Cells[i, 11].Value = item.RiceShift4 == 1 ? "Ăn Chính" : item.RiceShift4 == 2 ? "Ăn phụ" : item.RiceShift4 == 3 ? "Ăn chính, Ăn phụ" : "";
                            workSheet.Cells[i, 12].Value = item.Day5;
                            workSheet.Cells[i, 13].Value = item.RiceShift5 == 1 ? "Ăn Chính" : item.RiceShift5 == 2 ? "Ăn phụ" : item.RiceShift5 == 3 ? "Ăn chính, Ăn phụ" : "";
                            workSheet.Cells[i, 14].Value = item.Day6;
                            workSheet.Cells[i, 15].Value = item.RiceShift6 == 1 ? "Ăn Chính" : item.RiceShift6 == 2 ? "Ăn phụ" : item.RiceShift6 == 3 ? "Ăn chính, Ăn phụ" : "";
                            workSheet.Cells[i, 16].Value = item.Day7;
                            workSheet.Cells[i, 17].Value = item.RiceShift7 == 1 ? "Ăn Chính" : item.RiceShift7 == 2 ? "Ăn phụ" : item.RiceShift7 == 3 ? "Ăn chính, Ăn phụ" : "";
                            
                            i = i + 1;
                        }
                        package.SaveAs(fileName);
                        //fileName.CopyTo(package);

                    }
                }
                return new OkObjectResult(new GenericResult(model.Success, model.Message, fileUrl));


            }
            else
            {
                return new OkObjectResult(new GenericResult(false, model.Message));
            }



        }
    }
}
