﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using WAR3.Dtos;
using WAR3_DAL.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class DepartmentController : Controller
    {
        private IDepartmentRepositories _departmentRepositories;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IHttpClientFactory _httpClientFactory;
        public DepartmentController(IDepartmentRepositories departmentRepositories, IHostingEnvironment hostingEnvironment, IHttpClientFactory httpClientFactory)
        {
            _departmentRepositories = departmentRepositories;
            _hostingEnvironment = hostingEnvironment;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            if (Request.Cookies["LOAD"] != null)
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return new RedirectResult("../Event/Index");
                return View();
            }
            else
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return View();
                return new RedirectResult("~/Login/Login");
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetListDepartment()
        {
            var data = await _departmentRepositories.GetListDepartment();
            return new OkObjectResult(new GenericResult(data.Success, data.Message,data));
           
        }
        [HttpGet]
        public async Task<IActionResult> GetAllDepartmentPage(string keyword,int page,int pageSize)
        {
            var data = await _departmentRepositories.GetAllDepartmentPage(keyword, page, pageSize);
            return new OkObjectResult(new GenericResult(data.Success, data.Message, data));

        }
        [HttpPost]
        public async Task<IActionResult> SaveDepartment(string departmentCode, string departmentName)
        {
            var data = await _departmentRepositories.SaveDepartment(departmentCode, departmentName);
            return new OkObjectResult(new GenericResult(data.Success, data.Message));

        }
        [HttpGet]
        public async Task<IActionResult> GetDepartmentById(int id)
        {
            var data = await _departmentRepositories.GetDepartmentById(id);
            return new OkObjectResult(new GenericResult(data.Success, data.Message,data));

        }
        
       [HttpPost]
        public async Task<IActionResult> UpdateDepartment(int id, string departmentCode, string departmentName)
        {
            var data = await _departmentRepositories.UpdateDepartment(id, departmentCode, departmentName);
            return new OkObjectResult(new GenericResult(data.Success, data.Message)); //new OkObjectResult(model);
        }
        [HttpPost]
        public async Task<IActionResult> DeleteDepartment(int id)
        {
            var data = await _departmentRepositories.DeleteDepartment(id);
            return new OkObjectResult(new GenericResult(data.Success, data.Message)); //new OkObjectResult(model);
        }
    }
}
