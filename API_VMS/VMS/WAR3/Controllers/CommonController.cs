﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using WAR3.Dtos;
using WAR3_DAL.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class CommonController : Controller
    {
        private IStatusRepositories _statusRepositories;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IHttpClientFactory _httpClientFactory;
        public CommonController(IStatusRepositories statusRepositories, IHostingEnvironment hostingEnvironment, IHttpClientFactory httpClientFactory)
        {
            _statusRepositories = statusRepositories;
            _hostingEnvironment = hostingEnvironment;
            _httpClientFactory = httpClientFactory;
        }
       

        [HttpGet]
        public async Task<IActionResult> GetListStatus()
        {
            var data = await _statusRepositories.GetListStatus();
            return new OkObjectResult(new GenericResult(data.Success, data.Message,data));
           
        }
       
    }
}
