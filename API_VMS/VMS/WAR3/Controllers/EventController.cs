﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using RestSharp;
using WAR3.Dtos;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class EventController : Controller
    {
        private readonly string ConnectedAPIBox;
        private IEventRepositories _eventRepositories;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        public EventController(IEventRepositories eventRepositories, IHostingEnvironment hostingEnvironment, IHttpClientFactory httpClientFactory, IConfiguration configuration)
        {
            _eventRepositories = eventRepositories;
            _hostingEnvironment = hostingEnvironment;
            _httpClientFactory = httpClientFactory;
            _configuration = configuration; ;
            ConnectedAPIBox = _configuration.GetConnectionString("APIBox");
        }
        public IActionResult Index()
        {
            //ViewData["NAME"] = Request.Cookies["NAME"];
            //return View();
            if (Request.Cookies["LOAD"] != null)
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return new RedirectResult("../Event/Index");
                return View();
            }
            else
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return View();
                return new RedirectResult("~/Login/Login");
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetListEvent()
        {
            var data = await _eventRepositories.GetListEvent();
            return new OkObjectResult(new GenericResult(data.Success, data.Message, data));

        }

        [HttpPost]
        public async Task<IActionResult> GetStatisticalEvent(string eventID, string fromDate, string toDate, string search, int page, int pageSize, IList<IFormFile> files)
        {
            int idFace = 0;
            string staffCode = "";
            int MaxContentLength = 1024 * 1024 * 20;
            try
            {
                if (files != null && files.Count > 0)
                {
                    var file = files[0];
                    var filename = ContentDispositionHeaderValue
                                       .Parse(file.ContentDisposition)
                                       .FileName
                                       .Trim('"');
                    var duoifile = filename.Substring(filename.Length - 4);
                    if (duoifile == ".jpg" || duoifile == ".gif" || duoifile == ".png")
                    {
                        if (files[0].Length > MaxContentLength)
                        {
                           
                            return new OkObjectResult(new GenericResult(false, "Bạn đã uplooad file ảnh trên 20 Mb"));
                        }
                        else
                        {
                            //var filePath1 = string.Format(@"C:\upload\images\event\" + $"{DateTime.Now:yyyyMMddhhmmss}" + "_face" + duoifile);
                            //var filePath = string.Format(@"C:\upload\images\event\");
                            //using (FileStream fs = System.IO.File.Create(filePath1))
                            //{
                            //    file.CopyTo(fs);
                            //    fs.Flush();
                            //}
                           
                            var url = $"predict";
                            var client = _httpClientFactory.CreateClient();
                            //client.BaseAddress = new Uri("https://localhost:44343/");
                            client.BaseAddress = new Uri(ConnectedAPIBox);


                        var content = new MultipartFormDataContent();
                            
                                content.Add(new StreamContent(file.OpenReadStream())
                                {
                                    Headers =
                    {
                        ContentLength = file.Length,
                        ContentType = new MediaTypeHeaderValue(file.ContentType)
                    }
                                }, "file", filename);

                                var response = await client.PostAsync(url, content);
                                var body = await response.Content.ReadAsStringAsync();
                            

                            if (response.IsSuccessStatusCode == true)
                            {
                                //if (response.Content.Count() > 3)
                                //{
                                List<BoxAI> user = (List<BoxAI>) JsonConvert.DeserializeObject(body,
                    typeof(List<BoxAI>));
                                //List<Cloud> user = JsonConvert.DeserializeObject<List<Cloud>>(response.Content);
                                if (user.Count > 0)
                                {
                                    if (Convert.ToDouble(user[0].score) >= 0.7)
                                    {
                                        staffCode = user[0].name;
                                        var data = await _eventRepositories.GetStatisticalEvent(staffCode,idFace, eventID, fromDate, toDate, search, page, pageSize);
                                        return new OkObjectResult(new GenericResult(data.Success, data.Message, data));
                                    }
                                    else
                                    {
                                        PagedResultAPI<ImageEventHistory> data = new PagedResultAPI<ImageEventHistory>();
                                        return new OkObjectResult(new GenericResult(true, "", data));
                                    }
                                    //idFace = user[0].id;
                                    
                                    
                                    
                                }
                                else
                                {
                                    return new OkObjectResult(new GenericResult(false, "File ảnh không có mặt người để tìm kiếm.Mời bạn chọn lại ảnh tìm kiếm"));
                                }

                               


                            }
                            else
                            {
                                return new OkObjectResult(new GenericResult(false, response.RequestMessage));
                            }
                            //if (System.IO.File.Exists(filePath1))
                            //{
                            //    try
                            //    {
                            //        System.IO.File.Delete(filePath1);
                            //    }
                            //    catch (Exception ex)
                            //    {
                            //        //Do something
                            //    }
                            //}
                        }
                    }
                    else
                    {
                        return new OkObjectResult(new GenericResult(false, "File ảnh k đúng định dạng"));

                    }
                }
                else
                {
                    var data = await _eventRepositories.GetStatisticalEvent(staffCode,0, eventID, fromDate, toDate, search, page, pageSize);
                    return new OkObjectResult(new GenericResult(data.Success, data.Message, data));
                }
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }

           
        }

        [HttpGet]
        public async Task<IActionResult> GetEventDetailById(int id)
        {
            var data = await _eventRepositories.GetEventDetailById(id);
            return new OkObjectResult(new GenericResult(data.Success, data.Message, data));

        }
        [HttpGet]
        public async Task<IActionResult> DeleteEventStatistical(int id)
        {
            var data = await _eventRepositories.DeleteEventStatistical(id);
            return new OkObjectResult(new GenericResult(data.Success,data.Message));

        }
        [HttpPost]
        public async Task<IActionResult> SaveEvent(string StaffCode, int Event, int Device,string RecordTime)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var data = await _eventRepositories.SaveEvent(UserName, StaffCode, Event, Device, RecordTime);
            return new OkObjectResult(new GenericResult(data.Success, data.Message));

        }

        [HttpPost]
        public async Task<IActionResult> UpdateEvent(int id,string StaffCode, int Device, string RecordTime)
        {
            var UserName = Request.Cookies["USER_NAME"];
            var data = await _eventRepositories.UpdateEvent(UserName,id, StaffCode, Device, RecordTime);
            return new OkObjectResult(new GenericResult(data.Success, data.Message));

        }

    }
}
