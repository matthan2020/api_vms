﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using RestSharp;
using WAR3.Dtos;
using WAR3_DAL.Dtos;
using WAR3_DAL.Entities;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class FaceController : Controller
    {
        private readonly string ConnectedAPIBox;
        private IFaceRepositories _faceRepositories;
        private IDepartmentRepositories _departmentRepositories;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public FaceController(IFaceRepositories faceRepositories, IDepartmentRepositories departmentRepositories, IHostingEnvironment hostingEnvironment, IHttpClientFactory httpClientFactory, IConfiguration configuration)
        {
            _faceRepositories = faceRepositories;
            _departmentRepositories = departmentRepositories;
            _hostingEnvironment = hostingEnvironment;
            _httpClientFactory = httpClientFactory;
            _configuration = configuration; ;
            ConnectedAPIBox = _configuration.GetConnectionString("APIBox");
        }
        public IActionResult Index()
        {
            if (Request.Cookies["LOAD"] != null)
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return new RedirectResult("../Event/Index");
                return View();
            }
            else
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return View();
                return new RedirectResult("~/Login/Login");
            }
        }
        public IActionResult FaceConfig()
        {
            if (Request.Cookies["LOAD"] != null)
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return new RedirectResult("../Event/Index");
                return View();
            }
            else
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return View();
                return new RedirectResult("~/Login/Login");
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetSearchFaceCB(string search, int page, int pageSize)
        {
            var data = await _faceRepositories.GetSearchFaceCB( search, page, pageSize);
            return new OkObjectResult(new GenericResult(data.Success, data.Message, data));

        }
        [HttpPost]
        public async Task<IActionResult> GetSearchFaceNC(string faceName, string staffCode, string departmentId, string job, string sex, string known, int page, int pageSize)
        {
            var data = await _faceRepositories.GetSearchFaceNC(faceName, staffCode, departmentId, job, sex, known, page, pageSize);
            return new OkObjectResult(new GenericResult(data.Success, data.Message, data));

        }
        [HttpPost]
        public async Task<IActionResult> GetSearchFace(string search, string faceName, string staffCode, string departmentId, string job, string sex, string known, int page, int pageSize)
        {
            if((faceName==null|| faceName=="")&&(staffCode==null|| staffCode=="")&&(departmentId==null|| departmentId=="")&&(job==null|| job=="")&&(sex==null|| sex=="")&&(known==null|| known=="")&&(search == null|| search == ""))
            {
                var data = await _faceRepositories.GetSearchFaceCB(search, page, pageSize);
                return new OkObjectResult(new GenericResult(data.Success, data.Message, data));
            }
            else
            {
                if((faceName == null || faceName == "") && (staffCode == null || staffCode == "") && (departmentId == null || departmentId == "") && (job == null || job == "") && (sex == null || sex == "") && (known == null || known == "") && (search != null && search != ""))
                {
                    var data = await _faceRepositories.GetSearchFaceCB(search, page, pageSize);
                    return new OkObjectResult(new GenericResult(data.Success, data.Message, data));
                }
                else
                {
                    if(search == null || search == "")
                    {
                        var data = await _faceRepositories.GetSearchFaceNC(faceName, staffCode, departmentId, job, sex, known, page, pageSize);
                        return new OkObjectResult(new GenericResult(data.Success, data.Message, data));
                    }
                    else
                    {
                        var data = await _faceRepositories.GetSearchFace(search, faceName, staffCode, departmentId, job, sex, known, page, pageSize);
                        return new OkObjectResult(new GenericResult(data.Success, data.Message, data));
                    }
                }
            }
            

        }

        [HttpPost]
        public async Task<IActionResult> SaveFace1(string staffCode, string faceName, string agent, string job, string department, string sex, string known, IList<IFormFile> files)
        {

            int idFaceC = 0;
            int idFace = 0;
            string name = "";
            if (faceName == "" || faceName == null)
            {
                name = "Unknow";
            }
            else
            {
                name = faceName;
            }
            int MaxContentLength = 1024 * 1024 * 20;
            try
            {
                var abc = 0;
                foreach (var formFile in files)
                {
                    abc = abc + 1;
                }
                  
                   
                    return new OkObjectResult(new GenericResult(false, "Không có ảnh để thêm mới1"));

               
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }
         [HttpPost]
        public async Task<IActionResult> SaveFace2(string staffCode, string faceName, string agent, string job, string department, string sex, string known, IList<IFormFile> files)
        {
            int idFaceC = 0;
            int idFace = 0;
            int a = 0;
            int i = 0;
            string name = "";
            if (faceName == "" || faceName == null)
            {
                name = "Unknow";
            }
            else
            {
                name = faceName;
            }
            int MaxContentLength = 1024 * 1024 * 20;
            try
            {
                if (files != null && files.Count > 0)
                {
                    var loifile = 0;
                    var loiMB = 0;
                    //long lenghtImg = 0;
                    foreach (var formFile in files)
                    {
                        var file = files[a];
                        var filename = ContentDispositionHeaderValue
                                           .Parse(file.ContentDisposition)
                                           .FileName
                                           .Trim('"');
                        var duoifile = filename.Substring(filename.Length - 4);
                        if (duoifile == ".jpg" || duoifile == ".gif" || duoifile == ".png")
                        {
                            //lenghtImg = lenghtImg + files[a].Length;
                            if (files[a].Length > MaxContentLength)// || lenghtImg > MaxContentLength)
                            {
                                loiMB = 1;


                            }
                        }
                        else
                        {
                            loifile = 1;
                        }
                        a++;
                    }
                    if (loiMB == 0 && loifile == 0)
                    {
                        if(staffCode==""|| staffCode == null)
                        {
                            foreach (var formFile in files)
                            {
                                var file = files[i];
                                var filename = ContentDispositionHeaderValue
                                                   .Parse(file.ContentDisposition)
                                                   .FileName
                                                   .Trim('"');
                                var duoifile = filename.Substring(filename.Length - 4);
                                var filePath1 = string.Format(@"C:\upload\images\event\" + $"{DateTime.Now:yyyyMMddhhmmss}" + "_face" + duoifile);
                                var filePath = string.Format(@"C:\upload\images\root\");
                                //file.SaveAs(filePath);
                                using (FileStream fs = System.IO.File.Create(filePath1))
                                {
                                    file.CopyTo(fs);
                                    fs.Flush();
                                }
                                if (i == 0)
                                {


                                    //var filePath1 = string.Format(@"C:\upload\images\root\Untitled1.png");
                                    string _tokenUpdated = "13ff2b7b42954550879e3d8eba394e3a";
                                    var clientFace = new RestClient("https://api.luxand.cloud/subject/v2");
                                    clientFace.Timeout = -1;
                                    var request = new RestRequest(Method.POST);
                                    request.AddHeader("token", _tokenUpdated);
                                    request.AddParameter("name", name);
                                    request.AddFile("photo", filePath1);
                                    IRestResponse response = clientFace.Execute(request);
                                    var abc = "[" + response.Content + "]";
                                    if (response.IsSuccessful == true)
                                    {
                                        //if (response.Content.Count() > 3)
                                        //{
                                        List<Cloud> user = JsonConvert.DeserializeObject<List<Cloud>>(abc);

                                        idFaceC = user[0].id;

                                        var data = await _faceRepositories.SaveFace(idFaceC, staffCode, faceName, agent, job, department, sex, known, filePath, duoifile);
                                        if (data.Success == true)
                                        {
                                            idFace = data.Id;
                                            using (FileStream fs1 = System.IO.File.Create(data.FileSave))
                                            {
                                                file.CopyTo(fs1);
                                                fs1.Flush();
                                            }
                                            if (System.IO.File.Exists(filePath1))
                                            {
                                                try
                                                {
                                                    System.IO.File.Delete(filePath1);
                                                }
                                                catch (Exception ex)
                                                {
                                                    //Do something
                                                }
                                            }
                                        }







                                    }
                                    else
                                    {
                                        return new OkObjectResult(new GenericResult(false, response.ErrorMessage));
                                    }

                                }
                                else
                                {
                                    var data = await _faceRepositories.SaveFaceImg(idFace, filePath, duoifile);
                                    if (data.Success == true)
                                    {

                                        using (FileStream fs1 = System.IO.File.Create(data.FileSave))
                                        {
                                            file.CopyTo(fs1);
                                            fs1.Flush();
                                        }

                                    }
                                }
                                i++;
                            }
                            return new OkObjectResult(new GenericResult(true, null));
                        }
                        else
                        {
                            var data1 = await _faceRepositories.CheckStaffCode( staffCode);
                            if (data1.Success == true)
                            {
                                foreach (var formFile in files)
                                {
                                    var file = files[i];
                                    var filename = ContentDispositionHeaderValue
                                                       .Parse(file.ContentDisposition)
                                                       .FileName
                                                       .Trim('"');
                                    var duoifile = filename.Substring(filename.Length - 4);
                                    var filePath1 = string.Format(@"C:\upload\images\event\" + $"{DateTime.Now:yyyyMMddhhmmss}" + "_face" + duoifile);
                                    var filePath = string.Format(@"C:\upload\images\root\");
                                    //file.SaveAs(filePath);
                                    using (FileStream fs = System.IO.File.Create(filePath1))
                                    {
                                        file.CopyTo(fs);
                                        fs.Flush();
                                    }
                                    if (i == 0)
                                    {


                                        //var filePath1 = string.Format(@"C:\upload\images\root\Untitled1.png");
                                        string _tokenUpdated = "13ff2b7b42954550879e3d8eba394e3a";
                                        var clientFace = new RestClient("https://api.luxand.cloud/subject/v2");
                                        clientFace.Timeout = -1;
                                        var request = new RestRequest(Method.POST);
                                        request.AddHeader("token", _tokenUpdated);
                                        request.AddParameter("name", name);
                                        request.AddFile("photo", filePath1);
                                        IRestResponse response = clientFace.Execute(request);
                                        var abc = "[" + response.Content + "]";
                                        if (response.IsSuccessful == true)
                                        {
                                            //if (response.Content.Count() > 3)
                                            //{
                                            List<Cloud> user = JsonConvert.DeserializeObject<List<Cloud>>(abc);

                                            idFaceC = user[0].id;

                                            var data = await _faceRepositories.SaveFace(idFaceC, staffCode, faceName, agent, job, department, sex, known, filePath, duoifile);
                                            if (data.Success == true)
                                            {
                                                idFace = data.Id;
                                                using (FileStream fs1 = System.IO.File.Create(data.FileSave))
                                                {
                                                    file.CopyTo(fs1);
                                                    fs1.Flush();
                                                }
                                                if (System.IO.File.Exists(filePath1))
                                                {
                                                    try
                                                    {
                                                        System.IO.File.Delete(filePath1);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        //Do something
                                                    }
                                                }
                                            }







                                        }
                                        else
                                        {
                                            return new OkObjectResult(new GenericResult(false, response.ErrorMessage));
                                        }

                                    }
                                    else
                                    {
                                        var data = await _faceRepositories.SaveFaceImg(idFace, filePath, duoifile);
                                        if (data.Success == true)
                                        {

                                            using (FileStream fs1 = System.IO.File.Create(data.FileSave))
                                            {
                                                file.CopyTo(fs1);
                                                fs1.Flush();
                                            }

                                        }
                                    }
                                    i++;
                                }
                                return new OkObjectResult(new GenericResult(true, null));
                            }
                            else
                            {
                                return new OkObjectResult(new GenericResult(data1.Success, data1.Message));
                            }
                        }
                        
                    }
                    else
                    {
                        if (loifile > 0)
                        {
                            return new OkObjectResult(new GenericResult(false, "File ảnh k đúng định dạng"));
                        }
                        else
                        {
                            return new OkObjectResult(new GenericResult(false, "Bạn đã uplooad file ảnh trên 20 Mb"));
                        }

                    }

                }
                else
                {
                    //var data = await _eventRepositories.GetStatisticalEvent(0, eventID, fromDate, toDate, search, page, pageSize);
                    return new OkObjectResult(new GenericResult(false, "Không có ảnh để thêm mới"));
                }
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }

        }

        [HttpPost]
        public async Task<IActionResult> SaveFace(string staffCode, string faceName, string agent, string job, string department, string sex, string known, IList<IFormFile> files)
        {
            int idFaceC = 0;
            int idFace = 0;
            int a = 0;
            int i = 0;
            string name = "";
            if (faceName == "" || faceName == null)
            {
                name = "Unknow";
            }
            else
            {
                name = faceName;
            }
            int MaxContentLength = 1024 * 1024 * 20;
            try
            {
                if (files != null && files.Count > 0)
                {
                    var loifile = 0;
                    var loiMB = 0;
                    //long lenghtImg = 0;
                    foreach (var formFile in files)
                    {
                        var file = files[a];
                        var filename = ContentDispositionHeaderValue
                                           .Parse(file.ContentDisposition)
                                           .FileName
                                           .Trim('"');
                        var duoifile = filename.Substring(filename.Length - 4);
                        if (duoifile == ".jpg" || duoifile == ".gif" || duoifile == ".png")
                        {
                            //lenghtImg = lenghtImg + files[a].Length;
                            if (files[a].Length > MaxContentLength)// || lenghtImg > MaxContentLength)
                            {
                                loiMB = 1;


                            }
                        }
                        else
                        {
                            loifile = 1;
                        }
                        a++;
                    }
                    if (loiMB == 0 && loifile == 0)
                    {
                        if (staffCode == "" || staffCode == null)
                        {
                            foreach (var formFile in files)
                            {
                                var file = files[i];
                                var filename = ContentDispositionHeaderValue
                                                   .Parse(file.ContentDisposition)
                                                   .FileName
                                                   .Trim('"');
                                var duoifile = filename.Substring(filename.Length - 4);
                                //var filePath1 = string.Format(@"C:\upload\images\event\" + $"{DateTime.Now:yyyyMMddhhmmss}" + "_face" + duoifile);
                                var filePath = string.Format(@"C:\upload\images\root\");
                                //file.SaveAs(filePath);
                               
                                if (i == 0)
                                {
                                    var data = await _faceRepositories.SaveFace(idFaceC, staffCode, faceName, agent, job, department, sex, known, filePath, duoifile);
                                    if (data.Success == true)
                                    {
                                        idFace = data.Id;
                                        using (FileStream fs1 = System.IO.File.Create(data.FileSave))
                                        {
                                            file.CopyTo(fs1);
                                            fs1.Flush();
                                        }

                                    }

                                    var url = $"api/add_images_database?user_name={staffCode}";
                                    var client = _httpClientFactory.CreateClient();
                                    //client.BaseAddress = new Uri("https://localhost:44343/");
                                    client.BaseAddress = new Uri(ConnectedAPIBox);


                                    var content = new MultipartFormDataContent();

                                    content.Add(new StreamContent(file.OpenReadStream())
                                    {
                                        Headers =
                    {
                        ContentLength = file.Length,
                        ContentType = new MediaTypeHeaderValue(file.ContentType)
                    }
                                    }, "files", filename);

                                    var response = await client.PostAsync(url, content);
                                    var body = await response.Content.ReadAsStringAsync();
                                    if (response.IsSuccessStatusCode == true)
                                    {
                                        //if (response.Content.Count() > 3)
                                        //{
                                        List<BoxAI> user = (List<BoxAI>)JsonConvert.DeserializeObject(body,
                     typeof(List<BoxAI>));

                                        //idFaceC = user[0].id;

                                       







                                    }
                                    else
                                    {
                                        return new OkObjectResult(new GenericResult(false, response.RequestMessage));
                                    }

                                }
                                else
                                {
                                    var data = await _faceRepositories.SaveFaceImg(idFace, filePath, duoifile);
                                    if (data.Success == true)
                                    {

                                        using (FileStream fs1 = System.IO.File.Create(data.FileSave))
                                        {
                                            file.CopyTo(fs1);
                                            fs1.Flush();
                                        }

                                    }
                                }
                                i++;
                            }
                            return new OkObjectResult(new GenericResult(true, null));
                        }
                        else
                        {
                            var data1 = await _faceRepositories.CheckStaffCode(staffCode);
                            if (data1.Success == true)
                            {
                                foreach (var formFile in files)
                                {
                                    var file = files[i];
                                    var filename = ContentDispositionHeaderValue
                                                       .Parse(file.ContentDisposition)
                                                       .FileName
                                                       .Trim('"');
                                    var duoifile = filename.Substring(filename.Length - 4);
                                    //var filePath1 = string.Format(@"C:\upload\images\event\" + $"{DateTime.Now:yyyyMMddhhmmss}" + "_face" + duoifile);
                                    var filePath = string.Format(@"C:\upload\images\root\");
                                    //file.SaveAs(filePath);
                                    
                                    if (i == 0)
                                    {
                                        var data = await _faceRepositories.SaveFace(idFaceC, staffCode, faceName, agent, job, department, sex, known, filePath, duoifile);
                                        if (data.Success == true)
                                        {
                                            idFace = data.Id;
                                            using (FileStream fs1 = System.IO.File.Create(data.FileSave))
                                            {
                                                file.CopyTo(fs1);
                                                fs1.Flush();
                                            }

                                        }

                                        var url = $"api/add_images_database?user_name={staffCode}";
                                        var client = _httpClientFactory.CreateClient();
                                        //client.BaseAddress = new Uri("https://localhost:44343/");
                                        client.BaseAddress = new Uri(ConnectedAPIBox);


                                        var content = new MultipartFormDataContent();

                                        content.Add(new StreamContent(file.OpenReadStream())
                                        {
                                            Headers =
                    {
                        ContentLength = file.Length,
                        ContentType = new MediaTypeHeaderValue(file.ContentType)
                    }
                                        }, "files", filename);

                                        var response = await client.PostAsync(url, content);
                                        var body = await response.Content.ReadAsStringAsync();
                                        if (response.IsSuccessStatusCode == true)
                                        {
                                            //if (response.Content.Count() > 3)
                                            //{
                                            

                                            //idFaceC = user[0].id;

                                            







                                        }
                                        else
                                        {
                                            return new OkObjectResult(new GenericResult(false, response.RequestMessage));
                                        }

                                    }
                                    else
                                    {
                                        var data = await _faceRepositories.SaveFaceImg(idFace, filePath, duoifile);
                                        if (data.Success == true)
                                        {

                                            using (FileStream fs1 = System.IO.File.Create(data.FileSave))
                                            {
                                                file.CopyTo(fs1);
                                                fs1.Flush();
                                            }

                                        }
                                        var url = $"api/add_images_database?user_name={staffCode}";
                                        var client = _httpClientFactory.CreateClient();
                                        //client.BaseAddress = new Uri("https://localhost:44343/");
                                        client.BaseAddress = new Uri(ConnectedAPIBox);


                                        var content = new MultipartFormDataContent();

                                        content.Add(new StreamContent(file.OpenReadStream())
                                        {
                                            Headers =
                    {
                        ContentLength = file.Length,
                        ContentType = new MediaTypeHeaderValue(file.ContentType)
                    }
                                        }, "files", filename);

                                        var response = await client.PostAsync(url, content);
                                        var body = await response.Content.ReadAsStringAsync();
                                        if (response.IsSuccessStatusCode == true)
                                        {
                                            

                                        }
                                        else
                                        {
                                            return new OkObjectResult(new GenericResult(false, response.RequestMessage));
                                        }
                                        
                                    }
                                    i++;
                                }
                                return new OkObjectResult(new GenericResult(true, null));
                            }
                            else
                            {
                                return new OkObjectResult(new GenericResult(data1.Success, data1.Message));
                            }
                        }

                    }
                    else
                    {
                        if (loifile > 0)
                        {
                            return new OkObjectResult(new GenericResult(false, "File ảnh k đúng định dạng"));
                        }
                        else
                        {
                            return new OkObjectResult(new GenericResult(false, "Bạn đã uplooad file ảnh trên 20 Mb"));
                        }

                    }

                }
                else
                {
                    //var data = await _eventRepositories.GetStatisticalEvent(0, eventID, fromDate, toDate, search, page, pageSize);
                    return new OkObjectResult(new GenericResult(false, "Không có ảnh để thêm mới"));
                }
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }

        }
        [HttpPost]
        public async Task<IActionResult> SaveDepartment(string departmentCode,string departmentName)
        {
            var data = await _departmentRepositories.SaveDepartment(departmentCode, departmentName);
            return new OkObjectResult(new GenericResult(data.Success, data.Message));

        }

        [HttpGet]
        public async Task<IActionResult> GetFaceDetailById(int id)
        {
            var data = await _faceRepositories.GetFaceDetailById(id);
            return new OkObjectResult(new GenericResult(data.Success, data.Message, data));

        }
        [HttpGet]
        public async Task<IActionResult> DeleteFace(string IdFace,string IdCloud)
        {
            if(IdCloud==null|| IdCloud == "")
            {
                var data = await _faceRepositories.DeleteFace(IdFace, IdCloud);
                return new OkObjectResult(new GenericResult(data.Success, data.Message));
            }
            else
            {
                var IdCloudcheck = IdCloud.Split(',');
                for (int i = 1; i < IdCloudcheck.Length-1; i++)
                {
                    var url = $"api/delete_user_name?user_name={IdCloudcheck[i]}";
                    var client = _httpClientFactory.CreateClient();
                    //client.BaseAddress = new Uri("https://localhost:44343/");
                    client.BaseAddress = new Uri(ConnectedAPIBox);
                    var json = "";
                    var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(url, httpContent);
                    var body = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode == true)
                    {
                       







                    }
                    else
                    {
                        //return new OkObjectResult(new GenericResult(false, response.RequestMessage));
                    }
                }
                var data = await _faceRepositories.DeleteFace(IdFace, IdCloud);
                return new OkObjectResult(new GenericResult(data.Success, data.Message));
            }

        }
        [HttpPost]
        public async Task<IActionResult> UpdateFace(int idFace,string staffCode, string faceName, string agent, string job, int department, string sex, int importantLevel)
        {
            var data = await _faceRepositories.UpdateFace(idFace,  staffCode,  faceName,  agent,  job,  department,  sex,  importantLevel);
            return new OkObjectResult(new GenericResult(data.Success, data.Message));

        }
        [HttpPost]
        public async Task<IActionResult> UpdateTKSetting(int id,int deviceId, bool isCheckIn, bool isCheckOut, bool isTKOn)
        {
            var data = await _faceRepositories.UpdateTKSetting(id, deviceId, isCheckIn, isCheckOut, isTKOn);
            return new OkObjectResult(new GenericResult(data.Success, data.Message));

        }
        [HttpGet]
        public async Task<IActionResult> CannelTKSetting(int id)
        {
            var data = await _faceRepositories.CannelTKSetting(id);
            return new OkObjectResult(new GenericResult(data.Success, data.Message));

        }

    }
}
