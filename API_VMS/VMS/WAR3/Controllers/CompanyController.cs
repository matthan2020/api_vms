﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using WAR3.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class CompanyController : Controller
    {
        private ICompanyRepositories _companyRepositories;
        private readonly IHostingEnvironment _hostingEnvironment;

        public CompanyController(ICompanyRepositories companyRepositories,  IHostingEnvironment hostingEnvironment)
        {
            _companyRepositories = companyRepositories;
            _hostingEnvironment = hostingEnvironment; ;
        }
        //public IActionResult Index()
        //{
        //    //ViewData["NAME"] = Request.Cookies["NAME"];
        //    return View();
        //}

        [HttpGet]
        public async Task<IActionResult> GetCompany()
        {
            var model = await _companyRepositories.GetCompany();
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetCompanyModelById(int id)
        {
            var model = await _companyRepositories.GetCompanyModelById(id);
            return new OkObjectResult(new GenericResult(model.Success, model.Message, model)); //new OkObjectResult(model);
        }

    }
}
