﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using WAR3.Dtos;
using WAR3_DAL.Dtos;
using WAR3_DAL.Implementation.IRepositories;
using WAR3_DAL.ViewModel;

namespace WAR3.Controllers
{
    public class StatisticalController : Controller
    {
        private IStatisticalRepositories _statisticalRepositories;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IHttpClientFactory _httpClientFactory;
        public StatisticalController(IStatisticalRepositories statisticalRepositories, IHostingEnvironment hostingEnvironment, IHttpClientFactory httpClientFactory)
        {
            _statisticalRepositories = statisticalRepositories;
            _hostingEnvironment = hostingEnvironment;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            if (Request.Cookies["LOAD"] != null)
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return new RedirectResult("../Event/Index");
                return View();
            }
            else
            {
               // ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return View();
                return new RedirectResult("~/Login/Login");
            }
        }

        public IActionResult InOut()
        {
            if (Request.Cookies["LOAD"] != null)
            {
               // ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return new RedirectResult("../Event/Index");
                return View();
            }
            else
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return View();
                return new RedirectResult("~/Login/Login");
            }
        }
        public IActionResult StatisticalFaceShift()
        {
            if (Request.Cookies["LOAD"] != null)
            {
                //ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return new RedirectResult("../Event/Index");
                return View();
            }
            else
            {
                // ViewData["LOAD"] = Request.Cookies["LOAD"];
                //return View();
                return new RedirectResult("~/Login/Login");
            }
        }
       
        [HttpGet]
        public async Task<IActionResult> GetStatistical(string staffCode,string departmentId, int year, int month, int page, int pageSize)
        {
            var data = await _statisticalRepositories.GetStatistical(staffCode, departmentId, month, year, page, pageSize);
            return new OkObjectResult(new GenericResult(data.Success, data.Message, data));
        }
        [HttpGet]
        public async Task<IActionResult> ExportExcel(string staffCode, string departmentId, string departmentName, int year, int month)
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string directory = Path.Combine(sWebRootFolder, "export-files", "CHAMCONG");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            string sFileName = $"ChamCong_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
            string fileUrl = $"{Request.Scheme}://{Request.Host}/vms/export-files/CHAMCONG/{sFileName}";
            string templateDocument = Path.Combine(sWebRootFolder, "Template", "TemplateExport", "ExportChamCong.xlsx");
            FileInfo fileName = new FileInfo(Path.Combine(directory, sFileName));
            if (fileName.Exists)
            {
                fileName.Delete();

                fileName = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

            }

            var model = await _statisticalRepositories.ExportExcel(staffCode, departmentId, month, year);

            if (model.Success == true && model.Message == "SUCCESS")
            {
                using (FileStream templateDocumentStream = System.IO.File.OpenRead(templateDocument))
                {
                    using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                    {
                        // add a new worksheet to the empty workbook
                        // ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(fileNameTemp);

                        ExcelWorksheet workSheet = package.Workbook.Worksheets["CHAM_CONG"];
                        int i = 8;
                        if (departmentId == null || departmentId == "")
                        {
                            workSheet.Cells[4, 11].Value = "Phòng ban: Tất cả";
                        }
                        else
                        {
                            workSheet.Cells[4, 11].Value = "Phòng ban: " + departmentName;
                        }
                       
                        workSheet.Cells[5, 11].Value = "Tháng: " + month+"/"+year;
                        foreach (var item in model.Items)
                        {
                            workSheet.Cells[i, 1].Value = i - 7;
                            workSheet.Cells[i, 2].Value = item.StaffCode;
                            workSheet.Cells[i, 3].Value = item.StaffName;
                            workSheet.Cells[i, 4].Value = item.DepartmentName;
                            workSheet.Cells[i, 5].Value = item.Day1;
                            workSheet.Cells[i, 6].Value = item.Day2;
                            workSheet.Cells[i, 7].Value = item.Day3;
                            workSheet.Cells[i, 8].Value = item.Day4;
                            workSheet.Cells[i, 9].Value = item.Day5;
                            workSheet.Cells[i, 10].Value = item.Day6;
                            workSheet.Cells[i, 11].Value = item.Day7;
                            workSheet.Cells[i, 12].Value = item.Day8;
                            workSheet.Cells[i, 13].Value = item.Day9;
                            workSheet.Cells[i, 14].Value = item.Day10;
                            workSheet.Cells[i, 15].Value = item.Day11;
                            workSheet.Cells[i, 16].Value = item.Day12;
                            workSheet.Cells[i, 17].Value = item.Day13;
                            workSheet.Cells[i, 18].Value = item.Day14;
                            workSheet.Cells[i, 19].Value = item.Day15;
                            workSheet.Cells[i, 20].Value = item.Day16;
                            workSheet.Cells[i, 21].Value = item.Day17;
                            workSheet.Cells[i, 22].Value = item.Day18;
                            workSheet.Cells[i, 23].Value = item.Day19;
                            workSheet.Cells[i, 24].Value = item.Day20;
                            workSheet.Cells[i, 25].Value = item.Day21;
                            workSheet.Cells[i, 26].Value = item.Day22;
                            workSheet.Cells[i, 27].Value = item.Day23;
                            workSheet.Cells[i, 28].Value = item.Day24;
                            workSheet.Cells[i, 29].Value = item.Day25;
                            workSheet.Cells[i, 30].Value = item.Day26;
                            workSheet.Cells[i, 31].Value = item.Day27;
                            workSheet.Cells[i, 32].Value = item.Day28;
                            workSheet.Cells[i, 33].Value = item.Day29;
                            workSheet.Cells[i, 34].Value = item.Day30;
                            workSheet.Cells[i, 35].Value = item.Day31;
                            workSheet.Cells[i, 36].Value = item.Day1 + item.Day2 + item.Day3 + item.Day4 + item.Day5 + item.Day6 + item.Day7 + item.Day8 + item.Day9 + item.Day10 + item.Day11 + item.Day12 + item.Day13 + item.Day14 + item.Day15 + item.Day16 + item.Day17 + item.Day18 + item.Day19 + item.Day20 + item.Day21 + item.Day22 + item.Day23 + item.Day24 + item.Day25 + item.Day26 + item.Day27 + item.Day28 + item.Day29 + item.Day30 + item.Day31;
                            i = i + 1;
                        }
                        package.SaveAs(fileName);
                        //fileName.CopyTo(package);

                    }
                }
                return new OkObjectResult(new GenericResult(model.Success, model.Message, fileUrl));


            }
            else
            {
                return new OkObjectResult(new GenericResult(false, model.Message));
            }



        }
        [HttpGet]
        public async Task<IActionResult> UpdateData( int year, int month)
        {
            bool check = false;
            var data = await _statisticalRepositories.UpdateData( month, year);
            if (data.Status == "OK")
            {
                check = true;
            }
            return new OkObjectResult(new GenericResult(check, data.Message));
        }

        [HttpGet]
        public async Task<IActionResult> GetInOut(string staffCode, string departmentId, string fromDate, string toDate, int page, int pageSize)
        {
            var data = await _statisticalRepositories.GetInOutPage(staffCode, departmentId, fromDate, toDate, page, pageSize);
            return new OkObjectResult(new GenericResult(data.Success, data.Message, data));
        }

        [HttpGet]
        public async Task<IActionResult> ExportExcelInOut(string staffCode, string departmentId, string departmentName, string fromDate, string toDate)
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string directory = Path.Combine(sWebRootFolder, "export-files", "INOUT");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            string sFileName = $"InOut_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
            string fileUrl = $"{Request.Scheme}://{Request.Host}/vms/export-files/INOUT/{sFileName}";
            string templateDocument = Path.Combine(sWebRootFolder, "Template", "TemplateExport", "ExportInOut.xlsx");
            FileInfo fileName = new FileInfo(Path.Combine(directory, sFileName));
            if (fileName.Exists)
            {
                fileName.Delete();

                fileName = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

            }

            var model = await _statisticalRepositories.ExportExcelInOut(staffCode, departmentId, fromDate, toDate);

            if (model.Success == true && model.Message == "SUCCESS")
            {
                using (FileStream templateDocumentStream = System.IO.File.OpenRead(templateDocument))
                {
                    using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                    {
                        // add a new worksheet to the empty workbook
                        // ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(fileNameTemp);

                        ExcelWorksheet workSheet = package.Workbook.Worksheets["IN_OUT"];
                        int i = 8;
                        if (departmentId == null || departmentId == "")
                        {
                            workSheet.Cells[4, 5].Value = "Phòng ban: Tất cả";
                        }
                        else
                        {
                            workSheet.Cells[4, 5].Value = "Phòng ban: " + departmentName;
                        }

                        workSheet.Cells[5, 5].Value = "Từ ngày: " + fromDate + " đến ngày: " + toDate;
                        foreach (var item in model.Items)
                        {
                            workSheet.Cells[i, 1].Value = i - 7;
                            workSheet.Cells[i, 2].Value = item.RecordTime;
                            workSheet.Cells[i, 3].Value = item.StaffCode;
                            workSheet.Cells[i, 4].Value = item.FaceName;
                            workSheet.Cells[i, 5].Value = item.DepartmentName;
                            workSheet.Cells[i, 6].Value = item.RecordTimeIn;
                            workSheet.Cells[i, 7].Value = item.DeviceNameIn;
                            workSheet.Cells[i, 8].Value = item.RecordTimeOut;
                            workSheet.Cells[i, 9].Value = item.DeviceNameOut;
                            workSheet.Cells[i, 10].Value = item.Tong;
                            
                            i = i + 1;
                        }
                        package.SaveAs(fileName);
                        //fileName.CopyTo(package);

                    }
                }
                return new OkObjectResult(new GenericResult(model.Success, model.Message, fileUrl));


            }
            else
            {
                return new OkObjectResult(new GenericResult(false, model.Message));
            }



        }
        [HttpGet]
        public async Task<IActionResult> GetStatisticalFaceShift(string staffCode, string departmentId,string shiftName, int year, int month, int page, int pageSize)
        {
            var data = await _statisticalRepositories.GetStatisticalFaceShift(staffCode, departmentId, shiftName, month, year, page, pageSize);
            return new OkObjectResult(new GenericResult(data.Success, data.Message, data));
        }
        [HttpGet]
        public async Task<IActionResult> UpdateDataFaceShift(int year, int month)
        {
            bool check = false;
            var data = await _statisticalRepositories.UpdateDataFaceShift(month, year);
            if (data.Status == "OK")
            {
                check = true;
            }
            return new OkObjectResult(new GenericResult(check, data.Message));
        }
        [HttpGet]
        public async Task<IActionResult> ExportExcelFaceShift(string staffCode, string departmentId, string departmentName,string shiftName, int year, int month)
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string directory = Path.Combine(sWebRootFolder, "export-files", "CHAMCONG");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            string sFileName = $"ChamCongCa_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
            string fileUrl = $"{Request.Scheme}://{Request.Host}/vms/export-files/CHAMCONG/{sFileName}";
            string templateDocument = Path.Combine(sWebRootFolder, "Template", "TemplateExport", "ExportChamCongCa.xlsx");
            FileInfo fileName = new FileInfo(Path.Combine(directory, sFileName));
            if (fileName.Exists)
            {
                fileName.Delete();

                fileName = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

            }

            var model = await _statisticalRepositories.ExportExcelFaceShift(staffCode, departmentId, shiftName, month, year);

            if (model.Success == true && model.Message == "SUCCESS")
            {
                using (FileStream templateDocumentStream = System.IO.File.OpenRead(templateDocument))
                {
                    using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                    {
                        // add a new worksheet to the empty workbook
                        // ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(fileNameTemp);

                        ExcelWorksheet workSheet = package.Workbook.Worksheets["CHAM_CONG"];
                        int i = 10;
                        if (departmentId == null || departmentId == "")
                        {
                            workSheet.Cells[4, 14].Value = "Phòng ban: Tất cả";
                        }
                        else
                        {
                            workSheet.Cells[4, 14].Value = "Phòng ban: " + departmentName;
                        }
                        if (shiftName == null || shiftName == "")
                        {
                            workSheet.Cells[5, 14].Value = "Ca thiết lập: Tất cả";
                        }
                        else
                        {
                            workSheet.Cells[5, 14].Value = "Ca thiết lập: " + shiftName;
                        }
                        workSheet.Cells[6, 14].Value = "Tháng: " + month + "/" + year;
                        foreach (var item in model.Items)
                        {
                            workSheet.Cells[i, 1].Value = i - 9;
                            workSheet.Cells[i, 2].Value = item.StaffCode;
                            workSheet.Cells[i, 3].Value = item.StaffName;
                            workSheet.Cells[i, 4].Value = item.DepartmentName;
                            workSheet.Cells[i, 5].Value = item.StaffName;
                            workSheet.Cells[i, 6].Value = item.Day1;
                            workSheet.Cells[i, 7].Value = item.Day2;
                            workSheet.Cells[i, 8].Value = item.Day3;
                            workSheet.Cells[i, 9].Value = item.Day4;
                            workSheet.Cells[i, 10].Value = item.Day5;
                            workSheet.Cells[i, 11].Value = item.Day6;
                            workSheet.Cells[i, 12].Value = item.Day7;
                            workSheet.Cells[i, 13].Value = item.Day8;
                            workSheet.Cells[i, 14].Value = item.Day9;
                            workSheet.Cells[i, 15].Value = item.Day10;
                            workSheet.Cells[i, 16].Value = item.Day11;
                            workSheet.Cells[i, 17].Value = item.Day12;
                            workSheet.Cells[i, 18].Value = item.Day13;
                            workSheet.Cells[i, 19].Value = item.Day14;
                            workSheet.Cells[i, 20].Value = item.Day15;
                            workSheet.Cells[i, 21].Value = item.Day16;
                            workSheet.Cells[i, 22].Value = item.Day17;
                            workSheet.Cells[i, 23].Value = item.Day18;
                            workSheet.Cells[i, 24].Value = item.Day19;
                            workSheet.Cells[i, 25].Value = item.Day20;
                            workSheet.Cells[i, 26].Value = item.Day21;
                            workSheet.Cells[i, 27].Value = item.Day22;
                            workSheet.Cells[i, 28].Value = item.Day23;
                            workSheet.Cells[i, 29].Value = item.Day24;
                            workSheet.Cells[i, 30].Value = item.Day25;
                            workSheet.Cells[i, 31].Value = item.Day26;
                            workSheet.Cells[i, 32].Value = item.Day27;
                            workSheet.Cells[i, 33].Value = item.Day28;
                            workSheet.Cells[i, 34].Value = item.Day29;
                            workSheet.Cells[i, 35].Value = item.Day30;
                            workSheet.Cells[i, 36].Value = item.Day31;
                            workSheet.Cells[i, 37].Value = item.Day1 + item.Day2 + item.Day3 + item.Day4 + item.Day5 + item.Day6 + item.Day7 + item.Day8 + item.Day9 + item.Day10 + item.Day11 + item.Day12 + item.Day13 + item.Day14 + item.Day15 + item.Day16 + item.Day17 + item.Day18 + item.Day19 + item.Day20 + item.Day21 + item.Day22 + item.Day23 + item.Day24 + item.Day25 + item.Day26 + item.Day27 + item.Day28 + item.Day29 + item.Day30 + item.Day31;
                            workSheet.Cells[i, 40].Value = item.Late;
                            workSheet.Cells[i, 41].Value = item.Soon;
                            i = i + 1;
                        }
                        package.SaveAs(fileName);
                        //fileName.CopyTo(package);

                    }
                }
                return new OkObjectResult(new GenericResult(model.Success, model.Message, fileUrl));


            }
            else
            {
                return new OkObjectResult(new GenericResult(false, model.Message));
            }



        }
    }
}
