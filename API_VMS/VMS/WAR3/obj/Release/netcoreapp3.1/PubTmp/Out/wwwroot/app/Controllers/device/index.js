﻿var deviceController = function () {
    var checkxoa = '';
    this.initialize = function () {

      
        
        loadData(true);
        registerEvents();
        registerControls();
      

        
    }

    var registerEvents = function () {
        //$('#frmDevice').validate({
        //    errorClass: 'red',
        //    ignore: [],
        //    lang: 'vi',
        //    rules: {
        //        ddlCampany: {
        //            required: true
        //        },
               
        //    }
        //});
       
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            loadData(true);
        });
       
        $('#txtKeyword').on('keypress', function (e) {
            if (e.which === 13) {
                loadData(true);
            }
        });
       
        $("#btnCreate").on('click', function (e) {
            $('#frmDevice').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtUserName: {
                        required: true,
                        rangelength: [1,20]
                    }, 
                    ddlDeviceType: {
                        required: true
                    },
                    txtDeviceName: {
                        required: true,
                        minlength:4
                    },
                    txtIpAddress: {
                        required: true,
                       
                    },
                    txtPortNo: {
                        required: true,
                        maxlength:5
                    },
                    txtPass: {
                        required: true,
                        rangelength: [1, 16]
                    },
                }
            });
            resetFormMaintainance();
            $('#txtLink').val('rtsp://')
            //initTreeDropDownCategory();
            $('#modal-device-add-edit').modal('show');
          

        });
        
        $('#btnSave').on('click', function () {
            var check = $('#hidIdM').val();
           
            if (check == '0') {
                SaveDevice();
            }
            else {
                UpdateDevice();
            }

        });

        $('body').on('click', '.fa-pencil-square-o', function (e) {
            $('#hidIdM').val(1);
            $('#frmDevice').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtUserName: {
                        required: true,
                        rangelength: [1, 20]
                    },
                    ddlDeviceType: {
                        required: true
                    },
                    txtDeviceName: {
                        required: true,
                        minlength: 4
                    },
                    txtIpAddress: {
                        required: true,

                    },
                    txtPortNo: {
                        required: true,
                        maxlength: 5
                    },
                    txtPass: {
                        required: true,
                        rangelength: [1, 16]
                    },
                }
            });
            //$('#txtCode').prop('disabled', true);
            var that = $(this).data('id');
            e.preventDefault();
            GetDeviceById(that);
        });
        $('#ddlDeviceType').on('change', function () {
            var check = $('#ddlDeviceType').val();
            if (check == '1') {
                //dropdown.GetCompany($('#ddlCompany'), '');
                $.when(dropdown.GetCompany($('#ddlCompany'), '')).done(function (x) {
                    dropdown.GetCompanyModelById($('#ddlModel'), $('#ddlCompany').val(), '');
        });
                $('#txtAmountCam').val(1);
                $('#ddlCompany').prop('disabled', false);
                $('#ddlModel').prop('disabled', false);
                $('#txtAmountCam').prop('disabled', true);
            }
            else {
                if (check == '2') {
                    //dropdown.GetCompany($('#ddlCompany'), '');
                    $('#txtAmountCam').val(1);
                    $('#ddlCompany').val('');
                    $('#ddlModel').val('');
                    $('#ddlCompany').prop('disabled', true);
                    $('#ddlModel').prop('disabled', true);
                    $('#txtAmountCam').prop('disabled', true);
                }
                else {
                    $('#txtAmountCam').val(1);
                    $('#ddlCompany').val('');
                    $('#ddlModel').val('');
                    $('#ddlCompany').prop('disabled', true);
                    $('#ddlModel').prop('disabled', true);
                    $('#txtAmountCam').prop('disabled', false);
                }
            }

        });

        $('#ddlCompany').on('change', function () {
            var check = $('#ddlDeviceType').val();
            dropdown.GetCompanyModelById($('#ddlModel'), check, '');
               

        });
        $('body').on('click', '.fa-trash', function (e) {
            var that = $(this).data('id');
                common.confirm('Bạn có chắc chắn muốn xóa dữ liệu không?', function () {
                    DeleteDevice(that);
                });

        });
        $('body').on('click', '.linkTH', function (e) {
            var that = $(this).data('id');
            var idStatus = $(this).data('idstatus');
            common.confirm('Bạn có chắc chắn muốn thay đổi trạng thái không?', function () {
                UpdateDeviceStatus(that, idStatus);
            });

        });
        $('.ckcheck').click(function () {
            if ($(this).is(':checked')) {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = true;
                    var chuoi = "," + $(this).data('id') + ","
                    if (checkxoa.includes(chuoi) == true) {

                    }
                    else {
                        checkxoa += "," + $(this).data('id') + ",";
                    }
                   // checkxoa += "," + $(this).data('id') + ",";
                });
               
            } else {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = false;
                    checkxoa = '';
                });
            }
            //var res = str.replace(/blue/g, "red");
            checkxoa = checkxoa.replace(/,,/g, ",");
            console.log(checkxoa);
        });

        $('body').on('change', '.ckcheck1', function (e) {
            e.preventDefault();
            if ($(this).is(':checked')) {
                var chuoi = "," + $(this).data('id') + ",";
                if (checkxoa.includes(chuoi)==true) {

                }
                else {
                    checkxoa += "," + $(this).data('id') + ",";
                }
               
            }
            else {
                var chuoixoa = "," + $(this).data('id') + ",";
                checkxoa = checkxoa.replace(chuoixoa, ",");
            }
            //checkxoa = checkxoa.replace(",,", ",");
            checkxoa = checkxoa.replace(/,,/g, ",");
            if (checkxoa == ",") {
                checkxoa = "";
            }
            console.log(checkxoa);
        });
        $('#txtIpAddress').on('change', function () {
            $('#txtLink').val('rtsp://' + $('#txtUserName').val() + ':' + $('#txtPass').val() + '@' + $('#txtIpAddress').val() + ':554/Streaming/Channels/101');
        });
        $('#txtUserName').on('change', function () {
            $('#txtLink').val('rtsp://' + $('#txtUserName').val() + ':' + $('#txtPass').val() + '@' + $('#txtIpAddress').val() +':554/Streaming/Channels/101');
        });
        $('#txtPass').on('change', function () {
            $('#txtLink').val('rtsp://' + $('#txtUserName').val() + ':' + $('#txtPass').val() + '@' + $('#txtIpAddress').val() + ':554/Streaming/Channels/101');
        });
        //$('#btnImport').on('click', function () {
        //    $('#modal-import-product-excel').modal('show');
        //});

        $('#btnExport').on('click', function () {
            ExportExcel();
          
        });

        
    }
    var registerControls = function () {
        dropdown.GetDeviceType($('#ddlDeviceType'), '');
       
    }
    function loadData(isPageChanged) {
        var template = $('#table-template-device').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {
                keyword: $('#txtKeyword').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../Device/GetAllDevice',
            dataType: 'json',
            success: function (response) {
                console.log(response);
                if (response.Data.TotalRow > 0) {
                    $.each(response.Data.Items, function (i, item) {
                        render += Mustache.render(template, {
                            STT: (common.configs.pageIndex * common.configs.pageSize - common.configs.pageSize + 1) + i,
                            DeviceName: item.DeviceName,
                            IpAddress: item.IpAddress,
                            DeviceTypeName: item.DeviceTypeName,
                            DeviceCode: item.DeviceCode,
                            SecurityName: item.SecurityName,
                            PortNo: item.PortNo,
                            StatusName: item.StatusName,
                            DeviceId: item.DeviceId,
                            IdStatus: item.IdStatus
                        });
                      
                       
                    });
                    if (render != '') {
                        $('#tbl-device').html(render);
                    }
                    common.wrapPaging1('.paginationUL', response.Data.TotalRow, function () {
                        //alert(1);
                        loadData(false);
                    }, isPageChanged);
                }
                else {
                    render ='<tr>' +
                        '    <td colspan="9">Không có dữ liệu</td>' +
                        '</tr>';
                    $('#tbl-device').html(render);
                }
                $('#lblTotalRecords').text(response.Data.TotalRow);
            },
            error: function () {
                common.notify('Can not load ajax function GetAllDevice', 'error');
            }
        });
    }

    function resetFormMaintainance() {
        $('#hidIdM').val(0);
        //$('#ddlProductType').val('');
       // initTreeDropDownCategory('');

        $('#ddlDeviceType').val('');
        $('#txtDeviceName').val('');
        
        $('#txtIpAddress').val('');
        $('#txtPortNo').val('');
        $('#txtUserName').val('');

        $('#txtPass').val('');
        //$('#ddlCompany').val('');
        $('#ddlModel').val('');

        //$('#txtReceiver').val('');

        //$('#txtCreateDate').val('');

        $('#txtAmountCam').val('');
        $('#txtLink').val('');

    }

    function SaveDevice() {
         
        if ($('#frmDevice').valid()) {
            if ($('#txtIpAddress').val().length != $('#txtIpAddress').val().replace(" ", "").length) {
                common.popupOk('Cảnh báo', 'Địa chỉ IP không được chứa khoảng trắng.Mời bạn nhập lại', 'error');
                return;
            }
            if ($('#txtUserName').val().length != $('#txtUserName').val().replace(" ", "").length) {
                common.popupOk('Cảnh báo', 'Tên dăng nhập không được chứa khoảng trắng.Mời bạn nhập lại', 'error');
                return;
            }
            if ($('#txtPass').val().length != $('#txtPass').val().replace(" ", "").length) {
                common.popupOk('Cảnh báo', 'Mật khẩu không được chứa khoảng trắng.Mời bạn nhập lại', 'error');
                return;
            }
            var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
            if ($('#txtIpAddress').val().match(ipformat)) {
               
            }
            else {
                common.popupOk('Cảnh báo', 'Địa chỉ IP không đúng định dạng', 'error');
                return;
            }
       // if ($('#frmDevice').valid()) {
                $.ajax({
                    type: "POST",
                    url: "../Device/SaveDevice",
                    data: {
                        deviceType: $('#ddlDeviceType').val(),
                        deviceName: $('#txtDeviceName').val(),
                        ipAddress: $('#txtIpAddress').val(),
                        portNo: $('#txtPortNo').val(),
                        userName: $('#txtUserName').val(),
                        pass: $('#txtPass').val(),
                        link: $('#txtLink').val(),
                        model: $('#ddlModel').val(), 
                        campany: $('#ddlCompany').val(),
                        amountCam: $('#txtAmountCam').val(),
                    },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        console.log(response);
                        if (response.Success == true) {
                            common.notify('Thêm mới thành công', 'success');

                            $('#modal-device-add-edit').modal('hide');
                            resetFormMaintainance();

                            common.stopLoading();
                            loadData(true);
                        }
                        else {
                            
                                common.notify(response.Message, 'error');
                        }
                        common.stopLoading();
                    },
                    error: function () {
                        common.notify('Can not load ajax function SaveProduct', 'error');
                        common.stopLoading();
                    }
                });
           
        }
    }

    function GetDeviceById(Id) {
                $.ajax({
                    type: "GET",
                    url: "../Device/GetDevicetById",
                    data: { id: Id },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        if (response.Success == true) {
                            var data = response.Data.Items[0];
                            console.log(data);
                            if (data.IdDeviceType == '1') {
                                //dropdown.GetCompany($('#ddlCompany'), '');
                                $.when(dropdown.GetCompany($('#ddlCompany'), data.idCompany)).done(function (x) {
                                    dropdown.GetCompanyModelById($('#ddlModel'), data.idCompany, data.idModel);
                                });
                                //$('#txtAmountCam').val(1);
                                $('#ddlCompany').prop('disabled', false);
                                $('#ddlModel').prop('disabled', false);
                                $('#txtAmountCam').prop('disabled', true);
                            }
                            else {
                                if (data.IdDeviceType == '2') {
                                    //dropdown.GetCompany($('#ddlCompany'), '');
                                    //$('#txtAmountCam').val(1);
                                    $('#ddlCompany').val('');
                                    $('#ddlModel').val('');
                                    $('#ddlCompany').prop('disabled', true);
                                    $('#ddlModel').prop('disabled', true);
                                    $('#txtAmountCam').prop('disabled', true);
                                }
                                else {
                                    //$('#txtAmountCam').val(1);
                                    $('#ddlCompany').val('');
                                    $('#ddlModel').val('');
                                    $('#ddlCompany').prop('disabled', true);
                                    $('#ddlModel').prop('disabled', true);
                                    $('#txtAmountCam').prop('disabled', false);
                                }
                            }
                            $('#hidIdDevice').val(data.DeviceId);
                            $('#ddlDeviceType').val(data.IdDeviceType);
                            $('#txtDeviceName').val(data.DeviceName);
                            $('#txtIpAddress').val(data.IpAddress);
                            $('#txtPortNo').val(data.PortNo);
                            $('#txtUserName').val(data.UserName);
                            $('#txtPass').val(data.Pass); 
                            $('#txtLink').val(data.LinkRTSP),
                           // $('#ddlModel').val(data.idModel);
                           // $('#ddlCompany').val(data.idCompany);
                            $('#txtAmountCam').val(data.AmountCam);
                           
                            $('#modal-device-add-edit').modal('show');
                        }
                        else {
                            common.notify('Có lỗi xảy ra: ' + response.Message, 'error');
                            
                        }
                       
                       
                        common.stopLoading();

                    },
                    error: function () {
                        common.notify('Can not load ajax function GetContactById', 'error');
                        common.stopLoading();
                    }
                });
        //}
    }

    function UpdateDevice() {
        if ($('#frmDevice').valid()) {
            if ($('#txtIpAddress').val().length != $('#txtIpAddress').val().replace(" ", "").length) {
                common.popupOk('Cảnh báo', 'Địa chỉ IP không được chứa khoảng trắng.Mời bạn nhập lại', 'error');
                return;
            }
            if ($('#txtUserName').val().length != $('#txtUserName').val().replace(" ", "").length) {
                common.popupOk('Cảnh báo', 'Tên dăng nhập không được chứa khoảng trắng.Mời bạn nhập lại', 'error');
                return;
            }
            if ($('#txtPass').val().length != $('#txtPass').val().replace(" ", "").length) {
                common.popupOk('Cảnh báo', 'Mật khẩu không được chứa khoảng trắng.Mời bạn nhập lại', 'error');
                return;
            }
            // if ($('#frmDevice').valid()) {
            $.ajax({
                type: "POST",
                url: "../Device/UpdateDevice",
                data: {
                    id: $('#hidIdDevice').val(),
                    deviceType: $('#ddlDeviceType').val(),
                    deviceName: $('#txtDeviceName').val(),
                    ipAddress: $('#txtIpAddress').val(),
                    portNo: $('#txtPortNo').val(),
                    userName: $('#txtUserName').val(),
                    pass: $('#txtPass').val(),
                    link: $('#txtLink').val(),
                    model: $('#ddlModel').val(),
                    campany: $('#ddlCompany').val(),
                    amountCam: $('#txtAmountCam').val(),
                },
                dataType: "json",
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    console.log(response);
                    if (response.Success == true) {
                        common.notify('Sửa thành công', 'success');

                        $('#modal-device-add-edit').modal('hide');
                        resetFormMaintainance();

                        common.stopLoading();
                        loadData(true);
                    }
                    else {

                        common.notify(response.Message, 'error');
                    }
                    common.stopLoading();
                },
                error: function () {
                    common.notify('Can not load ajax function UpdateDevice', 'error');
                    common.stopLoading();
                }
            });

        }
    }

    function DeleteDevice(id) {
        $.ajax({
            type: "POST",
            url: "../Device/DeleteDevice",
            data: { Id: id },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    common.notify('Xóa thành công', 'success');

                    loadData(true);
                }
                else {

                    common.notify(response.Message, 'error');
                }
               
                common.stopLoading();
               
            },
            error: function () {
                common.notify('Can not load ajax function DeleteDevice', 'error');
                common.stopLoading();
            }
        });
    }

    function UpdateDeviceStatus(id, idStatus) {
        if (idStatus == 2) {
            idStatus = 1
        }
        else {
            idStatus = 2
        }
        $.ajax({
            type: "POST",
            url: "../Device/UpdateDeviceStatus",
            data: {
                Id: id,
                idStatus: idStatus
            },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    common.notify('Thay đổi trạng thái thành công', 'success');

                    loadData(true);
                }
                else {

                    common.notify(response.Message, 'error');
                }

                common.stopLoading();

            },
            error: function () {
                common.notify('Can not load ajax function UpdateDeviceStatus', 'error');
                common.stopLoading();
            }
        });
    }
   
}