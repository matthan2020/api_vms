﻿var inoutController = function () {
    this.initialize = function () {
        
       
      
        //$.when(loadData()).done(function (x) {
        //    $('.ckcheck1').click(function () {
        //        alert(2);
        //        if ($(this).is(':checked')) {
        //            alert(3);
        //        } else {
        //            alert(4);
        //        }
        //        console.log(checkxoa);
        //    });
        //});
        //loadData();
        registerEvents();
        registerControls();
        //loadDataTreeView();
    }

    function registerEvents() {
      
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            loadData(true);
        });
        $('#btnSearch').on('click', function () {
            loadData(true);
        });
        $('#txtStaffCode').on('keypress', function (e) {
            if (e.which === 13) {
                loadData(true);
            }
        });
     
      

        $('#btnExport').on('click', function () {
            ExportExcel();

        });
    }
    function registerControls() {
       
        $('.date-picker').datepicker({
            autoclose: true,
            dateFormat: 'dd/mm/yy',
            //format: 'dd/MM/yyyy',
            todayHighlight: true
        });
        $('.date-picker').val(common.dateLoading());
        dropdown.GetDepartment($('#ddlDepartment'), '');
      
    }
 
    function loadData(isPageChanged) {
        
        var template = $('#table-template-inout').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {
                staffCode: $('#txtStaffCode').val(),
                departmentId: $('#ddlDepartment').val(),
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../Statistical/GetInOut',
            dataType: 'json',
            success: function (response) {
                //console.log(response.Data);
                if (response.Data.TotalRow > 0) {
                    $.each(response.Data.Items, function (i, item) {
                        render += Mustache.render(template, {
                            STT: (common.configs.pageIndex * common.configs.pageSize - common.configs.pageSize+1) + i,
                            StaffCode: item.StaffCode,
                            FaceName: item.FaceName,
                            DepartmentName: item.DepartmentName,
                            RecordTimeIn: item.RecordTimeIn,
                            DeviceNameIn: item.DeviceNameIn,
                            RecordTimeOut: item.RecordTimeOut,
                            DeviceNameOut: item.DeviceNameOut,
                            Tong: item.Tong,
                            RecordTime: item.RecordTime
                            //.toFixed(2)
                          
                        });
                      
                        
                    });
                    if (render != '') {
                        $('#tbl-inout').html(render);
                    }
                    //common.wrapPaging( response.Data.TotalRow, function () {
                    //    loadData(false);
                    //}, isPageChanged);
                    common.wrapPaging1('.paginationUL',response.Data.TotalRow, function () {
                        loadData(false);
                    }, isPageChanged);
                }
                else {
                    render ='<tr>' +
                        '    <td colspan="10">Không có dữ liệu</td>' +
                        '</tr>';
                    $('#tbl-inout').html(render);
                }
                $('#lblTotalRecords').text(response.Data.TotalRow);
            },
            error: function (status) {
                common.notify('Cannot loading data', 'error');
            }
        });
    }
  

   

    function ExportExcel() {
        $.ajax({
            url: '../Statistical/ExportExcelInOut',
            type: 'GET',
            data: {
                staffCode: $('#txtStaffCode').val(),
                departmentId: $('#ddlDepartment').val(),
                departmentName: $('#ddlDepartment option:selected').text(),
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
            },
            dataType: 'json',
            success: function (response) {
                if (response.Success == true) {
                    window.location.href = response.Data;
                }
                else {
                    common.notify(response.Message, 'error');
                }
            },
            error: function () {
                common.notify('Can not load ajax function ExportExcel', 'error');
            }
        });
    }
}