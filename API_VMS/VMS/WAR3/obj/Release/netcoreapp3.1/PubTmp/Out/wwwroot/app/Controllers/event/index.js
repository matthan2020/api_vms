﻿var eventController = function () {
    var checkxoa = '';
    this.initialize = function () {

        //$.when(loadData()).done(function (x) {
        //    $('.ckcheck1').click(function () {
        //        alert(2);
        //        if ($(this).is(':checked')) {
        //            alert(3);
        //        } else {
        //            alert(4);
        //        }
        //        console.log(checkxoa);
        //    });
        //});
        //loadData();
        registerEvents();
        registerControls();
        //$.when(GetMultiSelect()).done(function (x) {
        //    $('#langOpt').multiselect({

        //        columns: 1,
        //        placeholder: '--Chọn--',
        //        search: true,
        //        selectAll: true
        //    });
        //});
    }

    var registerEvents = function () {
        //$('#frmProduct').validate({
        //    errorClass: 'red',
        //    ignore: [],
        //    lang: 'vi',
        //    rules: {
        //        ddlProductType: {
        //            required: true
        //        },
        //        txtCode: {
        //            required: true
        //        },
        //        txtModel: {
        //            required: false
        //        },
        //        txtName: {
        //            required: false
        //        },
        //        txtNameVN: {
        //            required: false
        //        },
        //        txtContact: {
        //            required: false
        //        },
        //        txtDes: {
        //            required: false
        //        },
        //    }
        //});
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            loadData(true);
        });
        $('#btnSearch').on('click', function () {
            loadData(true);
        });
        $('#txtKeyword').on('keypress', function (e) {
            if (e.which === 13) {
                loadData(true);
            }
        });
        $('#inPutFile').on('change', function (e) {
            readURL(this);
        });



      

        $('body').on('click', '.btn-detail', function (e) {
            resetFormMaintainance();
            var that = $(this).data('id');
            var imglink = $(this).data('imglink');
            e.preventDefault();
            $('#frmUpdateEvent').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtStaffCodeUpdate: {
                        required: true
                    },
                    txtRecordTimeUpdate: {
                        required: true
                    },
                    fromTimeUpdate: {
                        required: true
                    },
                    ddlDeviceUpdate: {
                        required: true
                    },
                }
            });
            $('#hidIdEvent').val(that);
            GetEventDetailById(that, imglink);
        });

        $('body').on('click', '.btn-delete', function (e) {
            resetFormMaintainance();
            var that = $(this).data('id');
            e.preventDefault();
            common.confirm('Bạn có chắc chắn muốn xóa dữ liệu không?', function () {
                DeleteEventStatistical(that);
            });
        });

        $('body').on('click', '.DeleteImg', function (e) {
            $('#inPutFile').val('');
            $('#imgSearch').html('');
        });
       
       

        $('body').on('change', '.ckcheck1', function (e) {
            e.preventDefault();
            if ($(this).is(':checked')) {
                var chuoi = "," + $(this).data('id') + ",";
                if (checkxoa.includes(chuoi)==true) {

                }
                else {
                    checkxoa += "," + $(this).data('id') + ",";
                }
               
            }
            else {
                var chuoixoa = "," + $(this).data('id') + ",";
                checkxoa = checkxoa.replace(chuoixoa, ",");
            }
            //checkxoa = checkxoa.replace(",,", ",");
            checkxoa = checkxoa.replace(/,,/g, ",");
            if (checkxoa == ",") {
                checkxoa = "";
            }
            console.log(checkxoa);
        });
        $('#btnAdd').on('click', function () {
            $('#frmAddEvent').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtStaffCode: {
                        required: true
                    },
                    ddlDevice: {
                        required: true
                    },
                    txtRecordTime: {
                        required: true
                    },
                    fromTimeAdd: {
                        required: true
                    },
                }
            });
            resetFormMaintainanceAdd();
            //initTreeDropDownCategory();
            $('#modal-event-add').modal('show');
        });
        $('#btnSave').on('click', function () {
           SaveEvent()
        });
        $('#btnUpdate').on('click', function () {
            UpdateEvent()
        });

       
    }
    var registerControls = function () {
        
        dropdown.GetDevice($('#ddlDevice'), '');
        dropdown.GetDevice($('#ddlDeviceUpdate'), '');
        $('.date-picker').datepicker({
            autoclose: true,
            dateFormat: 'dd/mm/yy',
            //format: 'dd/MM/yyyy',
            todayHighlight: true
        });
        $('.date-picker').val(common.dateLoading());
        //GetListEvent();
        
            $('#langOpt').multiselect({

            columns: 1,
            placeholder: '--Chọn--',
            search: true,
            selectAll: true
        });
       // GetMultiSelect();
       
    }
    function GetMultiSelect() {
        var render = "";
        $.ajax({
            url: '../Event/GetListEvent',
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                console.log(response);
                if (response.Success == true) {
                    if (response.Data.Items.length > 0) {
                        $.each(response.Data.Items, function (i, item) {
                            render += '<option value="' + item.EventId + '">' + item.EventName + '</option>';

                        });
                            $("#langOpt").html(render);

                            //$('#langOpt').multiselect({
                            // columns: 1,
                            // placeholder: 'Chọn',
                            // search: true,
                            // selectAll: true
                            // });
                        
                       
                    }
                    else {
                      
                    }

                }
                else {
                    common.notify(response.Message, 'error');
                }

            },
            error: function (status) {
                common.notify('Cannot loading data GetStatisticalEvent', 'error');
            }
        });
    }
    function GetListEvent() {
        var template = $('#table-template-event').html();
        var render = "";
        $.ajax({
            type: 'GET',
            url: '../Event/GetListEvent',
            dataType: 'json',
            success: function (response) {
                console.log(response.Data.Items);
                if (response.Data.Items.length > 0) {
                   
                    for (const property in response.Data.Items) {
                        if (property % 2 === 0) {
                            render += '<tr style="background-color: rgba(0,0,0,.00)">' +
                                '<td scope="row">' +
                                '<input type="checkbox" class="ckcheck1" data-id="' + response.Data.Items[property].EventId + '"> ' + response.Data.Items[property].EventName +
                                '</td>';
                            if (response.Data.Items - 1 == property) {
                                render +='</tr>';
                                $('#tbl-event').html(check)
                            }
                        }
                        else {
                            render +=  '<td scope="row">' +
                                '<input type="checkbox" class="ckcheck1" data-id="' + response.Data.Items[property].EventId + '"> ' + response.Data.Items[property].EventName +
                                '</td>' +
                                '</tr>';
                           
                        }
                        

                        
                    }
                    $('#tbl-event').html(render);
                    //$.each(response.Data.Items, function (i, item) {
                        //render += Mustache.render(template, {
                          //  EventId: item.EventId,
                           // EventName: item.EventName
                            //.toFixed(2)

                        //});

                       
                       
                    //});
                    //if (render != '') {
                      //  $('#tbl-event').html(render);
                    //}
                }
                
               
            },
            error: function (status) {
                common.notify('Cannot loading data', 'error');
            }
        });
    }

    function readURL(input) {
        $('#imgSearch').html('');
        //console.log(input.files[0]);
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            //console.log(input.files[0]);
          
            reader.onload = function (e) {
               // $('#blah')
              //      .attr('src', e.target.result);
                read = '<img class="blah1" src="' + e.target.result + '" alt="your image" style="width: 100%; height: 200px"/>' +
                    '<button class="btn btn-link DeleteImg" type="button"> Xóa </button>';
                $('#imgSearch').append(read);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

   
    function loadData(isPageChanged) {
        if ($('#langOpt').val() != "" && $('#langOpt').val() != null && $('#langOpt').val() != undefined) {
            checkxoa = ',' + $('#langOpt').val() + ',';
        }
        else {
            checkxoa =  '' ;
        }
        var toTime = "";
        if (($('#toTime').val() == null || $('#toTime').val() == "" || $('#toTime').val() == undefined) && ($('#toDate').val() != null && $('#toDate').val() != "" && $('#toDate').val() != undefined)) {

            toTime = $('#toDate').val() + ' 23:59:59';
        }
        else {
            toTime = $('#toDate').val() + ' ' + $('#toTime').val();
        }
        //console.log($("#inPutFile").files[0])
        var fileUpload = $("#inPutFile").get(0);
        var files = fileUpload.files;

        // Create FormData object  
        var fileData = new FormData();
        // Looping over all files and add it to FormData object  
        for (var i = 0; i < files.length; i++) {
            fileData.append("files", files[i]);
        }
        fileData.append("eventID", checkxoa);
        fileData.append("fromDate", $('#fromDate').val() + ' ' + $('#fromTime').val());
        fileData.append("toDate", toTime);
        fileData.append("search", $('#txtSearch').val());
        fileData.append("page", common.configs.pageIndex);
        fileData.append("pageSize", common.configs.pageSize)
        console.log(fileData);
       // alert(toTime);
        var template = $('#table-template-statistical-event').html();
        var render = "";
        $.ajax({
            type: 'POST',
            url: '../Event/GetStatisticalEvent',
            data: fileData,
            //data: {
            //    eventID: checkxoa,
            //    fromDate: $('#fromDate').val() + ' ' + $('#fromTime').val(),
            //    toDate: toTime,
            //    search: $('#txtSearch').val(),
            //    page: common.configs.pageIndex,
            //    pageSize: common.configs.pageSize
            //},
            
            processData: false,  // tell jQuery not to process the data
            contentType: false,
            beforeSend: function () {
                common.startLoading();
            },
            //dataType: 'json',
            success: function (response) {
                //console.log(response.Data);
                if (response.Success == true) {
                    if (response.Data.TotalRow > 0) {
                        $.each(response.Data.Items, function (i, item) {
                            render += Mustache.render(template, {
                                STT: (common.configs.pageIndex * common.configs.pageSize - common.configs.pageSize + 1) + i,
                                Base64ContentUrl: item.Base64ContentUrl!=null?'/img/event/' + item.Base64ContentUrl.split("\\")[item.Base64ContentUrl.split("\\").length - 1]:'',
                                FaceName: item.Sex == null ? item.FaceName + ' - Giới tính: ' : item.FaceName + ' - Giới tính: ' + item.Sex,
                                EventType: item.EventType,
                                DeviceName: item.DeviceName,
                                HisEventId: item.HisEventId,
                                RecordTime: item.Base64ContentUrl == null ? item.RecordTime + ' (' + item.CreatedUser + ' ' + item.CreatedDate + ')' : item.RecordTime
                                //.toFixed(2)

                            });

                        });
                        if (render != '') {
                            $('#tbl-statistical-event').html(render);
                        }
                        common.wrapPaging1('.paginationUL',response.Data.TotalRow, function () {
                            loadData(false);
                        }, isPageChanged);
                    }
                    else {
                        render = '<tr>' +
                            '    <td colspan="8">Không có dữ liệu</td>' +
                            '</tr>';
                        $('#tbl-statistical-event').html(render);
                    }
                    $('#lblTotalRecords').text(response.Data.TotalRow);
                }
                else {
                    common.notify(response.Message, 'error');
                    render = '<tr>' +
                        '    <td colspan="8"></td>' +
                        '</tr>';
                    $('#tbl-statistical-event').html(render);
                    $('#lblTotalRecords').text('');
                }
                common.stopLoading();
               
                
            },
            error: function (status) {
                common.notify('Cannot loading data GetStatisticalEvent', 'error');
                common.stopLoading();
            }
        });
    }
   
    //function loadData() {
    //    $.ajax({
    //        type: "GET",
    //        url: "/Department/GetListDepartment",
    //        dataType: 'json',
    //        success: function (response) {
    //            var data = [];
    //            //data.push({
    //            //    id: 0,
    //            //    text: 'Chọn tất cả',
    //            //    parentId: null,
    //            //    sortOrder: null
    //            //});
    //            $.each(response.Data.Items, function (i, item) {
    //                data.push({
    //                    id: item.DepartmentId,
    //                    text: item.DepartmentName,
    //                    parentId: null,
    //                    sortOrder: i+1
    //                });

    //            });
    //            var treeArr = common.unflattern(data);

    //            //var $tree = $('#treeProductCategory');

    //            $('#treeProductCategory').tree({
    //                data: treeArr,
    //                dnd: true
    //            });

    //        }
    //    });
    //}

    function resetFormMaintainance() {
        
        $('#txtFaceName').val('');
       // initTreeDropDownCategory('');
        $('#txtStaffCodeUpdate').val('');
        $('#txtRecordTimeUpdate').val('');
        $('#fromTimeUpdate').val('');
        $('#ddlDeviceUpdate').val('');
        $('#txtAgent').val('');
        $('#txtJob').val('');
        $('#txtSex').val('');
        $('#txtImportantLevel').val('');
        $('#imgTop5').html(''); 
        $('#imgdetail').html(''); 

    }
    function resetFormMaintainanceAdd() {

        $('#txtStaffCode').val('');
        // initTreeDropDownCategory('');

        $('#ddlDevice').val('');
        $('#txtRecordTime').val('');
        $('#fromTime').val('');

    }
   

    function GetEventDetailById(Id, imglink) {
                $.ajax({
                    type: "GET",
                    url: "../Event/GetEventDetailById",
                    data: { id: Id },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        if (response.Success == true) {
                            
                            var read = '';
                            var data = response.Data.Items[0];
                            console.log(data.StaffCode);
                            $('#txtFaceName').val(data.FaceName);
                            $('#txtAgent').val(data.AgeYear = '0' ? '' : data.AgeYear);
                            $('#txtJob').val(data.Job);
                            $('#txtSex').val(data.Sex);
                            $('#txtStaffCodeUpdate').val(data.StaffCode);
                            $('#ddlDeviceUpdate').val(data.DeviceId);
                            $('#txtRecordTimeUpdate').val(data.RecordTime.split(" ")[0]);
                            $('#fromTimeUpdate').val(data.RecordTime.split(" ")[1]);
                            $('#txtImportantLevel').val(data.ImportantLevel == 0 ? 'Không tin tưởng' : data.ImportantLevel == 1 ? 'Tin tưởng' : 'Vip');

                            if (imglink != null && imglink != "" && imglink != undefined) {
                                $('#txtStaffCodeUpdate').prop('disabled', true);
                                $('#txtRecordTimeUpdate').prop('disabled', true);
                                $('#fromTimeUpdate').prop('disabled', true); 
                                $('#ddlDeviceUpdate').prop('disabled', true);
                                $('#update').prop('hidden', true);
                                read = '<img src="' + imglink + '" alt=""  height=230 width=230 />';
                            }
                            else {
                                $('#txtStaffCodeUpdate').prop('disabled', false);
                                $('#txtRecordTimeUpdate').prop('disabled', false);
                                $('#fromTimeUpdate').prop('disabled', false);
                                $('#ddlDeviceUpdate').prop('disabled', false);
                                $('#update').prop('hidden', false);
                            }
                           
                            $('#imgdetail').append(read);
                            //Base64ContentUrl1: imglink;
                            if (response.Data.Items.length == 1) {
                               
                            }
                            else {
                                $.each(response.Data.Items, function (i, item) {
                                    var read = '';
                                    if (item.Base64ContentUrl != null) {
                                         read = '<img src="http://27.72.88.226:8081/img/event/' + item.Base64ContentUrl.split("\\")[item.Base64ContentUrl.split("\\").length - 1] + '" alt="" style="margin-right: 10px;" height=100 width=100 />';
                                    }
                                   
                                    
                                    $('#imgTop5').append(read);
                                });
                            }
                            $('#modal-event-detail').modal('show');
                        }
                        else {
                            common.notify('Có lỗi xảy ra: ' + response.Message, 'error');
                            
                        }
                       
                       
                        common.stopLoading();

                    },
                    error: function () {
                        common.notify('Can not load ajax function GetEventDetailById', 'error');
                        common.stopLoading();
                    }
                });
        //}
    }

   

    function DeleteEventStatistical(id) {
        $.ajax({
            type: "GET",
            url: "../Event/DeleteEventStatistical",
            data: { Id: id },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                common.notify('Xóa thành công', 'success');
                common.stopLoading();
                loadData(true);
            },
            error: function () {
                common.notify('Can not load ajax function DeleteEventStatistical', 'error');
                common.stopLoading();
            }
        });
    }
    function SaveEvent() {
        if ($('#frmAddEvent').valid()) {
            $.ajax({
                type: "POST",
                url: "../Event/SaveEvent",
                data: {
                    StaffCode: $('#txtStaffCode').val(),
                    Event: 1,
                    Device: $('#ddlDevice').val(),
                    RecordTime: $('#txtRecordTime').val() + ' ' + $('#fromTimeAdd').val()
                },
                dataType: "json",
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    if (response.Success == true) {
                        common.notify('Thêm mới thành công', 'success');
                        $('#modal-event-add').modal('hide');
                        loadData(true);
                    }
                    else {
                        if (response.Message == "TRUNG_MA") {
                            common.notify('Mã nhân viên không tồn tại. Mời bạn nhập lại', 'error');
                            //common.popupOk('Cảnh báo', 'Mã nhân viên bị trùng.Mời bạn nhập lại', 'error');
                        }
                        else {
                            common.notify(response.Message, 'error');
                        }
                    }
                    common.stopLoading();
                },
                error: function () {
                    common.notify('Can not load ajax function SaveEvent', 'error');
                    common.stopLoading();
                }
            });
        }
    }
    function UpdateEvent() {
        if ($('#frmUpdateEvent').valid()) {
            $.ajax({
                type: "POST",
                url: "../Event/UpdateEvent",
                data: {
                    id: $('#hidIdEvent').val(),
                    StaffCode: $('#txtStaffCodeUpdate').val(), 
                    Device: $('#ddlDeviceUpdate').val(),
                    RecordTime: $('#txtRecordTimeUpdate').val() + ' ' + $('#fromTimeUpdate').val()
                },
                dataType: "json",
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    if (response.Success == true) {
                        common.notify('Sửa thành công', 'success');
                        $('#modal-event-detail').modal('hide');
                        loadData(true);
                    }
                    else {
                        if (response.Message == "TRUNG_MA") {
                            common.notify('Mã nhân viên không tồn tại. Mời bạn nhập lại', 'error');
                            //common.popupOk('Cảnh báo', 'Mã nhân viên bị trùng.Mời bạn nhập lại', 'error');
                        }
                        else {
                            common.notify(response.Message, 'error');
                        }
                    }
                    common.stopLoading();
                },
                error: function () {
                    common.notify('Can not load ajax function UpdateEvent', 'error');
                    common.stopLoading();
                }
            });
        }
    }
}