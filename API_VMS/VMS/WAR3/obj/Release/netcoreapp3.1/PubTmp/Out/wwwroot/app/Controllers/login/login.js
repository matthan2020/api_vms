﻿var loginController = function () {
    this.initialize = function () {
        registerEvents();
    }

    var registerEvents = function () {
        $('#btnDangnhap').on('click', function (e) {
            e.preventDefault();
            var user = $('#txtUserName').val();
            var password = $('#txtPassword').val();
            var remenber = document.getElementById("remenber").checked;
            if (user == "" || user == null || user == undefined || password == "" || password == null || password == undefined) {
                common.notify('Tên đăng nhập và mật khẩu không được để trống', 'error')
            }
            else {
                login(user, password, remenber);
            }
            
        });
        //$('#txtUserName').on('keypress', function (e) {
        //    if (e.which === 13) {
        //        var user = $('#txtUserName').val();
        //        var password = $('#txtPassword').val();
        //        if (user == "" || user == null || user == undefined || password == "" || password == null || password == undefined) {
        //            common.notify('Tên đăng nhập và mật khẩu không được để trống', 'error')
        //        }
        //        else {
        //            login(user, password);
        //        }
        //    }
        //});
        //$('#txtPassword').on('keypress', function (e) {
        //    if (e.which === 13) {
        //        var user = $('#txtUserName').val();
        //        var password = $('#txtPassword').val();
        //        if (user == "" || user == null || user == undefined || password == "" || password == null || password == undefined) {
        //            common.notify('Tên đăng nhập và mật khẩu không được để trống', 'error')
        //        }
        //        else {
        //            login(user, password);
        //        }
        //    }
        //});
        if ($('#remenber').val() == 'true') {
            document.getElementById("remenber").checked = true;
        }
        else {
            document.getElementById("remenber").checked = false;
        }
    }

    var login = function (user, pass, remenber) {
        if ($('#txtUserName').val().length != $('#txtUserName').val().replace(" ", "").length) {
            common.notify('Tên đăng nhập không được chứa khoảng trắng.Mời bạn nhập lại', 'error')
            //common.popupOk('Cảnh báo', 'Tên đăng nhập không được chứa khoảng trắng.Mời bạn nhập lại', 'error');
            return;
        }
        if ($('#txtPassword').val().length != $('#txtPassword').val().replace(" ", "").length) {
            common.notify('Mật khẩu không được chứa khoảng trắng.Mời bạn nhập lại', 'error')
            //common.popupOk('Cảnh báo', 'Mật khẩu không được chứa khoảng trắng.Mời bạn nhập lại', 'error');
            return;
        }
        if ($('#txtUserName').val().length < 8 || $('#txtUserName').val().length >16) {
            common.notify('Tên đăng nhập tối thiểu là 8 và tối đa 16 ký tự.Mời bạn nhập lại', 'error')
            //common.popupOk('Cảnh báo', 'Tên đăng nhập không được chứa khoảng trắng.Mời bạn nhập lại', 'error');
            return;
        }
        if ($('#txtPassword').val().length < 8 || $('#txtPassword').val().length > 16) {
            common.notify('Tên đăng nhập tối thiểu là 8 và tối đa 16 ký tự.Mời bạn nhập lại', 'error')
            //common.popupOk('Cảnh báo', 'Tên đăng nhập không được chứa khoảng trắng.Mời bạn nhập lại', 'error');
            return;
        }
        $.ajax({
            type: 'POST',
            data: {
                UserName: user,
                Password: pass,
                Remenber: remenber
            },
            dateType: 'json',
            url: '/vms/Login/authen',
            success: function (response) {
                console.log(response.Data);
                if (response.Success == true) {
                    if (response.Data.Items.length > 0) {
                        window.location.href = "/vms/Home/Index";
                    }
                    else {
                        common.notify('Tên đăng nhập hoặc mật khẩu không đúng', 'error')
                    }
                }
                else {
                    //ommon.notify('Tên đăng nhập hoặc mật khẩu không đúng', 'error')
                    common.notify(response.Message, 'error');
                }
               
               
            }
        })
    }
}