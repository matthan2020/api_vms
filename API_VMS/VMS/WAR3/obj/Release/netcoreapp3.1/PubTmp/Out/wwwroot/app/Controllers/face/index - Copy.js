﻿var faceController = function () {
    var checkxoa = '';
    var checkIdCloud = '';
    var fileImg = [];
    var format = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    var formatMa = /[`!@#$%^&*()_+\=\[\]{};':"\\|,.<>\/?~]/;
   // var fileData1 = new FormData();
    var abc = {};
    var fileListAr = [];
    this.initialize = function () {

        //$.when(loadData()).done(function (x) {
        //    $('.ckcheck1').click(function () {
        //        alert(2);
        //        if ($(this).is(':checked')) {
        //            alert(3);
        //        } else {
        //            alert(4);
        //        }
        //        console.log(checkxoa);
        //    });
        //});
        //loadData();
        registerEvents();
        registerControls();
        
    }

    var registerEvents = function () {
        //$('#frmProduct').validate({
        //    errorClass: 'red',
        //    ignore: [],
        //    lang: 'vi',
        //    rules: {
        //        ddlProductType: {
        //            required: true
        //        },
        //        txtCode: {
        //            required: true
        //        },
        //        txtModel: {
        //            required: false
        //        },
        //        txtName: {
        //            required: false
        //        },
        //        txtNameVN: {
        //            required: false
        //        },
        //        txtContact: {
        //            required: false
        //        },
        //        txtDes: {
        //            required: false
        //        },
        //    }
        //});
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            if ($('#hidIdSearch').val() == '1') {
                loadDataList(true);
            }
            if ($('#hidIdSearch').val() == '0') {
                loadDataGrid(true);
            }
            //loadData(true);
        });
        $('#btnSearch').on('click', function () {
            //$('#hidIdSearch').val(1);
            if ($('#hidIdSearch').val() == '1') {
                loadDataList(true);
            }
            if ($('#hidIdSearch').val() == '0') {
                loadDataGrid(true);
            }
        });
        $('#btnList').on('click', function () {
            $('#hidIdSearch').val(1);
             checkxoa = '';
            checkIdCloud = '';
            $('#btnGrid').prop('hidden', false);
            $('#btnList').prop('hidden', true);
            loadDataList(true);
        });
        $('#btnGrid').on('click', function () {
            $('#hidIdSearch').val(0);
            checkxoa = '';
            checkIdCloud = '';
            $('#btnGrid').prop('hidden', true);
            $('#btnList').prop('hidden', false);
            loadDataGrid(true);
        });
        $('#btnSearchCB').on('click', function () {
            $('#hidIdSearch').val(1);
            loadDataCB(true);
        });
        $('#txtKeyword').on('keypress', function (e) {
            if (e.which === 13) {
                if ($('#hidIdSearch').val() == '1') {
                    loadDataList(true);
                }
                if ($('#hidIdSearch').val() == '0') {
                    loadDataGrid(true);
                }
            }
        });
       
        $('#btnCreate').on('click', function () {
          
            resetFormMaintainance();

            //initTreeDropDownCategory();
            $('#modal-add-face').modal('show');

        });

        $('#inPutFile').on('change', function (e) {
            readURL(this);
        });
        $('#btnSave').on('click', function () {
            //alert($('#txtStaffCode').val().length);
            SaveFace();
            //if (check == '0') {
            //    SaveDevice();
            //}
            //else {
            //    UpdateDevice();
            //}

        });
        $('body').on('click', '.btn-detail', function (e) {
            resetFormMaintainance1();
            var that = $(this).data('id');
            e.preventDefault();
            $('#frmFace').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtFaceNameView: {
                        required: true
                    },

                }
            });
            GetFaceDetailById(that);
        });
        $('body').on('click', '.viewImg', function (e) {
            resetFormMaintainance1();
            var that = $(this).data('id');
            e.preventDefault();
            $('#frmFace').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtFaceNameView: {
                        required: true
                    },

                }
            });
            GetFaceDetailById(that);
        });

        $('body').on('click', '.delete', function (e) {
            var that = $(this).data('id');
            common.confirm('Bạn có chắc chắn muốn xóa ảnh không?', function () {
                fileListAr.splice(that, 1);
                $('#imgInsert').html('');
                for (var i = 0; i < fileListAr.length; i++) {
                    html(i);
                }
            });
           
            
        }); 
        $('#btnDepartment').on('click', function () {
            $('#frmDepartment').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtDepartmentCode: {
                        required: true
                    },
                    txtDepartmentName: {
                        required: true
                    },
                   
                }
            });
            resetFormMaintainanceD();
            $('#modal-add-department').modal('show');

        });
        $('#btnDepartmentEdit').on('click', function () {
            $('#frmDepartment').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtDepartmentCode: {
                        required: true
                    },
                    txtDepartmentName: {
                        required: true
                    },

                }
            });
            resetFormMaintainanceD();
            $('#modal-add-department').modal('show');

        });
        $('#btnSaveD').on('click', function () {
           
           
            SaveDepartment();
        }); 
        $('#btnDelete').on('click', function () {
            if (checkxoa == null || checkxoa == "" || checkxoa == undefined) {
                common.popupOk("Cảnh báo", "Bạn chưa chọn dữ liệu nào để xóa!", "error")
            }
            else {
                common.confirm('Bạn có chắc chắn muốn xóa dữ liệu không?', function () {
                    DeleteFace();
                });
            }
           
        });
        $('#btnSaveEdit').on('click', function () {
           
                common.confirm('Bạn có chắc chắn muốn sửa dữ liệu không?', function () {
                    UpdateFace();
                });

        });
        $('body').on('click', '.btn-delete', function (e) {
            resetFormMaintainance();
            var that = $(this).data('id');
            e.preventDefault();
            common.confirm('Bạn có chắc chắn muốn xóa dữ liệu không?', function () {
                DeleteEventStatistical(that);
            });
        });

        $('body').on('change', '.ckcheck', function (e) {
            if ($(this).is(':checked')) {
                $('.ckcheck1').each(function () {
                    this.checked = true;
                    var chuoi = "," + $(this).data('id') + ","
                    if (checkxoa.includes(chuoi) == true) {

                    }
                    else {
                        
                        checkxoa += "," + $(this).data('id') + ",";
                        if ($(this).data('staffcode') != 0) {
                            checkIdCloud += "," + $(this).data('staffcode') + ",";
                        }
                    }
                    // checkxoa += "," + $(this).data('id') + ",";
                });

            } else {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = false;
                    checkxoa = '';
                    checkIdCloud = '';
                });
            }
            //var res = str.replace(/blue/g, "red");
            checkxoa = checkxoa.replace(/,,/g, ",");
            checkIdCloud = checkIdCloud.replace(/,,/g, ",");
            console.log(checkxoa);
            console.log(checkIdCloud);
        });

        $('body').on('change', '.ckcheck1', function (e) {
            e.preventDefault();
            if ($(this).is(':checked')) {
               
                var chuoi = "," + $(this).data('id') + ",";
                if (checkxoa.includes(chuoi)==true) {
                    
                }
                else {
                    checkxoa += "," + $(this).data('id') + ",";
                    if ($(this).data('staffcode') != 0) {
                        checkIdCloud += "," + $(this).data('staffcode') + ",";
                    }
                }
                
            }
            else {
                document.getElementById("ckcheck2").checked = false;
                //$('#ckcheck2').checked = false;
                var chuoixoa = "," + $(this).data('id') + ",";
                checkxoa = checkxoa.replace(chuoixoa, ",");
                var chuoixoacload = "," + $(this).data('staffcode') + ",";
                checkIdCloud = checkIdCloud.replace(chuoixoacload, ",");
            }
            //checkxoa = checkxoa.replace(",,", ",");
            checkxoa = checkxoa.replace(/,,/g, ",");
            checkIdCloud = checkIdCloud.replace(/,,/g, ",");
            if (checkxoa == ",") {
                checkxoa = "";
            }
            if (checkIdCloud == ",") {
                checkIdCloud = "";
            }
            if ($('#ddlShowPage').val() >= Number($("#lblTotalRecords").html())) {
                if ($("#lblTotalRecords").html() == checkxoa.split(',').length - 2) {
                    document.getElementById("ckcheck2").checked = true;
                }
            }
            else {
                if ($('#ddlShowPage').val() == checkxoa.split(',').length - 2) {
                    document.getElementById("ckcheck2").checked = true;
                }
            }
            
            console.log(checkxoa);
            console.log(checkIdCloud);
        });

        $('body').on('change', '.ckcheck3', function (e) {
            if ($(this).is(':checked')) {
                $('.ckcheck4').each(function () {
                    this.checked = true;
                    var chuoi = "," + $(this).data('id') + ","
                    if (checkxoa.includes(chuoi) == true) {

                    }
                    else {

                        checkxoa += "," + $(this).data('id') + ",";
                        if ($(this).data('staffcode') != 0) {
                            checkIdCloud += "," + $(this).data('staffcode') + ",";
                        }
                    }
                    // checkxoa += "," + $(this).data('id') + ",";
                });

            } else {
                $('.ckcheck4').each(function () {
                    //alert($(this).data('id'))
                    this.checked = false;
                    checkxoa = '';
                    checkIdCloud = '';
                });
            }
            //var res = str.replace(/blue/g, "red");
            checkxoa = checkxoa.replace(/,,/g, ",");
            checkIdCloud = checkIdCloud.replace(/,,/g, ",");
            console.log(checkxoa);
            console.log(checkIdCloud);
        });

        $('body').on('change', '.ckcheck4', function (e) {
            e.preventDefault();
            if ($(this).is(':checked')) {

                var chuoi = "," + $(this).data('id') + ",";
                if (checkxoa.includes(chuoi) == true) {

                }
                else {
                    checkxoa += "," + $(this).data('id') + ",";
                    if ($(this).data('staffcode') != 0) {
                        checkIdCloud += "," + $(this).data('staffcode') + ",";
                    }
                }

            }
            else {
                document.getElementById("ckcheck3").checked = false;
                //$('#ckcheck2').checked = false;
                var chuoixoa = "," + $(this).data('id') + ",";
                checkxoa = checkxoa.replace(chuoixoa, ",");
                var chuoixoacload = "," + $(this).data('staffcode') + ",";
                checkIdCloud = checkIdCloud.replace(chuoixoacload, ",");
            }
            //checkxoa = checkxoa.replace(",,", ",");
            checkxoa = checkxoa.replace(/,,/g, ",");
            checkIdCloud = checkIdCloud.replace(/,,/g, ",");
            if (checkxoa == ",") {
                checkxoa = "";
            }
            if (checkIdCloud == ",") {
                checkIdCloud = "";
            }
            if ($('#ddlShowPage').val() >= Number($("#lblTotalRecords").html())) {
                if ($("#lblTotalRecords").html() == checkxoa.split(',').length - 2) {
                    document.getElementById("ckcheck3").checked = true;
                }
            }
            else {
                if ($('#ddlShowPage').val() == checkxoa.split(',').length - 2) {
                    document.getElementById("ckcheck3").checked = true;
                }
            }

            console.log(checkxoa);
            console.log(checkIdCloud);
        });
      

       

       
    }
    var registerControls = function () {
        dropdown.GetDepartment($('#ddlDepartment'), '');
        $('.date-picker').datepicker({
            autoclose: true,
            dateFormat: 'dd/mm/yy',
            //format: 'dd/MM/yyyy',
            todayHighlight: true
        }); 
        dropdown.GetDepartment($('#ddlDepartmentAdd'), '');
        dropdown.GetDepartment($('#ddlDepartmentView'), '');
    }
  
    function html(isPageChanged) {
    var reader = new FileReader();
    //console.log(input.files[0]);
    reader.onload = function (e) {
        read = '<img class="blah1" src="' + e.target.result + '" alt="your image" style="width: 100px; height: 100px" data-id="' + isPageChanged + '"/>' +
            '<button class="btn btn-link delete" type="button" data-id="' + isPageChanged + '"> Xóa </button>';
        //$('#blah')
        //    .attr('src', e.target.result);
        $('#imgInsert').append(read);
    };

    reader.readAsDataURL(fileListAr[isPageChanged]);
}
   

    function loadDataCB(isPageChanged) {
        
       
       
        var render = "";
        $('#viewImage').html('');
       
        $.ajax({
            type: 'GET',
            data: {
                search: $('#txtKeyword').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../Face/GetSearchFaceCB',
            dataType: 'json',
            success: function (response) {
                console.log(response.Data);
                if (response.Data.TotalRow > 0) {
                    var check = response.Data.Items.length;
                    console.log(response.Data.Items);
                    render += '<div class="form-group" style="margin-top:10px">' +
                        '<input type="checkbox" class="ckcheck" id="ckcheck2"> Chọn tất cả'  +
                        '</div>';
                    $.each(response.Data.Items, function (i, item) {
                        if (check - 1 > i) {
                            render += '<div style="margin-right: 10px;float:left" height="210" width="210">' +
                                '<img src = "http://27.72.88.226:8081/img/root/' + item.Base64ContentUrl.split("\\")[item.Base64ContentUrl.split("\\").length - 1] + '" alt = "" style = "margin-right: 10px;" height = 200 width = 200 data-id="' + item.FaceId + '" class="viewImg"/>' +
                                '<div class="form-group" style="margin-top:10px">' +
                                '<input type="checkbox" class="ckcheck1" data-id="' + item.FaceId + '"data-idcloud="' + item.FaceIdCloud +'"> ' + item.FaceName +
                                '</div>' +
                                '</div>';
                        }
                        else {
                            render += '<div style="margin-right: 10px;" height="210" width="210">' +
                                '<img src = "http://27.72.88.226:8081/img/root/' + item.Base64ContentUrl.split("\\")[item.Base64ContentUrl.split("\\").length - 1] + '" alt = "" style = "margin-right: 10px;" height = 200 width = 200 data-id="' + item.FaceId + '" class="viewImg"/>' +
                                '<div class="form-group" style="margin-top:10px">' +
                                '<input type="checkbox" class="ckcheck1" data-id="' + item.FaceId + '"data-idcloud="'+ item.FaceIdCloud +'"> ' + item.FaceName +
                                '</div>' +
                                '</div>';
                        }
                        
                       

                       
                      
                    });
                    common.wrapPaging(response.Data.TotalRow, function () {
                        loadDataCB();
                    }, isPageChanged);
                    $('#viewImage').html(render);
                }
                else {
                    render = '<tr>' +
                        '    <td colspan="7">Không có dữ liệu</td>' +
                        '</tr>';
                    $('#viewImage').append(render);
                }
                $('#lblTotalRecords').text(response.Data.TotalRow);
            },
            error: function (status) {
                common.notify('Cannot loading data GetStatisticalEvent', 'error');
            }
        });
    }
   
    function loadDataNC(isPageChanged) {



        var render = "";
        $('#viewImage').html('');

        $.ajax({
            type: 'POST',
            data: {
                faceName: $('#txtFaceName').val(),
                staffCode: $('#txtStaffCode').val(),
                departmentId: $('#ddlDepartment').val(),
                job: $('#txtJob').val(),
                sex: $('#ddlSex').val(),
                known: $('#ddlKnown').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../Face/GetSearchFaceNC',
            dataType: 'json',
            success: function (response) {
                console.log(response.Data);
                if (response.Data.TotalRow > 0) {
                    var check = response.Data.Items.length;
                    console.log(check);
                    render += '<div class="form-group" style="margin-top:10px">' +
                        '<input type="checkbox" class="ckcheck" id="ckcheck2"> Chọn tất cả' +
                        '</div>';
                    $.each(response.Data.Items, function (i, item) {
                        if (check - 1 > i) {
                            render += '<div style="margin-right: 10px;float:left" height="210" width="210">' +
                                '<img src = "http://27.72.88.226:8081/img/root/' + item.Base64ContentUrl.split("\\")[item.Base64ContentUrl.split("\\").length - 1] + '" alt = "" style = "margin-right: 10px;" height = 200 width = 200 data-id="' + item.FaceId + '" class="viewImg"/>' +
                                '<div class="form-group" style="margin-top:10px">' +
                                '<input type="checkbox" class="ckcheck1" data-id="' + item.FaceId + '" data-idcloud="' + item.FaceIdCloud +'"> ' + item.FaceName +
                                '</div>' +
                                '</div>';
                        }
                        else {
                            render += '<div style="margin-right: 10px;" height="210" width="210">' +
                                '<img src = "http://27.72.88.226:8081/img/root/' + item.Base64ContentUrl.split("\\")[item.Base64ContentUrl.split("\\").length - 1] + '" alt = "" style = "margin-right: 10px;" height = 200 width = 200 data-id="' + item.FaceId + '" class="viewImg"/>' +
                                '<div class="form-group" style="margin-top:10px">' +
                                '<input type="checkbox" class="ckcheck1" data-id="' + item.FaceId + '" data-idcloud="' + item.FaceIdCloud +'"> ' + item.FaceName +
                                '</div>' +
                                '</div>';
                        }





                    });
                    common.wrapPaging(response.Data.TotalRow, function () {
                        loadDataNC();
                    }, isPageChanged);
                    $('#viewImage').html(render);
                }
                else {
                    render = '<tr>' +
                        '    <td colspan="7">Không có dữ liệu</td>' +
                        '</tr>';
                    $('#viewImage').append(render);
                }
                $('#lblTotalRecords').text(response.Data.TotalRow);
            },
            error: function (status) {
                common.notify('Cannot loading data GetStatisticalEvent', 'error');
            }
        });
    }

    function resetFormMaintainance() {
        
        $('#txtFaceNameAdd').val('');
       // initTreeDropDownCategory('');

        $('#txtAgent').val('');
        $('#txtJobAdd').val('');
        $('#txtSexAdd').val('');
        $('#txtStaffCodeAdd').val('');
        $('#ddlDepartmentAdd').val(''); 
        $('#ddlKnownAdd').val(''); 
        $('#inPutFile').val(''); 
        $('#imgInsert').html(''); 
        fileListAr = [];

    }
    function resetFormMaintainanceD() {

        $('#txtDepartmentCode').val('');
        // initTreeDropDownCategory('');

        $('#txtDepartmentName').val('');
       

    }
    function resetFormMaintainance1() {

        $('#txtFaceNameView').val('');
        // initTreeDropDownCategory('');
        $('#txtStaffCodeView').val('');
        $('#ddlDepartmentView').val('');

        $('#txtAgentView').val('');
        $('#txtJobView').val('');
        $('#ddlSexView').val('');
        $('#ddlImportantLevel').val('');
        $('#imgTop10').html('');
        $('#imgdetailFace').html(''); 

    }
    function readURL(input) {
       
        $('#imgInsert').html('');
        
        const fileListArr = Array.from(input.files);

        for (var a = 0; a < fileListArr.length; a++) {
            fileListAr.push(fileListArr[a]);
        }
        for (var ai = 0; ai < fileListAr.length; ai++) {
            html(ai);
        }
        //fileListAr.append(fileListArr);
        console.log(fileListAr)
    }
    //function readURL(input) {
    //    var read = "";
    //    var stt = fileListAr.length;
    //    console.log(fileListAr.length);
    //    for (var i = 0; i <= input.files.length; i++) {
    //        if (input.files && input.files[i]) {
    //            //var img = input.files[0].size;
    //            //var imgsize = img / 1024; 
               
    //            var reader = new FileReader();
    //            //console.log(input.files[0]);
    //            reader.onload = function (e) {
    //                read = '<img class="blah1" src="' + e.target.result + '" alt="your image" style="width: 100px; height: 100px" data-id="'+stt+'"/>' +
    //                    '<button class="btn btn-link delete" type="button" data-id="' + stt + '"> Xóa </button>';
    //                //$('#blah')
    //                //    .attr('src', e.target.result);
    //                $('#imgInsert').append(read);
    //            };
              
    //            reader.readAsDataURL(input.files[i]);
                
    //        }
    //        stt += i;
    //        //stt = stt + i;
    //    }
    //    //abc.add(input.files);
    //    //var fileUpload = $("#inPutFile").get(0);
    //    //        var files = fileUpload.files;
    //            // Create FormData object  
    //            //var fileData = new FormData();
    //            // Looping over all files and add it to FormData object  
    //    //for (var a = 0; a < files.length; a++) {
    //    //    console.log(files[a])
    //    //            fileData1.append("files", files[a]);
    //    //}
    //    const fileListArr = Array.from(input.files);
       
    //    for (var a = 0; a < fileListArr.length; a++) {
    //        fileListAr.push(fileListArr[a]);
    //    }
    //    //fileListAr.append(fileListArr);
    //    console.log(fileListAr )
    //}
    function SaveFace() {
        if ($('#txtStaffCodeAdd').val().length > 10 || $('#txtStaffCodeAdd').val().length ==0) {
            common.popupOk('Cảnh báo', 'Mã nhân viên phải trong khoảng từ 1 đến 10 ký tự. Mời bạn nhập lại', 'error');
            return;
        }
        else {
            if (formatMa.test($('#txtStaffCodeAdd').val()) == true) {
                common.popupOk('Cảnh báo', 'Mã nhân viên chứa các ký tự đặc biệt. Mời bạn nhập lại', 'error');
                return;
            }
        }
        if ($('#txtFaceNameAdd').val().length > 50) {
            common.popupOk('Cảnh báo', 'Tên nhân viên phải trong khoảng từ 1 đến 50 ký tự. Mời bạn nhập lại', 'error');
            return;
        }
        else {
            if (format.test($('#txtFaceNameAdd').val()) == true) {
                common.popupOk('Cảnh báo', 'Tên nhân viên chứa các ký tự đặc biệt. Mời bạn nhập lại', 'error');
                return;
            }
        }
        if ($('#txtJobAdd').val().length > 50) {
            common.popupOk('Cảnh báo', 'Nghề nghiệp phải trong khoảng từ 1 đến 50 ký tự. Mời bạn nhập lại', 'error');
            return;
        }
        else {
            if (format.test($('#txtJobAdd').val()) == true) {
                common.popupOk('Cảnh báo', 'Nghề nghiệp chứa các ký tự đặc biệt. Mời bạn nhập lại', 'error');
                return;
            }
        }
        var fileData1 = new FormData();
        for (var a = 0; a < fileListAr.length; a++) {
            fileData1.append("files", fileListAr[a]);
        }
        fileData1.append('staffCode', $('#txtStaffCodeAdd').val());
        fileData1.append('faceName', $('#txtFaceNameAdd').val());
        fileData1.append('agent', $('#txtAgent').val());
        fileData1.append('job', $('#txtJobAdd').val());
        fileData1.append('department', $('#ddlDepartmentAdd').val());
        fileData1.append('sex', $('#ddlSexAdd').val());
        fileData1.append('known', $('#ddlKnownAdd').val());
            // if ($('#frmDevice').valid()) {
            $.ajax({
                type: "POST",
                url: "../Face/SaveFace",
                data: fileData1,
                //data: {
                //    staffCode: $('#txtStaffCode').val(),
                //    faceName: $('#txtFaceName').val(),
                //    agent: $('#txtAgent').val(),
                //    job: $('#txtJob').val(),
                //    department: $('#ddlDepartment').val(),
                //    sex: $('#ddlSex').val(),
                //    known: $('#ddlKnown').val(),
                //    files: fileImg

                //},
                processData: false,  // tell jQuery not to process the data
                contentType: false,
                //dataType: "json",
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    console.log(response);
                    if (response.Success == true) {
                        common.notify('Thêm mới thành công', 'success');

                        $('#modal-add-face').modal('hide');
                        resetFormMaintainance();

                        if ($('#hidIdSearch').val() == '1') {
                            loadDataList(true);
                        }
                        if ($('#hidIdSearch').val() == '0') {
                            loadDataGrid(true);
                        }
                        //loadData(true);
                    }
                    else {
                        if (response.Message == "TRUNG_MA") {
                            common.notify('Mã nhân viên bị trùng. Mời bạn nhập lại', 'error');
                            //common.popupOk('Cảnh báo', 'Mã nhân viên bị trùng.Mời bạn nhập lại', 'error');
                        }
                        else {
                            common.notify(response.Message, 'error');
                        }
                        
                    }
                    common.stopLoading();
                },
                error: function () {
                    common.notify('Can not load ajax function SaveProduct', 'error');
                    common.stopLoading();
                }
            });
    }

    function SaveDepartment() {

        if ($('#frmDepartment').valid()) {
           
            // if ($('#frmDevice').valid()) {
            $.ajax({
                type: "POST",
                url: "../Face/SaveDepartment",
                data: {
                    departmentCode: $('#txtDepartmentCode').val(),
                    departmentName: $('#txtDepartmentName').val(),

                },
                dataType: "json",
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    console.log(response);
                    if (response.Success == true) {
                        common.notify('Thêm mới phòng ban thành công', 'success');

                        $('#modal-add-department').modal('hide');
                        resetFormMaintainanceD();
                        dropdown.GetDepartment($('#ddlDepartmentAdd'), '');
                        dropdown.GetDepartment($('#ddlDepartmentView'), '');
                        common.stopLoading();
                    }
                    else {

                        common.notify(response.Message, 'error');
                    }
                    common.stopLoading();
                },
                error: function () {
                    common.notify('Can not load ajax function SaveProduct', 'error');
                    common.stopLoading();
                }
            });

        }
    }

    function DeleteFace() {

        $.ajax({
            type: "GET",
            url: "../Face/DeleteFace",
            data: {
                IdFace: checkxoa,
                IdCloud: checkIdCloud
            },
            dataType: "json",
            beforeSend: function () {
                //("#loadingDiv").css("display", "block");
                common.startLoading();
            },
            success: function (response) {
                if (response.Success = true) {
                    common.notify('Xóa thành công', 'success');
                    if ($('#hidIdSearch').val() == '1') {
                        loadDataList(true);
                    }
                    if ($('#hidIdSearch').val() == '0') {
                        loadDataGrid(true);
                    }
                }
                else {
                    common.notify(response.Message, 'error');
                }
                common.stopLoading();
               
            },
            error: function () {
                common.notify('Can not load ajax function DeleteFace', 'error');
                common.stopLoading();
            }
        });
    }

    function loadDataGrid(isPageChanged) {

        checkxoa = '';
        checkIdCloud = '';

        var render = "";
        $('#viewImage').html('');

        $.ajax({
            type: 'POST',
            data: {
                search: $('#txtKeyword').val(),
                faceName: $('#txtFaceName').val(),
                staffCode: $('#txtStaffCode').val(),
                departmentId: $('#ddlDepartment').val(),
                job: $('#txtJob').val(),
                sex: $('#ddlSex').val(),
                known: $('#ddlKnown').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../Face/GetSearchFace',
            dataType: 'json',
            success: function (response) {
                console.log(response.Data);
                if (response.Data.TotalRow > 0) {
                    var check = response.Data.Items.length;
                    console.log(response.Data.Items);
                    render += '<div class="form-group" style="margin-top:10px">' +
                        '<input type="checkbox" class="ckcheck" id="ckcheck2"> Chọn tất cả' +
                        '</div>';
                    $.each(response.Data.Items, function (i, item) {
                        if (check - 1 > i) {
                            render += '<div style="margin-right: 10px;float:left" height="210" width="210">' +
                                '<img src = "http://27.72.88.226:8081/img/root/' + item.Base64ContentUrl.split("\\")[item.Base64ContentUrl.split("\\").length - 1] + '" alt = "" style = "margin-right: 10px;" height = 200 width = 200 data-id="' + item.FaceId + '" class="viewImg"/>' +
                                '<div class="form-group" style="margin-top:10px">' +
                                '<input type="checkbox" class="ckcheck1" data-id="' + item.FaceId + '"data-idcloud="' + item.FaceIdCloud + ' "data-staffcode="' + item.StaffCode + '"> ' + item.FaceName +
                                '</div>' +
                                '</div>';
                        }
                        else {
                            render += '<div style="margin-right: 10px;" height="210" width="210">' +
                                '<img src = "http://27.72.88.226:8081/img/root/' + item.Base64ContentUrl.split("\\")[item.Base64ContentUrl.split("\\").length - 1] + '" alt = "" style = "margin-right: 10px;" height = 200 width = 200 data-id="' + item.FaceId + '" class="viewImg"/>' +
                                '<div class="form-group" style="margin-top:10px">' +
                                '<input type="checkbox" class="ckcheck1" data-id="' + item.FaceId + '"data-idcloud="' + item.FaceIdCloud + ' "data-staffcode="' + item.StaffCode + '"> ' + item.FaceName +
                                '</div>' +
                                '</div>';
                        }





                    });
                    common.wrapPaging1('.paginationUL',response.Data.TotalRow, function () {
                        loadDataGrid();
                    }, isPageChanged);
                    $('#viewImage').html(render);
                }
                else {
                    render = '<tr>' +
                        '    <td colspan="7">Không có dữ liệu</td>' +
                        '</tr>';
                    $('#viewImage').append(render);
                }
                $('#list-face').prop('hidden',true);
                $('#viewImage').prop('hidden', false);

                $('#lblTotalRecords').text(response.Data.TotalRow);
            },
            error: function (status) {
                common.notify('Cannot loading data GetSearchFace', 'error');
            }
        });
    }

    function loadDataList(isPageChanged) {
        checkxoa = '';
        checkIdCloud = '';
        document.getElementById("ckcheck3").checked = false;
        var template = $('#table-template-list-face').html();
        var render = "";
        $.ajax({
            type: 'POST',
            data: {
                search: $('#txtKeyword').val(),
                faceName: $('#txtFaceName').val(),
                staffCode: $('#txtStaffCode').val(),
                departmentId: $('#ddlDepartment').val(),
                job: $('#txtJob').val(),
                sex: $('#ddlSex').val(),
                known: $('#ddlKnown').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../Face/GetSearchFace',
            dataType: 'json',
            success: function (response) {
                //console.log(response.Data);
                if (response.Data.TotalRow > 0) {
                    $.each(response.Data.Items, function (i, item) {
                        render += Mustache.render(template, {
                            STT: (common.configs.pageIndex * common.configs.pageSize - common.configs.pageSize+1)+i,
                            StaffCode: item.StaffCode,
                            FaceName: item.FaceName,
                            DepartmentName: item.DepartmentName,
                            Base64ContentUrl: 'http://27.72.88.226:8081/img/root/' + item.Base64ContentUrl.split("\\")[item.Base64ContentUrl.split("\\").length - 1],
                            AgeYear: item.AgeYear,
                            Sex: item.Sex,
                            Job: item.Job,
                            TRANG_THAI: item.IsKnown=='true'?'Known':'Unknown',
                            FaceIdCloud: item.FaceIdCloud,
                            FaceId: item.FaceId
                            //.toFixed(2)

                        });

                        
                    });
                    if (render != '') {
                        $('#tbl-list-face').html(render);
                    }
                    common.wrapPaging1('.paginationUL',response.Data.TotalRow, function () {
                        loadDataList(false);
                    }, isPageChanged);
                }
                else {
                    render = '<tr>' +
                        '    <td colspan="9">Không có dữ liệu</td>' +
                        '</tr>';
                    $('#tbl-list-face').html(render);
                }
                $('#list-face').prop('hidden', false);
                $('#viewImage').prop('hidden', true);
                $('#lblTotalRecords').text(response.Data.TotalRow);
            },
            error: function (status) {
                common.notify('Cannot loading data', 'error');
            }
        });



       
    }

    function GetFaceDetailById(Id) {
        $.ajax({
            type: "GET",
            url: "../Face/GetFaceDetailById",
            data: { id: Id },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    console.log(response.Data.Items[0].AgeYear);
                    var data = response.Data.Items[0]; 
                    $('#txtFaceNameView').val(data.FaceName);
                    $('#hidIdFace').val(data.FaceId);
                    $('#txtAgentView').val(data.AgeYear);
                    $('#txtJobView').val(data.Job);
                    $('#ddlSexView').val(data.Sex);
                    $('#txtStaffCodeView').val(data.StaffCode);
                    $('#ddlDepartmentView').val(data.DepartmentId);
                    $('#ddlImportantLevel').val(data.ImportantLevel); //== 0 ? 'Không tin tưởng' : data.ImportantLevel == 1 ? 'Tin tưởng' : 'Vip'); 
                    var read = '<img src="http://27.72.88.226:8081/img/root/' +data.ImagePath.split("\\")[data.ImagePath.split("\\").length - 1] + '" alt=""  height=230 width=230 />';
                    $('#imgdetailFace').append(read);
                    //Base64ContentUrl1: imglink;
                    if (response.Data.Items.length == 1) {

                    }
                    else {
                        $.each(response.Data.Items, function (i, item) {
                            if (i != 0) {
                                var read = '<img src="http://27.72.88.226:8081/img/root/' + item.Base64ContentUrl.split("\\")[item.Base64ContentUrl.split("\\").length - 1] + '" alt="" style="margin-right: 10px;" height=100 width=100 />';
                                $('#imgTop10').append(read);
                            }
                            
                        });
                    }
                    $('#modal-face-detail').modal('show');
                }
                else {
                    common.notify('Có lỗi xảy ra: ' + response.Message, 'error');

                }


                common.stopLoading();

            },
            error: function () {
                common.notify('Can not load ajax function GetFaceDetailById', 'error');
                common.stopLoading();
            }
        });
        //}
    }
    function UpdateFace() {
        if ($('#frmFace').valid()) {
            if ($('#txtStaffCodeView').val().length > 10) {
                common.popupOk('Cảnh báo', 'Mã nhân viên phải trong khoảng từ 1 đến 10 ký tự. Mời bạn nhập lại', 'error');
                return;
            }
            else {
                if (formatMa.test($('#txtStaffCodeView').val()) == true) {
                    common.popupOk('Cảnh báo', 'Mã nhân viên chứa các ký tự đặc biệt. Mời bạn nhập lại', 'error');
                    return;
                }
            }
            if ($('#txtFaceNameView').val().length > 50) {
                common.popupOk('Cảnh báo', 'Tên nhân viên phải trong khoảng từ 1 đến 50 ký tự. Mời bạn nhập lại', 'error');
                return;
            }
            else {
                if (format.test($('#txtFaceNameView').val()) == true) {
                    common.popupOk('Cảnh báo', 'Tên nhân viên chứa các ký tự đặc biệt. Mời bạn nhập lại', 'error');
                    return;
                }
            }
            if ($('#txtJobView').val().length > 50) {
                common.popupOk('Cảnh báo', 'Nghề nghiệp phải trong khoảng từ 1 đến 50 ký tự. Mời bạn nhập lại', 'error');
                return;
            }
            else {
                if (format.test($('#txtJobView').val()) == true) {
                    common.popupOk('Cảnh báo', 'Nghề nghiệp chứa các ký tự đặc biệt. Mời bạn nhập lại', 'error');
                    return;
                }
            }
            $.ajax({
                type: "POST",
                url: "../Face/UpdateFace",
                data: {
                    idFace: $('#hidIdFace').val(),
                    staffCode: $('#txtStaffCodeView').val(),
                    faceName: $('#txtFaceNameView').val(),
                    agent: $('#txtAgentView').val(),
                    job: $('#txtJobView').val(),
                    department: $('#ddlDepartmentView').val(),
                    sex: $('#ddlSexView').val(),
                    importantLevel: $('#ddlImportantLevel').val(),

                },
                dataType: "json",
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    console.log(response);
                    if (response.Success == true) {
                        common.notify('Sửa thành công', 'success');

                        $('#modal-face-detail').modal('hide');
                        //resetFormMaintainance();

                        if ($('#hidIdSearch').val() == '1') {
                            loadDataList(true);
                        }
                        if ($('#hidIdSearch').val() == '0') {
                            loadDataGrid(true);
                        }
                    }
                    else {

                        if (response.Message == "TRUNG_MA") {
                            common.notify('Mã nhân viên bị trùng. Mời bạn nhập lại', 'error');
                            //common.popupOk('Cảnh báo', 'Mã nhân viên bị trùng.Mời bạn nhập lại', 'error');
                        }
                        else {
                            common.notify(response.Message, 'error');
                        }
                    }
                    common.stopLoading();
                },
                error: function () {
                    common.notify('Can not load ajax function UpdateFace', 'error');
                    common.stopLoading();
                }
            });

        }
    }
}