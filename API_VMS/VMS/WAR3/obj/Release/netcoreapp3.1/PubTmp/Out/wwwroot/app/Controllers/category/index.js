﻿var departmentController = function () {
    var checkxoa = '';
    this.initialize = function () {

      
        
        loadData(true);
        registerEvents();
        registerControls();
      

        
    }

    var registerEvents = function () {
        //$('#frmDevice').validate({
        //    errorClass: 'red',
        //    ignore: [],
        //    lang: 'vi',
        //    rules: {
        //        ddlCampany: {
        //            required: true
        //        },
               
        //    }
        //});
       
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            loadData(true);
        });
       
        $('#txtKeyword').on('keypress', function (e) {
            if (e.which === 13) {
                loadData(true);
            }
        });
        $('#btnSearch').on('click', function () {
            loadData(true);

        });
        $("#btnCreate").on('click', function (e) {
            $('#frmDepartment').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtDepartmentCode: {
                        required: true,
                    }, 
                    txtDepartmentName: {
                        required: true
                    },
                }
            });
            resetFormMaintainance();
            //initTreeDropDownCategory();
            $('#modal-add-update-department').modal('show');
          

        });
        
        $('#btnSave').on('click', function () {
            var check = $('#hidIdM').val();
           
            if (check == '0') {
                SaveDepartment();
            }
            else {
                UpdateDepartment();
            }

        });

        $('body').on('click', '.fa-pencil-square-o', function (e) {
            $('#hidIdM').val(1);
            var that = $(this).data('id');
            $('#frmDepartment').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtDepartmentCode: {
                        required: true,
                    },
                    txtDepartmentName: {
                        required: true
                    },
                }
            });
            //resetFormMaintainance();
            //initTreeDropDownCategory();
            e.preventDefault();
            GetDepartmentById(that);
        });
       

       
        $('body').on('click', '.fa-trash', function (e) {
            var that = $(this).data('id');
                common.confirm('Bạn có chắc chắn muốn xóa dữ liệu không?', function () {
                    DeleteDepartment(that);
                });

        });

       
       
        $('.ckcheck').click(function () {
            if ($(this).is(':checked')) {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = true;
                    var chuoi = "," + $(this).data('id') + ","
                    if (checkxoa.includes(chuoi) == true) {

                    }
                    else {
                        checkxoa += "," + $(this).data('id') + ",";
                    }
                   // checkxoa += "," + $(this).data('id') + ",";
                });
               
            } else {
                $('.ckcheck1').each(function () {
                    //alert($(this).data('id'))
                    this.checked = false;
                    checkxoa = '';
                });
            }
            //var res = str.replace(/blue/g, "red");
            checkxoa = checkxoa.replace(/,,/g, ",");
            console.log(checkxoa);
        });

        $('body').on('change', '.ckcheck1', function (e) {
            e.preventDefault();
            if ($(this).is(':checked')) {
                var chuoi = "," + $(this).data('id') + ",";
                if (checkxoa.includes(chuoi)==true) {

                }
                else {
                    checkxoa += "," + $(this).data('id') + ",";
                }
               
            }
            else {
                var chuoixoa = "," + $(this).data('id') + ",";
                checkxoa = checkxoa.replace(chuoixoa, ",");
            }
            //checkxoa = checkxoa.replace(",,", ",");
            checkxoa = checkxoa.replace(/,,/g, ",");
            if (checkxoa == ",") {
                checkxoa = "";
            }
            console.log(checkxoa);
        });
     
       


        
    }
    var registerControls = function () {
        //dropdown.GetDeviceType($('#ddlDeviceType'), '');
       
    }
    function loadData(isPageChanged) {
        var template = $('#table-template-department').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {
                keyword: $('#txtKeyword').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../Department/GetAllDepartmentPage',
            dataType: 'json',
            success: function (response) {
                console.log(response);
                if (response.Data.TotalRow > 0) {
                    $.each(response.Data.Items, function (i, item) {
                        render += Mustache.render(template, {
                            STT: (common.configs.pageIndex * common.configs.pageSize - common.configs.pageSize + 1) + i,
                            DepartmentCode: item.Acronym,
                            DepartmentName: item.DepartmentName,
                            DepartmentId: item.DepartmentId,
                        });
                      
                       
                       
                    });
                    if (render != '') {
                        $('#tbl-department').html(render);
                    }
                    common.wrapPaging1('.paginationUL', response.Data.TotalRow, function () {
                        //alert(1);
                        loadData(false);
                    }, isPageChanged);
                }
                else {
                    render ='<tr>' +
                        '    <td colspan="4">Không có dữ liệu</td>' +
                        '</tr>';
                    $('#tbl-department').html(render);
                }
                $('#lblTotalRecords').text(response.Data.TotalRow);
            },
            error: function () {
                common.notify('Can not load ajax function GetAllDepartmentPage', 'error');
            }
        });
    }

    function resetFormMaintainance() {
        $('#hidIdM').val(0);
        //$('#ddlProductType').val('');
       // initTreeDropDownCategory('');

        $('#txtDepartmentName').val('');
        $('#txtDepartmentCode').val('');
        
        $('#hidIdDepartment').val('');
      

    }

    function SaveDepartment() {
         
        if ($('#frmDepartment').valid()) {
          
       // if ($('#frmDevice').valid()) {
            $.ajax({
                type: "POST",
                url: "../Department/SaveDepartment",
                data: {
                    departmentCode: $('#txtDepartmentCode').val(),
                    departmentName: $('#txtDepartmentName').val(),

                },
                dataType: "json",
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    console.log(response);
                    if (response.Success == true) {
                        common.notify('Thêm mới phòng ban thành công', 'success');

                        $('#modal-add-update-department').modal('hide');
                        resetFormMaintainance();
                        loadData(true);
                        common.stopLoading();
                    }
                    else {

                        if (response.Message == 'TRUNG_MA') {
                            common.notify('Mã phòng ban bị trùng. Mời bạn nhập lại', 'error');
                        }
                        else {
                            common.notify(response.Message, 'error');
                        }
                    }
                    common.stopLoading();
                },
                error: function () {
                    common.notify('Can not load ajax function SaveProduct', 'error');
                    common.stopLoading();
                }
            });
           
        }
    }

    function GetDepartmentById(Id) {
        $.ajax({
            type: "GET",
            url: "../Department/GetDepartmentById",
            data: { id: Id },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    console.log(response);
                    var data = response.Data.Items[0];
                    console.log(data);
                  
                    $('#hidIdDepartment').val(data.DepartmentId);
                    $('#txtDepartmentCode').val(data.Acronym);
                    $('#txtDepartmentName').val(data.DepartmentName);
                   

                    $('#modal-add-update-department').modal('show');
                }
                else {
                    common.notify('Có lỗi xảy ra: ' + response.Message, 'error');

                }


                common.stopLoading();

            },
            error: function () {
                common.notify('Can not load ajax function GetDepartmentById', 'error');
                common.stopLoading();
            }
        });
        //}
    }

    function UpdateDepartment() {
        if ($('#frmDepartment').valid()) {
           
            // if ($('#frmDevice').valid()) {
            $.ajax({
                type: "POST",
                url: "../Department/UpdateDepartment",
                data: {
                    id: $('#hidIdDepartment').val(),
                    departmentCode: $('#txtDepartmentCode').val(),
                    departmentName: $('#txtDepartmentName').val(),

                },
                dataType: "json",
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    console.log(response);
                    if (response.Success == true) {
                        common.notify('Sửa thành công', 'success');
                        $('#modal-add-update-department').modal('hide');
                        resetFormMaintainance();

                       // common.stopLoading();
                        loadData(true);
                    }
                    else {
                        if (response.Message == 'TRUNG_MA') {
                            common.notify('Mã phòng ban bị trùng. Mời bạn nhập lại', 'error');
                        }
                        else {
                            common.notify(response.Message, 'error');
                        }
                      
                    }
                    common.stopLoading();
                },
                error: function () {
                    common.notify('Can not load ajax function UpdateDepartment', 'error');
                    common.stopLoading();
                }
            });

        }
    }

    function DeleteDepartment(id) {
        $.ajax({
            type: "POST",
            url: "../Department/DeleteDepartment",
            data: { Id: id },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    common.notify('Xóa thành công', 'success');

                    loadData(true);
                }
                else {

                    common.notify(response.Message, 'error');
                }
               
                common.stopLoading();
               
            },
            error: function () {
                common.notify('Can not load ajax function DeleteDepartment', 'error');
                common.stopLoading();
            }
        });
    }

   
   
}