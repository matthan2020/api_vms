﻿var dropdown = {
    
    GetDepartment: function (ddltype, data) {
        return $.ajax({
            type: "GET",
            url: "../Department/GetListDepartment",
            dataType: "json",
            success: function (response) {
                var render = " <option value = '' >  --Chọn-- </option >";
               
                $.each(response.Data.Items, function (i, item) {
                    if (item.DepartmentId == data) {
                        render += "<option value='" + item.DepartmentId + "' selected>" + item.DepartmentId + ' - ' + item.DepartmentName + "</option>";
                    }
                    else {
                        render += "<option value='" + item.DepartmentId + "'>" + item.DepartmentId + ' - ' + item.DepartmentName + "</option>";
                    }
                    
                });
                ddltype.html(render);
            }
        });
    },
    GetDepartmentNoNull: function (ddltype, data) {
        return $.ajax({
            type: "GET",
            url: "../Department/GetListDepartment",
            dataType: "json",
            success: function (response) {
                var render = "";

                $.each(response.Data.Items, function (i, item) {
                    if (item.DepartmentId == data) {
                        render += "<option value='" + item.DepartmentId + "' selected>" + item.DepartmentId + ' - ' + item.DepartmentName + "</option>";
                    }
                    else {
                        render += "<option value='" + item.DepartmentId + "'>" + item.DepartmentId + ' - ' + item.DepartmentName + "</option>";
                    }

                });
                ddltype.html(render);
            }
        });
    },
    GetDeviceType: function (ddltype, data) {
        return $.ajax({
            type: "GET",
            url: "../Device/GetDeviceType",
            dataType: "json",
            success: function (response) {
                var render = "";
                $.each(response.Data.Items, function (i, item) {
                    if (item.IdDeviceType == data) {
                        render += "<option value='" + item.IdDeviceType + "' selected>" + item.DeviceTypeName + "</option>";
                    }
                    else {
                       
                            render += "<option value='" + item.IdDeviceType + "'>" + item.DeviceTypeName + "</option>";
                       
                    }

                });
                ddltype.html(render);
            }
        });
    },
    GetCompanyModelById: function (ddltype,id, data) {
        return $.ajax({
            type: "GET",
            url: "../Company/GetCompanyModelById",
            data: { id: id, },
            dataType: "json",
            success: function (response) {
                var render = "";
                $.each(response.Data.Items, function (i, item) {
                    if (item.IdModel == data) {
                        render += "<option value='" + item.IdModel + "' selected>" + item.ModelName + "</option>";
                    }
                    else {
                        render += "<option value='" + item.IdModel + "'>" + item.ModelName + "</option>";
                    }

                });
                ddltype.html(render);
            }
        });
    },
    GetCompany: function (ddltype,  data) {
        return $.ajax({
            type: "GET",
            url: "../Company/GetCompany",
            dataType: "json",
            success: function (response) {
                var render = "";
                $.each(response.Data.Items, function (i, item) {
                    if (item.IdCompany == data) {
                        render += "<option value='" + item.IdCompany + "' selected>" + item.CompanyName + "</option>";
                    }
                    else {
                        render += "<option value='" + item.IdCompany + "'>" + item.CompanyName + "</option>";
                    }

                });
                ddltype.html(render);
            }
        });
    },
    GetDevice: function (ddltype, data) {
        return $.ajax({
            type: "GET",
            url: "../Device/GetListDevice",
            dataType: "json",
            success: function (response) {
                var render = " <option value = '' >  --Chọn-- </option >";
                $.each(response.Data.Items, function (i, item) {
                    if (data == '' || data == null || data == undefined) {
                        render += "<option value='" + item.DeviceId + "' data-setting='" + item.TKSettingId + "'data-inout='" + item.IsCheckIn + "'data-onoff='" + item.IsTKOn + "'>" + item.DeviceName + " Kênh " + item.Channel + "</option>";
                    }
                    else {
                        if (item.DeviceId == data) {
                            render += "<option value='" + item.DeviceId + "' selected data-setting='" + item.TKSettingId + "'data-inout='" + item.IsCheckIn + "'data-onoff='" + item.IsTKOn + "'>" + item.DeviceName + " Kênh " + item.Channel + "</option>";
                        }
                        else {
                            render += "<option value='" + item.DeviceId + "' data-setting='" + item.TKSettingId + "'data-inout='" + item.IsCheckIn + "'data-onoff='" + item.IsTKOn + "'>" + item.DeviceName + " Kênh " + item.Channel + "</option>";
                        }
                    }
                    

                });
                ddltype.html(render);
            }
        });
    },
    GetSettingShift: function (ddltype, data) {
        return $.ajax({
            type: "GET",
            url: "../Setting/GetListSettingShift",
            dataType: "json",
            success: function (response) {
                var render = " <option value = '' >  --Chọn-- </option >";
                $.each(response.Data.Items, function (i, item) {
                    if (item.SettingShiftId == data) {
                        render += "<option value='" + item.SettingShiftId + "'data-code='" + item.SettingShiftCode+"' selected>" + (i+1) + ' - ' + item.SettingShiftName + "</option>";
                    }
                    else {
                        render += "<option value='" + item.SettingShiftId + "'data-code='" + item.SettingShiftCode + "'>" + (i + 1) + ' - ' + item.SettingShiftName + "</option>";
                    }

                });
                ddltype.html(render);
            }
        });
    },
    GetSettingShiftNoNull: function (ddltype, data) {
        return $.ajax({
            type: "GET",
            url: "../Setting/GetListSettingShift",
            dataType: "json",
            success: function (response) {
                var render = "";
                $.each(response.Data.Items, function (i, item) {
                    if (item.SettingShiftId == data) {
                        render += "<option value='" + item.SettingShiftId + "' selected>" + (i + 1) + ' - ' + item.SettingShiftName + "</option>";
                    }
                    else {
                        render += "<option value='" + item.SettingShiftId + "'>" + (i + 1)+ ' - ' + item.SettingShiftName + "</option>";
                    }

                });
                ddltype.html(render);
            }
        });
    },
    GetStatus: function (ddltype, data) {
        return $.ajax({
            type: "GET",
            url: "../Common/GetListStatus",
            dataType: "json",
            success: function (response) {
                var render = "";
                $.each(response.Data.Items, function (i, item) {
                    if (item.IdStatus == data) {
                        render += "<option value='" + item.IdStatus + "' selected>" + item.Status + "</option>";
                    }
                    else {

                        render += "<option value='" + item.IdStatus + "'>" + item.Status + "</option>";

                    }

                });
                ddltype.html(render);
            }
        });
    },
    GetShift: function (ddltype, data) {
        return $.ajax({
            type: "GET",
            url: "../Setting/GetListShiftGroup",
            data: {
                id: 1

            },
            dataType: "json",
            success: function (response) {
                var render = " <option value = '' >  --Chọn-- </option >";
                $.each(response.Data.Items, function (i, item) {
                    if (item.ShiftName == data) {
                        render += "<option value='" + item.ShiftName + "' selected>" + (i + 1) + ' - ' + item.ShiftName + "</option>";
                    }
                    else {
                        render += "<option value='" + item.ShiftName + "'>" + (i + 1) + ' - ' + item.ShiftName + "</option>";
                    }

                });
                ddltype.html(render);
            }
        });
    },
}