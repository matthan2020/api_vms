﻿var statisticalController = function () {
    var checkxoa = '';
    this.initialize = function () {
        
       
      
        //$.when(loadData()).done(function (x) {
        //    $('.ckcheck1').click(function () {
        //        alert(2);
        //        if ($(this).is(':checked')) {
        //            alert(3);
        //        } else {
        //            alert(4);
        //        }
        //        console.log(checkxoa);
        //    });
        //});
        //loadData();
        registerEvents();
        registerControls();
        
        //loadDataTreeView();
    }

    var registerEvents = function () {
      
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            loadData(true);
        });
        $('#btnSearch').on('click', function () {
            loadData(true);
        });
        $('#txtStaffCode').on('keypress', function (e) {
            if (e.which === 13) {
                loadData(true);
            }
        });
        $('#btnUpdate').on('click', function () {
            UpdateData();
        });
      

        $('#btnExport').on('click', function () {
            ExportExcel();

        });
    }
    var registerControls = function () {
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var year = dateObj.getUTCFullYear();
        //$('.dateTime').datepicker({
        //    autoclose: true,
        //    dateFormat: 'dd/mm/yy',
        //    //format: 'dd/MM/yyyy',
        //    todayHighlight: true
        //});
        //$('.dateTime').val(common.dateLoading());
        dropdown.GetDepartment($('#ddlDepartment'), '');
        $('#txtYear').val(year); 
        $('#ddlMonth').val(month);
    }
 
    function loadData(isPageChanged) {
        var template = $('#table-template-statistical').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {
                staffCode: $('#txtStaffCode').val(),
                departmentId: $('#ddlDepartment').val(),
                year: $('#txtYear').val(),
                month: $('#ddlMonth').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../Statistical/GetStatistical',
            dataType: 'json',
            success: function (response) {
                //console.log(response.Data);
                if (response.Data.TotalRow > 0) {
                    $.each(response.Data.Items, function (i, item) {
                        render += Mustache.render(template, {
                            STT: (common.configs.pageIndex * common.configs.pageSize - common.configs.pageSize + 1) + i,
                            StaffCode: item.StaffCode,
                            StaffName: item.StaffName,
                            DepartmentName: item.DepartmentName,
                            Day1: item.Day1.toFixed(1),
                            Day2: item.Day2.toFixed(1),
                            Day3: item.Day3.toFixed(1),
                            Day4: item.Day4.toFixed(1),
                            Day5: item.Day5.toFixed(1),
                            Day6: item.Day6.toFixed(1),
                            Day7: item.Day7.toFixed(1),
                            Day8: item.Day8.toFixed(1),
                            Day9: item.Day9.toFixed(1),
                            Day10: item.Day10.toFixed(1),
                            Day11: item.Day11.toFixed(1),
                            Day12: item.Day12.toFixed(1),
                            Day13: item.Day13.toFixed(1),
                            Day14: item.Day14.toFixed(1),
                            Day15: item.Day15.toFixed(1),
                            Day16: item.Day16.toFixed(1),
                            Day17: item.Day17.toFixed(1),
                            Day18: item.Day18.toFixed(1),
                            Day19: item.Day19.toFixed(1),
                            Day20: item.Day20.toFixed(1),
                            Day21: item.Day21.toFixed(1),
                            Day22: item.Day22.toFixed(1),
                            Day23: item.Day23.toFixed(1),
                            Day24: item.Day24.toFixed(1),
                            Day25: item.Day25.toFixed(1),
                            Day26: item.Day26.toFixed(1),
                            Day27: item.Day27.toFixed(1),
                            Day28: item.Day28.toFixed(1),
                            Day29: item.Day29.toFixed(1),
                            Day30: item.Day30.toFixed(1),
                            Day31: item.Day31.toFixed(1),
                            StyleDay1: item.Day1.toFixed(1) < 9 ?'color:red':'',
                            StyleDay2: item.Day2.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay3: item.Day3.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay4: item.Day4.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay5: item.Day5.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay6: item.Day6.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay7: item.Day7.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay8: item.Day8.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay9: item.Day9.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay10: item.Day10.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay11: item.Day11.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay12: item.Day12.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay13: item.Day13.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay14: item.Day14.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay15: item.Day15.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay16: item.Day16.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay17: item.Day17.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay18: item.Day18.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay19: item.Day19.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay20: item.Day20.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay21: item.Day21.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay22: item.Day22.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay23: item.Day23.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay24: item.Day24.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay25: item.Day25.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay26: item.Day26.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay27: item.Day27.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay28: item.Day28.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay29: item.Day29.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay30: item.Day30.toFixed(1) < 9 ? 'color:red' : '',
                            StyleDay31: item.Day31.toFixed(1) < 9 ? 'color:red' : '',
                            TONG_CONG: (item.Day1 + item.Day2 + item.Day3 + item.Day4 + item.Day5 + item.Day6 + item.Day7 + item.Day8 + item.Day9 + item.Day10 + item.Day11 + item.Day12 + item.Day13 + item.Day14 + item.Day15 + item.Day16 + item.Day17 + item.Day18 + item.Day19 + item.Day20 + item.Day21 + item.Day22 + item.Day23 + item.Day24 + item.Day25 + item.Day26 + item.Day27 + item.Day28 + item.Day29 + item.Day30 + item.Day31).toFixed(1),
                          
                          
                        });
                      
                       
                      
                    });
                    common.wrapPaging1('.paginationUL',response.Data.TotalRow, function () {
                        loadData(false);
                    }, isPageChanged);
                    if (render != '') {
                        $('#tbl-statistical').html(render);
                    }
                }
                else {
                    render ='<tr>' +
                        '    <td colspan="35">Không có dữ liệu</td>' +
                        '</tr>';
                    $('#tbl-statistical').html(render);
                }
                $('#lblTotalRecords').text(response.Data.TotalRow);
            },
            error: function (status) {
                common.notify('Cannot loading data', 'error');
            }
        });
    }
    function UpdateData() {
        $.ajax({
            url: '../Statistical/UpdateData',
            type: 'GET',
            data: {
                year: $('#txtYear').val(),
                month: $('#ddlMonth').val()
            },
            dataType: 'json',
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    common.popupOk("-----", "Cập nhật công tháng " + $('#ddlMonth').val() + "/" + $('#txtYear').val()+" thành công", "success")
                   
                    //loadData(true);
                }
                else {
                    common.popupOk("Lỗi", response.Message, "error")
                        //common.notify(response.Message, 'error');
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Can not load ajax function UpdateData', 'error');
                common.stopLoading();
            }
        });
    }

   

    function ExportExcel() {
        $.ajax({
            url: '../Statistical/ExportExcel',
            type: 'GET',
            data: {
                staffCode: $('#txtStaffCode').val(),
                departmentId: $('#ddlDepartment').val(),
                departmentName: $('#ddlDepartment option:selected').text(),
                year: $('#txtYear').val(),
                month: $('#ddlMonth').val()
            },
            dataType: 'json',
            success: function (response) {
                if (response.Success == true) {
                    window.location.href = response.Data;
                }
                else {
                    common.notify(response.Message, 'error');
                }
            },
            error: function () {
                common.notify('Can not load ajax function ExportExcel', 'error');
            }
        });
    }
}