﻿var statisticalFaceShiftController = function () {
    var checkxoa = '';
    var check = 0;
    this.initialize = function () {
        
       
      
        //$.when(loadData()).done(function (x) {
        //    $('.ckcheck1').click(function () {
        //        alert(2);
        //        if ($(this).is(':checked')) {
        //            alert(3);
        //        } else {
        //            alert(4);
        //        }
        //        console.log(checkxoa);
        //    });
        //});
        //loadData();
        registerEvents();
        registerControls();
        
        //loadDataTreeView();
    }

    var registerEvents = function () {
      
        $('#ddlShowPage').on('change', function () {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            if (check == 0) {
               
                loadDataCong(true)
            }
            else {
                loadData(true);
            }
            
        });
        $('#btnSearch').on('click', function () {
            if (check == 0) {
                loadDataCong(true)
            }
            else {
                loadData(true);
            }
        });
        $('#txtStaffCode').on('keypress', function (e) {
            if (e.which === 13) {
                if (check == 0) {
                    loadDataCong(true)
                }
                else {
                    loadData(true);
                }
            }
        });
        $('#btnUpdate').on('click', function () {
            UpdateData();
        });
      

        $('#btnExport').on('click', function () {
            ExportExcel();

        });
        $('#btnCong').on('click', function () {
            check = 0;
            loadDataCong(true);
            $('#btnHouse').prop('hidden', false);
            $('#btnCong').prop('hidden', true);
          
        });
        $('#btnHouse').on('click', function () {
            check = 1;
            loadData(true);
            $('#btnHouse').prop('hidden', true);
            $('#btnCong').prop('hidden', false);
        });
    }
    var registerControls = function () {
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var year = dateObj.getUTCFullYear();
        //$('.dateTime').datepicker({
        //    autoclose: true,
        //    dateFormat: 'dd/mm/yy',
        //    //format: 'dd/MM/yyyy',
        //    todayHighlight: true
        //});
        //$('.dateTime').val(common.dateLoading());
        dropdown.GetDepartment($('#ddlDepartment'), '');
        dropdown.GetShift($('#ddlShift'), '');
        $('#txtYear').val(year); 
        $('#ddlMonth').val(month);
    }
 
    function loadData(isPageChanged) {
        var template = $('#table-template-statistical-shift').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {
                staffCode: $('#txtStaffCode').val(),
                departmentId: $('#ddlDepartment').val(),
                shiftName: $('#ddlShift').val(),
                year: $('#txtYear').val(),
                month: $('#ddlMonth').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../Statistical/GetStatisticalFaceShift',
            dataType: 'json',
            success: function (response) {
                //console.log(response.Data);
                if (response.Data.TotalRow > 0) {
                    $.each(response.Data.Items, function (i, item) {
                        render += Mustache.render(template, {
                            STT: (common.configs.pageIndex * common.configs.pageSize - common.configs.pageSize + 1) + i,
                            StaffCode: item.StaffCode,
                            StaffName: item.StaffName,
                            DepartmentName: item.DepartmentName,
                            ShiftName: item.ShiftName,
                            Day1: item.CongKP01==-1?'':item.Day1.toFixed(1),
                            Day2: item.CongKP02 == -1 ? '' :item.Day2.toFixed(1),
                            Day3: item.CongKP03 == -1 ? '' :item.Day3.toFixed(1),
                            Day4: item.CongKP04 == -1 ? '' :item.Day4.toFixed(1),
                            Day5: item.CongKP05 == -1 ? '' :item.Day5.toFixed(1),
                            Day6: item.CongKP06 == -1 ? '' :item.Day6.toFixed(1),
                            Day7: item.CongKP07 == -1 ? '' :item.Day7.toFixed(1),
                            Day8: item.CongKP08 == -1 ? '' :item.Day8.toFixed(1),
                            Day9: item.CongKP09 == -1 ? '' :item.Day9.toFixed(1),
                            Day10: item.CongKP10 == -1 ? '' :item.Day10.toFixed(1),
                            Day11: item.CongKP11 == -1 ? '' :item.Day11.toFixed(1),
                            Day12: item.CongKP12 == -1 ? '' :item.Day12.toFixed(1),
                            Day13: item.CongKP13 == -1 ? '' :item.Day13.toFixed(1),
                            Day14: item.CongKP14 == -1 ? '' :item.Day14.toFixed(1),
                            Day15: item.CongKP15 == -1 ? '' :item.Day15.toFixed(1),
                            Day16: item.CongKP16 == -1 ? '' :item.Day16.toFixed(1),
                            Day17: item.CongKP17 == -1 ? '' :item.Day17.toFixed(1),
                            Day18: item.CongKP18 == -1 ? '' :item.Day18.toFixed(1),
                            Day19: item.CongKP19 == -1 ? '' :item.Day19.toFixed(1),
                            Day20: item.CongKP20 == -1 ? '' :item.Day20.toFixed(1),
                            Day21: item.CongKP21 == -1 ? '' :item.Day21.toFixed(1),
                            Day22: item.CongKP22 == -1 ? '' :item.Day22.toFixed(1),
                            Day23: item.CongKP23 == -1 ? '' :item.Day23.toFixed(1),
                            Day24: item.CongKP24 == -1 ? '' :item.Day24.toFixed(1),
                            Day25: item.CongKP25 == -1 ? '' :item.Day25.toFixed(1),
                            Day26: item.CongKP26 == -1 ? '' :item.Day26.toFixed(1),
                            Day27: item.CongKP27 == -1 ? '' :item.Day27.toFixed(1),
                            Day28: item.CongKP28 == -1 ? '' :item.Day28.toFixed(1),
                            Day29: item.CongKP29 == -1 ? '' :item.Day29.toFixed(1),
                            Day30: item.CongKP30 == -1 ? '' :item.Day30.toFixed(1),
                            Day31: item.CongKP31 == -1 ? '' :item.Day31.toFixed(1),
                            StyleDay1: item.Day1.toFixed(1) < 8 ?'color:red':'',
                            StyleDay2: item.Day2.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay3: item.Day3.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay4: item.Day4.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay5: item.Day5.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay6: item.Day6.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay7: item.Day7.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay8: item.Day8.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay9: item.Day9.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay10: item.Day10.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay11: item.Day11.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay12: item.Day12.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay13: item.Day13.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay14: item.Day14.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay15: item.Day15.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay16: item.Day16.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay17: item.Day17.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay18: item.Day18.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay19: item.Day19.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay20: item.Day20.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay21: item.Day21.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay22: item.Day22.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay23: item.Day23.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay24: item.Day24.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay25: item.Day25.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay26: item.Day26.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay27: item.Day27.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay28: item.Day28.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay29: item.Day29.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay30: item.Day30.toFixed(1) < 8 ? 'color:red' : '',
                            StyleDay31: item.Day31.toFixed(1) < 8 ? 'color:red' : '',
                            MUON: item.Late,
                            SOM: item.Soon,
                            CHINH: (item.Day1 + item.Day2 + item.Day3 + item.Day4 + item.Day5 + item.Day6 + item.Day7 + item.Day8 + item.Day9 + item.Day10 + item.Day11 + item.Day12 + item.Day13 + item.Day14 + item.Day15 + item.Day16 + item.Day17 + item.Day18 + item.Day19 + item.Day20 + item.Day21 + item.Day22 + item.Day23 + item.Day24 + item.Day25 + item.Day26 + item.Day27 + item.Day28 + item.Day29 + item.Day30 + item.Day31).toFixed(1),
                            KPHEP: ((item.CongKP01 == 0 ? item.Cong01 : item.CongKP01 == -1 ? 0 : item.Cong01 - item.Day1) + (item.CongKP02 == 0 ? item.Cong02 : item.CongKP02 == -1 ? 0 : item.Cong02 - item.Day2) + (item.CongKP03 == 0 ? item.Cong03 : item.CongKP03 == -1 ? 0 : item.Cong03 - item.Day3) + (item.CongKP04 == 0 ? item.Cong04 : item.CongKP04 == -1 ? 0 : item.Cong04 - item.Day4) + (item.CongKP05 == 0 ? item.Cong05 : item.CongKP05 == -1 ? 0 : item.Cong05 - item.Day5) + (item.CongKP06 == 0 ? item.Cong06 : item.CongKP06 == -1 ? 0 : item.Cong06 - item.Day6) +
                                (item.CongKP07 == 0 ? item.Cong07 : item.CongKP07 == -1 ? 0 : item.Cong07 - item.Day7) + (item.CongKP08 == 0 ? item.Cong08 : item.CongKP08 == -1 ? 0 : item.Cong08 - item.Day8) + (item.CongKP09 == 0 ? item.Cong09 : item.CongKP09 == -1 ? 0 : item.Cong09 - item.Day9) + (item.CongKP10 == 0 ? item.Cong10 : item.congKP10 == -1 ? 0 : item.Cong10 - item.Day10) + (item.CongKP11 == 0 ? item.Cong11 : item.CongKP11 == -1 ? 0 : item.Cong11 - item.Day11) + (item.CongKP12 ? item.Cong12 : item.CongKP12 == -1 ? 0 : item.Cong12 - item.Day12) + (item.CongKP13 == 0 ? item.Cong13 : item.CongKP13 == -1 ? 0 : item.Cong13 - item.Day13) + (item.CongKP14 == 0 ? item.Cong14 : item.CongKP14 == -1 ? 0 : item.Cong14 - item.Day14) +
                                (item.CongKP15 == 0 ? item.Cong15 : item.CongKP15 == -1 ? 0 : item.Cong15 - item.Day15) + (item.CongKP16 == 0 ? item.Cong16 : item.CongKP16 == -1 ? 0 : item.Cong16 - item.Day16) + (item.CongKP17 == 0 ? item.Cong17 : item.CongKP17 == -1 ? 0 : item.Cong17 - item.Day17) + (item.CongKP18 == 0 ? item.Cong18 : item.CongKP18 == -1 ? 0 : item.Cong18 - item.Day18) + (item.CongKP19 == 0 ? item.Cong19 : item.CongKP19 == -1 ? 0 : item.Cong19 - item.Day19) + (item.CongKP20 == 0 ? item.Cong20 : item.CongKP20 == -1 ? 0 : item.Cong20 - item.Day20) + (item.CongKP21 == 0 ? item.Cong21 : item.CongKP21 == -1 ? 0 : item.Cong21 - item.Day21) + (item.CongKP22 == 0 ? item.Cong22 : item.CongKP22 == -1 ? 0 : item.Cong22 - item.Day22) + (item.CongKP23 == 0 ? item.Cong23 : item.CongKP23 == -1 ? 0 : item.Cong23 - item.Day23) + (item.CongKP24 == 0 ? item.Cong24 : item.CongKP24 == -1 ? 0 : item.Cong24 - item.Day24) +
                                (item.CongKP25 == 0 ? item.Cong25 : item.CongKP25 == -1 ? 0 : item.Cong25 - item.Day25) + (item.CongKP26 == 0 ? item.Cong26 : item.CongKP26 == -1 ? 0 : item.Cong26 - item.Day26) + (item.CongKP27 == 0 ? item.Cong27 : item.CongKP27 == -1 ? 0 : item.Cong27 - item.Day27) + (item.CongKP28 == 0 ? item.Cong28 : item.congKP28 == -1 ? 0 : item.Cong28 - item.Day28) + (item.CongKP29 == 0 ? item.Cong29 : item.CongKP29 == -1 ? 0 : item.Cong29 - item.Day29) + (item.CongKP30 == 0 ? item.Cong30 : item.CongKP30 == -1 ? 0 : item.Cong30 - item.Day30) + (item.CongKP31 == 0 ? item.Cong31 : item.CongKP31 == -1 ? 0 : item.Cong31 - item.Day31)).toFixed(1),
                            //KPHEP: ((item.CongKP01 == 0 ? item.Cong01 : item.CongKP01 == 0.5 ? item.Cong01 / 2 : 0) + (item.CongKP02 == 0 ? item.Cong02 : item.CongKP02 == 0.5 ? item.Cong02 / 2 : 0) + (item.CongKP03 == 0 ? item.Cong03 : item.CongKP03 == 0.5 ? item.Cong03 / 2 : 0) + (item.CongKP04 == 0 ? item.Cong04 : item.CongKP04 == 0.5 ? item.Cong04 / 2 : 0) + (item.CongKP05 == 0 ? item.Cong05 : item.CongKP05 == 0.5 ? item.Cong05 / 2 : 0) + (item.CongKP06 == 0 ? item.Cong06 : item.CongKP06 == 0.5 ? item.Cong06 / 2 : 0) +
                            //    (item.CongKP07 == 0 ? item.Cong07 : item.CongKP07 == 0.5 ? item.Cong07 / 2 : 0) + (item.CongKP08 == 0 ? item.Cong08 : item.CongKP08 == 0.5 ? item.Cong08 / 2 : 0 )+ (item.CongKP09 == 0 ? item.Cong09 : item.CongKP09 == 0.5 ? item.Cong09 / 2 : 0) + (item.CongKP10 == 0 ? item.Cong10 : item.congKP10 == 0.5 ? item.Cong10 / 2 : 0) + (item.CongKP11 == 0 ? item.Cong11 : item.CongKP11 == 0.5 ? item.Cong11 / 2 : 0) + (item.CongKP12 ? item.Cong12 : item.CongKP12 == 0.5 ? item.Cong12 / 2 : 0) + (item.CongKP13 == 0 ? item.Cong13 : item.CongKP13 == 0.5 ? item.Cong13 / 2 : 0) + (item.CongKP14 == 0 ? item.Cong14 : item.CongKP14 == 0.5 ? item.Cong14 / 2 : 0) +
                            //    (item.CongKP15 == 0 ? item.Cong15 : item.CongKP15 == 0.5 ? item.Cong15 / 2 : 0) + (item.CongKP16 == 0 ? item.Cong16 : item.CongKP16 == 0.5 ? item.Cong16 / 2 : 0 )+ (item.CongKP17 == 0 ? item.Cong17 : item.CongKP17 == 0.5 ? item.Cong17 / 2 : 0 )+( item.CongKP18 == 0 ? item.Cong18 : item.CongKP18 == 0.5 ? item.Cong18 / 2 : 0 )+ (item.CongKP19 == 0 ? item.Cong19 : item.CongKP19 == 0.5 ? item.Cong19 / 2 : 0) + (item.CongKP20 == 0 ? item.Cong20 : item.CongKP20 == 0.5 ? item.Cong20 / 2 : 0) + (item.CongKP21 == 0 ? item.Cong21 : item.CongKP21 == 0.5 ? item.Cong21 / 2 : 0) + (item.CongKP22 == 0 ? item.Cong22 : item.CongKP22 == 0.5 ? item.Cong22 / 2 : 0) + (item.CongKP23 == 0 ? item.Cong23 : item.CongKP23 == 0.5 ? item.Cong23 / 2 : 0) + (item.CongKP24 == 0 ? item.Cong24 : item.CongKP24 == 0.5 ? item.Cong24 / 2 : 0) +
                            //    (item.CongKP25==0?item.Cong25:item.CongKP25==0.5?item.Cong25/2:0) + (item.CongKP26==0?item.Cong26:item.CongKP26==0.5?item.Cong26/2:0) + (item.CongKP27==0?item.Cong27:item.CongKP27==0.5?item.Cong27/2:0) + (item.CongKP28==0?item.Cong28:item.congKP28==0.5?item.Cong28/2:0 )+ (item.CongKP29==0?item.Cong29:item.CongKP29==0.5?item.Cong29/2:0) + (item.CongKP30==0?item.Cong30:item.CongKP30==0.5?item.Cong30/2:0) + (item.CongKP31==0?item.Cong31:item.CongKP31==0.5?item.Cong31/2:0)).toFixed(1),
                          
                        });
                      
                       
                      
                    });
                    common.wrapPaging1('.paginationUL',response.Data.TotalRow, function () {
                        loadData(false);
                    }, isPageChanged);
                    if (render != '') {
                        $('#tbl-statistical-shift').html(render);
                    }
                }
                else {
                    render ='<tr>' +
                        '    <td colspan="32">Không có dữ liệu</td>' +
                        '</tr>';
                    $('#tbl-statistical-shift').html(render);
                }
                $('#lblTotalRecords').text(response.Data.TotalRow);
            },
            error: function (status) {
                common.notify('Cannot loading data', 'error');
            }
        });
    }
    function loadDataCong(isPageChanged) {
        var template = $('#table-template-statistical-shift').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {
                staffCode: $('#txtStaffCode').val(),
                departmentId: $('#ddlDepartment').val(),
                shiftName: $('#ddlShift').val(),
                year: $('#txtYear').val(),
                month: $('#ddlMonth').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '../Statistical/GetStatisticalFaceShift',
            dataType: 'json',
            success: function (response) {
                console.log(response.Data);
                if (response.Data.TotalRow > 0) {
                    $.each(response.Data.Items, function (i, item) {
                        render += Mustache.render(template, {
                            STT: (common.configs.pageIndex * common.configs.pageSize - common.configs.pageSize + 1) + i,
                            StaffCode: item.StaffCode,
                            StaffName: item.StaffName,
                            DepartmentName: item.DepartmentName,
                            ShiftName: item.ShiftName,
                            Day1: item.CongKP01 == 1 ? 'X' : item.CongKP01 == 0.5 ? 'X/2' : item.CongKP01 == 0 ? 'K' : '',
                            Day2: item.CongKP02 == 1 ? 'X' : item.CongKP02 == 0.5 ? 'X/2' : item.CongKP02 == 0 ? 'K' : '',
                            Day3: item.CongKP03 == 1 ? 'X' : item.CongKP03 == 0.5 ? 'X/2' : item.CongKP03 == 0 ? 'K' : '',
                            Day4: item.CongKP04 == 1 ? 'X' : item.CongKP04 == 0.5 ? 'X/2' : item.CongKP04 == 0 ? 'K' : '',
                            Day5: item.CongKP05 == 1 ? 'X' : item.CongKP05 == 0.5 ? 'X/2' : item.CongKP05 == 0 ? 'K' : '',
                            Day6: item.CongKP06 == 1 ? 'X' : item.CongKP06 == 0.5 ? 'X/2' : item.CongKP06 == 0 ? 'K' : '',
                            Day7: item.CongKP07 == 1 ? 'X' : item.CongKP07 == 0.5 ? 'X/2' : item.CongKP07 == 0 ? 'K' : '',
                            Day8: item.CongKP08 == 1 ? 'X' : item.CongKP08 == 0.5 ? 'X/2' : item.CongKP08 == 0 ? 'K' : '',
                            Day9: item.CongKP09 == 1 ? 'X' : item.CongKP09 == 0.5 ? 'X/2' : item.CongKP09 == 0 ? 'K' : '',
                            Day10: item.CongKP10 == 1 ? 'X' : item.CongKP10 == 0.5 ? 'X/2' : item.CongKP10 == 0 ? 'K' : '',
                            Day11: item.CongKP11 == 1 ? 'X' : item.CongKP11 == 0.5 ? 'X/2' : item.CongKP11 == 0 ? 'K' : '',
                            Day12: item.CongKP12 == 1 ? 'X' : item.CongKP12 == 0.5 ? 'X/2' : item.CongKP12 == 0 ? 'K' : '',
                            Day13: item.CongKP13 == 1 ? 'X' : item.CongKP13 == 0.5 ? 'X/2' : item.CongKP13 == 0 ? 'K' : '',
                            Day14: item.CongKP14 == 1 ? 'X' : item.CongKP14 == 0.5 ? 'X/2' : item.CongKP14 == 0 ? 'K' : '',
                            Day15: item.CongKP15 == 1 ? 'X' : item.CongKP15 == 0.5 ? 'X/2' : item.CongKP15 == 0 ? 'K' : '',
                            Day16: item.CongKP16 == 1 ? 'X' : item.CongKP16 == 0.5 ? 'X/2' : item.CongKP16 == 0 ? 'K' : '',
                            Day17: item.CongKP17 == 1 ? 'X' : item.CongKP17 == 0.5 ? 'X/2' : item.CongKP17 == 0 ? 'K' : '',
                            Day18: item.CongKP18 == 1 ? 'X' : item.CongKP18 == 0.5 ? 'X/2' : item.CongKP18 == 0 ? 'K' : '',
                            Day19: item.CongKP19 == 1 ? 'X' : item.CongKP19 == 0.5 ? 'X/2' : item.CongKP29 == 0 ? 'K' : '',
                            Day20: item.CongKP20 == 1 ? 'X' : item.CongKP20 == 0.5 ? 'X/2' : item.CongKP20 == 0 ? 'K' : '',
                            Day21: item.CongKP21 == 1 ? 'X' : item.CongKP21 == 0.5 ? 'X/2' : item.CongKP21 == 0 ? 'K' : '',
                            Day22: item.CongKP22 == 1 ? 'X' : item.CongKP22 == 0.5 ? 'X/2' : item.CongKP22 == 0 ? 'K' : '',
                            Day23: item.CongKP23 == 1 ? 'X' : item.CongKP23 == 0.5 ? 'X/2' : item.CongKP23 == 0 ? 'K' : '',
                            Day24: item.CongKP24 == 1 ? 'X' : item.CongKP24 == 0.5 ? 'X/2' : item.CongKP24 == 0 ? 'K' : '',
                            Day25: item.CongKP25 == 1 ? 'X' : item.CongKP25 == 0.5 ? 'X/2' : item.CongKP25 == 0 ? 'K' : '',
                            Day26: item.CongKP26 == 1 ? 'X' : item.CongKP26 == 0.5 ? 'X/2' : item.CongKP26 == 0 ? 'K' : '',
                            Day27: item.CongKP27 == 1 ? 'X' : item.CongKP27 == 0.5 ? 'X/2' : item.CongKP27 == 0 ? 'K' : '',
                            Day28: item.CongKP28 == 1 ? 'X' : item.CongKP28 == 0.5 ? 'X/2' : item.CongKP28 == 0 ? 'K' : '',
                            Day29: item.CongKP29 == 1 ? 'X' : item.CongKP29 == 0.5 ? 'X/2' : item.CongKP29 == 0 ? 'K' : '',
                            Day30: item.CongKP30 == 1 ? 'X' : item.CongKP30 == 0.5 ? 'X/2' : item.CongKP30 == 0 ? 'K' : '',
                            Day31: item.CongKP31 == 1 ? 'X' : item.CongKP31 == 0.5 ? 'X/2' : item.CongKP31 == 0 ? 'K' : '',
                            //StyleDay1: item.Day1.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay2: item.Day2.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay3: item.Day3.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay4: item.Day4.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay5: item.Day5.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay6: item.Day6.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay7: item.Day7.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay8: item.Day8.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay9: item.Day9.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay10: item.Day10.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay11: item.Day11.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay12: item.Day12.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay13: item.Day13.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay14: item.Day14.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay15: item.Day15.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay16: item.Day16.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay17: item.Day17.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay18: item.Day18.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay19: item.Day19.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay20: item.Day20.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay21: item.Day21.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay22: item.Day22.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay23: item.Day23.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay24: item.Day24.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay25: item.Day25.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay26: item.Day26.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay27: item.Day27.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay28: item.Day28.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay29: item.Day29.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay30: item.Day30.toFixed(1) < 8 ? 'color:red' : '',
                            //StyleDay31: item.Day31.toFixed(1) < 8 ? 'color:red' : '',
                            MUON: item.Late,
                            SOM: item.Soon,
                            CHINH: ((item.CongKP01 == -1 ? 0 : item.CongKP01)+ (item.CongKP02 == -1 ? 0 : item.CongKP02)+ (item.CongKP03 == -1 ? 0 : item.CongKP03) + (item.CongKP04 == -1 ? 0 : item.CongKP04) + (item.CongKP05 == -1 ? 0 : item.CongKP05) + (item.CongKP06 == -1 ? 0 : item.CongKP06) +
                                (item.CongKP07 == -1 ? 0 : item.CongKP07) + (item.CongKP08 == -1 ? 0 : item.CongKP08)  + (item.CongKP09 == -1 ? 0 : item.CongKP09)  + (item.CongKP10 == -1 ? 0 : item.CongKP10) + (item.CongKP11 == -1 ? 0 : item.CongKP11)  + (item.CongKP12==-1 ? 0 : item.CongKP12)  + (item.CongKP13 == -1 ? 0 : item.CongKP13)  + (item.CongKP14 == -1 ? 0 : item.CongKP14)  +
                                    (item.CongKP15 == -1 ? 0 : item.CongKP15)  + (item.CongKP16 == -1 ? 0 : item.CongKP16)  + (item.CongKP17 == -1 ? 0 : item.CongKP17)  + (item.CongKP18 == -1 ? 0 : item.CongKP18)  + (item.CongKP19 == -1 ? 0 : item.CongKP19)  + (item.CongKP20 == -1 ? 0 : item.CongKP20)  + (item.CongKP21 == -1 ? 0 : item.CongKP21)  + (item.CongKP22 == -1 ? 0 : item.CongKP22)  + (item.CongKP23 == -1 ? 0 : item.CongKP23)  + (item.CongKP24 == -1 ? 0 : item.CongKP24)  +
                                        (item.CongKP25 == -1 ? 0 : item.CongKP25)  + (item.CongKP26 == -1 ? 0 : item.CongKP26)  + (item.CongKP27 == -1 ? 0 : item.CongKP27)  + (item.CongKP28 == -1 ? 0 : item.congKP28)  + (item.CongKP29 == -1 ? 0 : item.CongKP29)  + (item.CongKP30 == -1 ? 0 : item.CongKP30)  + (item.CongKP31 == -1 ? 0 : item.CongKP31)).toFixed(1),
                            KPHEP: ((item.CongKP01 == 0 ? 1 : item.CongKP01 == 0.5 ? item.CongKP01 : 0) + (item.CongKP02 == 0 ? 1 : item.CongKP02 == 0.5 ? item.CongKP02 : 0) + (item.CongKP03 == 0 ? 1 : item.CongKP03 == 0.5 ? item.CongKP03 : 0) + (item.CongKP04 == 0 ? 1 : item.CongKP04 == 0.5 ? item.CongKP04 : 0) + (item.CongKP05 == 0 ? 1 : item.CongKP05 == 0.5 ? item.CongKP05 : 0) + (item.CongKP06 == 0 ? 1 : item.CongKP06 == 0.5 ? item.CongKP06 : 0) +
                                (item.CongKP07 == 0 ? 1 : item.CongKP07 == 0.5 ? item.CongKP07 : 0) + (item.CongKP08 == 0 ? 1 : item.CongKP08 == 0.5 ? item.CongKP08 : 0) + (item.CongKP09 == 0 ? 1 : item.CongKP09 == 0.5 ? item.CongKP09 : 0 )+ (item.CongKP10 == 0 ? 1 : item.congKP10 == 0.5 ? item.CongKP10 : 0) + (item.CongKP11 == 0 ? 1 : item.CongKP11 == 0.5 ? item.CongKP11 : 0) + (item.CongKP12 == 0 ? 1 : item.CongKP12 == 0.5 ? item.CongKP12 : 0) + (item.CongKP13 == 0 ? 1 : item.CongKP13 == 0.5 ? item.CongKP13 : 0) +( item.CongKP14 == 0 ? 1 : item.CongKP14 == 0.5 ? item.CongKP14  : 0) +
                                    (item.CongKP15 == 0 ? 1 : item.CongKP15 == 0.5 ? item.CongKP15 : 0) + (item.CongKP16 == 0 ? 1 : item.CongKP16 == 0.5 ? item.CongKP16 : 0) + (item.CongKP17 == 0 ? 1 : item.CongKP17 == 0.5 ? item.CongKP17 : 0) + (item.CongKP18 == 0 ? 1 : item.CongKP18 == 0.5 ? item.CongKP18 : 0) + (item.CongKP19 == 0 ? 1 : item.CongKP19 == 0.5 ? item.CongKP19 : 0) + (item.CongKP20 == 0 ? 1 : item.CongKP20 == 0.5 ? item.CongKP20 : 0) + (item.CongKP21 == 0 ? 1 : item.CongKP21 == 0.5 ? item.CongKP21 : 0) + (item.CongKP22 == 0 ? 1 : item.CongKP22 == 0.5 ? item.CongKP22 : 0) + (item.CongKP23 == 0 ? 1 : item.CongKP23 == 0.5 ? item.CongKP23 : 0) + (item.CongKP24 == 0 ? 1 : item.CongKP24 == 0.5 ? item.CongKP24  : 0) +
                                        (item.CongKP25 == 0 ? 1 : item.CongKP25 == 0.5 ? item.CongKP25 : 0) +( item.CongKP26 == 0 ? 1 : item.CongKP26 == 0.5 ? item.CongKP26 : 0) + (item.CongKP27 == 0 ? 1 : item.CongKP27 == 0.5 ? item.CongKP27 : 0) + (item.CongKP28 == 0 ? 1 : item.congKP28 == 0.5 ? item.CongKP28 : 0 )+ (item.CongKP29 == 0 ? 1 : item.CongKP29 == 0.5 ? item.CongKP29 : 0) +( item.CongKP30 == 0 ? 1 : item.CongKP30 == 0.5 ? item.CongKP30 : 0) + (item.CongKP31 == 0 ? 1 : item.CongKP31 == 0.5 ? item.CongKP31: 0)).toFixed(1),

                        });



                    });
                    common.wrapPaging1('.paginationUL', response.Data.TotalRow, function () {
                        loadDataCong(false);
                    }, isPageChanged);
                    if (render != '') {
                        $('#tbl-statistical-shift').html(render);
                    }
                }
                else {
                    render = '<tr>' +
                        '    <td colspan="32">Không có dữ liệu</td>' +
                        '</tr>';
                    $('#tbl-statistical-shift').html(render);
                }
                $('#lblTotalRecords').text(response.Data.TotalRow);
            },
            error: function (status) {
                common.notify('Cannot loading data', 'error');
            }
        });
    }
    function UpdateData() {
        $.ajax({
            url: '../Statistical/UpdateDataFaceShift',
            type: 'GET',
            data: {
                year: $('#txtYear').val(),
                month: $('#ddlMonth').val()
            },
            dataType: 'json',
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                if (response.Success == true) {
                    common.popupOk("-----", "Cập nhật công tháng " + $('#ddlMonth').val() + "/" + $('#txtYear').val()+" thành công", "success")
                   
                    //loadData(true);
                }
                else {
                    common.popupOk("Lỗi", response.Message, "error")
                        //common.notify(response.Message, 'error');
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Can not load ajax function UpdateData', 'error');
                common.stopLoading();
            }
        });
    }

   

    function ExportExcel() {
        $.ajax({
            url: '../Statistical/ExportExcelFaceShift',
            type: 'GET',
            data: {
                staffCode: $('#txtStaffCode').val(),
                departmentId: $('#ddlDepartment').val(),
                departmentName: $('#ddlDepartment option:selected').text(),
                shiftName: $('#ddlShift').val(),
                year: $('#txtYear').val(),
                month: $('#ddlMonth').val()
            },
            dataType: 'json',
            success: function (response) {
                if (response.Success == true) {
                    window.location.href = response.Data;
                }
                else {
                    common.notify(response.Message, 'error');
                }
            },
            error: function () {
                common.notify('Can not load ajax function ExportExcel', 'error');
            }
        });
    }
}