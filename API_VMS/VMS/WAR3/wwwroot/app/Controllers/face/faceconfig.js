﻿var faceconfigController = function () {
    var TKSettingId = 0;
    this.initialize = function () {
        
       
      
        //$.when(loadData()).done(function (x) {
        //    $('.ckcheck1').click(function () {
        //        alert(2);
        //        if ($(this).is(':checked')) {
        //            alert(3);
        //        } else {
        //            alert(4);
        //        }
        //        console.log(checkxoa);
        //    });
        //});
        //loadData();
        registerEvents();
        registerControls();
        //loadDataTreeView();
    }

    function registerEvents() {
      
        
        $('#ddlDevice').on('change', function () {
            var that = $(this).find(':selected').data("inout");
            var onoff = $(this).find(':selected').data("onoff");
            
            if ($('#ddlDevice').val() != null && $('#ddlDevice').val() != "" && $('#ddlDevice').val() != undefined) {
                TKSettingId = $(this).find(':selected').data("setting");
                if (TKSettingId == 0) {
                    $('#btnHuy').prop('hidden', true);
                }
                else {
                    $('#btnHuy').prop('hidden', false);
                }
                if (that == true) {
                    $('#ddlStyle').val(1)
                }
                else {
                    $('#ddlStyle').val(2)
                }
                if (onoff == true) {
                    $('#btnOn').prop('hidden', true);
                    $('#btnOff').prop('hidden', false);
                }
                else {
                    $('#btnOn').prop('hidden', false);
                    $('#btnOff').prop('hidden', true);
                }
            }
            else {
                $('#btnOn').prop('hidden', true);
                $('#btnOff').prop('hidden', true);
                $('#btnHuy').prop('hidden', true);
            }

        });
        $('#btnOn').on('click', function () {
            common.confirm('Bạn có chắc chắn muốn bật thiết bị không?', function () {
                UpdateTKSetting(true);
            });
            
        });
        $('#btnOff').on('click', function () {
            common.confirm('Bạn có chắc chắn muốn tắt thiết bị không?', function () {
                UpdateTKSetting(false);
            });
            
        });
        $('#btnHuy').on('click', function () {
            common.confirm('Bạn có chắc chắn muốn hủy thiết lập không?', function () {
                CannelTKSetting();
            });

        });
    }
    function registerControls() {
       
       
        dropdown.GetDevice($('#ddlDevice'), '');
        $('#ddlConfig').multiselect({

            columns: 1,
            placeholder: '--Chọn--',
            search: true,
            selectAll: true
        });
    }
    function UpdateTKSetting(OnOff) {
        TKSettingId = $('#ddlDevice').find(':selected').data("setting");
        var isIN = false;
        var isOut = false;
        if ($('#ddlStyle').val() == 1) {
            isIN = true;
        }
        else {
            isOut = true;
        }
            $.ajax({
                type: "POST",
                url: "../Face/UpdateTKSetting",
                data: {
                    id: TKSettingId,
                    deviceId: $('#ddlDevice').val(),
                    isCheckIn: isIN,
                    isCheckOut: isOut,
                    isTKOn: OnOff

                },
                dataType: "json",
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    console.log(response);
                    if (response.Success == true) {
                        common.notify('Cập nhật thành công', 'success');
                       
                        dropdown.GetDevice($('#ddlDevice'), $('#ddlDevice').val());
                        if (OnOff == true) {
                            $('#btnOn').prop('hidden', true);
                            $('#btnOff').prop('hidden', false);
                        }
                        else {
                            $('#btnOn').prop('hidden', false);
                            $('#btnOff').prop('hidden', true);
                        }
                        $('#btnHuy').prop('hidden', false);
                    }
                    else {

                        
                            common.notify(response.Message, 'error');
                    }
                    common.stopLoading();
                },
                error: function () {
                    common.notify('Can not load ajax function UpdateTKSetting', 'error');
                    common.stopLoading();
                }
            });

    }
    
    function CannelTKSetting() {
        TKSettingId = $('#ddlDevice').find(':selected').data("setting");
        var isIN = false;
        var isOut = false;
        if ($('#ddlStyle').val() == 1) {
            isIN = true;
        }
        else {
            isOut = true;
        }
        $.ajax({
            type: "GET",
            url: "../Face/CannelTKSetting",
            data: {
                id: TKSettingId

            },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                console.log(response);
                if (response.Success == true) {
                    common.notify('Hủy thiết lập thành công', 'success');

                    dropdown.GetDevice($('#ddlDevice'), $('#ddlDevice').val());
                    
                    $('#btnOn').prop('hidden', false);
                    $('#btnOff').prop('hidden', true);
                    $('#btnHuy').prop('hidden', true);
                }
                else {


                    common.notify(response.Message, 'error');
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Can not load ajax function UpdateTKSetting', 'error');
                common.stopLoading();
            }
        });

    }

}