﻿var faceshiftconfigController = function () {
    var checkView = 0;
    var remote = 0;
    var checkxoa = '';
    var checkxoashift = '';
    var checkxoashift1 = '';
    var data1 = [];
    var data2 = [];
    var day = 0;
    var datalisstshift = [];
    this.initialize = function () {
        
       
      
        //$.when(loadData()).done(function (x) {
        //    $('.ckcheck1').click(function () {
        //        alert(2);
        //        if ($(this).is(':checked')) {
        //            alert(3);
        //        } else {
        //            alert(4);
        //        }
        //        console.log(checkxoa);
        //    });
        //});
        //loadData();
        registerEvents();
        registerControls();
        //loadDataTreeView();
       
    }

    function registerEvents() {
      
        
       
        $('#btnSearch').on('click', function () {
           
            $('#frmSearch').validate({
                errorClass: 'red',
                ignore: [],
                lang: 'vi',
                rules: {
                    txtFromDate: {
                        required: true
                    },
                }
            });
            if ($('#frmSearch').valid()) {
                var SerachFromDate = $('#txtFromDate').val().split("/")[2] + $('#txtFromDate').val().split("/")[1] + $('#txtFromDate').val().split("/")[0];
                var SerachToDate = $('#txtToDate').val().split("/")[2] + $('#txtToDate').val().split("/")[1] + $('#txtToDate').val().split("/")[0];
                var nowFrom1 = new Date();
                day = nowFrom1.getDay();
                var ngay1 = nowFrom1.toLocaleDateString().split("/")[0];
                var thang1 = nowFrom1.toLocaleDateString().split("/")[1];
                var nam1 = nowFrom1.toLocaleDateString().split("/")[2];
                if (ngay1.length == 1) {
                    ngay1 = '0' + ngay1;
                }
                if (thang1.length == 1) {
                    thang1 = '0' + thang1;
                }
                var nowFrom = nam1+thang1+ngay1;
                
                if (parseInt(nowFrom) > parseInt(SerachToDate)) {
                    day = 10;
                }
                else {
                    if (parseInt(nowFrom) < parseInt(SerachFromDate)) {
                        
                        day = 0;
                    }
                }

                var ngayTK = $('#txtFromDate').val().split("/")[2] + '/' + $('#txtFromDate').val().split("/")[1] + '/' + $('#txtFromDate').val().split("/")[0];
                var table = $('#table1').DataTable();

                table.destroy();
                LoadNgayByTuan(ngayTK);
                GetListFaceShift();
            }
           
        });
       
       
        $('#btnSaveFaceCa').on('click', function () {
            
            data1 = [];
            var tabel1 = document.getElementById('tbl-face-shift');
            var rijen1 = tabel1.getElementsByTagName('td');
            //var tabel2 = document.getElementById('tbl-face-shift');
            var rijen2 = tabel1.getElementsByTagName('input');
            var a = 0;
            //console.log(rijen1);
            //var rijen1 = tabel1.getElementsByClassName('txtStaffCode')
            for (c = 0; c < rijen1.length; c=c+17) {
                data1.push({
                    STT: rijen1[c].innerHTML,
                    FaceId: rijen1[c + 1].getAttribute('data-id'),
                    FaceName: rijen1[c + 1].innerHTML,
                    StaffCode: rijen1[c + 2].innerHTML,
                    Day1: $("tbody tr:nth-child(" + (rijen1[c].innerHTML) + ") > td:nth-child( " + (4) + " ) select option:selected").val(),
                    Day2: $("tbody tr:nth-child(" + (rijen1[c].innerHTML) + ") > td:nth-child( " + (6) + " ) select option:selected").val(),
                    Day3: $("tbody tr:nth-child(" + (rijen1[c].innerHTML) + ") > td:nth-child( " + (8) + " ) select option:selected").val(),
                    Day4: $("tbody tr:nth-child(" + (rijen1[c].innerHTML) + ") > td:nth-child( " + (10) + " ) select option:selected").val(),
                    Day5: $("tbody tr:nth-child(" + (rijen1[c].innerHTML) + ") > td:nth-child( " + (12) + " ) select option:selected").val(),
                    Day6: $("tbody tr:nth-child(" + (rijen1[c].innerHTML) + ") > td:nth-child( " + (14) + " ) select option:selected").val(),
                    Day7: $("tbody tr:nth-child(" + (rijen1[c].innerHTML) + ") > td:nth-child( " + (16) + " ) select option:selected").val(),
                    RiceShift1: rijen2[a].checked == true && rijen2[a + 1].checked == true ? 3 : rijen2[a].checked == true && rijen2[a + 1].checked == false ? 1 : rijen2[a].checked == false && rijen2[a + 1].checked == true ? 2 : 0,
                    RiceShift2: rijen2[a+2].checked == true && rijen2[a + 3].checked == true ? 3 : rijen2[a+2].checked == true && rijen2[a + 3].checked == false ? 1 : rijen2[a+2].checked == false && rijen2[a + 3].checked == true ? 2 : 0,
                    RiceShift3: rijen2[a+4].checked == true && rijen2[a + 5].checked == true ? 3 : rijen2[a+4].checked == true && rijen2[a + 5].checked == false ? 1 : rijen2[a+4].checked == false && rijen2[a + 5].checked == true ? 2 : 0,
                    RiceShift4: rijen2[a+6].checked == true && rijen2[a + 7].checked == true ? 3 : rijen2[a+6].checked == true && rijen2[a + 7].checked == false ? 1 : rijen2[a+6].checked == false && rijen2[a + 7].checked == true ? 2 : 0,
                    RiceShift5: rijen2[a+8].checked == true && rijen2[a + 9].checked == true ? 3 : rijen2[a+8].checked == true && rijen2[a + 9].checked == false ? 1 : rijen2[a+8].checked == false && rijen2[a + 9].checked == true ? 2 : 0,
                    RiceShift6: rijen2[a+10].checked == true && rijen2[a + 11].checked == true ? 3 : rijen2[a+10].checked == true && rijen2[a + 11].checked == false ? 1 : rijen2[a+10].checked == false && rijen2[a + 11].checked == true ? 2 : 0,
                    RiceShift7: rijen2[a+12].checked == true && rijen2[a + 13].checked == true ? 3 : rijen2[a+12].checked == true && rijen2[a + 13].checked == false ? 1 : rijen2[a+12].checked == false && rijen2[a + 13].checked == true ? 2 : 0,
                });
                a = a + 14;
            }
            console.log(data1);

            AddFaceShift();
           

        });
        $('#txtFromDate').on('change', function () {
            var fromdate = $('#txtFromDate').val();
            var date = fromdate.split("/")[2] + '-' + fromdate.split("/")[1] + '-' + fromdate.split("/")[0];
            var checkdate = new Date(date);
            var daynow = checkdate.getDate();
            var checkday = checkdate.getDay();
            if (checkday == 1) {
                checkdate.setDate(daynow + 6);
                var ngay = checkdate.toLocaleDateString().split("/")[0];
                var thang = checkdate.toLocaleDateString().split("/")[1];
                var nam = checkdate.toLocaleDateString().split("/")[2];
                if (ngay.length == 1) {
                    ngay = '0' + ngay;
                }
                if (thang.length == 1) {
                    thang = '0' + thang;
                }
                $('#txtToDate').val(ngay + '/' + thang + '/' + nam);

            }
            else {
                $('#txtFromDate').val('');
                $('#txtToDate').val('');
                //$('#txtFromDate').focus();
                common.popupOk('Cảnh báo', 'Tuần từ phải là thứ 2. Mời bạn chọn lại', 'error');
            }
        });
        $('body').on('change', '.ShiftConbo', function (e) {
            var checkid = $(this).data('id');
           
            var tabel1 = document.getElementById('tbl-face-shift');
            //var rijen1 = tabel1.getElementsByTagName('td');
            //var tabel2 = document.getElementById('tbl-face-shift');
            var rijen2 = tabel1.getElementsByTagName('input');
            var gioCheck = $(this).find(':selected').attr('data-gio');
            if (gioCheck >= 10) {
                rijen2[checkid * 2].checked = true;
                rijen2[(checkid * 2) + 1].checked = true;
            }
            else {
                if (gioCheck == undefined || gioCheck == null || gioCheck == "") {
                    rijen2[checkid * 2].checked = false;
                    rijen2[(checkid * 2) + 1].checked = false;
                }
                else {
                    rijen2[checkid * 2].checked = true;
                    rijen2[(checkid * 2) + 1].checked = false;
                }
                
            }
            
            
        });
        $('#btnExport').on('click', function () {
            ExportExcel();

        });
       
     
    }
    function registerControls() {
       
        $('.date-picker').datepicker({
            autoclose: true,
            dateFormat: 'dd/mm/yy',
            //format: 'dd/MM/yyyy',
            todayHighlight: true
        }); 
        $.when(dropdown.GetSettingShiftNoNull($('#ddlShift'), '')).done(function (x) {
            LoadTBody();
        });
        //dropdown.GetSettingShiftNoNull($('#ddlShift'), ''); 
        dropdown.GetDepartmentNoNull($('#ddlDepartment'), '');

        var now1 = new Date();
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
         day = now1.getDay();
        var dd = now1.getDate();
        var dd1 = now1.getDate();
        //var dayName = days[now1.getDay()];
        //var tomorrow = new Date(now1 -1);
        //var abc=tomorrow.setDate(now1.getDate() + 1);
        //console.log(tomorrow);

        if (day !== 1) {
            for (var i = day; i >= 1; i--) {
                var now = new Date();
                
                now.setDate(dd);
                var a = i + 1;
                var x = document.getElementById("T" + a);
                x.innerHTML = now.toLocaleDateString();
                dd = dd - 1;
            }

            for (var i = day + 1; i <= 7; i++) {
                dd1 = dd1 + 1;
                var now = new Date();
                //var b = i - 1;
                now.setDate(dd1);
                var a = i + 1;
                var x = document.getElementById("T" + a);
                x.innerHTML = now.toLocaleDateString();
               
            }
            var nowfrom = new Date();
            var nowto = new Date();
            nowfrom.setDate(dd + 1);
            nowto.setDate(dd1);
            var ngayfrom = nowfrom.toLocaleDateString().split("/")[0];
            var thangfrom = nowfrom.toLocaleDateString().split("/")[1];
            var namfrom = nowfrom.toLocaleDateString().split("/")[2];
            var ngayto = nowto.toLocaleDateString().split("/")[0];
            var thangto = nowto.toLocaleDateString().split("/")[1];
            var namto = nowto.toLocaleDateString().split("/")[2];
            if (ngayfrom.length == 1) {
                ngayfrom = '0' + ngayfrom;
            }
            if (thangfrom.length == 1) {
                thangfrom = '0' + thangfrom;
            }
            if (ngayto.length == 1) {
                ngayto = '0' + ngayto;
            }
            if (thangto.length == 1) {
                thangto = '0' + thangto;
            }
            $('#txtFromDate').val(ngayfrom + '/' + thangfrom + '/' + namfrom);
            $('#txtToDate').val(ngayto + '/' + thangto + '/' + namto);
        }
        else {
           
           
            for (var i = 1; i <= 7; i++) {
                var now = new Date();
                now.setDate(dd);
                var a = i + 1;
                //console.log(tomorrow);
                var x = document.getElementById("T"+a);
                x.innerHTML = now.toLocaleDateString();
                dd = dd + 1;
            }
            var nowfrom = new Date();
            var nowto = new Date();
            nowto.setDate(dd-1);
            var ngayfrom = nowfrom.toLocaleDateString().split("/")[0];
            var thangfrom = nowfrom.toLocaleDateString().split("/")[1];
            var namfrom = nowfrom.toLocaleDateString().split("/")[2];
            var ngayto = nowto.toLocaleDateString().split("/")[0];
            var thangto = nowto.toLocaleDateString().split("/")[1];
            var namto = nowto.toLocaleDateString().split("/")[2];
            if (ngayfrom.length == 1) {
                ngayfrom = '0' + ngayfrom;
            }
            if (thangfrom.length == 1) {
                thangfrom = '0' + thangfrom;
            }
            if (ngayto.length == 1) {
                ngayto = '0' + ngayto;
            }
            if (thangto.length == 1) {
                thangto = '0' + thangto;
            }
            $('#txtFromDate').val(ngayfrom + '/' + thangfrom + '/' + namfrom);
            $('#txtToDate').val(ngayto + '/' + thangto + '/' + namto);
            
        }
    }
    function resetFormMaintainanceShift() {

        $('#txtShiftName').val('');
    }
  
    function GetListFaceShift() {
        var template = $('#table-template-face-shift').html();
        var render = "";
        var render2 = 1;
        $.ajax({
            type: "GET",
            url: "../Setting/GetListFaceShift",
            data: {
                settingShift: $('#ddlShift').val(),
                department: $('#ddlDepartment').val(),
                fromDate: $('#txtFromDate').val(),
                toDate: $('#txtToDate').val(),
            },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                console.log(response);
                if (response.Success == true) {
                    if (response.Data.Items.length > 0) {
                        var d = 0;
                        if (response.Data.TotalRow > 0) {
                            $.each(response.Data.Items, function (i, item) {
                                var a = i + 1;
                                render += ' <tr style="background-color: #ffffff">' +
                                    '<td style="text-align: center;">' + a + '</td>' +
                                    '<td data-id="' + item.FaceId + '">' + item.FaceName + '</td >' +
                                    '<td>' + item.StaffCode + '</td>';
                                if (day > 1) {
                                    render += ' <td> <select class="form-control ShiftConbo" name="ddlShift" data-id="' + d + '"  disabled>' +
                                        '<option value="">--Chọn--</option>';
                                    for (var b = 0; b < datalisstshift.length; b++) {
                                        if (datalisstshift[b].ShiftName == item.Day1) {
                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '"  selected="selected">' + datalisstshift[b].ShiftName + '</option>';


                                        }
                                        else {

                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '">' + datalisstshift[b].ShiftName + '</option>';
                                        }
                                    }
                                    render += '</select></td>' +
                                        '<td >';
                                    if (item.RiceShift1 == 1) {
                                        render += '<input type="checkbox" class="ckcheck" checked="true" disabled > Ăn chính</br>' +
                                            '<input type="checkbox" class="ckcheck" data-id="{{FaceId}}" disabled> Ăn phụ';
                                    }
                                    else {
                                        if (item.RiceShift1 == 2) {
                                            render += '<input type="checkbox" class="ckcheck"  disabled> Ăn chính</br>' +
                                                '<input type="checkbox" class="ckcheck"  checked="true" disabled> Ăn phụ';
                                        }
                                        else {
                                            if (item.RiceShift1 == 3) {
                                                render += '<input type="checkbox" class="ckcheck" checked="true" disabled> Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck"  checked="true" disabled> Ăn phụ';
                                            }
                                            else {
                                                render += '<input type="checkbox" class="ckcheck" disabled> Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck" disabled> Ăn phụ';
                                            }
                                        }
                                    }

                                    render += '</td > ';
                                }
                                else {
                                    render += ' <td> <select class="form-control ShiftConbo" name="ddlShift" data-id="' + d + '">' +
                                        '<option value="">--Chọn--</option>';
                                    for (var b = 0; b < datalisstshift.length; b++) {
                                        if (datalisstshift[b].ShiftName == item.Day1) {
                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '" selected="selected">' + datalisstshift[b].ShiftName + '</option>';


                                        }
                                        else {

                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '">' + datalisstshift[b].ShiftName + '</option>';
                                        }
                                    }
                                    render += '</select></td>' +
                                        '<td >';
                                    if (item.RiceShift1 == 1) {
                                        render += '<input type="checkbox" class="ckcheck" checked="true" > Ăn chính</br>' +
                                            '<input type="checkbox" class="ckcheck" data-id="{{FaceId}}"> Ăn phụ';
                                    }
                                    else {
                                        if (item.RiceShift1 == 2) {
                                            render += '<input type="checkbox" class="ckcheck" > Ăn chính</br>' +
                                                '<input type="checkbox" class="ckcheck"  checked="true"> Ăn phụ';
                                        }
                                        else {
                                            if (item.RiceShift1 == 3) {
                                                render += '<input type="checkbox" class="ckcheck" checked="true" > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck"  checked="true"> Ăn phụ';
                                            }
                                            else {
                                                render += '<input type="checkbox" class="ckcheck" > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck" > Ăn phụ';
                                            }
                                        }
                                    }

                                    render += '</td > ';
                                }
                                if (day > 2) {
                                    render += '<td> <select class="form-control ShiftConbo" name="ddlShift" data-id="' + (d + 1) + '" disabled >' +
                                        '<option value="">--Chọn--</option>';
                                    for (var b = 0; b < datalisstshift.length; b++) {
                                        if (datalisstshift[b].ShiftName == item.Day2) {
                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '" selected="selected">' + datalisstshift[b].ShiftName + '</option>';


                                        }
                                        else {

                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '">' + datalisstshift[b].ShiftName + '</option>';
                                        }
                                    }
                                    render += '</select></td>' +
                                        '<td >';
                                    if (item.RiceShift2 == 1) {
                                        render += '<input type="checkbox" class="ckcheck" checked="true" disabled > Ăn chính</br>' +
                                            '<input type="checkbox" class="ckcheck" data-id="{{FaceId}}" disabled> Ăn phụ';
                                    }
                                    else {
                                        if (item.RiceShift2 == 2) {
                                            render += '<input type="checkbox" class="ckcheck" disabled > Ăn chính</br>' +
                                                '<input type="checkbox" class="ckcheck"  checked="true" disabled> Ăn phụ';
                                        }
                                        else {
                                            if (item.RiceShift2 == 3) {
                                                render += '<input type="checkbox" class="ckcheck" checked="true" disabled > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck"  checked="true" disabled> Ăn phụ';
                                            }
                                            else {
                                                render += '<input type="checkbox" class="ckcheck" disabled > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck" disabled> Ăn phụ';
                                            }
                                        }
                                    }

                                    render += '</td > ';
                                }
                                else {
                                    render += '<td> <select class="form-control ShiftConbo" name="ddlShift" data-id="' + (d + 1) + '">' +
                                        '<option value="">--Chọn--</option>';
                                    for (var b = 0; b < datalisstshift.length; b++) {
                                        if (datalisstshift[b].ShiftName == item.Day2) {
                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '" selected="selected">' + datalisstshift[b].ShiftName + '</option>';


                                        }
                                        else {

                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '">' + datalisstshift[b].ShiftName + '</option>';
                                        }
                                    }
                                    render += '</select></td>' +
                                        '<td >';
                                    if (item.RiceShift2 == 1) {
                                        render += '<input type="checkbox" class="ckcheck" checked="true" > Ăn chính</br>' +
                                            '<input type="checkbox" class="ckcheck" data-id="{{FaceId}}"> Ăn phụ';
                                    }
                                    else {
                                        if (item.RiceShift2 == 2) {
                                            render += '<input type="checkbox" class="ckcheck" > Ăn chính</br>' +
                                                '<input type="checkbox" class="ckcheck"  checked="true"> Ăn phụ';
                                        }
                                        else {
                                            if (item.RiceShift2 == 3) {
                                                render += '<input type="checkbox" class="ckcheck" checked="true" > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck"  checked="true"> Ăn phụ';
                                            }
                                            else {
                                                render += '<input type="checkbox" class="ckcheck" > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck" > Ăn phụ';
                                            }
                                        }
                                    }

                                    render += '</td > ';
                                }

                                if (day > 3) {
                                    render += '<td> <select class="form-control ShiftConbo" name="ddlShift" data-id="' + (d + 2) + '" disabled >' +
                                        '<option value="">--Chọn--</option>';
                                    for (var b = 0; b < datalisstshift.length; b++) {
                                        if (datalisstshift[b].ShiftName == item.Day3) {
                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '" selected="selected">' + datalisstshift[b].ShiftName + '</option>';


                                        }
                                        else {

                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '">' + datalisstshift[b].ShiftName + '</option>';
                                        }
                                    }
                                    render += '</select></td>' +
                                        '<td >';
                                    if (item.RiceShift3 == 1) {
                                        render += '<input type="checkbox" class="ckcheck" checked="true" disabled> Ăn chính</br>' +
                                            '<input type="checkbox" class="ckcheck" data-id="{{FaceId}}" disabled> Ăn phụ';
                                    }
                                    else {
                                        if (item.RiceShift3 == 2) {
                                            render += '<input type="checkbox" class="ckcheck" disabled > Ăn chính</br>' +
                                                '<input type="checkbox" class="ckcheck"  checked="true" disabled> Ăn phụ';
                                        }
                                        else {
                                            if (item.RiceShift3 == 3) {
                                                render += '<input type="checkbox" class="ckcheck" checked="true" disabled > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck"  checked="true" disabled> Ăn phụ';
                                            }
                                            else {
                                                render += '<input type="checkbox" class="ckcheck" disabled > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck" disabled > Ăn phụ';
                                            }
                                        }
                                    }

                                    render += '</td > ';
                                }
                                else {
                                    render += '<td> <select class="form-control ShiftConbo" name="ddlShift" data-id="' + (d + 2) + '">' +
                                        '<option value="">--Chọn--</option>';
                                    for (var b = 0; b < datalisstshift.length; b++) {
                                        if (datalisstshift[b].ShiftName == item.Day3) {
                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '" selected="selected">' + datalisstshift[b].ShiftName + '</option>';


                                        }
                                        else {

                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '">' + datalisstshift[b].ShiftName + '</option>';
                                        }
                                    }
                                    render += '</select></td>' +
                                        '<td >';
                                    if (item.RiceShift3 == 1) {
                                        render += '<input type="checkbox" class="ckcheck" checked="true" > Ăn chính</br>' +
                                            '<input type="checkbox" class="ckcheck" data-id="{{FaceId}}"> Ăn phụ';
                                    }
                                    else {
                                        if (item.RiceShift3 == 2) {
                                            render += '<input type="checkbox" class="ckcheck" > Ăn chính</br>' +
                                                '<input type="checkbox" class="ckcheck"  checked="true"> Ăn phụ';
                                        }
                                        else {
                                            if (item.RiceShift3 == 3) {
                                                render += '<input type="checkbox" class="ckcheck" checked="true" > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck"  checked="true"> Ăn phụ';
                                            }
                                            else {
                                                render += '<input type="checkbox" class="ckcheck" > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck" > Ăn phụ';
                                            }
                                        }
                                    }

                                    render += '</td > ';
                                }

                                if (day > 4) {
                                    render += '<td> <select class="form-control ShiftConbo" name="ddlShift" data-id="' + (d + 3) + '" disabled>' +
                                        '<option value="">--Chọn--</option>';
                                    for (var b = 0; b < datalisstshift.length; b++) {
                                        if (datalisstshift[b].ShiftName == item.Day4) {
                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '" selected="selected">' + datalisstshift[b].ShiftName + '</option>';


                                        }
                                        else {

                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '">' + datalisstshift[b].ShiftName + '</option>';
                                        }
                                    }
                                    render += '</select></td>' +
                                        '<td >';
                                    if (item.RiceShift4 == 1) {
                                        render += '<input type="checkbox" class="ckcheck" checked="true" disabled > Ăn chính</br>' +
                                            '<input type="checkbox" class="ckcheck" data-id="{{FaceId}}" disabled> Ăn phụ';
                                    }
                                    else {
                                        if (item.RiceShift4 == 2) {
                                            render += '<input type="checkbox" class="ckcheck" disabled > Ăn chính</br>' +
                                                '<input type="checkbox" class="ckcheck"  checked="true" disabled> Ăn phụ';
                                        }
                                        else {
                                            if (item.RiceShift4 == 3) {
                                                render += '<input type="checkbox" class="ckcheck" checked="true" disabled > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck"  checked="true" disabled> Ăn phụ';
                                            }
                                            else {
                                                render += '<input type="checkbox" class="ckcheck" disabled> Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck" disabled > Ăn phụ';
                                            }
                                        }
                                    }

                                    render += '</td > ';
                                }
                                else {
                                    render += '<td> <select class="form-control ShiftConbo" name="ddlShift" data-id="' + (d + 3) + '">' +
                                        '<option value="">--Chọn--</option>';
                                    for (var b = 0; b < datalisstshift.length; b++) {
                                        if (datalisstshift[b].ShiftName == item.Day4) {
                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '" selected="selected">' + datalisstshift[b].ShiftName + '</option>';


                                        }
                                        else {

                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '">' + datalisstshift[b].ShiftName + '</option>';
                                        }
                                    }
                                    render += '</select></td>' +
                                        '<td >';
                                    if (item.RiceShift4 == 1) {
                                        render += '<input type="checkbox" class="ckcheck" checked="true" > Ăn chính</br>' +
                                            '<input type="checkbox" class="ckcheck" data-id="{{FaceId}}"> Ăn phụ';
                                    }
                                    else {
                                        if (item.RiceShift4 == 2) {
                                            render += '<input type="checkbox" class="ckcheck" > Ăn chính</br>' +
                                                '<input type="checkbox" class="ckcheck"  checked="true"> Ăn phụ';
                                        }
                                        else {
                                            if (item.RiceShift4 == 3) {
                                                render += '<input type="checkbox" class="ckcheck" checked="true" > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck"  checked="true"> Ăn phụ';
                                            }
                                            else {
                                                render += '<input type="checkbox" class="ckcheck" > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck" > Ăn phụ';
                                            }
                                        }
                                    }

                                    render += '</td > ';
                                }

                                if (day > 5) {
                                    render += '<td> <select class="form-control ShiftConbo" name="ddlShift" data-id="' + (d + 4) + '" disabled>' +
                                        '<option value="">--Chọn--</option>';
                                    for (var b = 0; b < datalisstshift.length; b++) {
                                        if (datalisstshift[b].ShiftName == item.Day5) {
                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '" selected="selected">' + datalisstshift[b].ShiftName + '</option>';


                                        }
                                        else {

                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '">' + datalisstshift[b].ShiftName + '</option>';
                                        }
                                    }
                                    render += '</select></td>' +
                                        '<td >';
                                    if (item.RiceShift5 == 1) {
                                        render += '<input type="checkbox" class="ckcheck" checked="true" disabled > Ăn chính</br>' +
                                            '<input type="checkbox" class="ckcheck" data-id="{{FaceId}}" disabled> Ăn phụ';
                                    }
                                    else {
                                        if (item.RiceShift5 == 2) {
                                            render += '<input type="checkbox" class="ckcheck" disabled> Ăn chính</br>' +
                                                '<input type="checkbox" class="ckcheck"  checked="true" disabled> Ăn phụ';
                                        }
                                        else {
                                            if (item.RiceShift5 == 3) {
                                                render += '<input type="checkbox" class="ckcheck" checked="true" disabled> Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck"  checked="true" disabled> Ăn phụ';
                                            }
                                            else {
                                                render += '<input type="checkbox" class="ckcheck" disabled> Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck" disabled> Ăn phụ';
                                            }
                                        }
                                    }

                                    render += '</td > ';
                                }
                                else {
                                    render += '<td> <select class="form-control ShiftConbo" name="ddlShift" data-id="' + (d + 4) + '">' +
                                        '<option value="">--Chọn--</option>';
                                    for (var b = 0; b < datalisstshift.length; b++) {
                                        if (datalisstshift[b].ShiftName == item.Day5) {
                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '" selected="selected">' + datalisstshift[b].ShiftName + '</option>';


                                        }
                                        else {

                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '">' + datalisstshift[b].ShiftName + '</option>';
                                        }
                                    }
                                    render += '</select></td>' +
                                        '<td >';
                                    if (item.RiceShift5 == 1) {
                                        render += '<input type="checkbox" class="ckcheck" checked="true" > Ăn chính</br>' +
                                            '<input type="checkbox" class="ckcheck" data-id="{{FaceId}}"> Ăn phụ';
                                    }
                                    else {
                                        if (item.RiceShift5 == 2) {
                                            render += '<input type="checkbox" class="ckcheck" > Ăn chính</br>' +
                                                '<input type="checkbox" class="ckcheck"  checked="true"> Ăn phụ';
                                        }
                                        else {
                                            if (item.RiceShift5 == 3) {
                                                render += '<input type="checkbox" class="ckcheck" checked="true" > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck"  checked="true"> Ăn phụ';
                                            }
                                            else {
                                                render += '<input type="checkbox" class="ckcheck" > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck" > Ăn phụ';
                                            }
                                        }
                                    }

                                    render += '</td > ';
                                }

                                if (day > 6) {
                                    render += '<td> <select class="form-control ShiftConbo" name="ddlShift" data-id="' + (d + 5) + '" disabled >' +
                                        '<option value="">--Chọn--</option>';
                                    for (var b = 0; b < datalisstshift.length; b++) {
                                        if (datalisstshift[b].ShiftName == item.Day6) {
                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '" selected="selected">' + datalisstshift[b].ShiftName + '</option>';


                                        }
                                        else {

                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '">' + datalisstshift[b].ShiftName + '</option>';
                                        }
                                    }
                                    render += '</select></td>' +
                                        '<td >';
                                    if (item.RiceShift6 == 1) {
                                        render += '<input type="checkbox" class="ckcheck" checked="true" disabled> Ăn chính</br>' +
                                            '<input type="checkbox" class="ckcheck" data-id="{{FaceId}}" disabled> Ăn phụ';
                                    }
                                    else {
                                        if (item.RiceShift6 == 2) {
                                            render += '<input type="checkbox" class="ckcheck" disabled> Ăn chính</br>' +
                                                '<input type="checkbox" class="ckcheck"  checked="true" disabled> Ăn phụ';
                                        }
                                        else {
                                            if (item.RiceShift6 == 3) {
                                                render += '<input type="checkbox" class="ckcheck" checked="true" disabled> Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck"  checked="true" disabled> Ăn phụ';
                                            }
                                            else {
                                                render += '<input type="checkbox" class="ckcheck" disabled> Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck" disabled> Ăn phụ';
                                            }
                                        }
                                    }

                                    render += '</td > ';
                                }
                                else {
                                    render += '<td> <select class="form-control ShiftConbo" name="ddlShift" data-id="' + (d + 5) + '">' +
                                        '<option value="">--Chọn--</option>';
                                    for (var b = 0; b < datalisstshift.length; b++) {
                                        if (datalisstshift[b].ShiftName == item.Day6) {
                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '" selected="selected">' + datalisstshift[b].ShiftName + '</option>';


                                        }
                                        else {

                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '">' + datalisstshift[b].ShiftName + '</option>';
                                        }
                                    }
                                    render += '</select></td>' +
                                        '<td >';
                                    if (item.RiceShift6 == 1) {
                                        render += '<input type="checkbox" class="ckcheck" checked="true" > Ăn chính</br>' +
                                            '<input type="checkbox" class="ckcheck" data-id="{{FaceId}}"> Ăn phụ';
                                    }
                                    else {
                                        if (item.RiceShift6 == 2) {
                                            render += '<input type="checkbox" class="ckcheck" > Ăn chính</br>' +
                                                '<input type="checkbox" class="ckcheck"  checked="true"> Ăn phụ';
                                        }
                                        else {
                                            if (item.RiceShift6 == 3) {
                                                render += '<input type="checkbox" class="ckcheck" checked="true" > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck"  checked="true"> Ăn phụ';
                                            }
                                            else {
                                                render += '<input type="checkbox" class="ckcheck" > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck" > Ăn phụ';
                                            }
                                        }
                                    }

                                    render += '</td > ';
                                }

                                if (day > 7) {
                                    render += '<td> <select class="form-control ShiftConbo" name="ddlShift" data-id="' + (d + 6) + '" disabled>' +
                                        '<option value="">--Chọn--</option>';
                                    for (var b = 0; b < datalisstshift.length; b++) {
                                        if (datalisstshift[b].ShiftName == item.Day7) {
                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '" selected="selected">' + datalisstshift[b].ShiftName + '</option>';


                                        }
                                        else {

                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '">' + datalisstshift[b].ShiftName + '</option>';
                                        }
                                    }
                                    render += '</select></td>' +
                                        '<td >';
                                    if (item.RiceShift7 == 1) {
                                        render += '<input type="checkbox" class="ckcheck" checked="true" disabled> Ăn chính</br>' +
                                            '<input type="checkbox" class="ckcheck" data-id="{{FaceId}}" disabled> Ăn phụ';
                                    }
                                    else {
                                        if (item.RiceShift7 == 2) {
                                            render += '<input type="checkbox" class="ckcheck" disabled > Ăn chính</br>' +
                                                '<input type="checkbox" class="ckcheck"  checked="true" disabled> Ăn phụ';
                                        }
                                        else {
                                            if (item.RiceShift7 == 3) {
                                                render += '<input type="checkbox" class="ckcheck" checked="true" disabled > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck"  checked="true" disabled> Ăn phụ';
                                            }
                                            else {
                                                render += '<input type="checkbox" class="ckcheck" disabled> Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck" disabled> Ăn phụ';
                                            }
                                        }
                                    }

                                    render += '</td > ';
                                }
                                else {
                                    render += '<td> <select class="form-control ShiftConbo" name="ddlShift" data-id="' + (d + 6) + '">' +
                                        '<option value="">--Chọn--</option>';
                                    for (var b = 0; b < datalisstshift.length; b++) {
                                        if (datalisstshift[b].ShiftName == item.Day7) {
                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '" selected="selected">' + datalisstshift[b].ShiftName + '</option>';


                                        }
                                        else {

                                            render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '">' + datalisstshift[b].ShiftName + '</option>';
                                        }
                                    }
                                    render += '</select></td>' +
                                        '<td >';
                                    if (item.RiceShift7 == 1) {
                                        render += '<input type="checkbox" class="ckcheck" checked="true" > Ăn chính</br>' +
                                            '<input type="checkbox" class="ckcheck" data-id="{{FaceId}}"> Ăn phụ';
                                    }
                                    else {
                                        if (item.RiceShift7 == 2) {
                                            render += '<input type="checkbox" class="ckcheck" > Ăn chính</br>' +
                                                '<input type="checkbox" class="ckcheck"  checked="true"> Ăn phụ';
                                        }
                                        else {
                                            if (item.RiceShift7 == 3) {
                                                render += '<input type="checkbox" class="ckcheck" checked="true" > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck"  checked="true"> Ăn phụ';
                                            }
                                            else {
                                                render += '<input type="checkbox" class="ckcheck" > Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck" > Ăn phụ';
                                            }
                                        }
                                    }

                                    render += '</td > ';
                                }

                                render += '</tr>';
                                //render += Mustache.render(template, {
                                //    STT: i+1,
                                //    StaffCode: item.StaffCode,
                                //    FaceName: item.FaceName,
                                //    Day1: item.Day1,
                                //    Day2: item.Day2,
                                //    Day3: item.Day3,
                                //    Day4: item.Day4,
                                //    Day5: item.Day5,
                                //    Day6: item.Day6,
                                //    Day7: item.Day7,
                                //});
                                //console.log(render);
                                // document.getElementById(item.Day1).selected = "true";
                                //let $row = $(render)
                                //// set value of the select within this row instance
                                //$row.find('select.ShiftConbo').val(item.Day1);
                                d = d + 7;
                            });
                        }
                        else {
                            $.each(response.Data.Items, function (i, item) {
                                var a = i + 1;
                                render += ' <tr style="background-color: #ffffff">' +
                                    '<td style="text-align: center;">' + a + '</td>' +
                                    '<td data-id="' + item.FaceId + '">' + item.FaceName + '</td >' +
                                    '<td>' + item.StaffCode + '</td>';
                                for (var e = 1; e <= 7; e++) {
                                    if (day > e) {
                                        
                                        render += ' <td> <select class="form-control ShiftConbo" name="ddlShift" data-id="' + d + '"  disabled>' +
                                            '<option value="">--Chọn--</option>';
                                        for (var b = 0; b < datalisstshift.length; b++) {
                                            if (datalisstshift[b].ShiftName == item.Day + e) {
                                                render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '"  selected="selected">' + datalisstshift[b].ShiftName + '</option>';


                                            }
                                            else {

                                                render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '">' + datalisstshift[b].ShiftName + '</option>';
                                            }
                                        }
                                        render += '</select></td>' +
                                            '<td >';
                                        if (item.RiceShift+e == 1) {
                                            render += '<input type="checkbox" class="ckcheck" checked="true" disabled > Ăn chính</br>' +
                                                '<input type="checkbox" class="ckcheck" data-id="{{FaceId}}" disabled> Ăn phụ';
                                        }
                                        else {
                                            if (item.RiceShift+e == 2) {
                                                render += '<input type="checkbox" class="ckcheck"  disabled> Ăn chính</br>' +
                                                    '<input type="checkbox" class="ckcheck"  checked="true" disabled> Ăn phụ';
                                            }
                                            else {
                                                if (item.RiceShift+e == 3) {
                                                    render += '<input type="checkbox" class="ckcheck" checked="true" disabled> Ăn chính</br>' +
                                                        '<input type="checkbox" class="ckcheck"  checked="true" disabled> Ăn phụ';
                                                }
                                                else {
                                                    render += '<input type="checkbox" class="ckcheck" disabled> Ăn chính</br>' +
                                                        '<input type="checkbox" class="ckcheck" disabled> Ăn phụ';
                                                }
                                            }
                                        }

                                        render += '</td > ';
                                    }
                                    else {
                                        render += ' <td> <select class="form-control ShiftConbo" name="ddlShift" data-id="' + d + '">' +
                                            '<option value="">--Chọn--</option>';
                                        for (var b = 0; b < datalisstshift.length; b++) {
                                            if (b == 0) {
                                                render2 = datalisstshift[b].Gio;
                                                render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '" selected="selected">' + datalisstshift[b].ShiftName + '</option>';


                                            }
                                            else {

                                                render += '<option value="' + datalisstshift[b].ShiftName + '" data-gio="' + datalisstshift[b].Gio + '">' + datalisstshift[b].ShiftName + '</option>';
                                            }
                                        }
                                        render += '</select></td>' +
                                            '<td >';
                                        if (render2 <= 9) {
                                            render += '<input type="checkbox" class="ckcheck" checked="true" > Ăn chính</br>' +
                                                '<input type="checkbox" class="ckcheck" data-id="{{FaceId}}"> Ăn phụ';
                                        }
                                        else {
                                                    render += '<input type="checkbox" class="ckcheck" checked="true" > Ăn chính</br>' +
                                                        '<input type="checkbox" class="ckcheck"  checked="true"> Ăn phụ';
                                               
                                            
                                        }

                                        render += '</td > ';
                                    }
                                    d = d + 1;
                                }
                                render += '</tr>';
                                
                                
                            });
                        }
                        
                       
                        if (render != '') {
                            $('#tbl-face-shift').html(render);
                            $('#checkButton').prop('hidden', false);
                        }
                      
                    }
                    else {
                        render = '<tr>' +
                            '    <td colspan="10">Không có dữ liệu</td>' +
                            '</tr>';
                        $('#tbl-face-shift').html(render); 
                        $('#checkButton').prop('hidden', true);

                    }
                }
                else {
                    if (response.Message == 'TRUNG_MA') {
                        common.popupOk('Cảnh báo', 'Phòng ban trên đã được gán ca bởi 1 thiết lập khác.Mời bạn chọn lại thiết lập', 'error');
                        render = '<tr>' +
                            '    <td colspan="10">Không có dữ liệu</td>' +
                            '</tr>';
                        $('#tbl-face-shift').html(render);
                    }
                    else {
                        common.notify(response.Message, 'error');
                    }
                    $('#checkButton').prop('hidden', true);
                }
                 
                common.stopLoading();
                $('#table1').DataTable({
                    scrollY: "390px",
                    scrollX: true,
                    scrollCollapse: true,
                    paging: false,
                    fixedColumns: {
                        leftColumns: 2
                    }
                });
                $('#table1_filter').prop('hidden', true);

            },
            error: function () {
                common.notify('Can not load ajax function UpdateTKSetting', 'error');
                common.stopLoading();
            }
        });


    }
    
    function LoadTBody() {
        var render = "";
        var htmlscript = "";
        $('#table-template-face-shift').html('');
        $.ajax({
            type: "GET",
            url: "../Setting/GetListShiftGroup",
            data: {
                id: $('#ddlShift').val()

            },
            dataType: "json",
            beforeSend: function () {
                common.startLoading();
            },
            success: function (response) {
                //console.log(response);
                if (response.Success == true) {
                    if (response.Data.Items.length > 0) {
                        $.each(response.Data.Items, function (i, item) {

                        //    render += '<option value="' + item.ShiftName + '">' + item.ShiftName + '</option>';
                            datalisstshift.push(response.Data.Items[i]);    
                        //console.log(datalisstshift);
                       
                        });
                        //htmlscript = '<tr style="background-color: rgba(0,0,0,.00)">' +
                        //    ' <td style="text-align: center;">{{ STT }}</td>' +
                        //    '<td>{{ StaffCode }}</td>' +
                        //    '<td>{{ FaceName }}</td>' +
                        //    '<td> <select  class="form-control ShiftConbo" name="ddlShift">' +
                        //    '<option value="">--Chọn--</option>' +
                        //    render +
                        //    '</select></td>' +
                        //    '<td><input type="text" class="shiftface" value="{{Day2}}" /></td>' +
                        //    '<td><input type="text" class="shiftface" value="{{Day3}}" /></td>' +
                        //    '<td><input type="text" class="shiftface" value="{{Day4}}" /></td>' +
                        //    '<td><input type="text" class="shiftface" value="{{Day5}}" /></td>' +
                        //    '<td><input type="text" class="shiftface" value="{{Day6}}" /></td>' +
                        //    '<td><input type="text" class="shiftface" value="{{Day7}}" /></td>' +


                        //    '</tr>';
                    }
                    else {
                        //htmlscript = '<tr style="background-color: rgba(0,0,0,.00)">' +
                        //    ' <td style="text-align: center;">{{ STT }}</td>' +
                        //    '<td>{{ StaffCode }}</td>' +
                        //    '<td>{{ FaceName }}</td>' +
                        //    '<td> <select  class="form-control ShiftConbo" name="ddlShift">' +
                        //    '<option value="">--Chọn--</option>' +
                        //    '</select></td>' +
                        //    '<td><input type="text" class="shiftface" value="{{Day2}}" /></td>' +
                        //    '<td><input type="text" class="shiftface" value="{{Day3}}" /></td>' +
                        //    '<td><input type="text" class="shiftface" value="{{Day4}}" /></td>' +
                        //    '<td><input type="text" class="shiftface" value="{{Day5}}" /></td>' +
                        //    '<td><input type="text" class="shiftface" value="{{Day6}}" /></td>' +
                        //    '<td><input type="text" class="shiftface" value="{{Day7}}" /></td>' +


                        //    '</tr>';
                    }
                   // $('#table-template-face-shift').append(htmlscript);
                }
                else {


                    common.notify(response.Message, 'error');
                }
                common.stopLoading();
            },
            error: function () {
                common.notify('Can not load ajax function UpdateTKSetting', 'error');
                common.stopLoading();
            }
        });

    }

    function AddFaceShift() {


                $.ajax({
                    type: "POST",
                    url: "../Setting/AddFaceShift",
                    data: {
                        settingShift: $('#ddlShift').val(),
                        fromDate: $('#txtFromDate').val(),
                        toDate: $('#txtToDate').val(),
                        items: data1//[{ RowCells: JSON.stringify(children) }]//JSON.stringify(children)
                    },
                    dataType: "json",
                    beforeSend: function () {
                        common.startLoading();
                    },
                    success: function (response) {
                        //console.log(response.Data)
                        if (response.Success == true) {
                            common.notify('Xác nhận thành công', 'success');


                            //GetShiftBySettingShiftId();

                           
                            // loadData(true);
                        }
                        else {

                            common.notify(response.Message, 'error');
                        }
                        common.stopLoading();
                    },
                    error: function () {
                        common.notify('Can not load ajax function SaveProduct', 'error');
                        common.stopLoading();
                    }
                });
    }
    function LoadNgayByTuan(ngayTK) {
        //console.log($('#txtFromDate').val());
        var now1 = new Date(ngayTK);
            console.log(now1);
            var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            var day1 = now1.getDay();
            var dd = now1.getDate();
            var dd1 = now1.getDate();
            //var dayName = days[now1.getDay()];
            //var tomorrow = new Date(now1 -1);
            //var abc=tomorrow.setDate(now1.getDate() + 1);
            //console.log(tomorrow);

            if (day1 !== 1) {
                for (var i = day1; i >= 1; i--) {
                    var now = new Date();

                    now.setDate(dd);
                    var a = i + 1;
                    var x = document.getElementById("T" + a);
                    x.innerHTML = now.toLocaleDateString();
                    dd = dd - 1;
                }

                for (var i = day1 + 1; i <= 7; i++) {
                    dd1 = dd1 + 1;
                    var now = new Date();
                    //var b = i - 1;
                    now.setDate(dd1);
                    var a = i + 1;
                    var x = document.getElementById("T" + a);
                    x.innerHTML = now.toLocaleDateString();

                }
                var nowfrom = new Date();
                var nowto = new Date();
                nowfrom.setDate(dd + 1);
                nowto.setDate(dd1);
                var ngayfrom = nowfrom.toLocaleDateString().split("/")[0];
                var thangfrom = nowfrom.toLocaleDateString().split("/")[1];
                var namfrom = nowfrom.toLocaleDateString().split("/")[2];
                var ngayto = nowto.toLocaleDateString().split("/")[0];
                var thangto = nowto.toLocaleDateString().split("/")[1];
                var namto = nowto.toLocaleDateString().split("/")[2];
                if (ngayfrom.length == 1) {
                    ngayfrom = '0' + ngayfrom;
                }
                if (thangfrom.length == 1) {
                    thangfrom = '0' + thangfrom;
                }
                if (ngayto.length == 1) {
                    ngayto = '0' + ngayto;
                }
                if (thangto.length == 1) {
                    thangto = '0' + thangto;
                }
                //$('#txtFromDate').val(ngayfrom + '/' + thangfrom + '/' + namfrom);
                //$('#txtToDate').val(ngayto + '/' + thangto + '/' + namto);
            }
            else {


                for (var i = 1; i <= 7; i++) {
                    var now = new Date();
                    now.setDate(dd);
                    var a = i + 1;
                    //console.log(tomorrow);
                    var x = document.getElementById("T" + a);
                    x.innerHTML = now.toLocaleDateString();
                    dd = dd + 1;
                }
                var nowfrom = new Date();
                var nowto = new Date();
                nowto.setDate(dd - 1);
                var ngayfrom = nowfrom.toLocaleDateString().split("/")[0];
                var thangfrom = nowfrom.toLocaleDateString().split("/")[1];
                var namfrom = nowfrom.toLocaleDateString().split("/")[2];
                var ngayto = nowto.toLocaleDateString().split("/")[0];
                var thangto = nowto.toLocaleDateString().split("/")[1];
                var namto = nowto.toLocaleDateString().split("/")[2];
                if (ngayfrom.length == 1) {
                    ngayfrom = '0' + ngayfrom;
                }
                if (thangfrom.length == 1) {
                    thangfrom = '0' + thangfrom;
                }
                if (ngayto.length == 1) {
                    ngayto = '0' + ngayto;
                }
                if (thangto.length == 1) {
                    thangto = '0' + thangto;
                }
                //$('#txtFromDate').val(ngayfrom + '/' + thangfrom + '/' + namfrom);
                //$('#txtToDate').val(ngayto + '/' + thangto + '/' + namto);

            }

        }
    function ExportExcel() {
        $.ajax({
            url: '../Setting/ExportExcel',
            type: 'GET',
            data: {
                settingShift: $('#ddlShift').val(),
                settingShiftName: $('#ddlShift option:selected').text(),
                department: $('#ddlDepartment').val(),
                departmentName: $('#ddlDepartment  option:selected').text(),
                fromDate: $('#txtFromDate').val(),
                toDate: $('#txtToDate').val(),
            },
            dataType: 'json',
            success: function (response) {
                if (response.Success == true) {
                    window.location.href = response.Data;
                }
                else {
                    common.notify(response.Message, 'error');
                }
            },
            error: function () {
                common.notify('Can not load ajax function ExportExcel', 'error');
            }
        });
    }
}