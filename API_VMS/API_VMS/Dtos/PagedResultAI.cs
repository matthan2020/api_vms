﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Dtos
{
    public class PagedResultAI<T>
    {
        public List<T> Items { set; get; }

    }
}
