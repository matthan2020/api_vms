﻿using API_VMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Dtos
{
    public class PagedResultSearchLicese
    {
        public List<VehicleSearchViewModels> Vehicle { set; get; }
        public List<StaffSearchViewModels> Staff { set; get; }
        public List<GuestSearchViewModels> Guest { set; get; }
        public string Status { get; set; }
        public string Message { get; set; }

    }
}
