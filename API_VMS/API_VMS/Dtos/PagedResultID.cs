﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Dtos
{
    public class PagedResultID
    {
        //public List<T> Data { set; get; }

        //public string Status { get; set; }
        public int id { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public string FileSave { get; set; }
        
    }
}
