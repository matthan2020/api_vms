﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Dtos
{
    public class PagedResultNoDataWeb
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}
