﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace API_VMS.Dtos
{
    public class BaseApiClient
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        protected BaseApiClient(IHttpClientFactory httpClientFactory,
                    IConfiguration configuration)
        {
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }

        protected async Task<TResponse> GetAsync<TResponse>(string url)
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri("http://192.168.1.57:8081");//_configuration[SystemConstants.AppSettings.BaseAddress]);
                var response = await client.GetAsync(url);
                var body = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    TResponse myDeserializedObjList = (TResponse)JsonConvert.DeserializeObject(body,
                        typeof(TResponse));

                    return myDeserializedObjList;
                }
                return JsonConvert.DeserializeObject<TResponse>(body);
            }
            catch (Exception ex)
            {
                var body = "{" + "Success :" + false + ", Message : " + ex.Message + "}";

                return JsonConvert.DeserializeObject<TResponse>(body);
            }


        }

        public async Task<List<T>> GetListAsync<T>(string url, bool requiredLogin = false)
        {

            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri("http://192.168.1.57:8081");//_configuration[SystemConstants.AppSettings.BaseAddress]);

            var response = await client.GetAsync(url);
            var body = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                var data = (List<T>)JsonConvert.DeserializeObject(body, typeof(List<T>));
                return data;
            }
            throw new Exception(body);
        }

        protected async Task<TResponse> PostAsync<TResponse>(string url, string json)
        {

            var client = _httpClientFactory.CreateClient();
            string _tokenUpdated= "0af9b5d5f18a4dd0950d039fc9b0ec95";
            client.BaseAddress = new Uri("https://api.luxand.cloud/photo/search");//_configuration[SystemConstants.AppSettings.BaseAddress]);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("token", _tokenUpdated);
            // var response = await client.GetAsync(url);
            // var body = await response.Content.ReadAsStringAsync();

            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(url, httpContent);
            var body = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                TResponse myDeserializedObjList = (TResponse)JsonConvert.DeserializeObject(body,
                    typeof(TResponse));

                return myDeserializedObjList;
            }
            //var abc = JsonConvert.DeserializeObject<TResponse>(result);

            return JsonConvert.DeserializeObject<TResponse>(body);


        }
    }
}
