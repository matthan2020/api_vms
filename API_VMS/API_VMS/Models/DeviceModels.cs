﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class DeviceModels
    {
       public int DeviceId { get; set; }
       public string CameraLink { get; set; }
       public string TypeRecognition { get; set; }
    }
}
