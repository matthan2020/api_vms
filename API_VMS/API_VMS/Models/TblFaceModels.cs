﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class TblFaceModels
    {
        public int FaceId { get; set; }
        public string FaceName { get; set; }
        public string AgeYear { get; set; }
        public string Job { get; set; }
        public string Sex { get; set; }
        public string ImagePath { get; set; }
        public bool IsKnown { get; set; }
        public int ImportantLevel { get; set; }
        public int FaceIdCloud { get; set; }
        public int DepartmentId { get; set; }
        public string StaffCode { get; set; }
        public string CreatedDate { set; get; }
    }
}
