﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class CompanyModels
    {
       public int IdCompany { get; set; }
       public string CompanyName { get; set; }
    }
}
