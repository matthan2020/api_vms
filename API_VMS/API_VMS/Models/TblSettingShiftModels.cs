﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class TblSettingShiftModels
    {
        public int SettingShiftId { get; set; }
        public string SettingShiftName { get; set; }
        public string SettingShiftCode { get; set; }
    }
}
