﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class ServerConfigModels
    {
        public string ServerName { get; set; }
        public string IpServer { get; set; }
        public int IpPort { get; set; }
        public string LinkPath { get; set; }
    }
}
