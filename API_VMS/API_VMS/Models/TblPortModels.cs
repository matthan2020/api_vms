﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class TblPortModels
    {
        public int PortId { get; set; }
        public string PortName { get; set; }
        public string TypePort { get; set; }
        public int PortGroup { get; set; }
        
    }
}
