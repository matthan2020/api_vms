﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class TblImageEventHistoryModels
    {
        public int HisEventId { set; get; }
        public int FaceId { set; get; }
        public int EventId { set; get; }
        public int DeviceId { set; get; }
        public string RecordTime { set; get; }
        public string Base64ContentUrl { set; get; }
        public int LicensePlateId { set; get; }
        public string LicenseImagePath { set; get; }
    }
}
