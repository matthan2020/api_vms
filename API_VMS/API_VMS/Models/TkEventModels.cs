﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class TkEventModels
    {
        public int HisEventId { get; set; }
        public int FaceId { get; set; }
        public string FaceName { get; set; }
        public string StaffCode { get; set; }
        public string DeviceName { get; set; }
        public string RecordTime { get; set; }
        public string ImageUrl { get; set; }
        public bool IsCheckIn { get; set; }
        public bool IsCheckOut { get; set; }
        public int HisEventIdOut { get; set; }
        public string DeviceNameOut { get; set; }
        public string RecordTimeOut{ get; set; }
        public string Job { get; set; }
        public string ImageUrlOut { get; set; }
        
    }
}
