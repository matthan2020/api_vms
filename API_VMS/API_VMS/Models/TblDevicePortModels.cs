﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class TblDevicePortModels
    {
       public int DevicePortId { get; set; }
       public int PortId { get; set; }
       public int DeviceId { get; set; }
       public bool IsFace { get; set; }
    }
}
