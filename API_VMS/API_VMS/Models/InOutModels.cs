﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class InOutModels
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string StaffCode { get; set; }
        public string FaceName { get; set; }
        public string RecordTimeIn { get; set; }
        public string RecordTimeOut { get; set; }
        public string RecordTime { get; set; }
        public string DeviceNameIn { get; set; }
        public string DeviceNameOut { get; set; }
        public float Tong { get; set; }
    }
}
