﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class DepartmentModels
    {
        public int DepartmentId { set; get; }
        public string Acronym { set; get; }
        public string DepartmentName { set; get; }
        public string IdCompany { set; get; }
    }
}
