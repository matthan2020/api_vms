﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class DeviceTypeModels
    {
       public int IdDeviceType { get; set; }
       public string DeviceTypeName { get; set; }
    }
}
