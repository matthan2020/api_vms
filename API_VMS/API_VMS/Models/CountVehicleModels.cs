﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class CountVehicleModels
    {
       public int Vehicle { get; set; }
       public int Car { get; set; }
       public int Motorcycle { get; set; }
    }
}
