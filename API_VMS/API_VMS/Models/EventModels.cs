﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class EventModels
    {
        public int EventId { get; set; }
        public string EventName { get; set; }
        
    }
}
