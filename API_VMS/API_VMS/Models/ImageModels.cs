﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class ImageModels
    {
        public int FaceId { get; set; }
        public int ImageId { get; set; }
        public string Base64ContentUrl { get; set; }
        public string ImageName { get; set; }
        public int isDownload { get; set; }
    }
}
