﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class TblFaceShiftModels
    {
        public int FaceShiftId { get; set; }
        public int FaceId { get; set; }
        public int SettingShiftId { get; set; }
        public string ShiftName { get; set; }
        public string FaceDate { get; set; }
    }
}
