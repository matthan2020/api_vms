﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class UserModels
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Pwd { get; set; }
    }
}
