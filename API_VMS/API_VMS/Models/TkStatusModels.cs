﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class TkStatusModels
    {
        public int IdStatus { get; set; }
        public string Status { get; set; }
        
    }
}
