﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class TimeKeepingModels
    {
        public int HisEventId { get; set; }
        public int DeviceId { get; set; }
        public string StaffCode { get; set; }
        public string RecordTime { get; set; }
        public bool IsCheckIn { get; set; }
        public bool IsCheckOut { get; set; }
    }
}
