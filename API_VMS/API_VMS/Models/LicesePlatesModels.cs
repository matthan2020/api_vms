﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class LicesePlatesModels
    {
        public string LicensePlatesName { get; set; }
        public string LicensePlatesColor { get; set; }
        public string LicensePlatesType { get; set; }
    }
}
