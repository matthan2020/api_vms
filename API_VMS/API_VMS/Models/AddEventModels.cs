﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class AddEventModels
    {
        public string StaffCode { get; set; }
        public int Device { get; set; }
        public string RecordTime { get; set; }
        public string CreatedUser { get; set; }
        public int Event { get; set; }
    }
}
