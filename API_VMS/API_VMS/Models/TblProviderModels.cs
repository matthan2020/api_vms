﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class TblProviderModels
    {
        public int ProviderId { get; set; }
        public string ProviderName { get; set; }
        public string ProviderAddress { get; set; }
        public string ProviderPhone { get; set; }
        public string ProviderBegin { get; set; }
        public string ProviderNote { get; set; }
        public string CreatedDate { get; set; }
        public int CreatedUserId { get; set; }
    }
}
