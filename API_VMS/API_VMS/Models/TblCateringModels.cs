﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class TblCateringModels
    {
        public int ProviderId { get; set; }
        public int CateringId { get; set; }
        public string CateringName { get; set; }
        public string CateringType { get; set; }
        public string DishList { get; set; }
        public int CateringPrice { get; set; }
    }
}
