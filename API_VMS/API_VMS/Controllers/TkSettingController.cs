﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using API_VMS.Dtos;
using API_VMS.Models;
using API_VMS.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_VMS.Controllers
{
    [Route("api/tk")]
    [ApiController]
    public class TkSettingController : ControllerBase
    {
        private readonly string _connectionString;
        public TkSettingController(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }
      
        // GET api/<FaceController>/5
        /// <summary>
        /// ham them moi gia tri vao bang tksetting
        /// </summary>
        /// <param name="tkSetting"></param>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task<IActionResult> AddTKSetting([FromBody] TkSettingModels tkSetting)
        {
            string error = "";
            string data ="OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_DeviceId", tkSetting.DeviceId);
                paramaters.Add("@v_isCheckIn", tkSetting.IsCheckIn);
                paramaters.Add("@v_isCheckOut", tkSetting.IsCheckOut);
                paramaters.Add("@v_isTKOn", tkSetting.IsTKOn);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("AddTKSetting", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }

        // PUT api/<ValuesController>/5
      /// <summary>
      /// ham sua gia trị trong bang tksettting
      /// </summary>
      /// <param name="id"></param>
      /// <param name="tkSetting"></param>
        [HttpPost("edit/{id}")]
        public async Task<IActionResult> EditTKSetting(int id, [FromBody] TkSettingModels tkSetting)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@v_DeviceId", tkSetting.DeviceId);
                paramaters.Add("@v_isCheckIn", tkSetting.IsCheckIn);
                paramaters.Add("@v_isCheckOut", tkSetting.IsCheckOut);
                paramaters.Add("@v_isTKOn", tkSetting.IsTKOn);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("EditTKSetting", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }

        [HttpGet("get/{id}")]
        public async Task<IActionResult> GetTKSettingById(int id)
        {
            IEnumerable<TkSettingModels> result = new List<TkSettingModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TkSettingModels>("GetTKSettingById", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpGet("get-device/{id}")]
        public async Task<IActionResult> GetDeviceById(int id)
        {
            IEnumerable<TkSettingViewModels> result = new List<TkSettingViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TkSettingViewModels>("GetDeviceById", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);
                }
                //return pagedResult;
            }
        }

        [HttpPost("event/add")]
        public async Task<IActionResult> AddTimeKeeping([FromBody] TimeKeepingModels timeKeeping)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_HisEventId", timeKeeping.HisEventId);
                paramaters.Add("@v_DeviceId", timeKeeping.DeviceId);
                paramaters.Add("@v_StaffCode", timeKeeping.StaffCode);
                paramaters.Add("@v_RecordTime", timeKeeping.RecordTime);
                paramaters.Add("@v_isCheckIn", timeKeeping.IsCheckIn);
                paramaters.Add("@v_isCheckOut", timeKeeping.IsCheckOut);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("AddTimeKeeping", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }

        [HttpGet("event/get")]
        public async Task<IActionResult> GetTKEvent()
        {
            IEnumerable<TkEventModels> result = new List<TkEventModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TkEventModels>("GetTKEvent", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpPost("statistic/update/{year}/{month}")]
        public async Task<IActionResult> InsertStatistic(int year,int month)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Month", month);
                paramaters.Add("@Year", year);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("InsertStatistic", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }

        [HttpGet("statistic/get/{staffCode}")]
        public async Task<IActionResult> GetStatisticByStaffCode(string staffCode)
        {
            IEnumerable<TkStatisticModels> result = new List<TkStatisticModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_StaffCode", staffCode);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TkStatisticModels>("GetStatisticByStaffCode", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpGet("statistic/get/{fromDate}/{toDate}")]
        public async Task<IActionResult> GetStatisticByDate(string fromDate,string toDate)
        {
            IEnumerable<TkStatisticModels> result = new List<TkStatisticModels>();
            string error = "";
            string data = "OK";
            int ToYear = 0;
            int ToMonth = 0;
            if (toDate !="" && toDate != null)
            {
                ToYear = Convert.ToInt32(toDate.Split("-")[0]);
                ToMonth= Convert.ToInt32(toDate.Split("-")[1]); 
            }
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_FromYear", Convert.ToInt32(fromDate.Split("-")[0]));
                paramaters.Add("@v_FromMonth", Convert.ToInt32(fromDate.Split("-")[1]));
                paramaters.Add("@v_ToYear", ToYear);
                paramaters.Add("@v_ToMonth", ToMonth);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output); 
                try
                {
                    result = await conn.QueryAsync<TkStatisticModels>("GetStatisticByMonthYear", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpGet("statistic/get/{staffcode}/{fromDate}/{toDate}")]
        public async Task<IActionResult> GetStatisticByStaffCodeDate(string staffcode,string fromDate, string toDate)
        {
            IEnumerable<TkStatisticModels> result = new List<TkStatisticModels>();
            string error = "";
            string data = "OK";
            int ToYear = 0;
            int ToMonth = 0;
            if (toDate != "" && toDate != null)
            {
                ToYear = Convert.ToInt32(toDate.Split("-")[0]);
                ToMonth = Convert.ToInt32(toDate.Split("-")[1]);
            }
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_StaffCode", staffcode);
                paramaters.Add("@v_FromYear", Convert.ToInt32(fromDate.Split("-")[0]));
                paramaters.Add("@v_FromMonth", Convert.ToInt32(fromDate.Split("-")[1]));
                paramaters.Add("@v_ToYear", ToYear);
                paramaters.Add("@v_ToMonth", ToMonth);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TkStatisticModels>("GetStatisticByStaffCodeMonth", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }
        [HttpPost("statistic/get")]
        public async Task<IActionResult> GetStatistic([FromBody] TkStatisticViewModels statistic)
        {
            IEnumerable<TkStatisticModels> result = new List<TkStatisticModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_StaffCode", statistic.StaffCode);
                paramaters.Add("@v_DepartmentId", statistic.DepartmentId);
                paramaters.Add("@v_Year", statistic.TkYear);
                paramaters.Add("@v_Month", statistic.TkMonth);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TkStatisticModels>("GetStatistic", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }
        [HttpPost("statistic/get-page")]
        public async Task<PagedResultWeb<TkStatisticModels>> GetStatisticPage([FromBody] TkStatisticPageViewModels statistic)
        {
            IEnumerable<TkStatisticModels> result = new List<TkStatisticModels>();
            string error = "";
            bool data = true;
            int totalRow = 0;
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_StaffCode", statistic.StaffCode);
                paramaters.Add("@v_DepartmentId", statistic.DepartmentId);
                paramaters.Add("@v_Year", statistic.TkYear);
                paramaters.Add("@v_Month", statistic.TkMonth);
                paramaters.Add("@pageIndex", statistic.Page);
                paramaters.Add("@pageSize", statistic.PageSize);
                paramaters.Add("@totalRow", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TkStatisticModels>("GetStatisticPage", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    totalRow = paramaters.Get<int>("@totalRow");
                    if (error == "SUCCESS")
                    {
                        data = true;
                    }
                   
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data ==true)
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = false;
                }
                var pagedResult = new PagedResultWeb<TkStatisticModels>()
                {
                    Items = result.ToList(),
                    Message = error,
                    Success = data,
                    TotalRow = totalRow,
                    PageSize= statistic.PageSize,
                    PageIndex= statistic.Page,

                };

                return pagedResult;
            }
        }

        [HttpPost("statistic/get-inout-page")]
        public async Task<PagedResultWeb<InOutModels>> GetInOutPage([FromBody] TkStatisticPageViewModels statistic)
        {
            IEnumerable<InOutModels> result = new List<InOutModels>();
            string error = "";
            bool data = true;
            int totalRow = 0;
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_StaffCode", statistic.StaffCode);
                paramaters.Add("@v_DepartmentId", statistic.DepartmentId);
                paramaters.Add("@v_FromDate", statistic.FromDate);
                paramaters.Add("@v_ToDate", statistic.ToDate);
                paramaters.Add("@pageIndex", statistic.Page);
                paramaters.Add("@pageSize", statistic.PageSize);
                paramaters.Add("@totalRow", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<InOutModels>("GetInOutPage", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    totalRow = paramaters.Get<int>("@totalRow");
                    if (error == "SUCCESS")
                    {
                        data = true;
                    }

                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == true)
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = false;
                }
                var pagedResult = new PagedResultWeb<InOutModels>()
                {
                    Items = result.ToList(),
                    Message = error,
                    Success = data,
                    TotalRow = totalRow,
                    PageSize = statistic.PageSize,
                    PageIndex = statistic.Page,

                };

                return pagedResult;
            }
        }


        [HttpPost("statistic/get-inout")]
        public async Task<PagedResultWeb<InOutModels>> GetInOut([FromBody] TkStatisticPageViewModels statistic)
        {
            IEnumerable<InOutModels> result = new List<InOutModels>();
            string error = "";
            bool data = true;
            int totalRow = 0;
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_StaffCode", statistic.StaffCode);
                paramaters.Add("@v_DepartmentId", statistic.DepartmentId);
                paramaters.Add("@v_FromDate", statistic.FromDate);
                paramaters.Add("@v_ToDate", statistic.ToDate);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<InOutModels>("GetInOut", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = true;
                    }

                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == true)
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = false;
                }
                var pagedResult = new PagedResultWeb<InOutModels>()
                {
                    Items = result.ToList(),
                    Message = error,
                    Success = data,
                    TotalRow = totalRow,
                    PageSize = statistic.PageSize,
                    PageIndex = statistic.Page,

                };

                return pagedResult;
            }
        }

        [HttpGet("delete/{id}")]
        public async Task<IActionResult> DeleteTKSettingById(int id)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("DeleteTKSettingById", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }
        [HttpGet("add-setting-shift/{ShiftName}")]
        public async Task<IActionResult> AddSettingShift(string ShiftName)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_ShiftName", ShiftName);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("AddSettingShift", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }

        [HttpGet("get-setting-shift")]
        public async Task<IActionResult>GetListSettingShift()
        {
            IEnumerable<TblSettingShiftModels> result = new List<TblSettingShiftModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TblSettingShiftModels>("GetListSettingShift", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }
        [HttpGet("get-shift/{SettingShiftId}")]
        public async Task<IActionResult> GetListShift(int SettingShiftId)
        {
            IEnumerable<TblShiftModels> result = new List<TblShiftModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_SettingShiftId", SettingShiftId);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TblShiftModels>("GetListShift", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            
        }
        }
        [HttpPost("add-shift")]
        public async Task<IActionResult> AddShift( [FromBody] ShiftViewModels tkSetting)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_ShiftId", tkSetting.ShiftId);
                paramaters.Add("@v_SettingShiftId", tkSetting.SettingShiftId);
                paramaters.Add("@v_ShiftName", tkSetting.ShiftName);
                paramaters.Add("@v_FromDate", tkSetting.FromDate);
                paramaters.Add("@v_ToDate", tkSetting.ToDate);
                paramaters.Add("@v_DeleteShiftId", tkSetting.DeleteShiftId);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("AddShift", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }
        [HttpGet("delete-shift/{id}")]
        public async Task<IActionResult> DeleteShiftBySettingShiftId(int id)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("DeleteShiftBySettingShiftId", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }

        [HttpPost("get-face-shift")]
        public async Task<IActionResult> GetListFaceShift([FromBody] FaceShiftSearchViewModels tkSetting)
        {
            IEnumerable<FaceShiftViewModels> result = new List<FaceShiftViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_SettingShiftId", tkSetting.SettingShiftId);
                paramaters.Add("@v_DepartmentId", tkSetting.DepartmnetId);
                paramaters.Add("@v_FromDate", tkSetting.FromDate);
                paramaters.Add("@v_ToDate", tkSetting.ToDate);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<FaceShiftViewModels>("GetListFaceShift", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;

            }
        }

        [HttpGet("get-shift-group/{SettingShiftId}")]
        public async Task<IActionResult> GetListShiftGroup(int SettingShiftId)
        {
            IEnumerable<TblShiftModels> result = new List<TblShiftModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_SettingShiftId", SettingShiftId);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TblShiftModels>("GetListShiftGroup", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;

            }
        }

        [HttpPost("add-face-shift")]
        public async Task<IActionResult> AddFaceShift([FromBody] FaceShiftViewModels tkSetting)
        {
            IEnumerable<FaceShiftViewModels> result = new List<FaceShiftViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_SettingShiftId", tkSetting.SettingShiftId);
                paramaters.Add("@v_FaceId", tkSetting.FaceId);
                paramaters.Add("@v_FromDate", tkSetting.FromDate);
                paramaters.Add("@v_ToDate", tkSetting.ToDate);
                paramaters.Add("@v_Day1", tkSetting.Day1);
                paramaters.Add("@v_Day2", tkSetting.Day2);
                paramaters.Add("@v_Day3", tkSetting.Day3);
                paramaters.Add("@v_Day4", tkSetting.Day4);
                paramaters.Add("@v_Day5", tkSetting.Day5);
                paramaters.Add("@v_Day6", tkSetting.Day6);
                paramaters.Add("@v_Day7", tkSetting.Day7);
                paramaters.Add("@v_RiceShift1", tkSetting.RiceShift1);
                paramaters.Add("@v_RiceShift2", tkSetting.RiceShift2);
                paramaters.Add("@v_RiceShift3", tkSetting.RiceShift3);
                paramaters.Add("@v_RiceShift4", tkSetting.RiceShift4);
                paramaters.Add("@v_RiceShift5", tkSetting.RiceShift5);
                paramaters.Add("@v_RiceShift6", tkSetting.RiceShift6);
                paramaters.Add("@v_RiceShift7", tkSetting.RiceShift7);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("AddFaceShift", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);

            }
                //return pagedResult;

        }

        [HttpPost("get-face-shift-excel")]
        public async Task<IActionResult> GetListFaceShiftExcel([FromBody] FaceShiftSearchViewModels tkSetting)
        {
            IEnumerable<FaceShiftViewModels> result = new List<FaceShiftViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_SettingShiftId", tkSetting.SettingShiftId);
                paramaters.Add("@v_DepartmentId", tkSetting.DepartmnetId);
                paramaters.Add("@v_FromDate", tkSetting.FromDate);
                paramaters.Add("@v_ToDate", tkSetting.ToDate);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<FaceShiftViewModels>("GetListFaceShiftExcel", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;

            }
        }

        [HttpGet("get-list-status")]
        public async Task<IActionResult> GetListStatus()
        {
            IEnumerable<TkStatusModels> result = new List<TkStatusModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TkStatusModels>("GetListStatus", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;

            }
        }

        [HttpGet("statistic/update-face-shift/{year}/{month}")]
        public async Task<IActionResult>InsertStatisticFaceShift(int year, int month)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Month", month);
                paramaters.Add("@Year", year);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("InsertStatisticFaceShift", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }
        [HttpPost("statistic/get-face-shift-page")]
        public async Task<PagedResultWeb<StatisticFaceShiftViewModels>> GetStatisticFaceShiftPage([FromBody] TkStatisticPageViewModels statistic)
        {
            IEnumerable<StatisticFaceShiftViewModels> result = new List<StatisticFaceShiftViewModels>();
            string error = "";
            bool data = true;
            int totalRow = 0;
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_StaffCode", statistic.StaffCode);
                paramaters.Add("@v_DepartmentId", statistic.DepartmentId);
                paramaters.Add("@v_ShiftName", statistic.ShiftName);
                paramaters.Add("@v_Year", statistic.TkYear);
                paramaters.Add("@v_Month", statistic.TkMonth);
                paramaters.Add("@pageIndex", statistic.Page);
                paramaters.Add("@pageSize", statistic.PageSize);
                paramaters.Add("@totalRow", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<StatisticFaceShiftViewModels>("GetStatisticFaceShiftPage", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    totalRow = paramaters.Get<int>("@totalRow");
                    if (error == "SUCCESS")
                    {
                        data = true;
                    }

                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == true)
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = false;
                }
                var pagedResult = new PagedResultWeb<StatisticFaceShiftViewModels>()
                {
                    Items = result.ToList(),
                    Message = error,
                    Success = data,
                    TotalRow = totalRow,
                    PageSize = statistic.PageSize,
                    PageIndex = statistic.Page,

                };

                return pagedResult;
            }
        }
        [HttpPost("statistic/get-statistic-face-shift")]
        public async Task<IActionResult> GetStatisticFaceShift([FromBody] TkStatisticPageViewModels statistic)
        {
            IEnumerable<StatisticFaceShiftViewModels> result = new List<StatisticFaceShiftViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_StaffCode", statistic.StaffCode);
                paramaters.Add("@v_DepartmentId", statistic.DepartmentId);
                paramaters.Add("@v_ShiftName", statistic.ShiftName);
                paramaters.Add("@v_Year", statistic.TkYear);
                paramaters.Add("@v_Month", statistic.TkMonth);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<StatisticFaceShiftViewModels>("GetStatisticFaceShift", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpPost("add-setting-rice")]
        public async Task<IActionResult> AddSettingRice([FromBody] ShiftViewModels tkSetting)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_SettingShiftId", tkSetting.SettingShiftId);
                paramaters.Add("@v_FromDateMain", tkSetting.FromDateMain);
                paramaters.Add("@v_ToDateMain", tkSetting.ToDateMain);
                paramaters.Add("@v_FromDateExtra", tkSetting.FromDateExtra);
                paramaters.Add("@v_ToDateExtra", tkSetting.ToDateExtra);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("AddSettingRice", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }
    }
}
