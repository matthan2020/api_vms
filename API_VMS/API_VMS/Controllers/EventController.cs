﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using API_VMS.Dtos;
using API_VMS.Models;
using API_VMS.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_VMS.Controllers
{
    [Route("api/event")]
    [ApiController]
    public class EventController : ControllerBase
    {
        private readonly string _connectionString;
       
        public EventController(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }
     
        // GET api/<FaceController>/5
        [HttpGet("get")]
        public async Task<IActionResult> GetListEvent()
        {
            IEnumerable<EventModels> result = new List<EventModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<EventModels>("GetListEvent", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<FaceModels>()
                //{
                //    pagedResult = result.ToList();
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpPost("get-images-history-page")]
        public async Task<PagedResultWeb<ImageEventHistoryModels>> GetImagesEventHistoryPage([FromBody] ImageEventHistoryJsonViewModels statistic)
        {
            IEnumerable<ImageEventHistoryModels> result = new List<ImageEventHistoryModels>();
            string error = "";
            bool data = true;
            int totalRow = 0;
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_StaffCode", statistic.StaffCode);
                paramaters.Add("@v_EventId", statistic.EventId);
                paramaters.Add("@v_FromDate", statistic.FromDate);
                paramaters.Add("@v_ToDate", statistic.ToDate);
                paramaters.Add("@v_FaceId", statistic.FaceId);
                paramaters.Add("@v_Search", statistic.Search);
                paramaters.Add("@pageIndex", statistic.Page);
                paramaters.Add("@pageSize", statistic.PageSize);
                paramaters.Add("@totalRow", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<ImageEventHistoryModels>("GetImagesEventHistoryPage", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    totalRow = paramaters.Get<int>("@totalRow");
                    if (error == "SUCCESS")
                    {
                        data = true;
                    }

                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == true)
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = false;
                }
                var pagedResult = new PagedResultWeb<ImageEventHistoryModels>()
                {
                    Items = result.ToList(),
                    Message = error,
                    Success = data,
                    TotalRow = totalRow,
                    PageSize = statistic.PageSize,
                    PageIndex = statistic.Page,

                };

                return pagedResult;
            }
        }
       
        [HttpGet("get-event-by-id/{id}")]
        public async Task<IActionResult> GetEventDetailById(int id)
        {
            IEnumerable<FaceViewModels> result = new List<FaceViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<FaceViewModels>("GetEventDetailById", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<FaceModels>()
                //{
                //    pagedResult = result.ToList();
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }
        [HttpGet("delete/{id}")]
        public async Task<IActionResult> DeleteEventStatistical(int id)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("DeleteEventStatistical", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }
        [HttpPost("add-event")]
        public async Task<IActionResult> AddEvent([FromBody] AddEventModels eventadd)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_StaffCode", eventadd.StaffCode);
                paramaters.Add("@v_Event", eventadd.Event);
                paramaters.Add("@v_Device", eventadd.Device);
                paramaters.Add("@v_RecordTime", eventadd.RecordTime);
                paramaters.Add("@v_User", eventadd.CreatedUser);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("AddEvent", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }
        [HttpPost("update-event/{id}")]
        public async Task<IActionResult> UpdateEvent(int id,[FromBody] AddEventModels eventadd)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_HisEventId", id);
                paramaters.Add("@v_StaffCode", eventadd.StaffCode);
                paramaters.Add("@v_Device", eventadd.Device);
                paramaters.Add("@v_RecordTime", eventadd.RecordTime);
                paramaters.Add("@v_User", eventadd.CreatedUser);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("UpdateEvent", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }
    }
}
