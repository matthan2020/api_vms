﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using API_VMS.Dtos;
using API_VMS.Models;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_VMS.Controllers
{
    [Route("api/timekeeping")]
    [ApiController]
    public class TimeKeepingController : ControllerBase
    {
        private readonly string _connectionString;
        public TimeKeepingController(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }
        // GET api/<FaceController>/5
        /// <summary>
        /// ham them moi gia tri vao bang timekeeping
        /// </summary>
        /// <param name="tkSetting"></param>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task<IActionResult> AddTimeKeeping([FromBody] TimeKeepingModels timeKeeping)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_HisEventId", timeKeeping.HisEventId);
                paramaters.Add("@v_DeviceId", timeKeeping.DeviceId);
                paramaters.Add("@v_StaffCode", timeKeeping.StaffCode);
                paramaters.Add("@v_RecordTime", timeKeeping.RecordTime);
                paramaters.Add("@v_isCheckIn", timeKeeping.IsCheckIn);
                paramaters.Add("@v_isCheckOut", timeKeeping.IsCheckOut);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("AddTimeKeeping", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }

    }
}
