﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using API_VMS.Dtos;
using API_VMS.Models;
using API_VMS.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_VMS.Controllers
{
    [Route("api/department")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly string _connectionString;
        public DepartmentController(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }
     
        // GET api/<FaceController>/5
        [HttpGet("get")]
        public async Task<IActionResult> GetListDepartment()
        {
            IEnumerable<DepartmentModels> result = new List<DepartmentModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<DepartmentModels>("GetListDepartment", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<FaceModels>()
                //{
                //    pagedResult = result.ToList();
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }
        [HttpPost("add")]
        public async Task<IActionResult> InsertDepartment([FromBody] DepartmentModels department)
        {
            //IEnumerable<TkStatisticModels> result = new List<TkStatisticModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Acronym", department.Acronym);
                paramaters.Add("@v_DepartmentName", department.DepartmentName);
                paramaters.Add("@v_IdCompany", department.IdCompany);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                   // await conn.ExecuteAsync("DeleteEventStatistical", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    await conn.ExecuteAsync("InsertDepartment", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    //if (result.ToList().Count == 0 && data == "OK")
                    //    error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
               
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                
                //return pagedResult;
            }
        }

        [HttpGet("get/{id}")]
        public async Task<IActionResult> GetListDepartmentById(int id)
        {
            IEnumerable<DepartmentModels> result = new List<DepartmentModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<DepartmentModels>("GetListDepartmentById", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<FaceModels>()
                //{
                //    pagedResult = result.ToList();
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpGet("get-name/{name}")]
        public async Task<IActionResult> GetListDepartmentByName(string name)
        {
            IEnumerable<DepartmentModels> result = new List<DepartmentModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Name", name.Trim());
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<DepartmentModels>("GetListDepartmentByName", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<FaceModels>()
                //{
                //    pagedResult = result.ToList();
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpPost("get-page")]
        public async Task<PagedResultWeb<DepartmentModels>> GetListDepartmentPage([FromBody] PageViewModels page)
        {
            string error = "";
            bool data = true;
            int totalRow = 0;
            IEnumerable<DepartmentModels> result = new List<DepartmentModels>();
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@KeyWord", page.KeyWord);
                paramaters.Add("@pageIndex", page.Page);
                paramaters.Add("@pageSize", page.PageSize);
                paramaters.Add("@totalRow", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<DepartmentModels>("GetListDepartmentPage", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    totalRow = paramaters.Get<int>("@totalRow");
                    if (error == "SUCCESS")
                    {
                        data = true;
                    }

                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == true)
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = false;
                }
                var pagedResult = new PagedResultWeb<DepartmentModels>()
                {
                    Items = result.ToList(),
                    Message = error,
                    Success = data,
                    TotalRow = totalRow,
                    PageSize = page.PageSize,
                    PageIndex = page.Page,

                };

                return pagedResult;
                //return pagedResult;
            }
        }
        [HttpPost("edit/{id}")]
        public async Task<IActionResult> UpdateDepartment(int id, [FromBody] DepartmentModels department)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@v_Acronym", department.Acronym);
                paramaters.Add("@v_DepartmentName", department.DepartmentName);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("UpdateDepartment", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }
        [HttpGet("delete/{id}")]
        public async Task<IActionResult> DeleteDepartmentById(int id)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("DeleteDepartmentById", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }
    }
}
