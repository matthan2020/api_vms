﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using API_VMS.Dtos;
using API_VMS.Models;
using API_VMS.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_VMS.Controllers
{
    [Route("api/config")]
    [ApiController]
    public class ConfigController : ControllerBase
    {
        private readonly string _connectionString;
        public ConfigController(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }
      
        // GET api/<FaceController>/
        /// <summary>
        /// ham them moi gia tri vao bang serverconfig
        /// </summary>
        /// <param name="tkSetting"></param>
        /// <returns></returns>
        [HttpPost("server/add")]
        public async Task<IActionResult> AddServerConfig([FromBody] ServerConfigModels config)
        {
            string error = "";
            string data ="OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_ServerName", config.ServerName);
                paramaters.Add("@v_IpServer", config.IpServer);
                paramaters.Add("@v_IpPort", config.IpPort);
                paramaters.Add("@v_LinkPath", config.LinkPath);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("AddServerConfig", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }

        // PUT api/<ValuesController>/5
      /// <summary>
      /// ham sua gia trị trong bang tksettting
      /// </summary>
      /// <param name="id"></param>
      /// <param name="tkSetting"></param>
        [HttpPost("server/edit/{id}")]
        public async Task<IActionResult> EditServerConfig(int id, [FromBody] ServerConfigModels config)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@v_ServerName", config.ServerName);
                paramaters.Add("@v_IpServer", config.IpServer);
                paramaters.Add("@v_IpPort", config.IpPort);
                paramaters.Add("@v_LinkPath", config.LinkPath);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("EditServerConfig", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }

        [HttpDelete("server/delete/{id}")]
        public async Task<IActionResult> DeleteServerConfigById(int id)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("DeleteServerConfigById", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }



        [HttpGet("server/get")]
        public async Task<IActionResult> GetServerConfig()
        {
            IEnumerable<ServerConfigViewModels> result = new List<ServerConfigViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<ServerConfigViewModels>("GetServerConfig", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

       
    }
}
