﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using API_VMS.Dtos;
using API_VMS.Models;
using API_VMS.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_VMS.Controllers
{
    [Route("api/face")]
    [ApiController]
    public class FaceController : ControllerBase
    {
        private readonly string _connectionString;
        private static string _connectionStringSocket;
        private readonly string _connectionPoint;
        IPEndPoint ip;
        Socket client;
        public FaceController(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
            _connectionStringSocket = configuration.GetConnectionString("ConnectionSocket");
            _connectionPoint = configuration.GetConnectionString("ConnectionPoint");
        }
        void Receive()
        {
            try
            {
                while (true)
                {
                    byte[] data = new byte[1024 * 5000];
                    client.Receive(data);
                    string msg = Deserialize(data).ToString();
                }
            }
            catch
            {
                Disconnect();
            }


        }
        object Deserialize(byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            BinaryFormatter formatter = new BinaryFormatter();
            return formatter.Deserialize(stream);
        }
        void Disconnect()
        {
            client.Close();
        }
        void Connect()
        {
            ip = new IPEndPoint(IPAddress.Parse(_connectionPoint), 9999);
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            try
            {
                client.Connect(ip);
            }
            catch
            {

            }
            Thread th = new Thread(Receive);
            th.IsBackground = true;
            th.Start();
        }
        byte[] Serialize(object obj)
        {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, obj);
            return stream.ToArray();

        }

        //---------------------- BEGIN--------------------//
        private static bool isPause;
        public static async Task StartWebsockets()
        {
            var client = new ClientWebSocket();
            await client.ConnectAsync(new Uri(_connectionStringSocket), CancellationToken.None);
            var send = Task.Run(async () =>
            {
                while (isPause == false)
                {
                    var bytes = Encoding.UTF8.GetBytes("SEND-OK");
                    await client.SendAsync(new ArraySegment<byte>(bytes), WebSocketMessageType.Text, true,
                        CancellationToken.None);
                    isPause = true;
                }

                await client.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);
            });
        }

        private static async Task ReceivedAsync(ClientWebSocket client)
        {
            var buffer = new byte[1024 * 4];

            while (true)
            {
                var result = await client.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                if (result.MessageType == WebSocketMessageType.Text)
                {
                    Console.WriteLine(Encoding.UTF8.GetString(buffer, 0, result.Count));
                }
                else if (result.MessageType == WebSocketMessageType.Close)
                {
                    await client.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);
                    break;
                }
            }
        }
        // -------------end
        // GET api/<FaceController>/5
        [HttpGet("get-face-by-id/{id}")]
        public async Task<IActionResult> GetFaceByFaceId(int id)
        {
            IEnumerable<FaceViewModels> result = new List<FaceViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<FaceViewModels>("GetFaceByFaceId", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<FaceModels>()
                //{
                //    pagedResult = result.ToList();
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        // GET api/<FaceController>/5
        /// <summary>
        /// ham them moi gia tri vao bang timekeeping
        /// </summary>
        /// <param name="tkSetting"></param>
        /// <returns></returns>
        [HttpPost("event-cloud")]
        public async Task<IActionResult> AddLicesePlates(IList<IFormFile> files, string EventId, string DeviceId, string RecordTime)
        {
            string error = "";
            string data = "OK";
            int idFace = 0;
            //int max = 0;
            string fileSave = "";
            string fileRoot = "";
            string faceName = "Unknown";
            int MaxContentLength = 1024 * 1024 * 20;
            try
            {
                if (files != null && files.Count > 0)
                {
                    var file = files[0];
                    var filename = ContentDispositionHeaderValue
                                       .Parse(file.ContentDisposition)
                                       .FileName
                                       .Trim('"');
                    var duoifile = filename.Substring(filename.Length - 4);
                    if (duoifile == ".jpg" || duoifile == ".gif" || duoifile == ".png")
                    {
                        if (files[0].Length > MaxContentLength)
                        {
                            error = "Bạn đã uplooad file ảnh trên 20 Mb";
                            data = "Error";
                        }
                        else
                        {
                            //using (var conn = new SqlConnection(_connectionString))
                            //{
                            //    conn.Open();
                            //    var paramaters = new DynamicParameters();
                            //    paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                            //    paramaters.Add("@vMax", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                            //    try
                            //    {
                            //        await conn.ExecuteAsync("PostMaxImageFace", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                            //        error = paramaters.Get<string>("@vError");
                            //        max = paramaters.Get<int>("@vMax");
                            //        if (error == "SUCCESS")
                            //        {
                            //            data = "OK";
                            //        }
                            //        else
                            //        {
                            //            data = "Failed";
                            //        }


                            //    }
                            //    catch (Exception ex)
                            //    {
                            //        error = ex.Message;
                            //        data = "Failed";
                            //    }
                            //    //  }
                            //}
                            //string sFileName = "{DateTime.Now:yyyyMMddhhmmss}";
                            var filePath1 = string.Format(@"C:\upload\images\event\" + $"{DateTime.Now:yyyyMMddhhmmss}" + "_face" + duoifile);
                            var filePath = string.Format(@"C:\upload\images\");
                            //file.SaveAs(filePath);
                            using (FileStream fs = System.IO.File.Create(filePath1))
                            {
                                file.CopyTo(fs);
                                fs.Flush();
                            }
                            //var filePath1 = string.Format(@"C:\upload\images\root\Untitled1.png");
                            string _tokenUpdated = "13ff2b7b42954550879e3d8eba394e3a";
                            var clientFace = new RestClient("https://api.luxand.cloud/photo/search");
                            clientFace.Timeout = -1;
                            var request = new RestRequest(Method.POST);
                            request.AddHeader("token", _tokenUpdated);
                            request.AddFile("photo", filePath1);
                            IRestResponse response = clientFace.Execute(request);
                            if (response.IsSuccessful == true)
                            {
                                //if (response.Content.Count() > 3)
                                //{
                                List<PagedResult> user = JsonConvert.DeserializeObject<List<PagedResult>>(response.Content);
                                if (user.Count > 0)
                                {
                                    idFace = user[0].id;
                                    faceName = user[0].name;
                                }
                                using (var conn = new SqlConnection(_connectionString))
                                {
                                    conn.Open();
                                    var paramaters = new DynamicParameters();
                                    paramaters.Add("@v_FaceId", idFace);
                                    paramaters.Add("@v_EventId", EventId);
                                    paramaters.Add("@v_DeviceId", DeviceId);
                                    paramaters.Add("@v_RecordTime", RecordTime);
                                    paramaters.Add("@v_filePath", filePath);
                                    paramaters.Add("@v_duoiFile", duoifile);
                                    paramaters.Add("@vFileSave", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                                    paramaters.Add("@vFileRoot", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                                    paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                                    try
                                    {
                                        await conn.ExecuteAsync("PostImageFaceOther", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                                        fileSave = paramaters.Get<string>("@vFileSave");
                                        fileRoot = paramaters.Get<string>("@vFileRoot");
                                        error = paramaters.Get<string>("@vError");
                                        if (error == "SUCCESS")
                                        {
                                            using (FileStream fs1 = System.IO.File.Create(fileSave))
                                            {
                                                file.CopyTo(fs1);
                                                fs1.Flush();
                                            }
                                            using (FileStream fs2 = System.IO.File.Create(fileRoot))
                                            {
                                                file.CopyTo(fs2);
                                                fs2.Flush();
                                            }
                                            data = "OK";
                                            Connect();
                                            client.Send(Serialize("OK"));
                                        }
                                        else
                                        {
                                            data = "Failed";
                                        }


                                    }
                                    catch (Exception ex)
                                    {
                                        error = ex.Message;
                                        data = "Failed";
                                    }
                                    //  }
                                }
                                if (data == "OK")
                                {
                                    string[] array1 = fileSave.ToString().Split('\\');

                                    string picName1 = array1[array1.Length - 1];
                                    //------------------------------------------------------------------------
                                    // gọi api viber

                                    string url = string.Format("https://chatapi.viber.com/pa/send_message");
                                    WebRequest request1 = WebRequest.Create(url);

                                    request1.Method = "POST";
                                    request1.ContentType = "application/json";
                                    request1.Headers.Add("X-Viber-Auth-Token", "4bf0a48d2a27d2e1-1ebc2cecf16cfe2f-e52d82630e8cfe8");

                                    string[] array = fileSave.ToString().Split('\\');

                                    string picName = array[array.Length - 1];

                                    string postdata = "{\"receiver\":\"0lKTzYJTspY9mM5RTcjnFg==\",\"min_api_version\":\"1\",\"sender\":{\"name\":\"AZVision\"},\"tracking_data\":\"tracking data\",\"type\":\"picture\",\"text\":\"" + faceName + " đến lúc " + RecordTime + "\",\"media\":\"http://27.72.88.226:8081/img/event/" + picName + "\"}";

                                    using (var streamWriter = new StreamWriter(request1.GetRequestStream()))
                                    {
                                        streamWriter.Write(postdata);
                                        streamWriter.Flush();
                                        streamWriter.Close();

                                        var httpResponse = (HttpWebResponse)request1.GetResponse();
                                        if (httpResponse.StatusCode == HttpStatusCode.OK)
                                        {
                                            //Connect();
                                            //client.Send(Serialize("OK"));
                                        }
                                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                                        {
                                            var result2 = streamReader.ReadToEnd();
                                        }

                                    }
                                }

                            }
                            else
                            {
                                error = response.ErrorMessage;
                                data = "Error";
                            }
                            if (System.IO.File.Exists(filePath1))
                            {
                                try
                                {
                                    System.IO.File.Delete(filePath1);
                                }
                                catch (Exception ex)
                                {
                                    //Do something
                                }
                            }
                        }
                    }
                    else
                    {
                        error = "File ảnh k đúng định dạng";
                        data = "Error";

                    }
                }
                else
                {
                    error = "Không có ảnh để cập nhật";
                    data = "Error";
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
                data = "Error";
            }


            var pagedResult = new PagedResultNoData()
            {
                Message = error,
                Status = data,
            };
            return Ok(pagedResult);
            //return pagedResult;
            // }
        }
        //public async Task<IActionResult> AddLicesePlates(IList<IFormFile> files, string EventId, string DeviceId, string RecordTime)
        //{
        //    string error = "";
        //    string data = "OK";
        //    int idFace = 0;
        //    int max = 0;
        //    string faceName = "Unknown";
        //    int MaxContentLength = 1024 * 1024 * 20;
        //    try
        //    {
        //        if (files != null && files.Count > 0)
        //        {
        //            var file = files[0];
        //            var filename = ContentDispositionHeaderValue
        //                               .Parse(file.ContentDisposition)
        //                               .FileName
        //                               .Trim('"');
        //            var duoifile = filename.Substring(filename.Length - 4);
        //            if (duoifile == ".jpg" || duoifile == ".gif" || duoifile == ".png")
        //            {
        //                if (files[0].Length > MaxContentLength)
        //                {
        //                    error = "Bạn đã uplooad file ảnh trên 20 Mb";
        //                    data = "Error";
        //                }
        //                else
        //                {
        //                    using (var conn = new SqlConnection(_connectionString))
        //                    {
        //                        conn.Open();
        //                        var paramaters = new DynamicParameters();
        //                        paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
        //                        paramaters.Add("@vMax", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
        //                        try
        //                        {
        //                            await conn.ExecuteAsync("PostMaxImageFace", paramaters, null, null, System.Data.CommandType.StoredProcedure);

        //                            error = paramaters.Get<string>("@vError");
        //                            max = paramaters.Get<int>("@vMax");
        //                            if (error == "SUCCESS")
        //                            {
        //                                data = "OK";
        //                            }
        //                            else
        //                            {
        //                                data = "Failed";
        //                            }


        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            error = ex.Message;
        //                            data = "Failed";
        //                        }
        //                        //  }
        //                    }
        //                    var filePath = string.Format(@"C:\upload\images\event\" + max + "_face" + duoifile);
        //                    //file.SaveAs(filePath);
        //                    using (FileStream fs = System.IO.File.Create(filePath))
        //                    {
        //                        file.CopyTo(fs);
        //                        fs.Flush();
        //                    }
        //                    //var filePath1 = string.Format(@"C:\upload\images\root\Untitled1.png");
        //                    string _tokenUpdated = "13ff2b7b42954550879e3d8eba394e3a";
        //                    var clientFace = new RestClient("https://api.luxand.cloud/photo/search");
        //                    clientFace.Timeout = -1;
        //                    var request = new RestRequest(Method.POST);
        //                    request.AddHeader("token", _tokenUpdated);
        //                    request.AddFile("photo", filePath);
        //                    IRestResponse response = clientFace.Execute(request);
        //                    if (response.IsSuccessful == true)
        //                    {
        //                        //if (response.Content.Count() > 3)
        //                        //{
        //                        List<PagedResult> user = JsonConvert.DeserializeObject<List<PagedResult>>(response.Content);
        //                        if (user.Count > 0)
        //                        {
        //                            idFace = user[0].id;
        //                            faceName = user[0].name;
        //                        }
        //                        using (var conn = new SqlConnection(_connectionString))
        //                        {
        //                            conn.Open();
        //                            var paramaters = new DynamicParameters();
        //                            paramaters.Add("@v_FaceId", idFace);
        //                            paramaters.Add("@v_EventId", EventId);
        //                            paramaters.Add("@v_DeviceId", DeviceId);
        //                            paramaters.Add("@v_RecordTime", RecordTime);
        //                            paramaters.Add("@v_filePath", filePath);
        //                            paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
        //                            try
        //                            {
        //                                await conn.ExecuteAsync("PostImageFace", paramaters, null, null, System.Data.CommandType.StoredProcedure);

        //                                error = paramaters.Get<string>("@vError");
        //                                if (error == "SUCCESS")
        //                                {
        //                                    data = "OK";
        //                                    Connect();
        //                                    client.Send(Serialize("OK"));
        //                                }
        //                                else
        //                                {
        //                                    data = "Failed";
        //                                }


        //                            }
        //                            catch (Exception ex)
        //                            {
        //                                error = ex.Message;
        //                                data = "Failed";
        //                            }
        //                            //  }
        //                        }
        //                        if (data == "OK")
        //                        {
        //                            //------------------------------------------------------------------------
        //                            // gọi api viber

        //                            string url = string.Format("https://chatapi.viber.com/pa/send_message");
        //                            WebRequest request1 = WebRequest.Create(url);

        //                            request1.Method = "POST";
        //                            request1.ContentType = "application/json";
        //                            request1.Headers.Add("X-Viber-Auth-Token", "4bf0a48d2a27d2e1-1ebc2cecf16cfe2f-e52d82630e8cfe8");

        //                            string[] array = filePath.ToString().Split('\\');

        //                            string picName = array[array.Length - 1];

        //                            string postdata = "{\"receiver\":\"0lKTzYJTspY9mM5RTcjnFg==\",\"min_api_version\":\"1\",\"sender\":{\"name\":\"AZVision\"},\"tracking_data\":\"tracking data\",\"type\":\"picture\",\"text\":\"" + faceName + " đến lúc " + RecordTime + "\",\"media\":\"http://27.72.88.226:8081/img/event/" + picName + "\"}";

        //                            using (var streamWriter = new StreamWriter(request1.GetRequestStream()))
        //                            {
        //                                streamWriter.Write(postdata);
        //                                streamWriter.Flush();
        //                                streamWriter.Close();

        //                                var httpResponse = (HttpWebResponse)request1.GetResponse();
        //                                if (httpResponse.StatusCode == HttpStatusCode.OK)
        //                                {
        //                                    //Connect();
        //                                    //client.Send(Serialize("OK"));
        //                                }
        //                                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        //                                {
        //                                    var result2 = streamReader.ReadToEnd();
        //                                }

        //                            }
        //                        }

        //                    }
        //                    else
        //                    {
        //                        error = response.ErrorMessage;
        //                        data = "Error";
        //                    }



        //                }
        //            }
        //            else
        //            {
        //                error = "File ảnh k đúng định dạng";
        //                data = "Error";

        //            }
        //        }
        //        else
        //        {
        //            error = "Không có ảnh để cập nhật";
        //            data = "Error";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        error = ex.Message;
        //        data = "Error";
        //    }


        //    var pagedResult = new PagedResultNoData()
        //    {
        //        Message = error,
        //        Status = data,
        //    };
        //    return Ok(pagedResult);
        //    //return pagedResult;
        //    // }
        //}

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeletefaceById(int id)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("DeletefaceById", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }

        [HttpGet("base-search/{keyword}")]
        public async Task<IActionResult> GetFaceBaseSearch(string keyword)
        {
            IEnumerable<FaceViewModels> result = new List<FaceViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Keyword", keyword);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<FaceViewModels>("GetFaceBaseSearch", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpPost("advanced-search")]
        public async Task<IActionResult> GetFaceAdvancedSearch([FromBody] FaceSearchNotPagViewModels Face)
        {
            IEnumerable<FaceViewModels> result = new List<FaceViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_FaceName", Face.FaceName);
                paramaters.Add("@v_AgeYear", Face.AgeYear);
                paramaters.Add("@v_Job", Face.Job);
                paramaters.Add("@v_Sex", Face.Sex);
                paramaters.Add("@v_IsKnown", Face.IsKnown);
                paramaters.Add("@v_ImportantLevel", Face.ImportantLevel); 
                paramaters.Add("@v_DepartmentId", Face.DepartmentId);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<FaceViewModels>("GetFaceAdvancedSearch", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }
        [HttpPost("face-search-page")]
        public async Task<PagedResultWeb<FaceViewModels>> GetFaceSearchPage([FromBody] FaceSearchViewModels Face)
        {
            IEnumerable<FaceViewModels> result = new List<FaceViewModels>();
            string error = "";
            bool data = true;
            int totalRow = 0;
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Keyword", Face.Search);
                paramaters.Add("@v_FaceName", Face.FaceName);
                paramaters.Add("@v_StaffCode", Face.StaffCode);
                paramaters.Add("@v_AgeYear", Face.AgeYear);
                paramaters.Add("@v_Job", Face.Job);
                paramaters.Add("@v_Sex", Face.Sex);
                paramaters.Add("@v_IsKnown", Face.IsKnown);
                paramaters.Add("@v_ImportantLevel", Face.ImportantLevel);
                paramaters.Add("@v_DepartmentId", Face.DepartmentId);
                paramaters.Add("@pageIndex", Face.Page);
                paramaters.Add("@pageSize", Face.PageSize);
                paramaters.Add("@totalRow", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<FaceViewModels>("GetFaceSearchPage", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    totalRow = paramaters.Get<int>("@totalRow");
                    if (error == "SUCCESS")
                    {
                        data = true;
                    }

                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == true)
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = false;
                }
                var pagedResult = new PagedResultWeb<FaceViewModels>()
                {
                    Items = result.ToList(),
                    Message = error,
                    Success = data,
                    TotalRow = totalRow,
                    PageSize = Face.PageSize,
                    PageIndex = Face.Page,

                };

                return pagedResult;
            }
        }

        [HttpPost("base-search-page-keyword")]
        public async Task<PagedResultWeb<FaceViewModels>> GetFaceBaseSearchPage([FromBody] DevicePageViewModels Face)
        {
            IEnumerable<FaceViewModels> result = new List<FaceViewModels>();
            string error = "";
            bool data = true;
            int totalRow = 0;
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Keyword", Face.KeyWord);
                paramaters.Add("@pageIndex", Face.Page);
                paramaters.Add("@pageSize", Face.PageSize);
                paramaters.Add("@totalRow", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<FaceViewModels>("GetFaceBaseSearchPage", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    totalRow = paramaters.Get<int>("@totalRow");
                    if (error == "SUCCESS")
                    {
                        data = true;
                    }

                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == true)
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = false;
                }
                var pagedResult = new PagedResultWeb<FaceViewModels>()
                {
                    Items = result.ToList(),
                    Message = error,
                    Success = data,
                    TotalRow = totalRow,
                    PageSize = Face.PageSize,
                    PageIndex = Face.Page,

                };

                return pagedResult;
            }
        }

        [HttpPost("advanced-search-page")]
        public async Task<PagedResultWeb<FaceViewModels>> GetFaceAdvancedSearchPage([FromBody] FaceSearchViewModels Face)
        {
            IEnumerable<FaceViewModels> result = new List<FaceViewModels>();
            string error = "";
            bool data = true;
            int totalRow = 0;
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_FaceName", Face.FaceName);
                paramaters.Add("@v_StaffCode", Face.StaffCode);
                paramaters.Add("@v_AgeYear", Face.AgeYear);
                paramaters.Add("@v_Job", Face.Job);
                paramaters.Add("@v_Sex", Face.Sex);
                paramaters.Add("@v_IsKnown", Face.IsKnown);
                paramaters.Add("@v_ImportantLevel", Face.ImportantLevel);
                paramaters.Add("@v_DepartmentId", Face.DepartmentId);
                paramaters.Add("@pageIndex", Face.Page);
                paramaters.Add("@pageSize", Face.PageSize);
                paramaters.Add("@totalRow", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<FaceViewModels>("GetFaceAdvancedSearchPage", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    totalRow = paramaters.Get<int>("@totalRow");
                    if (error == "SUCCESS")
                    {
                        data = true;
                    }

                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == true)
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = false;
                }
                var pagedResult = new PagedResultWeb<FaceViewModels>()
                {
                    Items = result.ToList(),
                    Message = error,
                    Success = data,
                    TotalRow = totalRow,
                    PageSize = Face.PageSize,
                    PageIndex = Face.Page,

                };

                return pagedResult;
            }
        }

        [HttpGet("info/event/{id}")]
        public async Task<IActionResult> GetInfoEventById(int id)
        {
            IEnumerable<TblImageEventHistoryViewModels> result = new List<TblImageEventHistoryViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TblImageEventHistoryViewModels>("GetInfoEventById", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<FaceModels>()
                //{
                //    pagedResult = result.ToList();
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }


        [HttpPost("info/update/{faceid}")]
        public async Task<IActionResult> UpdateInfo(int faceid, [FromBody] TblFaceModels face)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_FaceId", faceid);
                paramaters.Add("@v_StaffCode", face.StaffCode);
                paramaters.Add("@v_FaceName", face.FaceName);
                paramaters.Add("@v_AgeYear", face.AgeYear);
                paramaters.Add("@v_DepartmentId", face.DepartmentId);
                paramaters.Add("@v_Job", face.Job);
                paramaters.Add("@v_Sex", face.Sex);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("UpdateInfo", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }
        [HttpPost("info/add")]
        public async Task<IActionResult> AddInfoFace( [FromBody] FaceSearchNotPagViewModels face)
        {
            string error = "";
            string data = "OK";
            int totalRow = 0;
            var fileSave = "";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_StaffCode", face.StaffCode);
                paramaters.Add("@v_FaceName", face.FaceName);
                paramaters.Add("@v_AgeYear", face.AgeYear);
                paramaters.Add("@v_DepartmentId", face.DepartmentId);
                paramaters.Add("@v_Job", face.Job);
                paramaters.Add("@v_Sex", face.Sex);
                paramaters.Add("@v_FaceIdCloud", face.FaceIdCloud);
                paramaters.Add("@v_ImportantLevel", face.ImportantLevel);
                paramaters.Add("@v_ImagePath", face.Base64ContentUrl);
                paramaters.Add("@v_duoiFile", face.ImagePath);
                paramaters.Add("@vFileSave", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@Id", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("AddInfoFace", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    totalRow = paramaters.Get<int>("@Id");
                    fileSave = paramaters.Get<string>("@vFileSave");
                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultID()
                {
                    id = totalRow,
                    FileSave= fileSave,
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }
        [HttpPost("info/add-face-img")]
        public async Task<IActionResult> AddInfoFaceImg([FromBody] FaceSearchNotPagViewModels face)
        {
            string error = "";
            string data = "OK";
            var fileSave = "";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_FaceId", face.FaceId);
                paramaters.Add("@v_ImagePath", face.Base64ContentUrl);
                paramaters.Add("@v_duoiFile", face.ImagePath);
                paramaters.Add("@vFileSave", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("AddInfoFaceImg", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    fileSave = paramaters.Get<string>("@vFileSave");
                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultID()
                {
                    FileSave = fileSave,
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }
        [HttpGet("delete-face-list-id/{id}")]
        public async Task<IActionResult> DeletefaceByListId(string id)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("DeletefaceByListId", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }

        [HttpGet("get-face-by-id-top10-img/{id}")]
        public async Task<IActionResult> GetFaceByIdTop10(int id)
        {
            IEnumerable<FaceViewModels> result = new List<FaceViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<FaceViewModels>("GetFaceByIdTop10", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<FaceModels>()
                //{
                //    pagedResult = result.ToList();
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpPost("update-face/{faceid}")]
        public async Task<IActionResult> UpdateFace(int faceid, [FromBody] TblFaceModels face)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_FaceId", faceid);
                paramaters.Add("@v_StaffCode", face.StaffCode);
                paramaters.Add("@v_FaceName", face.FaceName);
                paramaters.Add("@v_AgeYear", face.AgeYear);
                paramaters.Add("@v_DepartmentId", face.DepartmentId);
                paramaters.Add("@v_Job", face.Job);
                paramaters.Add("@v_Sex", face.Sex);
                paramaters.Add("@v_ImportantLevel", face.ImportantLevel);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("UpdateFace", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }

        [HttpGet("check-staffcode/{staffCode}")]
        public async Task<IActionResult> CheckStaffCode(string staffCode)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_StaffCode", staffCode);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("CheckStaffCode", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }

        /// <summary>
        /// ham them moi gia tri vao bang timekeeping
        /// </summary>
        /// <param name="tkSetting"></param>
        /// <returns></returns>
        [HttpPost("event")]
        public async Task<IActionResult> AddLicesePlatesNotCloud(IList<IFormFile> Files,int FaceId, string EventId, string DeviceId, string RecordTime)
        {
            string error = "";
            string data = "OK";
            //int idFace = 0;
            //int max = 0;
            string fileSave = "";
            //string fileRoot = "";
            string faceName = "Unknown";
            int MaxContentLength = 1024 * 1024 * 20;
            try
            {
                if (Files != null && Files.Count > 0)
                {
                    var file = Files[0];
                    var filename = ContentDispositionHeaderValue
                                       .Parse(file.ContentDisposition)
                                       .FileName
                                       .Trim('"');
                    var duoifile = filename.Substring(filename.Length - 4);
                    if (duoifile == ".jpg" || duoifile == ".gif" || duoifile == ".png")
                    {
                        if (Files[0].Length > MaxContentLength)
                        {
                            error = "Bạn đã uplooad file ảnh trên 20 Mb";
                            data = "Error";
                        }
                        else
                        {
                            
                            //var filePath1 = string.Format(@"C:\upload\images\event\" + $"{DateTime.Now:yyyyMMddhhmmss}" + "_face" + duoifile);
                            var filePath = string.Format(@"C:\upload\images\");
                                using (var conn = new SqlConnection(_connectionString))
                                {
                                    conn.Open();
                                    var paramaters = new DynamicParameters();
                                    paramaters.Add("@v_FaceId", FaceId);
                                    paramaters.Add("@v_EventId", EventId);
                                    paramaters.Add("@v_DeviceId", DeviceId);
                                    paramaters.Add("@v_RecordTime", RecordTime);
                                    paramaters.Add("@v_filePath", filePath);
                                    paramaters.Add("@v_duoiFile", duoifile);
                                    paramaters.Add("@vFileSave", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                                    //paramaters.Add("@vFileRoot", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                                    paramaters.Add("@vFaceName", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                                    paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                                    try
                                    {
                                        await conn.ExecuteAsync("PostImageFaceNoCloud", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                                        fileSave = paramaters.Get<string>("@vFileSave");
                                        //fileRoot = paramaters.Get<string>("@vFileRoot");
                                        error = paramaters.Get<string>("@vError");
                                    if (FaceId > 0)
                                    {
                                        faceName=paramaters.Get<string>("@vFaceName");
                                    }
                                        if (error == "SUCCESS")
                                        {
                                            using (FileStream fs1 = System.IO.File.Create(fileSave))
                                            {
                                                file.CopyTo(fs1);
                                                fs1.Flush();
                                            }
                                        //    if(fileRoot !=null&& fileRoot != "")
                                        //{
                                        //    using (FileStream fs2 = System.IO.File.Create(fileRoot))
                                        //    {
                                        //        file.CopyTo(fs2);
                                        //        fs2.Flush();
                                        //    }
                                        //}
                                            
                                            data = "OK";
                                            Connect();
                                            client.Send(Serialize("OK"));
                                        //---------- net core api
                                        isPause = false;
                                        StartWebsockets().GetAwaiter().GetResult();
                                        //Thread th = new Thread(() =>
                                        //{
                                        //    StartWebsockets().GetAwaiter().GetResult();
                                        //});
                                        //th.IsBackground = true;
                                        //th.Start();
                                        //------------------------
                                    }
                                    else
                                        {
                                            data = "Failed";
                                        }


                                    }
                                    catch (Exception ex)
                                    {
                                        error = ex.Message;
                                        data = "Failed";
                                    }
                                    //  }
                                }
                                if (data == "OK")
                                {
                                    string[] array1 = fileSave.ToString().Split('\\');

                                    string picName1 = array1[array1.Length - 1];
                                    //------------------------------------------------------------------------
                                    // gọi api viber

                                    string url = string.Format("https://chatapi.viber.com/pa/send_message");
                                    WebRequest request1 = WebRequest.Create(url);

                                    request1.Method = "POST";
                                    request1.ContentType = "application/json";
                                    request1.Headers.Add("X-Viber-Auth-Token", "4bf0a48d2a27d2e1-1ebc2cecf16cfe2f-e52d82630e8cfe8");

                                    string[] array = fileSave.ToString().Split('\\');

                                    string picName = array[array.Length - 1];

                                    string postdata = "{\"receiver\":\"0lKTzYJTspY9mM5RTcjnFg==\",\"min_api_version\":\"1\",\"sender\":{\"name\":\"AZVision\"},\"tracking_data\":\"tracking data\",\"type\":\"picture\",\"text\":\"" + faceName + " đến lúc " + RecordTime + "\",\"media\":\"http://27.72.88.226:8081/img/event/" + picName + "\"}";

                                    using (var streamWriter = new StreamWriter(request1.GetRequestStream()))
                                    {
                                        streamWriter.Write(postdata);
                                        streamWriter.Flush();
                                        streamWriter.Close();

                                        var httpResponse = (HttpWebResponse)request1.GetResponse();
                                        if (httpResponse.StatusCode == HttpStatusCode.OK)
                                        {
                                            //Connect();
                                            //client.Send(Serialize("OK"));
                                        }
                                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                                        {
                                            var result2 = streamReader.ReadToEnd();
                                        }

                                    }
                                //goi them 1 lan viber nua
                                WebRequest request2 = WebRequest.Create(url);

                                request2.Method = "POST";
                                request2.ContentType = "application/json";
                                request2.Headers.Add("X-Viber-Auth-Token", "4bf0a48d2a27d2e1-1ebc2cecf16cfe2f-e52d82630e8cfe8");
                                string postdata1 = "{\"receiver\":\"L1Umm1SGuu1G69VxYymqSw==\",\"min_api_version\":\"1\",\"sender\":{\"name\":\"AZVision\"},\"tracking_data\":\"tracking data\",\"type\":\"picture\",\"text\":\"" + faceName + " đến lúc " + RecordTime + "\",\"media\":\"http://27.72.88.226:8081/img/event/" + picName + "\"}";

                                using (var streamWriter1 = new StreamWriter(request2.GetRequestStream()))
                                {
                                    streamWriter1.Write(postdata1);
                                    streamWriter1.Flush();
                                    streamWriter1.Close();

                                    var httpResponse1 = (HttpWebResponse)request2.GetResponse();
                                    if (httpResponse1.StatusCode == HttpStatusCode.OK)
                                    {
                                        //Connect();
                                        //client.Send(Serialize("OK"));
                                    }
                                    using (var streamReader1 = new StreamReader(httpResponse1.GetResponseStream()))
                                    {
                                        var result3 = streamReader1.ReadToEnd();
                                    }

                                }
                            }

                           
                            
                        }
                    }
                    else
                    {
                        error = "File ảnh k đúng định dạng";
                        data = "Error";

                    }
                }
                else
                {
                    error = "Không có ảnh để cập nhật";
                    data = "Error";
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
                data = "Error";
            }


            var pagedResult = new PagedResultNoData()
            {
                Message = error,
                Status = data,
            };
            return Ok(pagedResult);
            //return pagedResult;
            // }
        }
        [HttpGet("get")]
        public async Task<IActionResult> GetListFace()
        {
            IEnumerable<TblFaceModels> result = new List<TblFaceModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TblFaceModels>("GetListFace", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<FaceModels>()
                //{
                //    pagedResult = result.ToList();
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }
    }
    
}
