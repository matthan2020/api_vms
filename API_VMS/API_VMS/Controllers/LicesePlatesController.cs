﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using API_VMS.Dtos;
using API_VMS.Models;
using API_VMS.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_VMS.Controllers
{
    [Route("api/liceseplates")]
    [ApiController]
    public class LicesePlatesController : ControllerBase
    {
        private readonly string _connectionString;
        //private readonly IHttpClientFactory _httpClientFactory;
        //private readonly IConfiguration _configuration;
        IPEndPoint ip;
        Socket client;
        public LicesePlatesController(//IHttpClientFactory httpClientFactory,
                    IConfiguration configuration)
        {
           // _configuration = configuration;
           // _httpClientFactory = httpClientFactory;
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }
        void Receive()
        {
            try
            {
                while (true)
                {
                    byte[] data = new byte[1024 * 5000];
                    client.Receive(data);
                    string msg = Deserialize(data).ToString();
                }
            }
            catch
            {
                Disconnect();
            }


        }
        object Deserialize(byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            BinaryFormatter formatter = new BinaryFormatter();
            return formatter.Deserialize(stream);
        }
        void Disconnect()
        {
            client.Close();
        }
        void Connect()
        {
            ip = new IPEndPoint(IPAddress.Parse("192.168.1.168"), 9999);
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            try
            {
                client.Connect(ip);
            }
            catch
            {

            }
            Thread th = new Thread(Receive);
            th.IsBackground = true;
            th.Start();
        }
        byte[] Serialize(object obj)
        {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, obj);
            return stream.ToArray();

        }
        // GET api/<FaceController>/5
        /// <summary>
        /// ham them moi gia tri vao bang timekeeping
        /// </summary>
        /// <param name="tkSetting"></param>
        /// <returns></returns>
        [HttpPost("liceseplates-upload")]
        public async Task<IActionResult> AddLicesePlates(IList<IFormFile> files,string LicesePlatesName,string EventId,string DeviceId,string RecordTime)
        {
            string error = "";
            string data = "OK";
            int max = 0;
            string faceName = "Unknown";
            int MaxContentLength = 1024 * 1024 * 20;
            try
            {
                if (files != null && files.Count > 0)
                {
                    var file = files[0];
                    var filename = ContentDispositionHeaderValue
                                       .Parse(file.ContentDisposition)
                                       .FileName
                                       .Trim('"');
                    var duoifile = filename.Substring(filename.Length - 4);
                    if (duoifile == ".jpg" || duoifile == ".gif" || duoifile == ".png")
                    {
                        if (files[0].Length > MaxContentLength)
                        {
                            error = "Bạn đã uplooad file ảnh trên 20 Mb";
                            data = "Error";
                        }
                        else
                        {
                            using (var conn = new SqlConnection(_connectionString))
                            {
                                conn.Open();
                                var paramaters = new DynamicParameters();
                                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                                paramaters.Add("@vMax", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                                try
                                {
                                    await conn.ExecuteAsync("PostMaxImageLicesePlates", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                                    error = paramaters.Get<string>("@vError");
                                    max = paramaters.Get<int>("@vMax");
                                    if (error == "SUCCESS")
                                    {
                                        data = "OK";
                                    }
                                    else
                                    {
                                        data = "Failed";
                                    }


                                }
                                catch (Exception ex)
                                {
                                    error = ex.Message;
                                    data = "Failed";
                                }
                                //  }
                            }
                            var filePath = string.Format(@"C:\upload\images\root\liceseplates" + max + "_licesePlates"+ duoifile);
                            //file.SaveAs(filePath);
                            using (FileStream fs = System.IO.File.Create(filePath))
                            {
                                file.CopyTo(fs);
                                fs.Flush();
                            }
                           
                                    using (var conn = new SqlConnection(_connectionString))
                                    {
                                        conn.Open();
                                        var paramaters = new DynamicParameters();
                                        paramaters.Add("@v_LicesePlatesName", LicesePlatesName);
                                        paramaters.Add("@v_EventId", EventId);
                                        paramaters.Add("@v_DeviceId", DeviceId); 
                                        paramaters.Add("@v_RecordTime", RecordTime);
                                        paramaters.Add("@v_filePath", filePath);
                                        paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                                        try
                                        {
                                            await conn.ExecuteAsync("PostImageFaceLicesePlates", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                                            error = paramaters.Get<string>("@vError");
                                            if (error == "SUCCESS")
                                            {
                                                data = "OK";
                                            }
                                            else
                                            {
                                                data = "Failed";
                                            }


                                        }
                                        catch (Exception ex)
                                        {
                                            error = ex.Message;
                                            data = "Failed";
                                        }
                                  //  }
                                }
                                   if (data == "OK")
                                {
                                    //------------------------------------------------------------------------
                                    // gọi api viber

                                    string url = string.Format("https://chatapi.viber.com/pa/send_message");
                                    WebRequest request1 = WebRequest.Create(url);

                                    request1.Method = "POST";
                                    request1.ContentType = "application/json";
                                    request1.Headers.Add("X-Viber-Auth-Token", "4bf0a48d2a27d2e1-1ebc2cecf16cfe2f-e52d82630e8cfe8");

                                    string[] array = filePath.ToString().Split('\\');

                                    string picName = array[array.Length - 1];

                                    string postdata = "{\"receiver\":\"0lKTzYJTspY9mM5RTcjnFg==\",\"min_api_version\":\"1\",\"sender\":{\"name\":\"AZVision\"},\"tracking_data\":\"tracking data\",\"type\":\"picture\",\"text\":\"" + faceName + " đến lúc " + RecordTime + "\",\"media\":\"http://171.244.49.1/img/root/" + picName + "\"}";

                                    using (var streamWriter = new StreamWriter(request1.GetRequestStream()))
                                    {
                                        streamWriter.Write(postdata);
                                        streamWriter.Flush();
                                        streamWriter.Close();

                                        var httpResponse = (HttpWebResponse)request1.GetResponse();
                                        if (httpResponse.StatusCode == HttpStatusCode.OK)
                                        {
                                            Connect();
                                            client.Send(Serialize("OK"));
                                        }
                                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                                        {
                                            var result2 = streamReader.ReadToEnd();
                                        }

                                    }
                                }
                                
                            


                            
                        }
                    }
                    else
                    {
                        error = "File ảnh k đúng định dạng";
                        data = "Error";

                    }
                }
                else
                {
                    error = "Không có ảnh để cập nhật";
                    data = "Error";
                }
            }
            catch(Exception ex)
            {
                error = ex.Message;
                data = "Error";
            }
           
            
            var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
            return Ok(pagedResult);
                //return pagedResult;
           // }
        }

        // GET api/<FaceController>/5
        /// <summary>
        /// ham lay 5 gia tri gan nhat
        /// </summary>
        /// <param name="tkSetting"></param>
        /// <returns></returns>
        [HttpGet("liceseplates-list")]
        public async Task<IActionResult> GetLicesePlatesTop5()
        {
            IEnumerable<LicesePlatesViewModels> result = new List<LicesePlatesViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<LicesePlatesViewModels>("GetLicesePlatesTop5", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

    }
}
