﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using API_VMS.Dtos;
using API_VMS.Models;
using API_VMS.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_VMS.Controllers
{
    [Route("api/provider")]
    [ApiController]
    public class ProviderController : ControllerBase
    {
        private readonly string _connectionString;
        public ProviderController(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }
      
        // GET api/<FaceController>/
        /// <summary>
        /// ham them moi gia tri vao bang serverconfig
        /// </summary>
        /// <param name="tkSetting"></param>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task<IActionResult> AddProvider([FromBody] TblProviderModels provider)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_ProviderName", provider.ProviderName);
                paramaters.Add("@v_ProviderAddress", provider.ProviderAddress);
                paramaters.Add("@v_ProviderPhone", provider.ProviderPhone);
                paramaters.Add("@v_ProviderBegin", provider.ProviderBegin);
                paramaters.Add("@v_ProviderNote", provider.ProviderNote);
                paramaters.Add("@v_CreatedUserId", provider.CreatedUserId);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("AddProvider", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }

        // PUT api/<ValuesController>/5
      /// <summary>
      /// ham sua gia trị trong bang tksettting
      /// </summary>
      /// <param name="id"></param>
      /// <param name="tkSetting"></param>
        [HttpPost("edit")]
        public async Task<IActionResult> UpdateProvider( [FromBody] TblProviderModels provider)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_ProviderId", provider.ProviderId);
                paramaters.Add("@v_ProviderName", provider.ProviderName);
                paramaters.Add("@v_ProviderAddress", provider.ProviderAddress);
                paramaters.Add("@v_ProviderPhone", provider.ProviderPhone);
                paramaters.Add("@v_ProviderBegin", provider.ProviderBegin);
                paramaters.Add("@v_ProviderNote", provider.ProviderNote);
                paramaters.Add("@v_CreatedUserId", provider.CreatedUserId);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("UpdateProvider", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }

        [HttpPost("delete/{id}")]
        public async Task<IActionResult> DeleteProvider( int id)
        {
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    await conn.ExecuteAsync("DeleteProvider", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                var pagedResult = new PagedResultNoData()
                {
                    Message = error,
                    Status = data,
                };
                return Ok(pagedResult);
                //return pagedResult;
            }
        }

      



        [HttpGet("get/{id}")]
        public async Task<IActionResult> GetProviderById(int id)
        {
            IEnumerable<TblProviderModels> result = new List<TblProviderModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TblProviderModels>("GetProviderById", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpGet("get")]
        public async Task<IActionResult> GetListProvider()
        {
            IEnumerable<TblProviderModels> result = new List<TblProviderModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TblProviderModels>("GetListProvider", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<FaceModels>()
                //{
                //    pagedResult = result.ToList();
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

      
    }
}
