﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using API_VMS.Dtos;
using API_VMS.Models;
using API_VMS.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_VMS.Controllers
{
    [Route("api/vehicle")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly string _connectionString;
        public VehicleController(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
        }
      
        [HttpGet("port-in")]
        public async Task<IActionResult> GetListPortIn()
        {
            IEnumerable<TblPortModels> result = new List<TblPortModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TblPortModels>("GetListPortIn", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpGet("port-out")]
        public async Task<IActionResult> GetListPortOut()
        {
            IEnumerable<TblPortModels> result = new List<TblPortModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TblPortModels>("GetListPortOut", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }
        [HttpGet("device")]
        public async Task<IActionResult> GetListDevicePort()
        {
            IEnumerable<TblDevicePortModels> result = new List<TblDevicePortModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<TblDevicePortModels>("GetListDevicePort", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpGet("device/{id}")]
        public async Task<IActionResult> GetListDevicePortById(int id)
        {
            IEnumerable<DevicePortViewModels> result = new List<DevicePortViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@v_Id", id);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<DevicePortViewModels>("GetListDevicePortById", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpGet("license-plate/event-in")]
        public async Task<IActionResult> GetListLicensePlateIn()
        {
            IEnumerable<LicensePlateViewModels> result = new List<LicensePlateViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<LicensePlateViewModels>("GetListLicensePlateIn", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpGet("license-plate/event-out")]
        public async Task<IActionResult> GetListLicensePlateOut()
        {
            IEnumerable<LicensePlateViewModels> result = new List<LicensePlateViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<LicensePlateViewModels>("GetListLicensePlateOut", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpGet("license-plate/overview")]
        public async Task<IActionResult> GetCountLicensePlate()
        {
            IEnumerable<CountVehicleModels> result = new List<CountVehicleModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<CountVehicleModels>("GetCountLicensePlate", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    return Ok(result);
                }
                else
                {
                    var pagedResult = new PagedResultNoData()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }

        [HttpGet("license-plate/search/{LicensePlate}")]
        public async Task<IActionResult> GetSearchLicensePlate(string LicensePlate)
        {
            IEnumerable<LicensePlateViewModels> result = new List<LicensePlateViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@LicensePlate", LicensePlate);
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<LicensePlateViewModels>("GetSearchLicensePlate", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    List<VehicleSearchViewModels> Vehicle = new List<VehicleSearchViewModels>();
                    List<StaffSearchViewModels> Staff = new List<StaffSearchViewModels>();
                    List<GuestSearchViewModels> Guest = new List<GuestSearchViewModels>();
                    if (result.ToList().Count == 1)
                    {
                        data = "Đang gửi";
                        if (result.ToList()[0].StaffCode == null || result.ToList()[0].StaffCode == "")
                        {
                            Vehicle.Add(
       new VehicleSearchViewModels()
       {
           DeviceName = result.ToList()[0].DeviceName,
           RecordTime = result.ToList()[0].RecordTime,
           PortName = result.ToList()[0].PortName,
           VehiceType = result.ToList()[0].VehiceType,
           LicensePlate = result.ToList()[0].LicensePlate,
           LicenseImagePath = result.ToList()[0].LicenseImagePath,
           Color= result.ToList()[0].Color,
           HisEventId = result.ToList()[0].HisEventId,
           //FaceImagePath = result.ToList()[0].FaceImagePath,
       });
                            Guest.Add(
       new GuestSearchViewModels()
       {
           FaceName = result.ToList()[0].FaceName,
           Sex = result.ToList()[0].Sex,
           IdentityCard = result.ToList()[0].IdentityCard,
           //LicenseImagePath = result.ToList()[0].LicenseImagePath,
           Note = result.ToList()[0].Note,
           FaceImagePath = result.ToList()[0].FaceImagePath,
           Job= result.ToList()[0].Job,
       });
                            //Vehicle.ToList() = result.ToList()[0].DeviceName;
                            //Vehicle.ToList()[0].RecordTime = result.ToList()[0].RecordTime;
                            //Vehicle.ToList()[0].PortName = result.ToList()[0].PortName;
                            //Vehicle.ToList()[0].VehiceType = result.ToList()[0].VehiceType;
                            //Vehicle.ToList()[0].LicensePlate = result.ToList()[0].LicensePlate;
                            //Vehicle.ToList()[0].FaceImagePath = result.ToList()[0].FaceImagePath;
                            //Guest.ToList()[0].FaceName = result.ToList()[0].FaceName;
                            //Guest.ToList()[0].Sex = result.ToList()[0].Sex;
                            //Guest.ToList()[0].IdentityCard = result.ToList()[0].IdentityCard;
                            //Guest.ToList()[0].LicenseImagePath = result.ToList()[0].LicenseImagePath;
                            //Guest.ToList()[0].Note = result.ToList()[0].Note;
                            //Guest.ToList()[0].FaceImagePath = result.ToList()[0].FaceImagePath;
                        }
                        if (result.ToList()[0].StaffCode != null && result.ToList()[0].StaffCode != "")
                        {
                            Vehicle.Add(new VehicleSearchViewModels()
                            {
                                DeviceName = result.ToList()[0].DeviceName,
                                RecordTime = result.ToList()[0].RecordTime,
                                PortName = result.ToList()[0].PortName,
                                VehiceType = result.ToList()[0].VehiceType,
                                LicensePlate = result.ToList()[0].LicensePlate,
                                LicenseImagePath = result.ToList()[0].LicenseImagePath,
                                Color= result.ToList()[0].Color,
                                HisEventId = result.ToList()[0].HisEventId,
                            });
                            Staff.Add(new StaffSearchViewModels()
                            {
                                FaceName = result.ToList()[0].FaceName,
                                Sex = result.ToList()[0].Sex,
                                AgeYear = result.ToList()[0].AgeYear,
                                FaceImagePath = result.ToList()[0].FaceImagePath,
                                StaffCode = result.ToList()[0].StaffCode,
                                DepartmentName = result.ToList()[0].DepartmentName,
                            });
                            //Vehicle.ToList()[0].DeviceName = result.ToList()[0].DeviceName;
                            //Vehicle.ToList()[0].RecordTime = result.ToList()[0].RecordTime;
                            //Vehicle.ToList()[0].PortName = result.ToList()[0].PortName;
                            //Vehicle.ToList()[0].VehiceType = result.ToList()[0].VehiceType;
                            //Vehicle.ToList()[0].LicensePlate = result.ToList()[0].LicensePlate;
                            //Vehicle.ToList()[0].FaceImagePath = result.ToList()[0].FaceImagePath;
                            //Staff.ToList()[0].FaceName = result.ToList()[0].FaceName;
                            //Staff.ToList()[0].Sex = result.ToList()[0].Sex;
                            //Staff.ToList()[0].AgeYear = result.ToList()[0].AgeYear;
                            //Staff.ToList()[0].LicenseImagePath = result.ToList()[0].LicenseImagePath;
                            //Staff.ToList()[0].StaffCode = result.ToList()[0].StaffCode;
                            //Staff.ToList()[0].DepartmentName = result.ToList()[0].DepartmentName;
                        }

                    }
                   
                    if (result.ToList().Count == 2)
                    {
                       

                        if (result.ToList()[0].StaffCode == null || result.ToList()[0].StaffCode == "")
                        {
                            List<PagedResultSearchLicese> pagedResult2 = new List<PagedResultSearchLicese>();
                            for (int i=0;i< result.ToList().Count; i++)
                            {
                                List<VehicleSearchViewModels> Vehicle1 = new List<VehicleSearchViewModels>();
                                List<StaffSearchViewModels> Staff1 = new List<StaffSearchViewModels>();
                                List<GuestSearchViewModels> Guest1 = new List<GuestSearchViewModels>();
                                if (i == 0)
                                {
                                    data = "Lần vào gần nhất";
                                }
                                else
                                {
                                    data = "Lần ra gần nhất";
                                }
                                Vehicle1.Add(
                                       new VehicleSearchViewModels()
                                       {
                                           DeviceName = result.ToList()[i].DeviceName,
                                           RecordTime = result.ToList()[i].RecordTime,
                                           PortName = result.ToList()[i].PortName,
                                           VehiceType = result.ToList()[i].VehiceType,
                                           LicensePlate = result.ToList()[i].LicensePlate,
                                           LicenseImagePath = result.ToList()[i].LicenseImagePath,
                                           Color= result.ToList()[i].Color,
                                           HisEventId = result.ToList()[i].HisEventId,
                                       });
                                Guest1.Add(
           new GuestSearchViewModels()
           {
               FaceName = result.ToList()[i].FaceName,
               Sex = result.ToList()[i].Sex,
               IdentityCard = result.ToList()[i].IdentityCard,
               //LicenseImagePath = result.ToList()[i].LicenseImagePath,
               Note = result.ToList()[i].Note,
               FaceImagePath = result.ToList()[i].FaceImagePath,
               Job= result.ToList()[i].Job,
           });
                                pagedResult2.Add(
        new PagedResultSearchLicese()
        {
            Vehicle = Vehicle1.ToList(),
            Staff = Staff1.ToList(),
            Guest = Guest1.ToList(),
            Status = data,
            Message = error,
        });
                            }
                            
                           
                           
                           
                           
                            //pagedResult2 = pagedResult1;
                            
                            
                            return Ok(pagedResult2);
                        }
                        if (result.ToList()[0].StaffCode != null && result.ToList()[0].StaffCode != "")
                        {
                            List<PagedResultSearchLicese> pagedResult2 = new List<PagedResultSearchLicese>();
                            for (int i = 0; i < result.ToList().Count; i++)
                            {
                                List<VehicleSearchViewModels> Vehicle1 = new List<VehicleSearchViewModels>();
                                List<StaffSearchViewModels> Staff1 = new List<StaffSearchViewModels>();
                                List<GuestSearchViewModels> Guest1 = new List<GuestSearchViewModels>();
                                if (i == 0)
                                {
                                    data = "Lần vào gần nhất";
                                }
                                else
                                {
                                    data = "Lần ra gần nhất";
                                }
                                Vehicle1.Add(
                                       new VehicleSearchViewModels()
                                       {
                                           DeviceName = result.ToList()[i].DeviceName,
                                           RecordTime = result.ToList()[i].RecordTime,
                                           PortName = result.ToList()[i].PortName,
                                           VehiceType = result.ToList()[i].VehiceType,
                                           LicensePlate = result.ToList()[i].LicensePlate,
                                           LicenseImagePath = result.ToList()[i].LicenseImagePath,
                                           Color = result.ToList()[i].Color,
                                           HisEventId = result.ToList()[i].HisEventId,
                                       });
                                Staff1.Add(new StaffSearchViewModels()
                                {
                                    FaceName = result.ToList()[i].FaceName,
                                    Sex = result.ToList()[i].Sex,
                                    AgeYear = result.ToList()[i].AgeYear,
                                    FaceImagePath = result.ToList()[i].FaceImagePath,
                                    StaffCode = result.ToList()[i].StaffCode,
                                    DepartmentName = result.ToList()[i].DepartmentName,
                                });
                                //pagedResult2 = pagedResult1;
                                pagedResult2.Add(
            new PagedResultSearchLicese()
            {
                Vehicle = Vehicle1.ToList(),
                Staff = Staff1.ToList(),
                Guest = Guest1.ToList(),
                Status = data,
                Message = error,
            });
                            }
                           
                           

                            return Ok(pagedResult2);
                        }

                    }
                    var pagedResult = new PagedResultSearchLicese()
                    {
                        Vehicle = Vehicle.ToList(),
                        Staff = Staff.ToList(),
                        Guest = Guest.ToList(),
                        Status = data,
                        Message= error,
                    };
                    //Vehicle1 = pagedResult.ToString();
                    return Ok(pagedResult);
                }
                else
                {
                    var pagedResult = new PagedResultSearchLicese()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }
        // public async Task<PagedResultSearchLicese<LicensePlateViewModels>> GetSearchLicensePlate(string LicensePlate)
        // {
        //     IEnumerable<LicensePlateViewModels> result = new List<LicensePlateViewModels>();
        //     string error = "";
        //     string data = "OK";
        //     using (var conn = new SqlConnection(_connectionString))
        //     {
        //         if (conn.State == System.Data.ConnectionState.Closed)
        //             conn.Open();
        //         var paramaters = new DynamicParameters();
        //         paramaters.Add("@LicensePlate", LicensePlate);
        //         paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
        //         try
        //         {
        //             result = await conn.QueryAsync<LicensePlateViewModels>("GetSearchLicensePlate", paramaters, null, null, System.Data.CommandType.StoredProcedure);

        //             error = paramaters.Get<string>("@vError");
        //             if (error == "SUCCESS")
        //             {
        //                 data = "OK";
        //             }
        //             else
        //             {
        //                 data = "Failed";
        //             }
        //             //result = message == CommonConstants.Message.Success ? true : false;
        //             if (result.ToList().Count == 0 && data == "OK")
        //                 error = "NoData";


        //         }
        //         catch (Exception ex)
        //         {
        //             error = ex.Message;
        //             data = "Failed";
        //         }
        //         //var pagedResult = new PagedResult<TkSettingModels>()
        //         //{
        //         //    Data = result.ToList(),
        //         //    Message = error,
        //         //    Status = data,
        //         //};
        //         if (data == "OK")
        //         {
        //             List<LicensePlateViewModels> Vehicle = new List<LicensePlateViewModels>();
        //             List<LicensePlateViewModels> Staff = new List<LicensePlateViewModels>();
        //             List<LicensePlateViewModels> Guest = new List<LicensePlateViewModels>();
        //             if (result.ToList().Count ==1)
        //             {
        //                 data = "Đang gửi";
        //                 if (result.ToList()[0].StaffCode==null|| result.ToList()[0].StaffCode == "")
        //                 {
        //                     Vehicle.Add(
        //new LicensePlateViewModels()
        //{
        //   DeviceName = result.ToList()[0].DeviceName,
        //   RecordTime = result.ToList()[0].RecordTime,
        //   PortName = result.ToList()[0].PortName,
        //   VehiceType = result.ToList()[0].VehiceType,
        //   LicensePlate = result.ToList()[0].LicensePlate,
        //   FaceImagePath = result.ToList()[0].FaceImagePath,
        //                 });
        //                     Staff.Add(
        //new LicensePlateViewModels()
        //{
        //    FaceName = result.ToList()[0].FaceName,
        //    Sex = result.ToList()[0].Sex,
        //    IdentityCard = result.ToList()[0].IdentityCard,
        //     LicenseImagePath = result.ToList()[0].LicenseImagePath,
        //      Note = result.ToList()[0].Note,
        //      FaceImagePath = result.ToList()[0].FaceImagePath,
        //                 });
        //                     //Vehicle.ToList() = result.ToList()[0].DeviceName;
        //                     //Vehicle.ToList()[0].RecordTime = result.ToList()[0].RecordTime;
        //                     //Vehicle.ToList()[0].PortName = result.ToList()[0].PortName;
        //                     //Vehicle.ToList()[0].VehiceType = result.ToList()[0].VehiceType;
        //                     //Vehicle.ToList()[0].LicensePlate = result.ToList()[0].LicensePlate;
        //                     //Vehicle.ToList()[0].FaceImagePath = result.ToList()[0].FaceImagePath;
        //                     //Guest.ToList()[0].FaceName = result.ToList()[0].FaceName;
        //                     //Guest.ToList()[0].Sex = result.ToList()[0].Sex;
        //                     //Guest.ToList()[0].IdentityCard = result.ToList()[0].IdentityCard;
        //                     //Guest.ToList()[0].LicenseImagePath = result.ToList()[0].LicenseImagePath;
        //                     //Guest.ToList()[0].Note = result.ToList()[0].Note;
        //                     //Guest.ToList()[0].FaceImagePath = result.ToList()[0].FaceImagePath;
        //                 }
        //                 if (result.ToList()[0].StaffCode != null && result.ToList()[0].StaffCode != "")
        //                 {
        //                    Vehicle.Add(new LicensePlateViewModels()
        //                     {
        //                         DeviceName = result.ToList()[0].DeviceName,
        //                         RecordTime = result.ToList()[0].RecordTime,
        //                         PortName = result.ToList()[0].PortName,
        //                         VehiceType = result.ToList()[0].VehiceType,
        //                         LicensePlate = result.ToList()[0].LicensePlate,
        //                         FaceImagePath = result.ToList()[0].FaceImagePath,
        //                     });
        //                     Guest.Add(new LicensePlateViewModels()
        //{
        //    FaceName = result.ToList()[0].FaceName,
        //    Sex = result.ToList()[0].Sex,
        //    AgeYear = result.ToList()[0].AgeYear,
        //    LicenseImagePath = result.ToList()[0].LicenseImagePath,
        //    StaffCode = result.ToList()[0].StaffCode,
        //    DepartmentName = result.ToList()[0].DepartmentName,
        //                 });
        //                     //Vehicle.ToList()[0].DeviceName = result.ToList()[0].DeviceName;
        //                     //Vehicle.ToList()[0].RecordTime = result.ToList()[0].RecordTime;
        //                     //Vehicle.ToList()[0].PortName = result.ToList()[0].PortName;
        //                     //Vehicle.ToList()[0].VehiceType = result.ToList()[0].VehiceType;
        //                     //Vehicle.ToList()[0].LicensePlate = result.ToList()[0].LicensePlate;
        //                     //Vehicle.ToList()[0].FaceImagePath = result.ToList()[0].FaceImagePath;
        //                     //Staff.ToList()[0].FaceName = result.ToList()[0].FaceName;
        //                     //Staff.ToList()[0].Sex = result.ToList()[0].Sex;
        //                     //Staff.ToList()[0].AgeYear = result.ToList()[0].AgeYear;
        //                     //Staff.ToList()[0].LicenseImagePath = result.ToList()[0].LicenseImagePath;
        //                     //Staff.ToList()[0].StaffCode = result.ToList()[0].StaffCode;
        //                     //Staff.ToList()[0].DepartmentName = result.ToList()[0].DepartmentName;
        //                 }

        //             }
        //             if (result.ToList().Count == 2)
        //             {
        //                 data = "Đã gửi";

        //                 if (result.ToList()[0].StaffCode == null || result.ToList()[0].StaffCode == "")
        //                 {
        //                     Vehicle.ToList()[0].DeviceName = result.ToList()[0].DeviceName;
        //                     Vehicle.ToList()[0].RecordTime = result.ToList()[0].RecordTime;
        //                     Vehicle.ToList()[0].PortName = result.ToList()[0].PortName;
        //                     Vehicle.ToList()[0].VehiceType = result.ToList()[0].VehiceType;
        //                     Vehicle.ToList()[0].LicensePlate = result.ToList()[0].LicensePlate;
        //                     Vehicle.ToList()[0].FaceImagePath = result.ToList()[0].FaceImagePath;
        //                     Guest.ToList()[0].FaceName = result.ToList()[0].FaceName;
        //                     Guest.ToList()[0].Sex = result.ToList()[0].Sex;
        //                     Guest.ToList()[0].IdentityCard = result.ToList()[0].IdentityCard;
        //                     Guest.ToList()[0].LicenseImagePath = result.ToList()[0].LicenseImagePath;
        //                     Guest.ToList()[0].Note = result.ToList()[0].Note;
        //                     Guest.ToList()[0].FaceImagePath = result.ToList()[0].FaceImagePath;
        //                     Vehicle.ToList()[1].DeviceName = result.ToList()[1].DeviceName;
        //                     Vehicle.ToList()[1].RecordTime = result.ToList()[1].RecordTime;
        //                     Vehicle.ToList()[1].PortName = result.ToList()[1].PortName;
        //                     Vehicle.ToList()[1].VehiceType = result.ToList()[1].VehiceType;
        //                     Vehicle.ToList()[1].LicensePlate = result.ToList()[1].LicensePlate;
        //                     Vehicle.ToList()[1].FaceImagePath = result.ToList()[1].FaceImagePath;
        //                     Guest.ToList()[1].FaceName = result.ToList()[1].FaceName;
        //                     Guest.ToList()[1].Sex = result.ToList()[1].Sex;
        //                     Guest.ToList()[1].IdentityCard = result.ToList()[1].IdentityCard;
        //                     Guest.ToList()[1].LicenseImagePath = result.ToList()[1].LicenseImagePath;
        //                     Guest.ToList()[1].Note = result.ToList()[1].Note;
        //                     Guest.ToList()[1].FaceImagePath = result.ToList()[1].FaceImagePath;
        //                 }
        //                 if (result.ToList()[0].StaffCode != null && result.ToList()[0].StaffCode != "")
        //                 {
        //                     Vehicle.ToList()[0].DeviceName = result.ToList()[0].DeviceName;
        //                     Vehicle.ToList()[0].RecordTime = result.ToList()[0].RecordTime;
        //                     Vehicle.ToList()[0].PortName = result.ToList()[0].PortName;
        //                     Vehicle.ToList()[0].VehiceType = result.ToList()[0].VehiceType;
        //                     Vehicle.ToList()[0].LicensePlate = result.ToList()[0].LicensePlate;
        //                     Vehicle.ToList()[0].FaceImagePath = result.ToList()[0].FaceImagePath;
        //                     Staff.ToList()[0].FaceName = result.ToList()[0].FaceName;
        //                     Staff.ToList()[0].Sex = result.ToList()[0].Sex;
        //                     Staff.ToList()[0].AgeYear = result.ToList()[0].AgeYear;
        //                     Staff.ToList()[0].LicenseImagePath = result.ToList()[0].LicenseImagePath;
        //                     Staff.ToList()[0].StaffCode = result.ToList()[0].StaffCode;
        //                     Staff.ToList()[0].DepartmentName = result.ToList()[0].DepartmentName;
        //                 }

        //             }
        //             var pagedResult = new PagedResultSearchLicese<LicensePlateViewModels>()
        //             {
        //                 Vehicle = Vehicle.ToList(),
        //                 Staff = Staff.ToList(),
        //                 Guest= Guest.ToList(),
        //                 Status=data,
        //             };
        //             IEnumerable<LicensePlateViewModels> Vehicle1 = new List<LicensePlateViewModels>();
        //             //Vehicle1 = pagedResult.ToString();
        //             return pagedResult;
        //         }
        //         else
        //         {
        //             var pagedResult = new PagedResultSearchLicese<LicensePlateViewModels>()
        //             {
        //                 Message = error,
        //                 Status = data,
        //             };
        //             return pagedResult;

        //         }
        //         //return pagedResult;
        //     }
        // }
        [HttpGet("license-plate/search")]
        public async Task<IActionResult> GetSearchLicensePlateNoBy()
        {
            IEnumerable<LicensePlateViewModels> result = new List<LicensePlateViewModels>();
            string error = "";
            string data = "OK";
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@vError", "", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output);
                try
                {
                    result = await conn.QueryAsync<LicensePlateViewModels>("GetSearchLicensePlateNoBy", paramaters, null, null, System.Data.CommandType.StoredProcedure);

                    error = paramaters.Get<string>("@vError");
                    if (error == "SUCCESS")
                    {
                        data = "OK";
                    }
                    else
                    {
                        data = "Failed";
                    }
                    //result = message == CommonConstants.Message.Success ? true : false;
                    if (result.ToList().Count == 0 && data == "OK")
                        error = "NoData";


                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    data = "Failed";
                }
                //var pagedResult = new PagedResult<TkSettingModels>()
                //{
                //    Data = result.ToList(),
                //    Message = error,
                //    Status = data,
                //};
                if (data == "OK")
                {
                    List<PagedResultSearchLicese> pagedResult = new List<PagedResultSearchLicese>();
                    for (int i = 0; i < result.ToList().Count; i++)
                    {
                        List<VehicleSearchViewModels> Vehicle = new List<VehicleSearchViewModels>();
                        List<StaffSearchViewModels> Staff = new List<StaffSearchViewModels>();
                        List<GuestSearchViewModels> Guest = new List<GuestSearchViewModels>();
                        if (result.ToList()[i].StaffCode==null|| result.ToList()[i].StaffCode == "")
                        {
                            if(result.ToList()[i].row_num_in!= result.ToList()[i].row_num_out)
                            {
                                data = "Đang gửi";
                                Vehicle.Add(
                                      new VehicleSearchViewModels()
                                      {
                                          DeviceName = result.ToList()[i].DeviceName,
                                          RecordTime = result.ToList()[i].RecordTime,
                                          PortName = result.ToList()[i].PortName,
                                          VehiceType = result.ToList()[i].VehiceType,
                                          LicensePlate = result.ToList()[i].LicensePlate,
                                          LicenseImagePath = result.ToList()[i].LicenseImagePath,
                                          Color = result.ToList()[i].Color,
                                          HisEventId = result.ToList()[i].HisEventId,
                                      });
                                Guest.Add(
           new GuestSearchViewModels()
           {
               FaceName = result.ToList()[i].FaceName,
               Sex = result.ToList()[i].Sex,
               IdentityCard = result.ToList()[i].IdentityCard,
               //LicenseImagePath = result.ToList()[i].LicenseImagePath,
               Note = result.ToList()[i].Note,
               FaceImagePath = result.ToList()[i].FaceImagePath,
               Job= result.ToList()[i].Job,
           });
                                pagedResult.Add(
            new PagedResultSearchLicese()
            {
                Vehicle = Vehicle.ToList(),
                Staff = Staff.ToList(),
                Guest = Guest.ToList(),
                Status = data,
                Message = error,
            });
                            }
                            else
                            {
                                List<VehicleSearchViewModels> Vehicle1 = new List<VehicleSearchViewModels>();
                                List<GuestSearchViewModels> Guest1 = new List<GuestSearchViewModels>();
                                //data = "Lần vào gần nhất";
                                Vehicle.Add(
                                      new VehicleSearchViewModels()
                                      {
                                          DeviceName = result.ToList()[i].DeviceName,
                                          RecordTime = result.ToList()[i].RecordTime,
                                          PortName = result.ToList()[i].PortName,
                                          VehiceType = result.ToList()[i].VehiceType,
                                          LicensePlate = result.ToList()[i].LicensePlate,
                                          LicenseImagePath = result.ToList()[i].LicenseImagePath,
                                          Color = result.ToList()[i].Color,
                                          HisEventId = result.ToList()[i].HisEventId,
                                      });
                                Guest.Add(
           new GuestSearchViewModels()
           {
               FaceName = result.ToList()[i].FaceName,
               Sex = result.ToList()[i].Sex,
               IdentityCard = result.ToList()[i].IdentityCard,
               //LicenseImagePath = result.ToList()[i].LicenseImagePath,
               Note = result.ToList()[i].Note,
               FaceImagePath = result.ToList()[i].FaceImagePath,
               Job = result.ToList()[i].Job,
           });
                                pagedResult.Add(
           new PagedResultSearchLicese()
           {
               Vehicle = Vehicle.ToList(),
               Staff = Staff.ToList(),
               Guest = Guest.ToList(),
               Status = "Lần vào gần nhất",
               Message = error,
           });
                                Vehicle1.Add(
                                      new VehicleSearchViewModels()
                                      {
                                          DeviceName = result.ToList()[i].DeviceNameOut,
                                          RecordTime = result.ToList()[i].RecordTimeOut,
                                          PortName = result.ToList()[i].PortNameOut,
                                          VehiceType = result.ToList()[i].VehiceType,
                                          LicensePlate = result.ToList()[i].LicensePlate,
                                          LicenseImagePath = result.ToList()[i].LicenseImagePath,
                                          Color = result.ToList()[i].Color,
                                          HisEventId = result.ToList()[i].HisEventIdOut,
                                      });
                                Guest1.Add(
           new GuestSearchViewModels()
           {
               FaceName = result.ToList()[i].FaceName,
               Sex = result.ToList()[i].Sex,
               IdentityCard = result.ToList()[i].IdentityCard,
               //LicenseImagePath = result.ToList()[i].LicenseImagePathOut,
               Note = result.ToList()[i].Note,
               FaceImagePath = result.ToList()[i].FaceImagePathOut,
               Job = result.ToList()[i].Job,
           });
                                pagedResult.Add(
           new PagedResultSearchLicese()
           {
               Vehicle = Vehicle1.ToList(),
               Staff = Staff.ToList(),
               Guest = Guest1.ToList(),
               Status = "Lần ra gần nhất",
               Message = error,
           });
                            }
                        }
                        else
                        {
                            if (result.ToList()[i].row_num_in != result.ToList()[i].row_num_out)
                            {
                                data = "Đang gửi";
                                Vehicle.Add(
                                      new VehicleSearchViewModels()
                                      {
                                          DeviceName = result.ToList()[i].DeviceName,
                                          RecordTime = result.ToList()[i].RecordTime,
                                          PortName = result.ToList()[i].PortName,
                                          VehiceType = result.ToList()[i].VehiceType,
                                          LicensePlate = result.ToList()[i].LicensePlate,
                                          LicenseImagePath = result.ToList()[i].LicenseImagePath,
                                          Color = result.ToList()[i].Color,
                                          HisEventId = result.ToList()[i].HisEventId,
                                      });
                                Staff.Add(new StaffSearchViewModels()
                                {
                                    FaceName = result.ToList()[i].FaceName,
                                    Sex = result.ToList()[i].Sex,
                                    AgeYear = result.ToList()[i].AgeYear,
                                    FaceImagePath = result.ToList()[i].FaceImagePath,
                                    StaffCode = result.ToList()[i].StaffCode,
                                    DepartmentName = result.ToList()[i].DepartmentName,
                                });
                                pagedResult.Add(
            new PagedResultSearchLicese()
            {
                Vehicle = Vehicle.ToList(),
                Staff = Staff.ToList(),
                Guest = Guest.ToList(),
                Status = data,
                Message = error,
            });
                            }
                            else
                            {
                                List<VehicleSearchViewModels> Vehicle1 = new List<VehicleSearchViewModels>();
                                List<StaffSearchViewModels> Staff1 = new List<StaffSearchViewModels>();
                                //data = "Lần vào gần nhất";
                                Vehicle.Add(
                                      new VehicleSearchViewModels()
                                      {
                                          DeviceName = result.ToList()[i].DeviceName,
                                          RecordTime = result.ToList()[i].RecordTime,
                                          PortName = result.ToList()[i].PortName,
                                          VehiceType = result.ToList()[i].VehiceType,
                                          LicensePlate = result.ToList()[i].LicensePlate,
                                          LicenseImagePath = result.ToList()[i].LicenseImagePath,
                                          Color = result.ToList()[i].Color,
                                          HisEventId = result.ToList()[i].HisEventId,
                                      });
                                Staff.Add(new StaffSearchViewModels()
                                {
                                    FaceName = result.ToList()[i].FaceName,
                                    Sex = result.ToList()[i].Sex,
                                    AgeYear = result.ToList()[i].AgeYear,
                                    FaceImagePath = result.ToList()[i].FaceImagePath,
                                    StaffCode = result.ToList()[i].StaffCode,
                                    DepartmentName = result.ToList()[i].DepartmentName,
                                });
                                pagedResult.Add(
           new PagedResultSearchLicese()
           {
               Vehicle = Vehicle.ToList(),
               Staff = Staff.ToList(),
               Guest = Guest.ToList(),
               Status = "Lần vào gần nhất",
               Message = error,
           });
                                Vehicle1.Add(
                                      new VehicleSearchViewModels()
                                      {
                                          DeviceName = result.ToList()[i].DeviceNameOut,
                                          RecordTime = result.ToList()[i].RecordTimeOut,
                                          PortName = result.ToList()[i].PortNameOut,
                                          VehiceType = result.ToList()[i].VehiceType,
                                          LicensePlate = result.ToList()[i].LicensePlate,
                                          LicenseImagePath = result.ToList()[i].LicenseImagePath,
                                          Color = result.ToList()[i].Color,
                                          HisEventId = result.ToList()[i].HisEventIdOut,
                                      });
                                Staff1.Add(new StaffSearchViewModels()
                                {
                                    FaceName = result.ToList()[i].FaceName,
                                    Sex = result.ToList()[i].Sex,
                                    AgeYear = result.ToList()[i].AgeYear,
                                    FaceImagePath = result.ToList()[i].FaceImagePath,
                                    StaffCode = result.ToList()[i].StaffCode,
                                    DepartmentName = result.ToList()[i].DepartmentName,
                                });
                                pagedResult.Add(
           new PagedResultSearchLicese()
           {
               Vehicle = Vehicle1.ToList(),
               Staff = Staff1.ToList(),
               Guest = Guest.ToList(),
               Status = "Lần ra gần nhất",
               Message = error,
           });
                            }
                        }
                    }

                    return Ok(pagedResult);
                }
                else
                {
                    var pagedResult = new PagedResultSearchLicese()
                    {
                        Message = error,
                        Status = data,
                    };
                    return Ok(pagedResult);

                }
                //return pagedResult;
            }
        }
    }
}
