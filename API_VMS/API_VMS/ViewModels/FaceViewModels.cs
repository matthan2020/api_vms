﻿using API_VMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.ViewModels
{
    public class FaceViewModels : TblFaceModels
    {
        public int ImageId { get; set; }
        public string Base64ContentUrl { get; set; }
        public string DepartmentName { get; set; }
        public string CreatedUser { set; get; }
        public string RecordTime { set; get; }
        public int DeviceId { get; set; }


    }
}
