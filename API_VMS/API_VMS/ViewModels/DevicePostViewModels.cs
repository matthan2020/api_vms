﻿using API_VMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.ViewModels
{
    public class DevicePostViewModels
    {

        public int IdDeviceType { set; get; }
        public string DeviceName { set; get; }
        public string IpAddress { set; get; }
        public int PortNo { set; get; }
        public string UserName { set; get; }
        public string Pass { set; get; }
        public string idCompany { set; get; }
        public string idModel { set; get; }
        public string AmountCam { set; get; }
        public string LinkRTSP { set; get; }
        public int IdStatus { set; get; }



    }
}
