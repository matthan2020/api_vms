﻿using API_VMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.ViewModels
{
    public class LicensePlateViewModels : TblImageEventHistoryModels
    {

        public string PortName { get; set; }
        public string VehiceType { get; set; }
        public string LicensePlate { get; set; }
        public string FaceName { get; set; }
        public string StaffCode { get; set; }
        public string DeviceName { get; set; }
        public string Sex { get; set; }
        public string AgeYear { get; set; }
        public string DepartmentName { get; set; }
        public string Note { get; set; }
        public string FaceImagePath { get; set; }
        public string IdentityCard { get; set; }
        public string FaceImagePathOut { get; set; }
        public string RecordTimeOut { set; get; }
        public string LicenseImagePathOut { set; get; }
        public string PortNameOut { get; set; }
        public string DeviceNameOut { get; set; }
        public int row_num_in { get; set; }
        public int row_num_out { get; set; }
        public string Color { get; set; }
        public string Job { get; set; }
        public int HisEventIdOut { set; get; }
    }
}
