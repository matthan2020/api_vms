﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class TkStatisticPageViewModels
    {
        public string DepartmentId { get; set; }
        public string StaffCode { get; set; }
        public string ShiftName { get; set; }
        public int TkYear { get; set; }
        public int TkMonth { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
       

    }
}
