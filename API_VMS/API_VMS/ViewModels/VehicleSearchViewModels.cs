﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class VehicleSearchViewModels
    {
        public string RecordTime { get; set; }
        public string PortName { get; set; }
        public string VehiceType { get; set; }
        public string LicensePlate { get; set; }
        public string DeviceName { get; set; }
        public string LicenseImagePath { get; set; }
        public string Color { get; set; }
        public int HisEventId { set; get; }
    }
}
