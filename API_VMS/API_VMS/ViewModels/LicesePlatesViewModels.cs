﻿using API_VMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.ViewModels
{
    public class LicesePlatesViewModels : LicesePlatesModels
    {
        
        public string RecordTime { get; set; }
        public string DeviceName { get; set; }
        public string Base64Big { get; set; }
        public string Base64Small { get; set; }

    }
}
