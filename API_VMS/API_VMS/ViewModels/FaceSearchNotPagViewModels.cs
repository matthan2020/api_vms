﻿using API_VMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.ViewModels
{
    public class FaceSearchNotPagViewModels
    {
        public int ImageId { get; set; }
        public string Base64ContentUrl { get; set; }
        public int FaceId { get; set; }
        public string FaceName { get; set; }
        public string AgeYear { get; set; }
        public string Job { get; set; }
        public string Sex { get; set; }
        public string ImagePath { get; set; }
        public string IsKnown { get; set; }
        public string ImportantLevel { get; set; }
        public int FaceIdCloud { get; set; }
        public string DepartmentId { get; set; }
        public string StaffCode { get; set; }





    }
}
