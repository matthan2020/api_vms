﻿using API_VMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.ViewModels
{
    public class DeviceViewModels
    {

        public int DeviceId { set; get; }
        public int IdDeviceType { set; get; }
        public string DeviceName { set; get; }
        public string IpAddress { set; get; }
        public int PortNo { set; get; }
        public string UserName { set; get; }
        public string Pass { set; get; }
        public string idCompany { set; get; }
        public string idModel { set; get; }
        public string AmountCam { set; get; }
        public bool isDelete { set; get; }
        public int IdSecurity { set; get; }
        public string DeviceCode { set; get; }
        public int IdStatus { set; get; }
        public int GroupId { set; get; }
        public int Channel { set; get; }
        public int Thread { set; get; }
        public int ROW_NUM { get; set; }
        public string DeviceTypeName { get; set; }
        public string SecurityName { get; set; }
        public string StatusName { get; set; }
        public string LinkRTSP { set; get; }
        public int TKSettingId { set; get; }
        public bool IsCheckIn { get; set; }
        public bool IsCheckOut { get; set; }
        public bool IsTKOn { get; set; }

    }
}
