﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class FaceShiftSearchViewModels
    {
        

        public int SettingShiftId { get; set; }
        public int DepartmnetId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }

    }
}
