﻿using API_VMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.ViewModels
{
    public class FaceShiftViewModels : TblFaceShiftModels
    {
        
        public string FaceName { get; set; }
        public string StaffCode { get; set; }
        public string Day1 { get; set; }
        public string Day2 { get; set; }
        public string Day3 { get; set; }
        public string Day4 { get; set; }
        public string Day5 { get; set; }
        public string Day6 { get; set; }
        public string Day7 { get; set; }
        public int RiceShift1 { get; set; }
        public int RiceShift2 { get; set; }
        public int RiceShift3 { get; set; }
        public int RiceShift4 { get; set; }
        public int RiceShift5 { get; set; }
        public int RiceShift6 { get; set; }
        public int RiceShift7 { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int CheckSave { get; set; }
    }
}
