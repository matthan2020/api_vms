﻿using API_VMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.ViewModels
{
    public class ImageEventHistoryJsonViewModels
    {
        public string EventId { get; set; }
        public string Search { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int FaceId { get; set; }
        public string StaffCode { get; set; }


    }
}
