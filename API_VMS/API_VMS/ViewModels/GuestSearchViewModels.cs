﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class GuestSearchViewModels
    {
        public string FaceName { get; set; }
        public string Sex { get; set; }
        public string IdentityCard { get; set; }
        public string Note { get; set; }
        public string FaceImagePath { get; set; }
        public string Job { get; set; }
    }
}
