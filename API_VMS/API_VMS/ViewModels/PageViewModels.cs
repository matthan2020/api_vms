﻿using API_VMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.ViewModels
{
    public class PageViewModels
    {

        public string KeyWord { set; get; }
        public int Page { set; get; }
        public int PageSize { set; get; }
        

    }
}
