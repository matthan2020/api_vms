﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class TkStatisticViewModels
    {
        public string DepartmentId { get; set; }
        public string StaffCode { get; set; }
        public int TkYear { get; set; }
        public int TkMonth { get; set; }
        
    }
}
