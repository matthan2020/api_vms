﻿using API_VMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.ViewModels
{
    public class FaceSearchViewModels : FaceSearchNotPagViewModels
    {
        
        public string Search { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }




    }
}
