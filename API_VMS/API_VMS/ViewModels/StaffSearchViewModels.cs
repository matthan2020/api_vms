﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_VMS.Models
{
    public class StaffSearchViewModels
    {
        

        public string FaceName { get; set; }
        public string Sex { get; set; }
        public string AgeYear { get; set; }
        public string FaceImagePath { get; set; }
        public string StaffCode { get; set; }
        public string DepartmentName { get; set; }

    }
}
